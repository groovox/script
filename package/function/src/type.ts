import type { Sdk } from "@groovox/client";
import type { ImageApi } from "@groovox/image-url";

export interface Context {
  client: Sdk;
  image: ImageApi;
}
