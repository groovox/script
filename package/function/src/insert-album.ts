import type { InsertAlbumMutationVariables } from "@groovox/client";

import { Context } from "./type";
import { generateImage } from "./utils";

export interface InsertAlbumOption {
  imageCoverPath?: string;
  imageBackgroundPath?: string;
}

export const insertAlbum = async (
  ctx: Context,
  vars: Omit<InsertAlbumMutationVariables, "imageCover" | "imageBackground">,
  opts: InsertAlbumOption = {}
): Promise<string> => {
  const { client, image } = ctx;
  const { imageCoverPath, imageBackgroundPath } = opts;
  let id = "";
  try {
    const { insertAlbum } = await client.insertAlbum(vars);
    if (!insertAlbum) {
      // This should never happen
      throw new Error("Empty insertAlbum");
    }
    id = insertAlbum.id;
  } catch (e) {
    const objStr = JSON.stringify(vars);
    const errStr = JSON.stringify(e);
    throw new Error(`Failed to insert: ${objStr}\n${errStr}`);
  }
  if (imageCoverPath) {
    const key = `album/${id}/cover`;
    const imageCover = await generateImage({
      image,
      filePath: imageCoverPath,
      payload: { key, format: "webp", method: "cover" }
    });
    await client.updateAlbumImageCover({ imageCover, id });
  } else {
    console.warn(`${id} does not have cover image.`);
  }
  if (imageBackgroundPath) {
    const key = `album/${id}/background`;
    const imageBackground = await generateImage({
      image,
      filePath: imageBackgroundPath,
      payload: { key, format: "webp", method: "background" }
    });
    await client.updateAlbumImageBackground({ imageBackground, id });
  } else {
    console.warn(`${id} does not have background image.`);
  }
  return id;
};
