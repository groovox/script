import type { InsertPersonMutationVariables } from "@groovox/client";

import { Context } from "./type";
import { generateImage } from "./utils";

export interface InsertPersonOption {
  imageCoverPath?: string;
}

export const insertPerson = async (
  ctx: Context,
  vars: Omit<InsertPersonMutationVariables, "imageCover">,
  opts: InsertPersonOption = {}
): Promise<string> => {
  const { client, image } = ctx;
  const { imageCoverPath: filePath } = opts;
  const { nameMatch } = vars;
  let id = "";
  try {
    const { insertPerson } = await client.insertPerson(vars);
    if (!insertPerson) {
      const { person } = await client.findPersonByNameMatch({ nameMatch });
      if (person.length > 0) {
        throw new Error(`${nameMatch} already exists`);
      }
      // This should never happen
      throw new Error("Empty insertPerson");
    }
    id = insertPerson.id;
  } catch (e) {
    const objStr = JSON.stringify(vars);
    const errStr = JSON.stringify(e);
    throw new Error(`Failed to insert: ${objStr}\n${errStr}`);
  }
  if (filePath) {
    const key = `person/${id}`;
    const imageCover = await generateImage({
      image,
      filePath,
      payload: { key, format: "webp", method: "person" }
    });
    await client.updatePersonImage({ imageCover, id });
  } else {
    console.warn(`${id} does not have cover image.`);
  }
  return id;
};
