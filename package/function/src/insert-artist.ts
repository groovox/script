import type { InsertArtistMutationVariables } from "@groovox/client";

import { Context } from "./type";
import { generateImage } from "./utils";

export interface InsertArtistOption {
  imageCoverPath?: string;
  imageBackgroundPath?: string;
}

export const insertArtist = async (
  ctx: Context,
  vars: Omit<InsertArtistMutationVariables, "imageCover" | "imageBackground">,
  opts: InsertArtistOption = {}
): Promise<string> => {
  const { client, image } = ctx;
  const { imageCoverPath, imageBackgroundPath } = opts;
  let id = "";
  try {
    const { insertArtist } = await client.insertArtist(vars);
    if (!insertArtist) {
      // This should never happen
      throw new Error("Empty insertArtist");
    }
    id = insertArtist.id;
  } catch (e) {
    const objStr = JSON.stringify(vars);
    const errStr = JSON.stringify(e);
    throw new Error(`Failed to insert: ${objStr}\n${errStr}`);
  }

  if (imageCoverPath) {
    const key = `artist/${id}/cover`;
    const imageCover = await generateImage({
      image,
      filePath: imageCoverPath,
      payload: { key, format: "webp", method: "cover" }
    });
    await client.updateArtistImageCover({ imageCover, id });
  } else {
    console.warn(`${id} does not have cover image.`);
  }
  if (imageBackgroundPath) {
    const key = `artist/${id}/background`;
    const imageBackground = await generateImage({
      image,
      filePath: imageBackgroundPath,
      payload: { key, format: "webp", method: "background" }
    });
    await client.updateArtistImageBackground({ imageBackground, id });
  } else {
    console.warn(`${id} does not have background image.`);
  }
  return id;
};
