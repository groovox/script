import type { InsertCollectionMutationVariables } from "@groovox/client";

import { Context } from "./type";
import { generateImage } from "./utils";

export interface InsertCollectionOption {
  imageCoverPath?: string;
  imageBackgroundPath?: string;
}

export const insertCollection = async (
  ctx: Context,
  vars: Omit<
    InsertCollectionMutationVariables,
    "imageCover" | "imageBackground"
  >,
  opts: InsertCollectionOption = {}
): Promise<string> => {
  const { client, image } = ctx;
  const { imageCoverPath, imageBackgroundPath } = opts;
  let id = "";
  try {
    const { insertCollection } = await client.insertCollection(vars);
    if (!insertCollection) {
      // This should never happen
      throw new Error("Empty insertCollection");
    }
    id = insertCollection.id;
  } catch (e) {
    const objStr = JSON.stringify(vars);
    const errStr = JSON.stringify(e);
    throw new Error(`Failed to insert: ${objStr}\n${errStr}`);
  }
  if (imageCoverPath) {
    const key = `album/${id}/cover`;
    const imageCover = await generateImage({
      image,
      filePath: imageCoverPath,
      payload: { key, format: "webp", method: "cover" }
    });
    await client.updateCollectionImageCover({ imageCover, id });
  } else {
    console.warn(`${id} does not have cover image.`);
  }
  if (imageBackgroundPath) {
    const key = `album/${id}/background`;
    const imageBackground = await generateImage({
      image,
      filePath: imageBackgroundPath,
      payload: { key, format: "webp", method: "background" }
    });
    await client.updateCollectionImageBackground({ imageBackground, id });
  } else {
    console.warn(`${id} does not have background image.`);
  }
  return id;
};
