import {
  generateStatic,
  generateUpload,
  ImageApi,
  StaticPayload,
  upload
} from "@groovox/image-url";

export interface GenerateImageOption {
  image: ImageApi;
  filePath: string;
  payload: StaticPayload;
}

export const generateImage = async (
  opts: GenerateImageOption
): Promise<string> => {
  const { image, filePath, payload } = opts;
  const { key } = payload;
  const result = await generateUpload({ key, maxSize: 10 }, image);
  await upload(result, filePath);
  return generateStatic(payload, image);
};
