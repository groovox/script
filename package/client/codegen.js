const { HttpsProxyAgent } = require("https-proxy-agent");
require("dotenv").config();
const { fetch } = require("cross-fetch");

const {
  SCHEMA_URL: schemaUrl,
  SCHEMA_FILE: schemaFile,
  SCHEMA_ADMIN_SECRET: adminSecret,
  SCHEMA_PROXY: proxy
} = process.env;

const proxyFetch = proxy => (input, init = {}) => {
  if (!("agent" in init)) {
    init.agent = new HttpsProxyAgent(proxy);
  }
  return fetch(input, init);
};

const createSchema = () => {
  if (schemaFile) {
    return schemaFile;
  }
  if (!schemaUrl) {
    throw new Error("You must define either SCHEMA_URL or SCHEMA_FILE");
  }
  const headers = adminSecret
    ? {
        "x-hasura-admin-secret": adminSecret
      }
    : {};
  const customFetch = proxy ? proxyFetch(proxy) : undefined;
  return [
    {
      [schemaUrl]: {
        headers,
        customFetch
      }
    }
  ];
};

module.exports = {
  schema: createSchema(),
  documents: "./graphql/**/*.gql",
  overwrite: true,
  generates: {
    "./schema.json": {
      plugins: ["@graphql-codegen/introspection"]
    }
  }
};
