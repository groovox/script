# @groovox/client

Use [graphql-codegen] to generate type safe client.

## Getting Started

First, you need a configuration file. You can create your own by referring to the documentation or use the default configuration, i.e. `codegen.js`.

If you use the provided `codegen.js`, You need to set the environment variables:

- `SCHEMA_FILE`: Local schema file.
- `SCHEMA_URL`: Url for the GraphQL endpoint. It should be `https://<YOUR_URL>/v1/graphql
- `SCHEMA_ADMIN_SECRET`: If you enable admin secret for the GraphQL endpoint, you must set it here.
- `SCHEMA_PROXY`: Only if you are behind a proxy.

You only need either `SCHEMA_FILE` or `SCHEMA_URL`.

[graphql-codegen]: https://github.com/dotansimha/graphql-code-generator
