import { GraphQLClient } from 'graphql-request';
import * as Dom from 'graphql-request/dist/types.dom';
import { print } from 'graphql';
import gql from 'graphql-tag';
export type Maybe<T> = T | null;
export type Exact<T extends { [key: string]: unknown }> = { [K in keyof T]: T[K] };
export type MakeOptional<T, K extends keyof T> = Omit<T, K> & { [SubKey in K]?: Maybe<T[SubKey]> };
export type MakeMaybe<T, K extends keyof T> = Omit<T, K> & { [SubKey in K]: Maybe<T[SubKey]> };
/** All built-in and custom scalars, mapped to their actual values */
export type Scalars = {
  ID: string;
  String: string;
  Boolean: boolean;
  Int: number;
  Float: number;
  _text: any;
  date: string;
  numeric: number;
  uuid: string;
};

/** expression to compare columns of type Int. All fields are combined with logical 'AND'. */
export type IntComparisonExp = {
  _eq?: Maybe<Scalars['Int']>;
  _gt?: Maybe<Scalars['Int']>;
  _gte?: Maybe<Scalars['Int']>;
  _in?: Maybe<Array<Scalars['Int']>>;
  _is_null?: Maybe<Scalars['Boolean']>;
  _lt?: Maybe<Scalars['Int']>;
  _lte?: Maybe<Scalars['Int']>;
  _neq?: Maybe<Scalars['Int']>;
  _nin?: Maybe<Array<Scalars['Int']>>;
};

/** expression to compare columns of type String. All fields are combined with logical 'AND'. */
export type StringComparisonExp = {
  _eq?: Maybe<Scalars['String']>;
  _gt?: Maybe<Scalars['String']>;
  _gte?: Maybe<Scalars['String']>;
  _ilike?: Maybe<Scalars['String']>;
  _in?: Maybe<Array<Scalars['String']>>;
  _is_null?: Maybe<Scalars['Boolean']>;
  _like?: Maybe<Scalars['String']>;
  _lt?: Maybe<Scalars['String']>;
  _lte?: Maybe<Scalars['String']>;
  _neq?: Maybe<Scalars['String']>;
  _nilike?: Maybe<Scalars['String']>;
  _nin?: Maybe<Array<Scalars['String']>>;
  _nlike?: Maybe<Scalars['String']>;
  _nsimilar?: Maybe<Scalars['String']>;
  _similar?: Maybe<Scalars['String']>;
};


/** expression to compare columns of type _text. All fields are combined with logical 'AND'. */
export type TextComparisonExp = {
  _eq?: Maybe<Scalars['_text']>;
  _gt?: Maybe<Scalars['_text']>;
  _gte?: Maybe<Scalars['_text']>;
  _in?: Maybe<Array<Scalars['_text']>>;
  _is_null?: Maybe<Scalars['Boolean']>;
  _lt?: Maybe<Scalars['_text']>;
  _lte?: Maybe<Scalars['_text']>;
  _neq?: Maybe<Scalars['_text']>;
  _nin?: Maybe<Array<Scalars['_text']>>;
};

/** columns and relationships of "album" */
export type Album = {
  aired?: Maybe<Scalars['date']>;
  /** An array relationship */
  album_collections: Array<AlbumCollection>;
  /** An aggregated array relationship */
  album_collections_aggregate: AlbumCollectionAggregate;
  /** An array relationship */
  album_countries: Array<AlbumCountry>;
  /** An aggregated array relationship */
  album_countries_aggregate: AlbumCountryAggregate;
  /** An array relationship */
  album_genres: Array<AlbumGenre>;
  /** An aggregated array relationship */
  album_genres_aggregate: AlbumGenreAggregate;
  /** An array relationship */
  album_moods: Array<AlbumMood>;
  /** An aggregated array relationship */
  album_moods_aggregate: AlbumMoodAggregate;
  /** An array relationship */
  album_studios: Array<AlbumStudio>;
  /** An aggregated array relationship */
  album_studios_aggregate: AlbumStudioAggregate;
  /** An array relationship */
  album_styles: Array<AlbumStyle>;
  /** An aggregated array relationship */
  album_styles_aggregate: AlbumStyleAggregate;
  /** An array relationship */
  album_tracks: Array<AlbumTrack>;
  /** An aggregated array relationship */
  album_tracks_aggregate: AlbumTrackAggregate;
  /** An object relationship */
  artist: Artist;
  artist_id: Scalars['uuid'];
  id: Scalars['uuid'];
  image_background?: Maybe<Scalars['String']>;
  image_cover?: Maybe<Scalars['String']>;
  name: Scalars['String'];
  name_match: Scalars['String'];
  name_sort?: Maybe<Scalars['String']>;
  rating?: Maybe<Scalars['numeric']>;
  summary: Scalars['String'];
};


/** columns and relationships of "album" */
export type AlbumAlbumCollectionsArgs = {
  distinct_on?: Maybe<Array<AlbumCollectionSelectColumn>>;
  limit?: Maybe<Scalars['Int']>;
  offset?: Maybe<Scalars['Int']>;
  order_by?: Maybe<Array<AlbumCollectionOrderBy>>;
  where?: Maybe<AlbumCollectionBoolExp>;
};


/** columns and relationships of "album" */
export type AlbumAlbumCollectionsAggregateArgs = {
  distinct_on?: Maybe<Array<AlbumCollectionSelectColumn>>;
  limit?: Maybe<Scalars['Int']>;
  offset?: Maybe<Scalars['Int']>;
  order_by?: Maybe<Array<AlbumCollectionOrderBy>>;
  where?: Maybe<AlbumCollectionBoolExp>;
};


/** columns and relationships of "album" */
export type AlbumAlbumCountriesArgs = {
  distinct_on?: Maybe<Array<AlbumCountrySelectColumn>>;
  limit?: Maybe<Scalars['Int']>;
  offset?: Maybe<Scalars['Int']>;
  order_by?: Maybe<Array<AlbumCountryOrderBy>>;
  where?: Maybe<AlbumCountryBoolExp>;
};


/** columns and relationships of "album" */
export type AlbumAlbumCountriesAggregateArgs = {
  distinct_on?: Maybe<Array<AlbumCountrySelectColumn>>;
  limit?: Maybe<Scalars['Int']>;
  offset?: Maybe<Scalars['Int']>;
  order_by?: Maybe<Array<AlbumCountryOrderBy>>;
  where?: Maybe<AlbumCountryBoolExp>;
};


/** columns and relationships of "album" */
export type AlbumAlbumGenresArgs = {
  distinct_on?: Maybe<Array<AlbumGenreSelectColumn>>;
  limit?: Maybe<Scalars['Int']>;
  offset?: Maybe<Scalars['Int']>;
  order_by?: Maybe<Array<AlbumGenreOrderBy>>;
  where?: Maybe<AlbumGenreBoolExp>;
};


/** columns and relationships of "album" */
export type AlbumAlbumGenresAggregateArgs = {
  distinct_on?: Maybe<Array<AlbumGenreSelectColumn>>;
  limit?: Maybe<Scalars['Int']>;
  offset?: Maybe<Scalars['Int']>;
  order_by?: Maybe<Array<AlbumGenreOrderBy>>;
  where?: Maybe<AlbumGenreBoolExp>;
};


/** columns and relationships of "album" */
export type AlbumAlbumMoodsArgs = {
  distinct_on?: Maybe<Array<AlbumMoodSelectColumn>>;
  limit?: Maybe<Scalars['Int']>;
  offset?: Maybe<Scalars['Int']>;
  order_by?: Maybe<Array<AlbumMoodOrderBy>>;
  where?: Maybe<AlbumMoodBoolExp>;
};


/** columns and relationships of "album" */
export type AlbumAlbumMoodsAggregateArgs = {
  distinct_on?: Maybe<Array<AlbumMoodSelectColumn>>;
  limit?: Maybe<Scalars['Int']>;
  offset?: Maybe<Scalars['Int']>;
  order_by?: Maybe<Array<AlbumMoodOrderBy>>;
  where?: Maybe<AlbumMoodBoolExp>;
};


/** columns and relationships of "album" */
export type AlbumAlbumStudiosArgs = {
  distinct_on?: Maybe<Array<AlbumStudioSelectColumn>>;
  limit?: Maybe<Scalars['Int']>;
  offset?: Maybe<Scalars['Int']>;
  order_by?: Maybe<Array<AlbumStudioOrderBy>>;
  where?: Maybe<AlbumStudioBoolExp>;
};


/** columns and relationships of "album" */
export type AlbumAlbumStudiosAggregateArgs = {
  distinct_on?: Maybe<Array<AlbumStudioSelectColumn>>;
  limit?: Maybe<Scalars['Int']>;
  offset?: Maybe<Scalars['Int']>;
  order_by?: Maybe<Array<AlbumStudioOrderBy>>;
  where?: Maybe<AlbumStudioBoolExp>;
};


/** columns and relationships of "album" */
export type AlbumAlbumStylesArgs = {
  distinct_on?: Maybe<Array<AlbumStyleSelectColumn>>;
  limit?: Maybe<Scalars['Int']>;
  offset?: Maybe<Scalars['Int']>;
  order_by?: Maybe<Array<AlbumStyleOrderBy>>;
  where?: Maybe<AlbumStyleBoolExp>;
};


/** columns and relationships of "album" */
export type AlbumAlbumStylesAggregateArgs = {
  distinct_on?: Maybe<Array<AlbumStyleSelectColumn>>;
  limit?: Maybe<Scalars['Int']>;
  offset?: Maybe<Scalars['Int']>;
  order_by?: Maybe<Array<AlbumStyleOrderBy>>;
  where?: Maybe<AlbumStyleBoolExp>;
};


/** columns and relationships of "album" */
export type AlbumAlbumTracksArgs = {
  distinct_on?: Maybe<Array<AlbumTrackSelectColumn>>;
  limit?: Maybe<Scalars['Int']>;
  offset?: Maybe<Scalars['Int']>;
  order_by?: Maybe<Array<AlbumTrackOrderBy>>;
  where?: Maybe<AlbumTrackBoolExp>;
};


/** columns and relationships of "album" */
export type AlbumAlbumTracksAggregateArgs = {
  distinct_on?: Maybe<Array<AlbumTrackSelectColumn>>;
  limit?: Maybe<Scalars['Int']>;
  offset?: Maybe<Scalars['Int']>;
  order_by?: Maybe<Array<AlbumTrackOrderBy>>;
  where?: Maybe<AlbumTrackBoolExp>;
};

/** aggregated selection of "album" */
export type AlbumAggregate = {
  aggregate?: Maybe<AlbumAggregateFields>;
  nodes: Array<Album>;
};

/** aggregate fields of "album" */
export type AlbumAggregateFields = {
  avg?: Maybe<AlbumAvgFields>;
  count?: Maybe<Scalars['Int']>;
  max?: Maybe<AlbumMaxFields>;
  min?: Maybe<AlbumMinFields>;
  stddev?: Maybe<AlbumStddevFields>;
  stddev_pop?: Maybe<AlbumStddevPopFields>;
  stddev_samp?: Maybe<AlbumStddevSampFields>;
  sum?: Maybe<AlbumSumFields>;
  var_pop?: Maybe<AlbumVarPopFields>;
  var_samp?: Maybe<AlbumVarSampFields>;
  variance?: Maybe<AlbumVarianceFields>;
};


/** aggregate fields of "album" */
export type AlbumAggregateFieldsCountArgs = {
  columns?: Maybe<Array<AlbumSelectColumn>>;
  distinct?: Maybe<Scalars['Boolean']>;
};

/** order by aggregate values of table "album" */
export type AlbumAggregateOrderBy = {
  avg?: Maybe<AlbumAvgOrderBy>;
  count?: Maybe<OrderBy>;
  max?: Maybe<AlbumMaxOrderBy>;
  min?: Maybe<AlbumMinOrderBy>;
  stddev?: Maybe<AlbumStddevOrderBy>;
  stddev_pop?: Maybe<AlbumStddevPopOrderBy>;
  stddev_samp?: Maybe<AlbumStddevSampOrderBy>;
  sum?: Maybe<AlbumSumOrderBy>;
  var_pop?: Maybe<AlbumVarPopOrderBy>;
  var_samp?: Maybe<AlbumVarSampOrderBy>;
  variance?: Maybe<AlbumVarianceOrderBy>;
};

/** input type for inserting array relation for remote table "album" */
export type AlbumArrRelInsertInput = {
  data: Array<AlbumInsertInput>;
  on_conflict?: Maybe<AlbumOnConflict>;
};

/** aggregate avg on columns */
export type AlbumAvgFields = {
  rating?: Maybe<Scalars['Float']>;
};

/** order by avg() on columns of table "album" */
export type AlbumAvgOrderBy = {
  rating?: Maybe<OrderBy>;
};

/** Boolean expression to filter rows from the table "album". All fields are combined with a logical 'AND'. */
export type AlbumBoolExp = {
  _and?: Maybe<Array<Maybe<AlbumBoolExp>>>;
  _not?: Maybe<AlbumBoolExp>;
  _or?: Maybe<Array<Maybe<AlbumBoolExp>>>;
  aired?: Maybe<DateComparisonExp>;
  album_collections?: Maybe<AlbumCollectionBoolExp>;
  album_countries?: Maybe<AlbumCountryBoolExp>;
  album_genres?: Maybe<AlbumGenreBoolExp>;
  album_moods?: Maybe<AlbumMoodBoolExp>;
  album_studios?: Maybe<AlbumStudioBoolExp>;
  album_styles?: Maybe<AlbumStyleBoolExp>;
  album_tracks?: Maybe<AlbumTrackBoolExp>;
  artist?: Maybe<ArtistBoolExp>;
  artist_id?: Maybe<UuidComparisonExp>;
  id?: Maybe<UuidComparisonExp>;
  image_background?: Maybe<StringComparisonExp>;
  image_cover?: Maybe<StringComparisonExp>;
  name?: Maybe<StringComparisonExp>;
  name_match?: Maybe<StringComparisonExp>;
  name_sort?: Maybe<StringComparisonExp>;
  rating?: Maybe<NumericComparisonExp>;
  summary?: Maybe<StringComparisonExp>;
};

/** columns and relationships of "album_collection" */
export type AlbumCollection = {
  /** An object relationship */
  album: Album;
  album_id: Scalars['uuid'];
  /** An object relationship */
  collection: Collection;
  collection_id: Scalars['uuid'];
  index: Scalars['Int'];
};

/** aggregated selection of "album_collection" */
export type AlbumCollectionAggregate = {
  aggregate?: Maybe<AlbumCollectionAggregateFields>;
  nodes: Array<AlbumCollection>;
};

/** aggregate fields of "album_collection" */
export type AlbumCollectionAggregateFields = {
  avg?: Maybe<AlbumCollectionAvgFields>;
  count?: Maybe<Scalars['Int']>;
  max?: Maybe<AlbumCollectionMaxFields>;
  min?: Maybe<AlbumCollectionMinFields>;
  stddev?: Maybe<AlbumCollectionStddevFields>;
  stddev_pop?: Maybe<AlbumCollectionStddevPopFields>;
  stddev_samp?: Maybe<AlbumCollectionStddevSampFields>;
  sum?: Maybe<AlbumCollectionSumFields>;
  var_pop?: Maybe<AlbumCollectionVarPopFields>;
  var_samp?: Maybe<AlbumCollectionVarSampFields>;
  variance?: Maybe<AlbumCollectionVarianceFields>;
};


/** aggregate fields of "album_collection" */
export type AlbumCollectionAggregateFieldsCountArgs = {
  columns?: Maybe<Array<AlbumCollectionSelectColumn>>;
  distinct?: Maybe<Scalars['Boolean']>;
};

/** order by aggregate values of table "album_collection" */
export type AlbumCollectionAggregateOrderBy = {
  avg?: Maybe<AlbumCollectionAvgOrderBy>;
  count?: Maybe<OrderBy>;
  max?: Maybe<AlbumCollectionMaxOrderBy>;
  min?: Maybe<AlbumCollectionMinOrderBy>;
  stddev?: Maybe<AlbumCollectionStddevOrderBy>;
  stddev_pop?: Maybe<AlbumCollectionStddevPopOrderBy>;
  stddev_samp?: Maybe<AlbumCollectionStddevSampOrderBy>;
  sum?: Maybe<AlbumCollectionSumOrderBy>;
  var_pop?: Maybe<AlbumCollectionVarPopOrderBy>;
  var_samp?: Maybe<AlbumCollectionVarSampOrderBy>;
  variance?: Maybe<AlbumCollectionVarianceOrderBy>;
};

/** input type for inserting array relation for remote table "album_collection" */
export type AlbumCollectionArrRelInsertInput = {
  data: Array<AlbumCollectionInsertInput>;
  on_conflict?: Maybe<AlbumCollectionOnConflict>;
};

/** aggregate avg on columns */
export type AlbumCollectionAvgFields = {
  index?: Maybe<Scalars['Float']>;
};

/** order by avg() on columns of table "album_collection" */
export type AlbumCollectionAvgOrderBy = {
  index?: Maybe<OrderBy>;
};

/** Boolean expression to filter rows from the table "album_collection". All fields are combined with a logical 'AND'. */
export type AlbumCollectionBoolExp = {
  _and?: Maybe<Array<Maybe<AlbumCollectionBoolExp>>>;
  _not?: Maybe<AlbumCollectionBoolExp>;
  _or?: Maybe<Array<Maybe<AlbumCollectionBoolExp>>>;
  album?: Maybe<AlbumBoolExp>;
  album_id?: Maybe<UuidComparisonExp>;
  collection?: Maybe<CollectionBoolExp>;
  collection_id?: Maybe<UuidComparisonExp>;
  index?: Maybe<IntComparisonExp>;
};

/** unique or primary key constraints on table "album_collection" */
export enum AlbumCollectionConstraint {
  /** unique or primary key constraint */
  AlbumCollectionAlbumIdCollectionIdKey = 'album_collection_album_id_collection_id_key',
  /** unique or primary key constraint */
  AlbumCollectionPkey = 'album_collection_pkey'
}

/** input type for incrementing integer column in table "album_collection" */
export type AlbumCollectionIncInput = {
  index?: Maybe<Scalars['Int']>;
};

/** input type for inserting data into table "album_collection" */
export type AlbumCollectionInsertInput = {
  album?: Maybe<AlbumObjRelInsertInput>;
  album_id?: Maybe<Scalars['uuid']>;
  collection?: Maybe<CollectionObjRelInsertInput>;
  collection_id?: Maybe<Scalars['uuid']>;
  index?: Maybe<Scalars['Int']>;
};

/** aggregate max on columns */
export type AlbumCollectionMaxFields = {
  album_id?: Maybe<Scalars['uuid']>;
  collection_id?: Maybe<Scalars['uuid']>;
  index?: Maybe<Scalars['Int']>;
};

/** order by max() on columns of table "album_collection" */
export type AlbumCollectionMaxOrderBy = {
  album_id?: Maybe<OrderBy>;
  collection_id?: Maybe<OrderBy>;
  index?: Maybe<OrderBy>;
};

/** aggregate min on columns */
export type AlbumCollectionMinFields = {
  album_id?: Maybe<Scalars['uuid']>;
  collection_id?: Maybe<Scalars['uuid']>;
  index?: Maybe<Scalars['Int']>;
};

/** order by min() on columns of table "album_collection" */
export type AlbumCollectionMinOrderBy = {
  album_id?: Maybe<OrderBy>;
  collection_id?: Maybe<OrderBy>;
  index?: Maybe<OrderBy>;
};

/** response of any mutation on the table "album_collection" */
export type AlbumCollectionMutationResponse = {
  /** number of affected rows by the mutation */
  affected_rows: Scalars['Int'];
  /** data of the affected rows by the mutation */
  returning: Array<AlbumCollection>;
};

/** input type for inserting object relation for remote table "album_collection" */
export type AlbumCollectionObjRelInsertInput = {
  data: AlbumCollectionInsertInput;
  on_conflict?: Maybe<AlbumCollectionOnConflict>;
};

/** on conflict condition type for table "album_collection" */
export type AlbumCollectionOnConflict = {
  constraint: AlbumCollectionConstraint;
  update_columns: Array<AlbumCollectionUpdateColumn>;
  where?: Maybe<AlbumCollectionBoolExp>;
};

/** ordering options when selecting data from "album_collection" */
export type AlbumCollectionOrderBy = {
  album?: Maybe<AlbumOrderBy>;
  album_id?: Maybe<OrderBy>;
  collection?: Maybe<CollectionOrderBy>;
  collection_id?: Maybe<OrderBy>;
  index?: Maybe<OrderBy>;
};

/** primary key columns input for table: "album_collection" */
export type AlbumCollectionPkColumnsInput = {
  album_id: Scalars['uuid'];
  index: Scalars['Int'];
};

/** select columns of table "album_collection" */
export enum AlbumCollectionSelectColumn {
  /** column name */
  AlbumId = 'album_id',
  /** column name */
  CollectionId = 'collection_id',
  /** column name */
  Index = 'index'
}

/** input type for updating data in table "album_collection" */
export type AlbumCollectionSetInput = {
  album_id?: Maybe<Scalars['uuid']>;
  collection_id?: Maybe<Scalars['uuid']>;
  index?: Maybe<Scalars['Int']>;
};

/** aggregate stddev on columns */
export type AlbumCollectionStddevFields = {
  index?: Maybe<Scalars['Float']>;
};

/** order by stddev() on columns of table "album_collection" */
export type AlbumCollectionStddevOrderBy = {
  index?: Maybe<OrderBy>;
};

/** aggregate stddev_pop on columns */
export type AlbumCollectionStddevPopFields = {
  index?: Maybe<Scalars['Float']>;
};

/** order by stddev_pop() on columns of table "album_collection" */
export type AlbumCollectionStddevPopOrderBy = {
  index?: Maybe<OrderBy>;
};

/** aggregate stddev_samp on columns */
export type AlbumCollectionStddevSampFields = {
  index?: Maybe<Scalars['Float']>;
};

/** order by stddev_samp() on columns of table "album_collection" */
export type AlbumCollectionStddevSampOrderBy = {
  index?: Maybe<OrderBy>;
};

/** aggregate sum on columns */
export type AlbumCollectionSumFields = {
  index?: Maybe<Scalars['Int']>;
};

/** order by sum() on columns of table "album_collection" */
export type AlbumCollectionSumOrderBy = {
  index?: Maybe<OrderBy>;
};

/** update columns of table "album_collection" */
export enum AlbumCollectionUpdateColumn {
  /** column name */
  AlbumId = 'album_id',
  /** column name */
  CollectionId = 'collection_id',
  /** column name */
  Index = 'index'
}

/** aggregate var_pop on columns */
export type AlbumCollectionVarPopFields = {
  index?: Maybe<Scalars['Float']>;
};

/** order by var_pop() on columns of table "album_collection" */
export type AlbumCollectionVarPopOrderBy = {
  index?: Maybe<OrderBy>;
};

/** aggregate var_samp on columns */
export type AlbumCollectionVarSampFields = {
  index?: Maybe<Scalars['Float']>;
};

/** order by var_samp() on columns of table "album_collection" */
export type AlbumCollectionVarSampOrderBy = {
  index?: Maybe<OrderBy>;
};

/** aggregate variance on columns */
export type AlbumCollectionVarianceFields = {
  index?: Maybe<Scalars['Float']>;
};

/** order by variance() on columns of table "album_collection" */
export type AlbumCollectionVarianceOrderBy = {
  index?: Maybe<OrderBy>;
};

/** unique or primary key constraints on table "album" */
export enum AlbumConstraint {
  /** unique or primary key constraint */
  AlbumNameMatchKey = 'album_name_match_key',
  /** unique or primary key constraint */
  AlbumPkey = 'album_pkey'
}

/** columns and relationships of "album_country" */
export type AlbumCountry = {
  /** An object relationship */
  album: Album;
  album_id: Scalars['uuid'];
  /** An object relationship */
  country: Country;
  country_code: Scalars['String'];
  index: Scalars['Int'];
};

/** aggregated selection of "album_country" */
export type AlbumCountryAggregate = {
  aggregate?: Maybe<AlbumCountryAggregateFields>;
  nodes: Array<AlbumCountry>;
};

/** aggregate fields of "album_country" */
export type AlbumCountryAggregateFields = {
  avg?: Maybe<AlbumCountryAvgFields>;
  count?: Maybe<Scalars['Int']>;
  max?: Maybe<AlbumCountryMaxFields>;
  min?: Maybe<AlbumCountryMinFields>;
  stddev?: Maybe<AlbumCountryStddevFields>;
  stddev_pop?: Maybe<AlbumCountryStddevPopFields>;
  stddev_samp?: Maybe<AlbumCountryStddevSampFields>;
  sum?: Maybe<AlbumCountrySumFields>;
  var_pop?: Maybe<AlbumCountryVarPopFields>;
  var_samp?: Maybe<AlbumCountryVarSampFields>;
  variance?: Maybe<AlbumCountryVarianceFields>;
};


/** aggregate fields of "album_country" */
export type AlbumCountryAggregateFieldsCountArgs = {
  columns?: Maybe<Array<AlbumCountrySelectColumn>>;
  distinct?: Maybe<Scalars['Boolean']>;
};

/** order by aggregate values of table "album_country" */
export type AlbumCountryAggregateOrderBy = {
  avg?: Maybe<AlbumCountryAvgOrderBy>;
  count?: Maybe<OrderBy>;
  max?: Maybe<AlbumCountryMaxOrderBy>;
  min?: Maybe<AlbumCountryMinOrderBy>;
  stddev?: Maybe<AlbumCountryStddevOrderBy>;
  stddev_pop?: Maybe<AlbumCountryStddevPopOrderBy>;
  stddev_samp?: Maybe<AlbumCountryStddevSampOrderBy>;
  sum?: Maybe<AlbumCountrySumOrderBy>;
  var_pop?: Maybe<AlbumCountryVarPopOrderBy>;
  var_samp?: Maybe<AlbumCountryVarSampOrderBy>;
  variance?: Maybe<AlbumCountryVarianceOrderBy>;
};

/** input type for inserting array relation for remote table "album_country" */
export type AlbumCountryArrRelInsertInput = {
  data: Array<AlbumCountryInsertInput>;
  on_conflict?: Maybe<AlbumCountryOnConflict>;
};

/** aggregate avg on columns */
export type AlbumCountryAvgFields = {
  index?: Maybe<Scalars['Float']>;
};

/** order by avg() on columns of table "album_country" */
export type AlbumCountryAvgOrderBy = {
  index?: Maybe<OrderBy>;
};

/** Boolean expression to filter rows from the table "album_country". All fields are combined with a logical 'AND'. */
export type AlbumCountryBoolExp = {
  _and?: Maybe<Array<Maybe<AlbumCountryBoolExp>>>;
  _not?: Maybe<AlbumCountryBoolExp>;
  _or?: Maybe<Array<Maybe<AlbumCountryBoolExp>>>;
  album?: Maybe<AlbumBoolExp>;
  album_id?: Maybe<UuidComparisonExp>;
  country?: Maybe<CountryBoolExp>;
  country_code?: Maybe<StringComparisonExp>;
  index?: Maybe<IntComparisonExp>;
};

/** unique or primary key constraints on table "album_country" */
export enum AlbumCountryConstraint {
  /** unique or primary key constraint */
  AlbumCountryAlbumIdCountryCodeKey = 'album_country_album_id_country_code_key',
  /** unique or primary key constraint */
  AlbumCountryPkey = 'album_country_pkey'
}

/** input type for incrementing integer column in table "album_country" */
export type AlbumCountryIncInput = {
  index?: Maybe<Scalars['Int']>;
};

/** input type for inserting data into table "album_country" */
export type AlbumCountryInsertInput = {
  album?: Maybe<AlbumObjRelInsertInput>;
  album_id?: Maybe<Scalars['uuid']>;
  country?: Maybe<CountryObjRelInsertInput>;
  country_code?: Maybe<Scalars['String']>;
  index?: Maybe<Scalars['Int']>;
};

/** aggregate max on columns */
export type AlbumCountryMaxFields = {
  album_id?: Maybe<Scalars['uuid']>;
  country_code?: Maybe<Scalars['String']>;
  index?: Maybe<Scalars['Int']>;
};

/** order by max() on columns of table "album_country" */
export type AlbumCountryMaxOrderBy = {
  album_id?: Maybe<OrderBy>;
  country_code?: Maybe<OrderBy>;
  index?: Maybe<OrderBy>;
};

/** aggregate min on columns */
export type AlbumCountryMinFields = {
  album_id?: Maybe<Scalars['uuid']>;
  country_code?: Maybe<Scalars['String']>;
  index?: Maybe<Scalars['Int']>;
};

/** order by min() on columns of table "album_country" */
export type AlbumCountryMinOrderBy = {
  album_id?: Maybe<OrderBy>;
  country_code?: Maybe<OrderBy>;
  index?: Maybe<OrderBy>;
};

/** response of any mutation on the table "album_country" */
export type AlbumCountryMutationResponse = {
  /** number of affected rows by the mutation */
  affected_rows: Scalars['Int'];
  /** data of the affected rows by the mutation */
  returning: Array<AlbumCountry>;
};

/** input type for inserting object relation for remote table "album_country" */
export type AlbumCountryObjRelInsertInput = {
  data: AlbumCountryInsertInput;
  on_conflict?: Maybe<AlbumCountryOnConflict>;
};

/** on conflict condition type for table "album_country" */
export type AlbumCountryOnConflict = {
  constraint: AlbumCountryConstraint;
  update_columns: Array<AlbumCountryUpdateColumn>;
  where?: Maybe<AlbumCountryBoolExp>;
};

/** ordering options when selecting data from "album_country" */
export type AlbumCountryOrderBy = {
  album?: Maybe<AlbumOrderBy>;
  album_id?: Maybe<OrderBy>;
  country?: Maybe<CountryOrderBy>;
  country_code?: Maybe<OrderBy>;
  index?: Maybe<OrderBy>;
};

/** primary key columns input for table: "album_country" */
export type AlbumCountryPkColumnsInput = {
  album_id: Scalars['uuid'];
  index: Scalars['Int'];
};

/** select columns of table "album_country" */
export enum AlbumCountrySelectColumn {
  /** column name */
  AlbumId = 'album_id',
  /** column name */
  CountryCode = 'country_code',
  /** column name */
  Index = 'index'
}

/** input type for updating data in table "album_country" */
export type AlbumCountrySetInput = {
  album_id?: Maybe<Scalars['uuid']>;
  country_code?: Maybe<Scalars['String']>;
  index?: Maybe<Scalars['Int']>;
};

/** aggregate stddev on columns */
export type AlbumCountryStddevFields = {
  index?: Maybe<Scalars['Float']>;
};

/** order by stddev() on columns of table "album_country" */
export type AlbumCountryStddevOrderBy = {
  index?: Maybe<OrderBy>;
};

/** aggregate stddev_pop on columns */
export type AlbumCountryStddevPopFields = {
  index?: Maybe<Scalars['Float']>;
};

/** order by stddev_pop() on columns of table "album_country" */
export type AlbumCountryStddevPopOrderBy = {
  index?: Maybe<OrderBy>;
};

/** aggregate stddev_samp on columns */
export type AlbumCountryStddevSampFields = {
  index?: Maybe<Scalars['Float']>;
};

/** order by stddev_samp() on columns of table "album_country" */
export type AlbumCountryStddevSampOrderBy = {
  index?: Maybe<OrderBy>;
};

/** aggregate sum on columns */
export type AlbumCountrySumFields = {
  index?: Maybe<Scalars['Int']>;
};

/** order by sum() on columns of table "album_country" */
export type AlbumCountrySumOrderBy = {
  index?: Maybe<OrderBy>;
};

/** update columns of table "album_country" */
export enum AlbumCountryUpdateColumn {
  /** column name */
  AlbumId = 'album_id',
  /** column name */
  CountryCode = 'country_code',
  /** column name */
  Index = 'index'
}

/** aggregate var_pop on columns */
export type AlbumCountryVarPopFields = {
  index?: Maybe<Scalars['Float']>;
};

/** order by var_pop() on columns of table "album_country" */
export type AlbumCountryVarPopOrderBy = {
  index?: Maybe<OrderBy>;
};

/** aggregate var_samp on columns */
export type AlbumCountryVarSampFields = {
  index?: Maybe<Scalars['Float']>;
};

/** order by var_samp() on columns of table "album_country" */
export type AlbumCountryVarSampOrderBy = {
  index?: Maybe<OrderBy>;
};

/** aggregate variance on columns */
export type AlbumCountryVarianceFields = {
  index?: Maybe<Scalars['Float']>;
};

/** order by variance() on columns of table "album_country" */
export type AlbumCountryVarianceOrderBy = {
  index?: Maybe<OrderBy>;
};

/** columns and relationships of "album_genre" */
export type AlbumGenre = {
  /** An object relationship */
  album: Album;
  album_id: Scalars['uuid'];
  /** An object relationship */
  genre: Genre;
  genre_id: Scalars['uuid'];
  index: Scalars['Int'];
};

/** aggregated selection of "album_genre" */
export type AlbumGenreAggregate = {
  aggregate?: Maybe<AlbumGenreAggregateFields>;
  nodes: Array<AlbumGenre>;
};

/** aggregate fields of "album_genre" */
export type AlbumGenreAggregateFields = {
  avg?: Maybe<AlbumGenreAvgFields>;
  count?: Maybe<Scalars['Int']>;
  max?: Maybe<AlbumGenreMaxFields>;
  min?: Maybe<AlbumGenreMinFields>;
  stddev?: Maybe<AlbumGenreStddevFields>;
  stddev_pop?: Maybe<AlbumGenreStddevPopFields>;
  stddev_samp?: Maybe<AlbumGenreStddevSampFields>;
  sum?: Maybe<AlbumGenreSumFields>;
  var_pop?: Maybe<AlbumGenreVarPopFields>;
  var_samp?: Maybe<AlbumGenreVarSampFields>;
  variance?: Maybe<AlbumGenreVarianceFields>;
};


/** aggregate fields of "album_genre" */
export type AlbumGenreAggregateFieldsCountArgs = {
  columns?: Maybe<Array<AlbumGenreSelectColumn>>;
  distinct?: Maybe<Scalars['Boolean']>;
};

/** order by aggregate values of table "album_genre" */
export type AlbumGenreAggregateOrderBy = {
  avg?: Maybe<AlbumGenreAvgOrderBy>;
  count?: Maybe<OrderBy>;
  max?: Maybe<AlbumGenreMaxOrderBy>;
  min?: Maybe<AlbumGenreMinOrderBy>;
  stddev?: Maybe<AlbumGenreStddevOrderBy>;
  stddev_pop?: Maybe<AlbumGenreStddevPopOrderBy>;
  stddev_samp?: Maybe<AlbumGenreStddevSampOrderBy>;
  sum?: Maybe<AlbumGenreSumOrderBy>;
  var_pop?: Maybe<AlbumGenreVarPopOrderBy>;
  var_samp?: Maybe<AlbumGenreVarSampOrderBy>;
  variance?: Maybe<AlbumGenreVarianceOrderBy>;
};

/** input type for inserting array relation for remote table "album_genre" */
export type AlbumGenreArrRelInsertInput = {
  data: Array<AlbumGenreInsertInput>;
  on_conflict?: Maybe<AlbumGenreOnConflict>;
};

/** aggregate avg on columns */
export type AlbumGenreAvgFields = {
  index?: Maybe<Scalars['Float']>;
};

/** order by avg() on columns of table "album_genre" */
export type AlbumGenreAvgOrderBy = {
  index?: Maybe<OrderBy>;
};

/** Boolean expression to filter rows from the table "album_genre". All fields are combined with a logical 'AND'. */
export type AlbumGenreBoolExp = {
  _and?: Maybe<Array<Maybe<AlbumGenreBoolExp>>>;
  _not?: Maybe<AlbumGenreBoolExp>;
  _or?: Maybe<Array<Maybe<AlbumGenreBoolExp>>>;
  album?: Maybe<AlbumBoolExp>;
  album_id?: Maybe<UuidComparisonExp>;
  genre?: Maybe<GenreBoolExp>;
  genre_id?: Maybe<UuidComparisonExp>;
  index?: Maybe<IntComparisonExp>;
};

/** unique or primary key constraints on table "album_genre" */
export enum AlbumGenreConstraint {
  /** unique or primary key constraint */
  AlbumGenreAlbumIdGenreIdKey = 'album_genre_album_id_genre_id_key',
  /** unique or primary key constraint */
  AlbumGenrePkey = 'album_genre_pkey'
}

/** input type for incrementing integer column in table "album_genre" */
export type AlbumGenreIncInput = {
  index?: Maybe<Scalars['Int']>;
};

/** input type for inserting data into table "album_genre" */
export type AlbumGenreInsertInput = {
  album?: Maybe<AlbumObjRelInsertInput>;
  album_id?: Maybe<Scalars['uuid']>;
  genre?: Maybe<GenreObjRelInsertInput>;
  genre_id?: Maybe<Scalars['uuid']>;
  index?: Maybe<Scalars['Int']>;
};

/** aggregate max on columns */
export type AlbumGenreMaxFields = {
  album_id?: Maybe<Scalars['uuid']>;
  genre_id?: Maybe<Scalars['uuid']>;
  index?: Maybe<Scalars['Int']>;
};

/** order by max() on columns of table "album_genre" */
export type AlbumGenreMaxOrderBy = {
  album_id?: Maybe<OrderBy>;
  genre_id?: Maybe<OrderBy>;
  index?: Maybe<OrderBy>;
};

/** aggregate min on columns */
export type AlbumGenreMinFields = {
  album_id?: Maybe<Scalars['uuid']>;
  genre_id?: Maybe<Scalars['uuid']>;
  index?: Maybe<Scalars['Int']>;
};

/** order by min() on columns of table "album_genre" */
export type AlbumGenreMinOrderBy = {
  album_id?: Maybe<OrderBy>;
  genre_id?: Maybe<OrderBy>;
  index?: Maybe<OrderBy>;
};

/** response of any mutation on the table "album_genre" */
export type AlbumGenreMutationResponse = {
  /** number of affected rows by the mutation */
  affected_rows: Scalars['Int'];
  /** data of the affected rows by the mutation */
  returning: Array<AlbumGenre>;
};

/** input type for inserting object relation for remote table "album_genre" */
export type AlbumGenreObjRelInsertInput = {
  data: AlbumGenreInsertInput;
  on_conflict?: Maybe<AlbumGenreOnConflict>;
};

/** on conflict condition type for table "album_genre" */
export type AlbumGenreOnConflict = {
  constraint: AlbumGenreConstraint;
  update_columns: Array<AlbumGenreUpdateColumn>;
  where?: Maybe<AlbumGenreBoolExp>;
};

/** ordering options when selecting data from "album_genre" */
export type AlbumGenreOrderBy = {
  album?: Maybe<AlbumOrderBy>;
  album_id?: Maybe<OrderBy>;
  genre?: Maybe<GenreOrderBy>;
  genre_id?: Maybe<OrderBy>;
  index?: Maybe<OrderBy>;
};

/** primary key columns input for table: "album_genre" */
export type AlbumGenrePkColumnsInput = {
  album_id: Scalars['uuid'];
  index: Scalars['Int'];
};

/** select columns of table "album_genre" */
export enum AlbumGenreSelectColumn {
  /** column name */
  AlbumId = 'album_id',
  /** column name */
  GenreId = 'genre_id',
  /** column name */
  Index = 'index'
}

/** input type for updating data in table "album_genre" */
export type AlbumGenreSetInput = {
  album_id?: Maybe<Scalars['uuid']>;
  genre_id?: Maybe<Scalars['uuid']>;
  index?: Maybe<Scalars['Int']>;
};

/** aggregate stddev on columns */
export type AlbumGenreStddevFields = {
  index?: Maybe<Scalars['Float']>;
};

/** order by stddev() on columns of table "album_genre" */
export type AlbumGenreStddevOrderBy = {
  index?: Maybe<OrderBy>;
};

/** aggregate stddev_pop on columns */
export type AlbumGenreStddevPopFields = {
  index?: Maybe<Scalars['Float']>;
};

/** order by stddev_pop() on columns of table "album_genre" */
export type AlbumGenreStddevPopOrderBy = {
  index?: Maybe<OrderBy>;
};

/** aggregate stddev_samp on columns */
export type AlbumGenreStddevSampFields = {
  index?: Maybe<Scalars['Float']>;
};

/** order by stddev_samp() on columns of table "album_genre" */
export type AlbumGenreStddevSampOrderBy = {
  index?: Maybe<OrderBy>;
};

/** aggregate sum on columns */
export type AlbumGenreSumFields = {
  index?: Maybe<Scalars['Int']>;
};

/** order by sum() on columns of table "album_genre" */
export type AlbumGenreSumOrderBy = {
  index?: Maybe<OrderBy>;
};

/** update columns of table "album_genre" */
export enum AlbumGenreUpdateColumn {
  /** column name */
  AlbumId = 'album_id',
  /** column name */
  GenreId = 'genre_id',
  /** column name */
  Index = 'index'
}

/** aggregate var_pop on columns */
export type AlbumGenreVarPopFields = {
  index?: Maybe<Scalars['Float']>;
};

/** order by var_pop() on columns of table "album_genre" */
export type AlbumGenreVarPopOrderBy = {
  index?: Maybe<OrderBy>;
};

/** aggregate var_samp on columns */
export type AlbumGenreVarSampFields = {
  index?: Maybe<Scalars['Float']>;
};

/** order by var_samp() on columns of table "album_genre" */
export type AlbumGenreVarSampOrderBy = {
  index?: Maybe<OrderBy>;
};

/** aggregate variance on columns */
export type AlbumGenreVarianceFields = {
  index?: Maybe<Scalars['Float']>;
};

/** order by variance() on columns of table "album_genre" */
export type AlbumGenreVarianceOrderBy = {
  index?: Maybe<OrderBy>;
};

/** input type for incrementing integer column in table "album" */
export type AlbumIncInput = {
  rating?: Maybe<Scalars['numeric']>;
};

/** input type for inserting data into table "album" */
export type AlbumInsertInput = {
  aired?: Maybe<Scalars['date']>;
  album_collections?: Maybe<AlbumCollectionArrRelInsertInput>;
  album_countries?: Maybe<AlbumCountryArrRelInsertInput>;
  album_genres?: Maybe<AlbumGenreArrRelInsertInput>;
  album_moods?: Maybe<AlbumMoodArrRelInsertInput>;
  album_studios?: Maybe<AlbumStudioArrRelInsertInput>;
  album_styles?: Maybe<AlbumStyleArrRelInsertInput>;
  album_tracks?: Maybe<AlbumTrackArrRelInsertInput>;
  artist?: Maybe<ArtistObjRelInsertInput>;
  artist_id?: Maybe<Scalars['uuid']>;
  id?: Maybe<Scalars['uuid']>;
  image_background?: Maybe<Scalars['String']>;
  image_cover?: Maybe<Scalars['String']>;
  name?: Maybe<Scalars['String']>;
  name_match?: Maybe<Scalars['String']>;
  name_sort?: Maybe<Scalars['String']>;
  rating?: Maybe<Scalars['numeric']>;
  summary?: Maybe<Scalars['String']>;
};

/** aggregate max on columns */
export type AlbumMaxFields = {
  aired?: Maybe<Scalars['date']>;
  artist_id?: Maybe<Scalars['uuid']>;
  id?: Maybe<Scalars['uuid']>;
  image_background?: Maybe<Scalars['String']>;
  image_cover?: Maybe<Scalars['String']>;
  name?: Maybe<Scalars['String']>;
  name_match?: Maybe<Scalars['String']>;
  name_sort?: Maybe<Scalars['String']>;
  rating?: Maybe<Scalars['numeric']>;
  summary?: Maybe<Scalars['String']>;
};

/** order by max() on columns of table "album" */
export type AlbumMaxOrderBy = {
  aired?: Maybe<OrderBy>;
  artist_id?: Maybe<OrderBy>;
  id?: Maybe<OrderBy>;
  image_background?: Maybe<OrderBy>;
  image_cover?: Maybe<OrderBy>;
  name?: Maybe<OrderBy>;
  name_match?: Maybe<OrderBy>;
  name_sort?: Maybe<OrderBy>;
  rating?: Maybe<OrderBy>;
  summary?: Maybe<OrderBy>;
};

/** aggregate min on columns */
export type AlbumMinFields = {
  aired?: Maybe<Scalars['date']>;
  artist_id?: Maybe<Scalars['uuid']>;
  id?: Maybe<Scalars['uuid']>;
  image_background?: Maybe<Scalars['String']>;
  image_cover?: Maybe<Scalars['String']>;
  name?: Maybe<Scalars['String']>;
  name_match?: Maybe<Scalars['String']>;
  name_sort?: Maybe<Scalars['String']>;
  rating?: Maybe<Scalars['numeric']>;
  summary?: Maybe<Scalars['String']>;
};

/** order by min() on columns of table "album" */
export type AlbumMinOrderBy = {
  aired?: Maybe<OrderBy>;
  artist_id?: Maybe<OrderBy>;
  id?: Maybe<OrderBy>;
  image_background?: Maybe<OrderBy>;
  image_cover?: Maybe<OrderBy>;
  name?: Maybe<OrderBy>;
  name_match?: Maybe<OrderBy>;
  name_sort?: Maybe<OrderBy>;
  rating?: Maybe<OrderBy>;
  summary?: Maybe<OrderBy>;
};

/** columns and relationships of "album_mood" */
export type AlbumMood = {
  /** An object relationship */
  album: Album;
  album_id: Scalars['uuid'];
  index: Scalars['Int'];
  /** An object relationship */
  mood: Mood;
  mood_id: Scalars['uuid'];
};

/** aggregated selection of "album_mood" */
export type AlbumMoodAggregate = {
  aggregate?: Maybe<AlbumMoodAggregateFields>;
  nodes: Array<AlbumMood>;
};

/** aggregate fields of "album_mood" */
export type AlbumMoodAggregateFields = {
  avg?: Maybe<AlbumMoodAvgFields>;
  count?: Maybe<Scalars['Int']>;
  max?: Maybe<AlbumMoodMaxFields>;
  min?: Maybe<AlbumMoodMinFields>;
  stddev?: Maybe<AlbumMoodStddevFields>;
  stddev_pop?: Maybe<AlbumMoodStddevPopFields>;
  stddev_samp?: Maybe<AlbumMoodStddevSampFields>;
  sum?: Maybe<AlbumMoodSumFields>;
  var_pop?: Maybe<AlbumMoodVarPopFields>;
  var_samp?: Maybe<AlbumMoodVarSampFields>;
  variance?: Maybe<AlbumMoodVarianceFields>;
};


/** aggregate fields of "album_mood" */
export type AlbumMoodAggregateFieldsCountArgs = {
  columns?: Maybe<Array<AlbumMoodSelectColumn>>;
  distinct?: Maybe<Scalars['Boolean']>;
};

/** order by aggregate values of table "album_mood" */
export type AlbumMoodAggregateOrderBy = {
  avg?: Maybe<AlbumMoodAvgOrderBy>;
  count?: Maybe<OrderBy>;
  max?: Maybe<AlbumMoodMaxOrderBy>;
  min?: Maybe<AlbumMoodMinOrderBy>;
  stddev?: Maybe<AlbumMoodStddevOrderBy>;
  stddev_pop?: Maybe<AlbumMoodStddevPopOrderBy>;
  stddev_samp?: Maybe<AlbumMoodStddevSampOrderBy>;
  sum?: Maybe<AlbumMoodSumOrderBy>;
  var_pop?: Maybe<AlbumMoodVarPopOrderBy>;
  var_samp?: Maybe<AlbumMoodVarSampOrderBy>;
  variance?: Maybe<AlbumMoodVarianceOrderBy>;
};

/** input type for inserting array relation for remote table "album_mood" */
export type AlbumMoodArrRelInsertInput = {
  data: Array<AlbumMoodInsertInput>;
  on_conflict?: Maybe<AlbumMoodOnConflict>;
};

/** aggregate avg on columns */
export type AlbumMoodAvgFields = {
  index?: Maybe<Scalars['Float']>;
};

/** order by avg() on columns of table "album_mood" */
export type AlbumMoodAvgOrderBy = {
  index?: Maybe<OrderBy>;
};

/** Boolean expression to filter rows from the table "album_mood". All fields are combined with a logical 'AND'. */
export type AlbumMoodBoolExp = {
  _and?: Maybe<Array<Maybe<AlbumMoodBoolExp>>>;
  _not?: Maybe<AlbumMoodBoolExp>;
  _or?: Maybe<Array<Maybe<AlbumMoodBoolExp>>>;
  album?: Maybe<AlbumBoolExp>;
  album_id?: Maybe<UuidComparisonExp>;
  index?: Maybe<IntComparisonExp>;
  mood?: Maybe<MoodBoolExp>;
  mood_id?: Maybe<UuidComparisonExp>;
};

/** unique or primary key constraints on table "album_mood" */
export enum AlbumMoodConstraint {
  /** unique or primary key constraint */
  AlbumMoodAlbumIdMoodIdKey = 'album_mood_album_id_mood_id_key',
  /** unique or primary key constraint */
  AlbumMoodPkey = 'album_mood_pkey'
}

/** input type for incrementing integer column in table "album_mood" */
export type AlbumMoodIncInput = {
  index?: Maybe<Scalars['Int']>;
};

/** input type for inserting data into table "album_mood" */
export type AlbumMoodInsertInput = {
  album?: Maybe<AlbumObjRelInsertInput>;
  album_id?: Maybe<Scalars['uuid']>;
  index?: Maybe<Scalars['Int']>;
  mood?: Maybe<MoodObjRelInsertInput>;
  mood_id?: Maybe<Scalars['uuid']>;
};

/** aggregate max on columns */
export type AlbumMoodMaxFields = {
  album_id?: Maybe<Scalars['uuid']>;
  index?: Maybe<Scalars['Int']>;
  mood_id?: Maybe<Scalars['uuid']>;
};

/** order by max() on columns of table "album_mood" */
export type AlbumMoodMaxOrderBy = {
  album_id?: Maybe<OrderBy>;
  index?: Maybe<OrderBy>;
  mood_id?: Maybe<OrderBy>;
};

/** aggregate min on columns */
export type AlbumMoodMinFields = {
  album_id?: Maybe<Scalars['uuid']>;
  index?: Maybe<Scalars['Int']>;
  mood_id?: Maybe<Scalars['uuid']>;
};

/** order by min() on columns of table "album_mood" */
export type AlbumMoodMinOrderBy = {
  album_id?: Maybe<OrderBy>;
  index?: Maybe<OrderBy>;
  mood_id?: Maybe<OrderBy>;
};

/** response of any mutation on the table "album_mood" */
export type AlbumMoodMutationResponse = {
  /** number of affected rows by the mutation */
  affected_rows: Scalars['Int'];
  /** data of the affected rows by the mutation */
  returning: Array<AlbumMood>;
};

/** input type for inserting object relation for remote table "album_mood" */
export type AlbumMoodObjRelInsertInput = {
  data: AlbumMoodInsertInput;
  on_conflict?: Maybe<AlbumMoodOnConflict>;
};

/** on conflict condition type for table "album_mood" */
export type AlbumMoodOnConflict = {
  constraint: AlbumMoodConstraint;
  update_columns: Array<AlbumMoodUpdateColumn>;
  where?: Maybe<AlbumMoodBoolExp>;
};

/** ordering options when selecting data from "album_mood" */
export type AlbumMoodOrderBy = {
  album?: Maybe<AlbumOrderBy>;
  album_id?: Maybe<OrderBy>;
  index?: Maybe<OrderBy>;
  mood?: Maybe<MoodOrderBy>;
  mood_id?: Maybe<OrderBy>;
};

/** primary key columns input for table: "album_mood" */
export type AlbumMoodPkColumnsInput = {
  album_id: Scalars['uuid'];
  index: Scalars['Int'];
};

/** select columns of table "album_mood" */
export enum AlbumMoodSelectColumn {
  /** column name */
  AlbumId = 'album_id',
  /** column name */
  Index = 'index',
  /** column name */
  MoodId = 'mood_id'
}

/** input type for updating data in table "album_mood" */
export type AlbumMoodSetInput = {
  album_id?: Maybe<Scalars['uuid']>;
  index?: Maybe<Scalars['Int']>;
  mood_id?: Maybe<Scalars['uuid']>;
};

/** aggregate stddev on columns */
export type AlbumMoodStddevFields = {
  index?: Maybe<Scalars['Float']>;
};

/** order by stddev() on columns of table "album_mood" */
export type AlbumMoodStddevOrderBy = {
  index?: Maybe<OrderBy>;
};

/** aggregate stddev_pop on columns */
export type AlbumMoodStddevPopFields = {
  index?: Maybe<Scalars['Float']>;
};

/** order by stddev_pop() on columns of table "album_mood" */
export type AlbumMoodStddevPopOrderBy = {
  index?: Maybe<OrderBy>;
};

/** aggregate stddev_samp on columns */
export type AlbumMoodStddevSampFields = {
  index?: Maybe<Scalars['Float']>;
};

/** order by stddev_samp() on columns of table "album_mood" */
export type AlbumMoodStddevSampOrderBy = {
  index?: Maybe<OrderBy>;
};

/** aggregate sum on columns */
export type AlbumMoodSumFields = {
  index?: Maybe<Scalars['Int']>;
};

/** order by sum() on columns of table "album_mood" */
export type AlbumMoodSumOrderBy = {
  index?: Maybe<OrderBy>;
};

/** update columns of table "album_mood" */
export enum AlbumMoodUpdateColumn {
  /** column name */
  AlbumId = 'album_id',
  /** column name */
  Index = 'index',
  /** column name */
  MoodId = 'mood_id'
}

/** aggregate var_pop on columns */
export type AlbumMoodVarPopFields = {
  index?: Maybe<Scalars['Float']>;
};

/** order by var_pop() on columns of table "album_mood" */
export type AlbumMoodVarPopOrderBy = {
  index?: Maybe<OrderBy>;
};

/** aggregate var_samp on columns */
export type AlbumMoodVarSampFields = {
  index?: Maybe<Scalars['Float']>;
};

/** order by var_samp() on columns of table "album_mood" */
export type AlbumMoodVarSampOrderBy = {
  index?: Maybe<OrderBy>;
};

/** aggregate variance on columns */
export type AlbumMoodVarianceFields = {
  index?: Maybe<Scalars['Float']>;
};

/** order by variance() on columns of table "album_mood" */
export type AlbumMoodVarianceOrderBy = {
  index?: Maybe<OrderBy>;
};

/** response of any mutation on the table "album" */
export type AlbumMutationResponse = {
  /** number of affected rows by the mutation */
  affected_rows: Scalars['Int'];
  /** data of the affected rows by the mutation */
  returning: Array<Album>;
};

/** input type for inserting object relation for remote table "album" */
export type AlbumObjRelInsertInput = {
  data: AlbumInsertInput;
  on_conflict?: Maybe<AlbumOnConflict>;
};

/** on conflict condition type for table "album" */
export type AlbumOnConflict = {
  constraint: AlbumConstraint;
  update_columns: Array<AlbumUpdateColumn>;
  where?: Maybe<AlbumBoolExp>;
};

/** ordering options when selecting data from "album" */
export type AlbumOrderBy = {
  aired?: Maybe<OrderBy>;
  album_collections_aggregate?: Maybe<AlbumCollectionAggregateOrderBy>;
  album_countries_aggregate?: Maybe<AlbumCountryAggregateOrderBy>;
  album_genres_aggregate?: Maybe<AlbumGenreAggregateOrderBy>;
  album_moods_aggregate?: Maybe<AlbumMoodAggregateOrderBy>;
  album_studios_aggregate?: Maybe<AlbumStudioAggregateOrderBy>;
  album_styles_aggregate?: Maybe<AlbumStyleAggregateOrderBy>;
  album_tracks_aggregate?: Maybe<AlbumTrackAggregateOrderBy>;
  artist?: Maybe<ArtistOrderBy>;
  artist_id?: Maybe<OrderBy>;
  id?: Maybe<OrderBy>;
  image_background?: Maybe<OrderBy>;
  image_cover?: Maybe<OrderBy>;
  name?: Maybe<OrderBy>;
  name_match?: Maybe<OrderBy>;
  name_sort?: Maybe<OrderBy>;
  rating?: Maybe<OrderBy>;
  summary?: Maybe<OrderBy>;
};

/** primary key columns input for table: "album" */
export type AlbumPkColumnsInput = {
  id: Scalars['uuid'];
};

/** select columns of table "album" */
export enum AlbumSelectColumn {
  /** column name */
  Aired = 'aired',
  /** column name */
  ArtistId = 'artist_id',
  /** column name */
  Id = 'id',
  /** column name */
  ImageBackground = 'image_background',
  /** column name */
  ImageCover = 'image_cover',
  /** column name */
  Name = 'name',
  /** column name */
  NameMatch = 'name_match',
  /** column name */
  NameSort = 'name_sort',
  /** column name */
  Rating = 'rating',
  /** column name */
  Summary = 'summary'
}

/** input type for updating data in table "album" */
export type AlbumSetInput = {
  aired?: Maybe<Scalars['date']>;
  artist_id?: Maybe<Scalars['uuid']>;
  id?: Maybe<Scalars['uuid']>;
  image_background?: Maybe<Scalars['String']>;
  image_cover?: Maybe<Scalars['String']>;
  name?: Maybe<Scalars['String']>;
  name_match?: Maybe<Scalars['String']>;
  name_sort?: Maybe<Scalars['String']>;
  rating?: Maybe<Scalars['numeric']>;
  summary?: Maybe<Scalars['String']>;
};

/** aggregate stddev on columns */
export type AlbumStddevFields = {
  rating?: Maybe<Scalars['Float']>;
};

/** order by stddev() on columns of table "album" */
export type AlbumStddevOrderBy = {
  rating?: Maybe<OrderBy>;
};

/** aggregate stddev_pop on columns */
export type AlbumStddevPopFields = {
  rating?: Maybe<Scalars['Float']>;
};

/** order by stddev_pop() on columns of table "album" */
export type AlbumStddevPopOrderBy = {
  rating?: Maybe<OrderBy>;
};

/** aggregate stddev_samp on columns */
export type AlbumStddevSampFields = {
  rating?: Maybe<Scalars['Float']>;
};

/** order by stddev_samp() on columns of table "album" */
export type AlbumStddevSampOrderBy = {
  rating?: Maybe<OrderBy>;
};

/** columns and relationships of "album_studio" */
export type AlbumStudio = {
  /** An object relationship */
  album: Album;
  album_id: Scalars['uuid'];
  index: Scalars['Int'];
  /** An object relationship */
  studio: Studio;
  studio_id: Scalars['uuid'];
};

/** aggregated selection of "album_studio" */
export type AlbumStudioAggregate = {
  aggregate?: Maybe<AlbumStudioAggregateFields>;
  nodes: Array<AlbumStudio>;
};

/** aggregate fields of "album_studio" */
export type AlbumStudioAggregateFields = {
  avg?: Maybe<AlbumStudioAvgFields>;
  count?: Maybe<Scalars['Int']>;
  max?: Maybe<AlbumStudioMaxFields>;
  min?: Maybe<AlbumStudioMinFields>;
  stddev?: Maybe<AlbumStudioStddevFields>;
  stddev_pop?: Maybe<AlbumStudioStddevPopFields>;
  stddev_samp?: Maybe<AlbumStudioStddevSampFields>;
  sum?: Maybe<AlbumStudioSumFields>;
  var_pop?: Maybe<AlbumStudioVarPopFields>;
  var_samp?: Maybe<AlbumStudioVarSampFields>;
  variance?: Maybe<AlbumStudioVarianceFields>;
};


/** aggregate fields of "album_studio" */
export type AlbumStudioAggregateFieldsCountArgs = {
  columns?: Maybe<Array<AlbumStudioSelectColumn>>;
  distinct?: Maybe<Scalars['Boolean']>;
};

/** order by aggregate values of table "album_studio" */
export type AlbumStudioAggregateOrderBy = {
  avg?: Maybe<AlbumStudioAvgOrderBy>;
  count?: Maybe<OrderBy>;
  max?: Maybe<AlbumStudioMaxOrderBy>;
  min?: Maybe<AlbumStudioMinOrderBy>;
  stddev?: Maybe<AlbumStudioStddevOrderBy>;
  stddev_pop?: Maybe<AlbumStudioStddevPopOrderBy>;
  stddev_samp?: Maybe<AlbumStudioStddevSampOrderBy>;
  sum?: Maybe<AlbumStudioSumOrderBy>;
  var_pop?: Maybe<AlbumStudioVarPopOrderBy>;
  var_samp?: Maybe<AlbumStudioVarSampOrderBy>;
  variance?: Maybe<AlbumStudioVarianceOrderBy>;
};

/** input type for inserting array relation for remote table "album_studio" */
export type AlbumStudioArrRelInsertInput = {
  data: Array<AlbumStudioInsertInput>;
  on_conflict?: Maybe<AlbumStudioOnConflict>;
};

/** aggregate avg on columns */
export type AlbumStudioAvgFields = {
  index?: Maybe<Scalars['Float']>;
};

/** order by avg() on columns of table "album_studio" */
export type AlbumStudioAvgOrderBy = {
  index?: Maybe<OrderBy>;
};

/** Boolean expression to filter rows from the table "album_studio". All fields are combined with a logical 'AND'. */
export type AlbumStudioBoolExp = {
  _and?: Maybe<Array<Maybe<AlbumStudioBoolExp>>>;
  _not?: Maybe<AlbumStudioBoolExp>;
  _or?: Maybe<Array<Maybe<AlbumStudioBoolExp>>>;
  album?: Maybe<AlbumBoolExp>;
  album_id?: Maybe<UuidComparisonExp>;
  index?: Maybe<IntComparisonExp>;
  studio?: Maybe<StudioBoolExp>;
  studio_id?: Maybe<UuidComparisonExp>;
};

/** unique or primary key constraints on table "album_studio" */
export enum AlbumStudioConstraint {
  /** unique or primary key constraint */
  AlbumStudioAlbumIdStudioIdKey = 'album_studio_album_id_studio_id_key',
  /** unique or primary key constraint */
  AlbumStudioPkey = 'album_studio_pkey'
}

/** input type for incrementing integer column in table "album_studio" */
export type AlbumStudioIncInput = {
  index?: Maybe<Scalars['Int']>;
};

/** input type for inserting data into table "album_studio" */
export type AlbumStudioInsertInput = {
  album?: Maybe<AlbumObjRelInsertInput>;
  album_id?: Maybe<Scalars['uuid']>;
  index?: Maybe<Scalars['Int']>;
  studio?: Maybe<StudioObjRelInsertInput>;
  studio_id?: Maybe<Scalars['uuid']>;
};

/** aggregate max on columns */
export type AlbumStudioMaxFields = {
  album_id?: Maybe<Scalars['uuid']>;
  index?: Maybe<Scalars['Int']>;
  studio_id?: Maybe<Scalars['uuid']>;
};

/** order by max() on columns of table "album_studio" */
export type AlbumStudioMaxOrderBy = {
  album_id?: Maybe<OrderBy>;
  index?: Maybe<OrderBy>;
  studio_id?: Maybe<OrderBy>;
};

/** aggregate min on columns */
export type AlbumStudioMinFields = {
  album_id?: Maybe<Scalars['uuid']>;
  index?: Maybe<Scalars['Int']>;
  studio_id?: Maybe<Scalars['uuid']>;
};

/** order by min() on columns of table "album_studio" */
export type AlbumStudioMinOrderBy = {
  album_id?: Maybe<OrderBy>;
  index?: Maybe<OrderBy>;
  studio_id?: Maybe<OrderBy>;
};

/** response of any mutation on the table "album_studio" */
export type AlbumStudioMutationResponse = {
  /** number of affected rows by the mutation */
  affected_rows: Scalars['Int'];
  /** data of the affected rows by the mutation */
  returning: Array<AlbumStudio>;
};

/** input type for inserting object relation for remote table "album_studio" */
export type AlbumStudioObjRelInsertInput = {
  data: AlbumStudioInsertInput;
  on_conflict?: Maybe<AlbumStudioOnConflict>;
};

/** on conflict condition type for table "album_studio" */
export type AlbumStudioOnConflict = {
  constraint: AlbumStudioConstraint;
  update_columns: Array<AlbumStudioUpdateColumn>;
  where?: Maybe<AlbumStudioBoolExp>;
};

/** ordering options when selecting data from "album_studio" */
export type AlbumStudioOrderBy = {
  album?: Maybe<AlbumOrderBy>;
  album_id?: Maybe<OrderBy>;
  index?: Maybe<OrderBy>;
  studio?: Maybe<StudioOrderBy>;
  studio_id?: Maybe<OrderBy>;
};

/** primary key columns input for table: "album_studio" */
export type AlbumStudioPkColumnsInput = {
  album_id: Scalars['uuid'];
  index: Scalars['Int'];
};

/** select columns of table "album_studio" */
export enum AlbumStudioSelectColumn {
  /** column name */
  AlbumId = 'album_id',
  /** column name */
  Index = 'index',
  /** column name */
  StudioId = 'studio_id'
}

/** input type for updating data in table "album_studio" */
export type AlbumStudioSetInput = {
  album_id?: Maybe<Scalars['uuid']>;
  index?: Maybe<Scalars['Int']>;
  studio_id?: Maybe<Scalars['uuid']>;
};

/** aggregate stddev on columns */
export type AlbumStudioStddevFields = {
  index?: Maybe<Scalars['Float']>;
};

/** order by stddev() on columns of table "album_studio" */
export type AlbumStudioStddevOrderBy = {
  index?: Maybe<OrderBy>;
};

/** aggregate stddev_pop on columns */
export type AlbumStudioStddevPopFields = {
  index?: Maybe<Scalars['Float']>;
};

/** order by stddev_pop() on columns of table "album_studio" */
export type AlbumStudioStddevPopOrderBy = {
  index?: Maybe<OrderBy>;
};

/** aggregate stddev_samp on columns */
export type AlbumStudioStddevSampFields = {
  index?: Maybe<Scalars['Float']>;
};

/** order by stddev_samp() on columns of table "album_studio" */
export type AlbumStudioStddevSampOrderBy = {
  index?: Maybe<OrderBy>;
};

/** aggregate sum on columns */
export type AlbumStudioSumFields = {
  index?: Maybe<Scalars['Int']>;
};

/** order by sum() on columns of table "album_studio" */
export type AlbumStudioSumOrderBy = {
  index?: Maybe<OrderBy>;
};

/** update columns of table "album_studio" */
export enum AlbumStudioUpdateColumn {
  /** column name */
  AlbumId = 'album_id',
  /** column name */
  Index = 'index',
  /** column name */
  StudioId = 'studio_id'
}

/** aggregate var_pop on columns */
export type AlbumStudioVarPopFields = {
  index?: Maybe<Scalars['Float']>;
};

/** order by var_pop() on columns of table "album_studio" */
export type AlbumStudioVarPopOrderBy = {
  index?: Maybe<OrderBy>;
};

/** aggregate var_samp on columns */
export type AlbumStudioVarSampFields = {
  index?: Maybe<Scalars['Float']>;
};

/** order by var_samp() on columns of table "album_studio" */
export type AlbumStudioVarSampOrderBy = {
  index?: Maybe<OrderBy>;
};

/** aggregate variance on columns */
export type AlbumStudioVarianceFields = {
  index?: Maybe<Scalars['Float']>;
};

/** order by variance() on columns of table "album_studio" */
export type AlbumStudioVarianceOrderBy = {
  index?: Maybe<OrderBy>;
};

/** columns and relationships of "album_style" */
export type AlbumStyle = {
  /** An object relationship */
  album: Album;
  album_id: Scalars['uuid'];
  index: Scalars['Int'];
  /** An object relationship */
  style: Style;
  style_id: Scalars['uuid'];
};

/** aggregated selection of "album_style" */
export type AlbumStyleAggregate = {
  aggregate?: Maybe<AlbumStyleAggregateFields>;
  nodes: Array<AlbumStyle>;
};

/** aggregate fields of "album_style" */
export type AlbumStyleAggregateFields = {
  avg?: Maybe<AlbumStyleAvgFields>;
  count?: Maybe<Scalars['Int']>;
  max?: Maybe<AlbumStyleMaxFields>;
  min?: Maybe<AlbumStyleMinFields>;
  stddev?: Maybe<AlbumStyleStddevFields>;
  stddev_pop?: Maybe<AlbumStyleStddevPopFields>;
  stddev_samp?: Maybe<AlbumStyleStddevSampFields>;
  sum?: Maybe<AlbumStyleSumFields>;
  var_pop?: Maybe<AlbumStyleVarPopFields>;
  var_samp?: Maybe<AlbumStyleVarSampFields>;
  variance?: Maybe<AlbumStyleVarianceFields>;
};


/** aggregate fields of "album_style" */
export type AlbumStyleAggregateFieldsCountArgs = {
  columns?: Maybe<Array<AlbumStyleSelectColumn>>;
  distinct?: Maybe<Scalars['Boolean']>;
};

/** order by aggregate values of table "album_style" */
export type AlbumStyleAggregateOrderBy = {
  avg?: Maybe<AlbumStyleAvgOrderBy>;
  count?: Maybe<OrderBy>;
  max?: Maybe<AlbumStyleMaxOrderBy>;
  min?: Maybe<AlbumStyleMinOrderBy>;
  stddev?: Maybe<AlbumStyleStddevOrderBy>;
  stddev_pop?: Maybe<AlbumStyleStddevPopOrderBy>;
  stddev_samp?: Maybe<AlbumStyleStddevSampOrderBy>;
  sum?: Maybe<AlbumStyleSumOrderBy>;
  var_pop?: Maybe<AlbumStyleVarPopOrderBy>;
  var_samp?: Maybe<AlbumStyleVarSampOrderBy>;
  variance?: Maybe<AlbumStyleVarianceOrderBy>;
};

/** input type for inserting array relation for remote table "album_style" */
export type AlbumStyleArrRelInsertInput = {
  data: Array<AlbumStyleInsertInput>;
  on_conflict?: Maybe<AlbumStyleOnConflict>;
};

/** aggregate avg on columns */
export type AlbumStyleAvgFields = {
  index?: Maybe<Scalars['Float']>;
};

/** order by avg() on columns of table "album_style" */
export type AlbumStyleAvgOrderBy = {
  index?: Maybe<OrderBy>;
};

/** Boolean expression to filter rows from the table "album_style". All fields are combined with a logical 'AND'. */
export type AlbumStyleBoolExp = {
  _and?: Maybe<Array<Maybe<AlbumStyleBoolExp>>>;
  _not?: Maybe<AlbumStyleBoolExp>;
  _or?: Maybe<Array<Maybe<AlbumStyleBoolExp>>>;
  album?: Maybe<AlbumBoolExp>;
  album_id?: Maybe<UuidComparisonExp>;
  index?: Maybe<IntComparisonExp>;
  style?: Maybe<StyleBoolExp>;
  style_id?: Maybe<UuidComparisonExp>;
};

/** unique or primary key constraints on table "album_style" */
export enum AlbumStyleConstraint {
  /** unique or primary key constraint */
  AlbumStyleAlbumIdStyleIdKey = 'album_style_album_id_style_id_key',
  /** unique or primary key constraint */
  AlbumStylePkey = 'album_style_pkey'
}

/** input type for incrementing integer column in table "album_style" */
export type AlbumStyleIncInput = {
  index?: Maybe<Scalars['Int']>;
};

/** input type for inserting data into table "album_style" */
export type AlbumStyleInsertInput = {
  album?: Maybe<AlbumObjRelInsertInput>;
  album_id?: Maybe<Scalars['uuid']>;
  index?: Maybe<Scalars['Int']>;
  style?: Maybe<StyleObjRelInsertInput>;
  style_id?: Maybe<Scalars['uuid']>;
};

/** aggregate max on columns */
export type AlbumStyleMaxFields = {
  album_id?: Maybe<Scalars['uuid']>;
  index?: Maybe<Scalars['Int']>;
  style_id?: Maybe<Scalars['uuid']>;
};

/** order by max() on columns of table "album_style" */
export type AlbumStyleMaxOrderBy = {
  album_id?: Maybe<OrderBy>;
  index?: Maybe<OrderBy>;
  style_id?: Maybe<OrderBy>;
};

/** aggregate min on columns */
export type AlbumStyleMinFields = {
  album_id?: Maybe<Scalars['uuid']>;
  index?: Maybe<Scalars['Int']>;
  style_id?: Maybe<Scalars['uuid']>;
};

/** order by min() on columns of table "album_style" */
export type AlbumStyleMinOrderBy = {
  album_id?: Maybe<OrderBy>;
  index?: Maybe<OrderBy>;
  style_id?: Maybe<OrderBy>;
};

/** response of any mutation on the table "album_style" */
export type AlbumStyleMutationResponse = {
  /** number of affected rows by the mutation */
  affected_rows: Scalars['Int'];
  /** data of the affected rows by the mutation */
  returning: Array<AlbumStyle>;
};

/** input type for inserting object relation for remote table "album_style" */
export type AlbumStyleObjRelInsertInput = {
  data: AlbumStyleInsertInput;
  on_conflict?: Maybe<AlbumStyleOnConflict>;
};

/** on conflict condition type for table "album_style" */
export type AlbumStyleOnConflict = {
  constraint: AlbumStyleConstraint;
  update_columns: Array<AlbumStyleUpdateColumn>;
  where?: Maybe<AlbumStyleBoolExp>;
};

/** ordering options when selecting data from "album_style" */
export type AlbumStyleOrderBy = {
  album?: Maybe<AlbumOrderBy>;
  album_id?: Maybe<OrderBy>;
  index?: Maybe<OrderBy>;
  style?: Maybe<StyleOrderBy>;
  style_id?: Maybe<OrderBy>;
};

/** primary key columns input for table: "album_style" */
export type AlbumStylePkColumnsInput = {
  album_id: Scalars['uuid'];
  index: Scalars['Int'];
};

/** select columns of table "album_style" */
export enum AlbumStyleSelectColumn {
  /** column name */
  AlbumId = 'album_id',
  /** column name */
  Index = 'index',
  /** column name */
  StyleId = 'style_id'
}

/** input type for updating data in table "album_style" */
export type AlbumStyleSetInput = {
  album_id?: Maybe<Scalars['uuid']>;
  index?: Maybe<Scalars['Int']>;
  style_id?: Maybe<Scalars['uuid']>;
};

/** aggregate stddev on columns */
export type AlbumStyleStddevFields = {
  index?: Maybe<Scalars['Float']>;
};

/** order by stddev() on columns of table "album_style" */
export type AlbumStyleStddevOrderBy = {
  index?: Maybe<OrderBy>;
};

/** aggregate stddev_pop on columns */
export type AlbumStyleStddevPopFields = {
  index?: Maybe<Scalars['Float']>;
};

/** order by stddev_pop() on columns of table "album_style" */
export type AlbumStyleStddevPopOrderBy = {
  index?: Maybe<OrderBy>;
};

/** aggregate stddev_samp on columns */
export type AlbumStyleStddevSampFields = {
  index?: Maybe<Scalars['Float']>;
};

/** order by stddev_samp() on columns of table "album_style" */
export type AlbumStyleStddevSampOrderBy = {
  index?: Maybe<OrderBy>;
};

/** aggregate sum on columns */
export type AlbumStyleSumFields = {
  index?: Maybe<Scalars['Int']>;
};

/** order by sum() on columns of table "album_style" */
export type AlbumStyleSumOrderBy = {
  index?: Maybe<OrderBy>;
};

/** update columns of table "album_style" */
export enum AlbumStyleUpdateColumn {
  /** column name */
  AlbumId = 'album_id',
  /** column name */
  Index = 'index',
  /** column name */
  StyleId = 'style_id'
}

/** aggregate var_pop on columns */
export type AlbumStyleVarPopFields = {
  index?: Maybe<Scalars['Float']>;
};

/** order by var_pop() on columns of table "album_style" */
export type AlbumStyleVarPopOrderBy = {
  index?: Maybe<OrderBy>;
};

/** aggregate var_samp on columns */
export type AlbumStyleVarSampFields = {
  index?: Maybe<Scalars['Float']>;
};

/** order by var_samp() on columns of table "album_style" */
export type AlbumStyleVarSampOrderBy = {
  index?: Maybe<OrderBy>;
};

/** aggregate variance on columns */
export type AlbumStyleVarianceFields = {
  index?: Maybe<Scalars['Float']>;
};

/** order by variance() on columns of table "album_style" */
export type AlbumStyleVarianceOrderBy = {
  index?: Maybe<OrderBy>;
};

/** aggregate sum on columns */
export type AlbumSumFields = {
  rating?: Maybe<Scalars['numeric']>;
};

/** order by sum() on columns of table "album" */
export type AlbumSumOrderBy = {
  rating?: Maybe<OrderBy>;
};

/** columns and relationships of "album_track" */
export type AlbumTrack = {
  /** An object relationship */
  album: Album;
  album_id: Scalars['uuid'];
  disk: Scalars['Int'];
  index: Scalars['Int'];
  /** An object relationship */
  track: Track;
  track_id: Scalars['uuid'];
};

/** aggregated selection of "album_track" */
export type AlbumTrackAggregate = {
  aggregate?: Maybe<AlbumTrackAggregateFields>;
  nodes: Array<AlbumTrack>;
};

/** aggregate fields of "album_track" */
export type AlbumTrackAggregateFields = {
  avg?: Maybe<AlbumTrackAvgFields>;
  count?: Maybe<Scalars['Int']>;
  max?: Maybe<AlbumTrackMaxFields>;
  min?: Maybe<AlbumTrackMinFields>;
  stddev?: Maybe<AlbumTrackStddevFields>;
  stddev_pop?: Maybe<AlbumTrackStddevPopFields>;
  stddev_samp?: Maybe<AlbumTrackStddevSampFields>;
  sum?: Maybe<AlbumTrackSumFields>;
  var_pop?: Maybe<AlbumTrackVarPopFields>;
  var_samp?: Maybe<AlbumTrackVarSampFields>;
  variance?: Maybe<AlbumTrackVarianceFields>;
};


/** aggregate fields of "album_track" */
export type AlbumTrackAggregateFieldsCountArgs = {
  columns?: Maybe<Array<AlbumTrackSelectColumn>>;
  distinct?: Maybe<Scalars['Boolean']>;
};

/** order by aggregate values of table "album_track" */
export type AlbumTrackAggregateOrderBy = {
  avg?: Maybe<AlbumTrackAvgOrderBy>;
  count?: Maybe<OrderBy>;
  max?: Maybe<AlbumTrackMaxOrderBy>;
  min?: Maybe<AlbumTrackMinOrderBy>;
  stddev?: Maybe<AlbumTrackStddevOrderBy>;
  stddev_pop?: Maybe<AlbumTrackStddevPopOrderBy>;
  stddev_samp?: Maybe<AlbumTrackStddevSampOrderBy>;
  sum?: Maybe<AlbumTrackSumOrderBy>;
  var_pop?: Maybe<AlbumTrackVarPopOrderBy>;
  var_samp?: Maybe<AlbumTrackVarSampOrderBy>;
  variance?: Maybe<AlbumTrackVarianceOrderBy>;
};

/** input type for inserting array relation for remote table "album_track" */
export type AlbumTrackArrRelInsertInput = {
  data: Array<AlbumTrackInsertInput>;
  on_conflict?: Maybe<AlbumTrackOnConflict>;
};

/** aggregate avg on columns */
export type AlbumTrackAvgFields = {
  disk?: Maybe<Scalars['Float']>;
  index?: Maybe<Scalars['Float']>;
};

/** order by avg() on columns of table "album_track" */
export type AlbumTrackAvgOrderBy = {
  disk?: Maybe<OrderBy>;
  index?: Maybe<OrderBy>;
};

/** Boolean expression to filter rows from the table "album_track". All fields are combined with a logical 'AND'. */
export type AlbumTrackBoolExp = {
  _and?: Maybe<Array<Maybe<AlbumTrackBoolExp>>>;
  _not?: Maybe<AlbumTrackBoolExp>;
  _or?: Maybe<Array<Maybe<AlbumTrackBoolExp>>>;
  album?: Maybe<AlbumBoolExp>;
  album_id?: Maybe<UuidComparisonExp>;
  disk?: Maybe<IntComparisonExp>;
  index?: Maybe<IntComparisonExp>;
  track?: Maybe<TrackBoolExp>;
  track_id?: Maybe<UuidComparisonExp>;
};

/** unique or primary key constraints on table "album_track" */
export enum AlbumTrackConstraint {
  /** unique or primary key constraint */
  AlbumTrackAlbumIdTrackIdKey = 'album_track_album_id_track_id_key',
  /** unique or primary key constraint */
  AlbumTrackPkey = 'album_track_pkey'
}

/** input type for incrementing integer column in table "album_track" */
export type AlbumTrackIncInput = {
  disk?: Maybe<Scalars['Int']>;
  index?: Maybe<Scalars['Int']>;
};

/** input type for inserting data into table "album_track" */
export type AlbumTrackInsertInput = {
  album?: Maybe<AlbumObjRelInsertInput>;
  album_id?: Maybe<Scalars['uuid']>;
  disk?: Maybe<Scalars['Int']>;
  index?: Maybe<Scalars['Int']>;
  track?: Maybe<TrackObjRelInsertInput>;
  track_id?: Maybe<Scalars['uuid']>;
};

/** aggregate max on columns */
export type AlbumTrackMaxFields = {
  album_id?: Maybe<Scalars['uuid']>;
  disk?: Maybe<Scalars['Int']>;
  index?: Maybe<Scalars['Int']>;
  track_id?: Maybe<Scalars['uuid']>;
};

/** order by max() on columns of table "album_track" */
export type AlbumTrackMaxOrderBy = {
  album_id?: Maybe<OrderBy>;
  disk?: Maybe<OrderBy>;
  index?: Maybe<OrderBy>;
  track_id?: Maybe<OrderBy>;
};

/** aggregate min on columns */
export type AlbumTrackMinFields = {
  album_id?: Maybe<Scalars['uuid']>;
  disk?: Maybe<Scalars['Int']>;
  index?: Maybe<Scalars['Int']>;
  track_id?: Maybe<Scalars['uuid']>;
};

/** order by min() on columns of table "album_track" */
export type AlbumTrackMinOrderBy = {
  album_id?: Maybe<OrderBy>;
  disk?: Maybe<OrderBy>;
  index?: Maybe<OrderBy>;
  track_id?: Maybe<OrderBy>;
};

/** response of any mutation on the table "album_track" */
export type AlbumTrackMutationResponse = {
  /** number of affected rows by the mutation */
  affected_rows: Scalars['Int'];
  /** data of the affected rows by the mutation */
  returning: Array<AlbumTrack>;
};

/** input type for inserting object relation for remote table "album_track" */
export type AlbumTrackObjRelInsertInput = {
  data: AlbumTrackInsertInput;
  on_conflict?: Maybe<AlbumTrackOnConflict>;
};

/** on conflict condition type for table "album_track" */
export type AlbumTrackOnConflict = {
  constraint: AlbumTrackConstraint;
  update_columns: Array<AlbumTrackUpdateColumn>;
  where?: Maybe<AlbumTrackBoolExp>;
};

/** ordering options when selecting data from "album_track" */
export type AlbumTrackOrderBy = {
  album?: Maybe<AlbumOrderBy>;
  album_id?: Maybe<OrderBy>;
  disk?: Maybe<OrderBy>;
  index?: Maybe<OrderBy>;
  track?: Maybe<TrackOrderBy>;
  track_id?: Maybe<OrderBy>;
};

/** primary key columns input for table: "album_track" */
export type AlbumTrackPkColumnsInput = {
  disk: Scalars['Int'];
  index: Scalars['Int'];
  track_id: Scalars['uuid'];
};

/** select columns of table "album_track" */
export enum AlbumTrackSelectColumn {
  /** column name */
  AlbumId = 'album_id',
  /** column name */
  Disk = 'disk',
  /** column name */
  Index = 'index',
  /** column name */
  TrackId = 'track_id'
}

/** input type for updating data in table "album_track" */
export type AlbumTrackSetInput = {
  album_id?: Maybe<Scalars['uuid']>;
  disk?: Maybe<Scalars['Int']>;
  index?: Maybe<Scalars['Int']>;
  track_id?: Maybe<Scalars['uuid']>;
};

/** aggregate stddev on columns */
export type AlbumTrackStddevFields = {
  disk?: Maybe<Scalars['Float']>;
  index?: Maybe<Scalars['Float']>;
};

/** order by stddev() on columns of table "album_track" */
export type AlbumTrackStddevOrderBy = {
  disk?: Maybe<OrderBy>;
  index?: Maybe<OrderBy>;
};

/** aggregate stddev_pop on columns */
export type AlbumTrackStddevPopFields = {
  disk?: Maybe<Scalars['Float']>;
  index?: Maybe<Scalars['Float']>;
};

/** order by stddev_pop() on columns of table "album_track" */
export type AlbumTrackStddevPopOrderBy = {
  disk?: Maybe<OrderBy>;
  index?: Maybe<OrderBy>;
};

/** aggregate stddev_samp on columns */
export type AlbumTrackStddevSampFields = {
  disk?: Maybe<Scalars['Float']>;
  index?: Maybe<Scalars['Float']>;
};

/** order by stddev_samp() on columns of table "album_track" */
export type AlbumTrackStddevSampOrderBy = {
  disk?: Maybe<OrderBy>;
  index?: Maybe<OrderBy>;
};

/** aggregate sum on columns */
export type AlbumTrackSumFields = {
  disk?: Maybe<Scalars['Int']>;
  index?: Maybe<Scalars['Int']>;
};

/** order by sum() on columns of table "album_track" */
export type AlbumTrackSumOrderBy = {
  disk?: Maybe<OrderBy>;
  index?: Maybe<OrderBy>;
};

/** update columns of table "album_track" */
export enum AlbumTrackUpdateColumn {
  /** column name */
  AlbumId = 'album_id',
  /** column name */
  Disk = 'disk',
  /** column name */
  Index = 'index',
  /** column name */
  TrackId = 'track_id'
}

/** aggregate var_pop on columns */
export type AlbumTrackVarPopFields = {
  disk?: Maybe<Scalars['Float']>;
  index?: Maybe<Scalars['Float']>;
};

/** order by var_pop() on columns of table "album_track" */
export type AlbumTrackVarPopOrderBy = {
  disk?: Maybe<OrderBy>;
  index?: Maybe<OrderBy>;
};

/** aggregate var_samp on columns */
export type AlbumTrackVarSampFields = {
  disk?: Maybe<Scalars['Float']>;
  index?: Maybe<Scalars['Float']>;
};

/** order by var_samp() on columns of table "album_track" */
export type AlbumTrackVarSampOrderBy = {
  disk?: Maybe<OrderBy>;
  index?: Maybe<OrderBy>;
};

/** aggregate variance on columns */
export type AlbumTrackVarianceFields = {
  disk?: Maybe<Scalars['Float']>;
  index?: Maybe<Scalars['Float']>;
};

/** order by variance() on columns of table "album_track" */
export type AlbumTrackVarianceOrderBy = {
  disk?: Maybe<OrderBy>;
  index?: Maybe<OrderBy>;
};

/** update columns of table "album" */
export enum AlbumUpdateColumn {
  /** column name */
  Aired = 'aired',
  /** column name */
  ArtistId = 'artist_id',
  /** column name */
  Id = 'id',
  /** column name */
  ImageBackground = 'image_background',
  /** column name */
  ImageCover = 'image_cover',
  /** column name */
  Name = 'name',
  /** column name */
  NameMatch = 'name_match',
  /** column name */
  NameSort = 'name_sort',
  /** column name */
  Rating = 'rating',
  /** column name */
  Summary = 'summary'
}

/** aggregate var_pop on columns */
export type AlbumVarPopFields = {
  rating?: Maybe<Scalars['Float']>;
};

/** order by var_pop() on columns of table "album" */
export type AlbumVarPopOrderBy = {
  rating?: Maybe<OrderBy>;
};

/** aggregate var_samp on columns */
export type AlbumVarSampFields = {
  rating?: Maybe<Scalars['Float']>;
};

/** order by var_samp() on columns of table "album" */
export type AlbumVarSampOrderBy = {
  rating?: Maybe<OrderBy>;
};

/** aggregate variance on columns */
export type AlbumVarianceFields = {
  rating?: Maybe<Scalars['Float']>;
};

/** order by variance() on columns of table "album" */
export type AlbumVarianceOrderBy = {
  rating?: Maybe<OrderBy>;
};

/** columns and relationships of "artist" */
export type Artist = {
  /** An array relationship */
  albums: Array<Album>;
  /** An aggregated array relationship */
  albums_aggregate: AlbumAggregate;
  /** An array relationship */
  artistSimilarsByArtist2Id: Array<ArtistSimilar>;
  /** An aggregated array relationship */
  artistSimilarsByArtist2Id_aggregate: ArtistSimilarAggregate;
  /** An array relationship */
  artist_collections: Array<ArtistCollection>;
  /** An aggregated array relationship */
  artist_collections_aggregate: ArtistCollectionAggregate;
  /** An array relationship */
  artist_countries: Array<ArtistCountry>;
  /** An aggregated array relationship */
  artist_countries_aggregate: ArtistCountryAggregate;
  /** An array relationship */
  artist_genres: Array<ArtistGenre>;
  /** An aggregated array relationship */
  artist_genres_aggregate: ArtistGenreAggregate;
  /** An array relationship */
  artist_moods: Array<ArtistMood>;
  /** An aggregated array relationship */
  artist_moods_aggregate: ArtistMoodAggregate;
  /** An array relationship */
  artist_similars: Array<ArtistSimilar>;
  /** An aggregated array relationship */
  artist_similars_aggregate: ArtistSimilarAggregate;
  /** An array relationship */
  artist_styles: Array<ArtistStyle>;
  /** An aggregated array relationship */
  artist_styles_aggregate: ArtistStyleAggregate;
  id: Scalars['uuid'];
  image_background?: Maybe<Scalars['String']>;
  image_cover?: Maybe<Scalars['String']>;
  /** An object relationship */
  person: Person;
  person_id: Scalars['uuid'];
  summary: Scalars['String'];
};


/** columns and relationships of "artist" */
export type ArtistAlbumsArgs = {
  distinct_on?: Maybe<Array<AlbumSelectColumn>>;
  limit?: Maybe<Scalars['Int']>;
  offset?: Maybe<Scalars['Int']>;
  order_by?: Maybe<Array<AlbumOrderBy>>;
  where?: Maybe<AlbumBoolExp>;
};


/** columns and relationships of "artist" */
export type ArtistAlbumsAggregateArgs = {
  distinct_on?: Maybe<Array<AlbumSelectColumn>>;
  limit?: Maybe<Scalars['Int']>;
  offset?: Maybe<Scalars['Int']>;
  order_by?: Maybe<Array<AlbumOrderBy>>;
  where?: Maybe<AlbumBoolExp>;
};


/** columns and relationships of "artist" */
export type ArtistArtistSimilarsByArtist2IdArgs = {
  distinct_on?: Maybe<Array<ArtistSimilarSelectColumn>>;
  limit?: Maybe<Scalars['Int']>;
  offset?: Maybe<Scalars['Int']>;
  order_by?: Maybe<Array<ArtistSimilarOrderBy>>;
  where?: Maybe<ArtistSimilarBoolExp>;
};


/** columns and relationships of "artist" */
export type ArtistArtistSimilarsByArtist2IdAggregateArgs = {
  distinct_on?: Maybe<Array<ArtistSimilarSelectColumn>>;
  limit?: Maybe<Scalars['Int']>;
  offset?: Maybe<Scalars['Int']>;
  order_by?: Maybe<Array<ArtistSimilarOrderBy>>;
  where?: Maybe<ArtistSimilarBoolExp>;
};


/** columns and relationships of "artist" */
export type ArtistArtistCollectionsArgs = {
  distinct_on?: Maybe<Array<ArtistCollectionSelectColumn>>;
  limit?: Maybe<Scalars['Int']>;
  offset?: Maybe<Scalars['Int']>;
  order_by?: Maybe<Array<ArtistCollectionOrderBy>>;
  where?: Maybe<ArtistCollectionBoolExp>;
};


/** columns and relationships of "artist" */
export type ArtistArtistCollectionsAggregateArgs = {
  distinct_on?: Maybe<Array<ArtistCollectionSelectColumn>>;
  limit?: Maybe<Scalars['Int']>;
  offset?: Maybe<Scalars['Int']>;
  order_by?: Maybe<Array<ArtistCollectionOrderBy>>;
  where?: Maybe<ArtistCollectionBoolExp>;
};


/** columns and relationships of "artist" */
export type ArtistArtistCountriesArgs = {
  distinct_on?: Maybe<Array<ArtistCountrySelectColumn>>;
  limit?: Maybe<Scalars['Int']>;
  offset?: Maybe<Scalars['Int']>;
  order_by?: Maybe<Array<ArtistCountryOrderBy>>;
  where?: Maybe<ArtistCountryBoolExp>;
};


/** columns and relationships of "artist" */
export type ArtistArtistCountriesAggregateArgs = {
  distinct_on?: Maybe<Array<ArtistCountrySelectColumn>>;
  limit?: Maybe<Scalars['Int']>;
  offset?: Maybe<Scalars['Int']>;
  order_by?: Maybe<Array<ArtistCountryOrderBy>>;
  where?: Maybe<ArtistCountryBoolExp>;
};


/** columns and relationships of "artist" */
export type ArtistArtistGenresArgs = {
  distinct_on?: Maybe<Array<ArtistGenreSelectColumn>>;
  limit?: Maybe<Scalars['Int']>;
  offset?: Maybe<Scalars['Int']>;
  order_by?: Maybe<Array<ArtistGenreOrderBy>>;
  where?: Maybe<ArtistGenreBoolExp>;
};


/** columns and relationships of "artist" */
export type ArtistArtistGenresAggregateArgs = {
  distinct_on?: Maybe<Array<ArtistGenreSelectColumn>>;
  limit?: Maybe<Scalars['Int']>;
  offset?: Maybe<Scalars['Int']>;
  order_by?: Maybe<Array<ArtistGenreOrderBy>>;
  where?: Maybe<ArtistGenreBoolExp>;
};


/** columns and relationships of "artist" */
export type ArtistArtistMoodsArgs = {
  distinct_on?: Maybe<Array<ArtistMoodSelectColumn>>;
  limit?: Maybe<Scalars['Int']>;
  offset?: Maybe<Scalars['Int']>;
  order_by?: Maybe<Array<ArtistMoodOrderBy>>;
  where?: Maybe<ArtistMoodBoolExp>;
};


/** columns and relationships of "artist" */
export type ArtistArtistMoodsAggregateArgs = {
  distinct_on?: Maybe<Array<ArtistMoodSelectColumn>>;
  limit?: Maybe<Scalars['Int']>;
  offset?: Maybe<Scalars['Int']>;
  order_by?: Maybe<Array<ArtistMoodOrderBy>>;
  where?: Maybe<ArtistMoodBoolExp>;
};


/** columns and relationships of "artist" */
export type ArtistArtistSimilarsArgs = {
  distinct_on?: Maybe<Array<ArtistSimilarSelectColumn>>;
  limit?: Maybe<Scalars['Int']>;
  offset?: Maybe<Scalars['Int']>;
  order_by?: Maybe<Array<ArtistSimilarOrderBy>>;
  where?: Maybe<ArtistSimilarBoolExp>;
};


/** columns and relationships of "artist" */
export type ArtistArtistSimilarsAggregateArgs = {
  distinct_on?: Maybe<Array<ArtistSimilarSelectColumn>>;
  limit?: Maybe<Scalars['Int']>;
  offset?: Maybe<Scalars['Int']>;
  order_by?: Maybe<Array<ArtistSimilarOrderBy>>;
  where?: Maybe<ArtistSimilarBoolExp>;
};


/** columns and relationships of "artist" */
export type ArtistArtistStylesArgs = {
  distinct_on?: Maybe<Array<ArtistStyleSelectColumn>>;
  limit?: Maybe<Scalars['Int']>;
  offset?: Maybe<Scalars['Int']>;
  order_by?: Maybe<Array<ArtistStyleOrderBy>>;
  where?: Maybe<ArtistStyleBoolExp>;
};


/** columns and relationships of "artist" */
export type ArtistArtistStylesAggregateArgs = {
  distinct_on?: Maybe<Array<ArtistStyleSelectColumn>>;
  limit?: Maybe<Scalars['Int']>;
  offset?: Maybe<Scalars['Int']>;
  order_by?: Maybe<Array<ArtistStyleOrderBy>>;
  where?: Maybe<ArtistStyleBoolExp>;
};

/** aggregated selection of "artist" */
export type ArtistAggregate = {
  aggregate?: Maybe<ArtistAggregateFields>;
  nodes: Array<Artist>;
};

/** aggregate fields of "artist" */
export type ArtistAggregateFields = {
  count?: Maybe<Scalars['Int']>;
  max?: Maybe<ArtistMaxFields>;
  min?: Maybe<ArtistMinFields>;
};


/** aggregate fields of "artist" */
export type ArtistAggregateFieldsCountArgs = {
  columns?: Maybe<Array<ArtistSelectColumn>>;
  distinct?: Maybe<Scalars['Boolean']>;
};

/** order by aggregate values of table "artist" */
export type ArtistAggregateOrderBy = {
  count?: Maybe<OrderBy>;
  max?: Maybe<ArtistMaxOrderBy>;
  min?: Maybe<ArtistMinOrderBy>;
};

/** input type for inserting array relation for remote table "artist" */
export type ArtistArrRelInsertInput = {
  data: Array<ArtistInsertInput>;
  on_conflict?: Maybe<ArtistOnConflict>;
};

/** Boolean expression to filter rows from the table "artist". All fields are combined with a logical 'AND'. */
export type ArtistBoolExp = {
  _and?: Maybe<Array<Maybe<ArtistBoolExp>>>;
  _not?: Maybe<ArtistBoolExp>;
  _or?: Maybe<Array<Maybe<ArtistBoolExp>>>;
  albums?: Maybe<AlbumBoolExp>;
  artistSimilarsByArtist2Id?: Maybe<ArtistSimilarBoolExp>;
  artist_collections?: Maybe<ArtistCollectionBoolExp>;
  artist_countries?: Maybe<ArtistCountryBoolExp>;
  artist_genres?: Maybe<ArtistGenreBoolExp>;
  artist_moods?: Maybe<ArtistMoodBoolExp>;
  artist_similars?: Maybe<ArtistSimilarBoolExp>;
  artist_styles?: Maybe<ArtistStyleBoolExp>;
  id?: Maybe<UuidComparisonExp>;
  image_background?: Maybe<StringComparisonExp>;
  image_cover?: Maybe<StringComparisonExp>;
  person?: Maybe<PersonBoolExp>;
  person_id?: Maybe<UuidComparisonExp>;
  summary?: Maybe<StringComparisonExp>;
};

/** columns and relationships of "artist_collection" */
export type ArtistCollection = {
  /** An object relationship */
  artist: Artist;
  artist_id: Scalars['uuid'];
  /** An object relationship */
  collection: Collection;
  collection_id: Scalars['uuid'];
  index: Scalars['Int'];
};

/** aggregated selection of "artist_collection" */
export type ArtistCollectionAggregate = {
  aggregate?: Maybe<ArtistCollectionAggregateFields>;
  nodes: Array<ArtistCollection>;
};

/** aggregate fields of "artist_collection" */
export type ArtistCollectionAggregateFields = {
  avg?: Maybe<ArtistCollectionAvgFields>;
  count?: Maybe<Scalars['Int']>;
  max?: Maybe<ArtistCollectionMaxFields>;
  min?: Maybe<ArtistCollectionMinFields>;
  stddev?: Maybe<ArtistCollectionStddevFields>;
  stddev_pop?: Maybe<ArtistCollectionStddevPopFields>;
  stddev_samp?: Maybe<ArtistCollectionStddevSampFields>;
  sum?: Maybe<ArtistCollectionSumFields>;
  var_pop?: Maybe<ArtistCollectionVarPopFields>;
  var_samp?: Maybe<ArtistCollectionVarSampFields>;
  variance?: Maybe<ArtistCollectionVarianceFields>;
};


/** aggregate fields of "artist_collection" */
export type ArtistCollectionAggregateFieldsCountArgs = {
  columns?: Maybe<Array<ArtistCollectionSelectColumn>>;
  distinct?: Maybe<Scalars['Boolean']>;
};

/** order by aggregate values of table "artist_collection" */
export type ArtistCollectionAggregateOrderBy = {
  avg?: Maybe<ArtistCollectionAvgOrderBy>;
  count?: Maybe<OrderBy>;
  max?: Maybe<ArtistCollectionMaxOrderBy>;
  min?: Maybe<ArtistCollectionMinOrderBy>;
  stddev?: Maybe<ArtistCollectionStddevOrderBy>;
  stddev_pop?: Maybe<ArtistCollectionStddevPopOrderBy>;
  stddev_samp?: Maybe<ArtistCollectionStddevSampOrderBy>;
  sum?: Maybe<ArtistCollectionSumOrderBy>;
  var_pop?: Maybe<ArtistCollectionVarPopOrderBy>;
  var_samp?: Maybe<ArtistCollectionVarSampOrderBy>;
  variance?: Maybe<ArtistCollectionVarianceOrderBy>;
};

/** input type for inserting array relation for remote table "artist_collection" */
export type ArtistCollectionArrRelInsertInput = {
  data: Array<ArtistCollectionInsertInput>;
  on_conflict?: Maybe<ArtistCollectionOnConflict>;
};

/** aggregate avg on columns */
export type ArtistCollectionAvgFields = {
  index?: Maybe<Scalars['Float']>;
};

/** order by avg() on columns of table "artist_collection" */
export type ArtistCollectionAvgOrderBy = {
  index?: Maybe<OrderBy>;
};

/** Boolean expression to filter rows from the table "artist_collection". All fields are combined with a logical 'AND'. */
export type ArtistCollectionBoolExp = {
  _and?: Maybe<Array<Maybe<ArtistCollectionBoolExp>>>;
  _not?: Maybe<ArtistCollectionBoolExp>;
  _or?: Maybe<Array<Maybe<ArtistCollectionBoolExp>>>;
  artist?: Maybe<ArtistBoolExp>;
  artist_id?: Maybe<UuidComparisonExp>;
  collection?: Maybe<CollectionBoolExp>;
  collection_id?: Maybe<UuidComparisonExp>;
  index?: Maybe<IntComparisonExp>;
};

/** unique or primary key constraints on table "artist_collection" */
export enum ArtistCollectionConstraint {
  /** unique or primary key constraint */
  ArtistCollectionArtistIdCollectionIdKey = 'artist_collection_artist_id_collection_id_key',
  /** unique or primary key constraint */
  ArtistCollectionPkey = 'artist_collection_pkey'
}

/** input type for incrementing integer column in table "artist_collection" */
export type ArtistCollectionIncInput = {
  index?: Maybe<Scalars['Int']>;
};

/** input type for inserting data into table "artist_collection" */
export type ArtistCollectionInsertInput = {
  artist?: Maybe<ArtistObjRelInsertInput>;
  artist_id?: Maybe<Scalars['uuid']>;
  collection?: Maybe<CollectionObjRelInsertInput>;
  collection_id?: Maybe<Scalars['uuid']>;
  index?: Maybe<Scalars['Int']>;
};

/** aggregate max on columns */
export type ArtistCollectionMaxFields = {
  artist_id?: Maybe<Scalars['uuid']>;
  collection_id?: Maybe<Scalars['uuid']>;
  index?: Maybe<Scalars['Int']>;
};

/** order by max() on columns of table "artist_collection" */
export type ArtistCollectionMaxOrderBy = {
  artist_id?: Maybe<OrderBy>;
  collection_id?: Maybe<OrderBy>;
  index?: Maybe<OrderBy>;
};

/** aggregate min on columns */
export type ArtistCollectionMinFields = {
  artist_id?: Maybe<Scalars['uuid']>;
  collection_id?: Maybe<Scalars['uuid']>;
  index?: Maybe<Scalars['Int']>;
};

/** order by min() on columns of table "artist_collection" */
export type ArtistCollectionMinOrderBy = {
  artist_id?: Maybe<OrderBy>;
  collection_id?: Maybe<OrderBy>;
  index?: Maybe<OrderBy>;
};

/** response of any mutation on the table "artist_collection" */
export type ArtistCollectionMutationResponse = {
  /** number of affected rows by the mutation */
  affected_rows: Scalars['Int'];
  /** data of the affected rows by the mutation */
  returning: Array<ArtistCollection>;
};

/** input type for inserting object relation for remote table "artist_collection" */
export type ArtistCollectionObjRelInsertInput = {
  data: ArtistCollectionInsertInput;
  on_conflict?: Maybe<ArtistCollectionOnConflict>;
};

/** on conflict condition type for table "artist_collection" */
export type ArtistCollectionOnConflict = {
  constraint: ArtistCollectionConstraint;
  update_columns: Array<ArtistCollectionUpdateColumn>;
  where?: Maybe<ArtistCollectionBoolExp>;
};

/** ordering options when selecting data from "artist_collection" */
export type ArtistCollectionOrderBy = {
  artist?: Maybe<ArtistOrderBy>;
  artist_id?: Maybe<OrderBy>;
  collection?: Maybe<CollectionOrderBy>;
  collection_id?: Maybe<OrderBy>;
  index?: Maybe<OrderBy>;
};

/** primary key columns input for table: "artist_collection" */
export type ArtistCollectionPkColumnsInput = {
  artist_id: Scalars['uuid'];
  index: Scalars['Int'];
};

/** select columns of table "artist_collection" */
export enum ArtistCollectionSelectColumn {
  /** column name */
  ArtistId = 'artist_id',
  /** column name */
  CollectionId = 'collection_id',
  /** column name */
  Index = 'index'
}

/** input type for updating data in table "artist_collection" */
export type ArtistCollectionSetInput = {
  artist_id?: Maybe<Scalars['uuid']>;
  collection_id?: Maybe<Scalars['uuid']>;
  index?: Maybe<Scalars['Int']>;
};

/** aggregate stddev on columns */
export type ArtistCollectionStddevFields = {
  index?: Maybe<Scalars['Float']>;
};

/** order by stddev() on columns of table "artist_collection" */
export type ArtistCollectionStddevOrderBy = {
  index?: Maybe<OrderBy>;
};

/** aggregate stddev_pop on columns */
export type ArtistCollectionStddevPopFields = {
  index?: Maybe<Scalars['Float']>;
};

/** order by stddev_pop() on columns of table "artist_collection" */
export type ArtistCollectionStddevPopOrderBy = {
  index?: Maybe<OrderBy>;
};

/** aggregate stddev_samp on columns */
export type ArtistCollectionStddevSampFields = {
  index?: Maybe<Scalars['Float']>;
};

/** order by stddev_samp() on columns of table "artist_collection" */
export type ArtistCollectionStddevSampOrderBy = {
  index?: Maybe<OrderBy>;
};

/** aggregate sum on columns */
export type ArtistCollectionSumFields = {
  index?: Maybe<Scalars['Int']>;
};

/** order by sum() on columns of table "artist_collection" */
export type ArtistCollectionSumOrderBy = {
  index?: Maybe<OrderBy>;
};

/** update columns of table "artist_collection" */
export enum ArtistCollectionUpdateColumn {
  /** column name */
  ArtistId = 'artist_id',
  /** column name */
  CollectionId = 'collection_id',
  /** column name */
  Index = 'index'
}

/** aggregate var_pop on columns */
export type ArtistCollectionVarPopFields = {
  index?: Maybe<Scalars['Float']>;
};

/** order by var_pop() on columns of table "artist_collection" */
export type ArtistCollectionVarPopOrderBy = {
  index?: Maybe<OrderBy>;
};

/** aggregate var_samp on columns */
export type ArtistCollectionVarSampFields = {
  index?: Maybe<Scalars['Float']>;
};

/** order by var_samp() on columns of table "artist_collection" */
export type ArtistCollectionVarSampOrderBy = {
  index?: Maybe<OrderBy>;
};

/** aggregate variance on columns */
export type ArtistCollectionVarianceFields = {
  index?: Maybe<Scalars['Float']>;
};

/** order by variance() on columns of table "artist_collection" */
export type ArtistCollectionVarianceOrderBy = {
  index?: Maybe<OrderBy>;
};

/** unique or primary key constraints on table "artist" */
export enum ArtistConstraint {
  /** unique or primary key constraint */
  ArtistPkey = 'artist_pkey'
}

/** columns and relationships of "artist_country" */
export type ArtistCountry = {
  /** An object relationship */
  artist: Artist;
  artist_id: Scalars['uuid'];
  /** An object relationship */
  country: Country;
  country_code: Scalars['String'];
  index: Scalars['Int'];
};

/** aggregated selection of "artist_country" */
export type ArtistCountryAggregate = {
  aggregate?: Maybe<ArtistCountryAggregateFields>;
  nodes: Array<ArtistCountry>;
};

/** aggregate fields of "artist_country" */
export type ArtistCountryAggregateFields = {
  avg?: Maybe<ArtistCountryAvgFields>;
  count?: Maybe<Scalars['Int']>;
  max?: Maybe<ArtistCountryMaxFields>;
  min?: Maybe<ArtistCountryMinFields>;
  stddev?: Maybe<ArtistCountryStddevFields>;
  stddev_pop?: Maybe<ArtistCountryStddevPopFields>;
  stddev_samp?: Maybe<ArtistCountryStddevSampFields>;
  sum?: Maybe<ArtistCountrySumFields>;
  var_pop?: Maybe<ArtistCountryVarPopFields>;
  var_samp?: Maybe<ArtistCountryVarSampFields>;
  variance?: Maybe<ArtistCountryVarianceFields>;
};


/** aggregate fields of "artist_country" */
export type ArtistCountryAggregateFieldsCountArgs = {
  columns?: Maybe<Array<ArtistCountrySelectColumn>>;
  distinct?: Maybe<Scalars['Boolean']>;
};

/** order by aggregate values of table "artist_country" */
export type ArtistCountryAggregateOrderBy = {
  avg?: Maybe<ArtistCountryAvgOrderBy>;
  count?: Maybe<OrderBy>;
  max?: Maybe<ArtistCountryMaxOrderBy>;
  min?: Maybe<ArtistCountryMinOrderBy>;
  stddev?: Maybe<ArtistCountryStddevOrderBy>;
  stddev_pop?: Maybe<ArtistCountryStddevPopOrderBy>;
  stddev_samp?: Maybe<ArtistCountryStddevSampOrderBy>;
  sum?: Maybe<ArtistCountrySumOrderBy>;
  var_pop?: Maybe<ArtistCountryVarPopOrderBy>;
  var_samp?: Maybe<ArtistCountryVarSampOrderBy>;
  variance?: Maybe<ArtistCountryVarianceOrderBy>;
};

/** input type for inserting array relation for remote table "artist_country" */
export type ArtistCountryArrRelInsertInput = {
  data: Array<ArtistCountryInsertInput>;
  on_conflict?: Maybe<ArtistCountryOnConflict>;
};

/** aggregate avg on columns */
export type ArtistCountryAvgFields = {
  index?: Maybe<Scalars['Float']>;
};

/** order by avg() on columns of table "artist_country" */
export type ArtistCountryAvgOrderBy = {
  index?: Maybe<OrderBy>;
};

/** Boolean expression to filter rows from the table "artist_country". All fields are combined with a logical 'AND'. */
export type ArtistCountryBoolExp = {
  _and?: Maybe<Array<Maybe<ArtistCountryBoolExp>>>;
  _not?: Maybe<ArtistCountryBoolExp>;
  _or?: Maybe<Array<Maybe<ArtistCountryBoolExp>>>;
  artist?: Maybe<ArtistBoolExp>;
  artist_id?: Maybe<UuidComparisonExp>;
  country?: Maybe<CountryBoolExp>;
  country_code?: Maybe<StringComparisonExp>;
  index?: Maybe<IntComparisonExp>;
};

/** unique or primary key constraints on table "artist_country" */
export enum ArtistCountryConstraint {
  /** unique or primary key constraint */
  ArtistCountryArtistIdCountryCodeKey = 'artist_country_artist_id_country_code_key',
  /** unique or primary key constraint */
  ArtistCountryPkey = 'artist_country_pkey'
}

/** input type for incrementing integer column in table "artist_country" */
export type ArtistCountryIncInput = {
  index?: Maybe<Scalars['Int']>;
};

/** input type for inserting data into table "artist_country" */
export type ArtistCountryInsertInput = {
  artist?: Maybe<ArtistObjRelInsertInput>;
  artist_id?: Maybe<Scalars['uuid']>;
  country?: Maybe<CountryObjRelInsertInput>;
  country_code?: Maybe<Scalars['String']>;
  index?: Maybe<Scalars['Int']>;
};

/** aggregate max on columns */
export type ArtistCountryMaxFields = {
  artist_id?: Maybe<Scalars['uuid']>;
  country_code?: Maybe<Scalars['String']>;
  index?: Maybe<Scalars['Int']>;
};

/** order by max() on columns of table "artist_country" */
export type ArtistCountryMaxOrderBy = {
  artist_id?: Maybe<OrderBy>;
  country_code?: Maybe<OrderBy>;
  index?: Maybe<OrderBy>;
};

/** aggregate min on columns */
export type ArtistCountryMinFields = {
  artist_id?: Maybe<Scalars['uuid']>;
  country_code?: Maybe<Scalars['String']>;
  index?: Maybe<Scalars['Int']>;
};

/** order by min() on columns of table "artist_country" */
export type ArtistCountryMinOrderBy = {
  artist_id?: Maybe<OrderBy>;
  country_code?: Maybe<OrderBy>;
  index?: Maybe<OrderBy>;
};

/** response of any mutation on the table "artist_country" */
export type ArtistCountryMutationResponse = {
  /** number of affected rows by the mutation */
  affected_rows: Scalars['Int'];
  /** data of the affected rows by the mutation */
  returning: Array<ArtistCountry>;
};

/** input type for inserting object relation for remote table "artist_country" */
export type ArtistCountryObjRelInsertInput = {
  data: ArtistCountryInsertInput;
  on_conflict?: Maybe<ArtistCountryOnConflict>;
};

/** on conflict condition type for table "artist_country" */
export type ArtistCountryOnConflict = {
  constraint: ArtistCountryConstraint;
  update_columns: Array<ArtistCountryUpdateColumn>;
  where?: Maybe<ArtistCountryBoolExp>;
};

/** ordering options when selecting data from "artist_country" */
export type ArtistCountryOrderBy = {
  artist?: Maybe<ArtistOrderBy>;
  artist_id?: Maybe<OrderBy>;
  country?: Maybe<CountryOrderBy>;
  country_code?: Maybe<OrderBy>;
  index?: Maybe<OrderBy>;
};

/** primary key columns input for table: "artist_country" */
export type ArtistCountryPkColumnsInput = {
  artist_id: Scalars['uuid'];
  index: Scalars['Int'];
};

/** select columns of table "artist_country" */
export enum ArtistCountrySelectColumn {
  /** column name */
  ArtistId = 'artist_id',
  /** column name */
  CountryCode = 'country_code',
  /** column name */
  Index = 'index'
}

/** input type for updating data in table "artist_country" */
export type ArtistCountrySetInput = {
  artist_id?: Maybe<Scalars['uuid']>;
  country_code?: Maybe<Scalars['String']>;
  index?: Maybe<Scalars['Int']>;
};

/** aggregate stddev on columns */
export type ArtistCountryStddevFields = {
  index?: Maybe<Scalars['Float']>;
};

/** order by stddev() on columns of table "artist_country" */
export type ArtistCountryStddevOrderBy = {
  index?: Maybe<OrderBy>;
};

/** aggregate stddev_pop on columns */
export type ArtistCountryStddevPopFields = {
  index?: Maybe<Scalars['Float']>;
};

/** order by stddev_pop() on columns of table "artist_country" */
export type ArtistCountryStddevPopOrderBy = {
  index?: Maybe<OrderBy>;
};

/** aggregate stddev_samp on columns */
export type ArtistCountryStddevSampFields = {
  index?: Maybe<Scalars['Float']>;
};

/** order by stddev_samp() on columns of table "artist_country" */
export type ArtistCountryStddevSampOrderBy = {
  index?: Maybe<OrderBy>;
};

/** aggregate sum on columns */
export type ArtistCountrySumFields = {
  index?: Maybe<Scalars['Int']>;
};

/** order by sum() on columns of table "artist_country" */
export type ArtistCountrySumOrderBy = {
  index?: Maybe<OrderBy>;
};

/** update columns of table "artist_country" */
export enum ArtistCountryUpdateColumn {
  /** column name */
  ArtistId = 'artist_id',
  /** column name */
  CountryCode = 'country_code',
  /** column name */
  Index = 'index'
}

/** aggregate var_pop on columns */
export type ArtistCountryVarPopFields = {
  index?: Maybe<Scalars['Float']>;
};

/** order by var_pop() on columns of table "artist_country" */
export type ArtistCountryVarPopOrderBy = {
  index?: Maybe<OrderBy>;
};

/** aggregate var_samp on columns */
export type ArtistCountryVarSampFields = {
  index?: Maybe<Scalars['Float']>;
};

/** order by var_samp() on columns of table "artist_country" */
export type ArtistCountryVarSampOrderBy = {
  index?: Maybe<OrderBy>;
};

/** aggregate variance on columns */
export type ArtistCountryVarianceFields = {
  index?: Maybe<Scalars['Float']>;
};

/** order by variance() on columns of table "artist_country" */
export type ArtistCountryVarianceOrderBy = {
  index?: Maybe<OrderBy>;
};

/** columns and relationships of "artist_genre" */
export type ArtistGenre = {
  /** An object relationship */
  artist: Artist;
  artist_id: Scalars['uuid'];
  /** An object relationship */
  genre: Genre;
  genre_id: Scalars['uuid'];
  index: Scalars['Int'];
};

/** aggregated selection of "artist_genre" */
export type ArtistGenreAggregate = {
  aggregate?: Maybe<ArtistGenreAggregateFields>;
  nodes: Array<ArtistGenre>;
};

/** aggregate fields of "artist_genre" */
export type ArtistGenreAggregateFields = {
  avg?: Maybe<ArtistGenreAvgFields>;
  count?: Maybe<Scalars['Int']>;
  max?: Maybe<ArtistGenreMaxFields>;
  min?: Maybe<ArtistGenreMinFields>;
  stddev?: Maybe<ArtistGenreStddevFields>;
  stddev_pop?: Maybe<ArtistGenreStddevPopFields>;
  stddev_samp?: Maybe<ArtistGenreStddevSampFields>;
  sum?: Maybe<ArtistGenreSumFields>;
  var_pop?: Maybe<ArtistGenreVarPopFields>;
  var_samp?: Maybe<ArtistGenreVarSampFields>;
  variance?: Maybe<ArtistGenreVarianceFields>;
};


/** aggregate fields of "artist_genre" */
export type ArtistGenreAggregateFieldsCountArgs = {
  columns?: Maybe<Array<ArtistGenreSelectColumn>>;
  distinct?: Maybe<Scalars['Boolean']>;
};

/** order by aggregate values of table "artist_genre" */
export type ArtistGenreAggregateOrderBy = {
  avg?: Maybe<ArtistGenreAvgOrderBy>;
  count?: Maybe<OrderBy>;
  max?: Maybe<ArtistGenreMaxOrderBy>;
  min?: Maybe<ArtistGenreMinOrderBy>;
  stddev?: Maybe<ArtistGenreStddevOrderBy>;
  stddev_pop?: Maybe<ArtistGenreStddevPopOrderBy>;
  stddev_samp?: Maybe<ArtistGenreStddevSampOrderBy>;
  sum?: Maybe<ArtistGenreSumOrderBy>;
  var_pop?: Maybe<ArtistGenreVarPopOrderBy>;
  var_samp?: Maybe<ArtistGenreVarSampOrderBy>;
  variance?: Maybe<ArtistGenreVarianceOrderBy>;
};

/** input type for inserting array relation for remote table "artist_genre" */
export type ArtistGenreArrRelInsertInput = {
  data: Array<ArtistGenreInsertInput>;
  on_conflict?: Maybe<ArtistGenreOnConflict>;
};

/** aggregate avg on columns */
export type ArtistGenreAvgFields = {
  index?: Maybe<Scalars['Float']>;
};

/** order by avg() on columns of table "artist_genre" */
export type ArtistGenreAvgOrderBy = {
  index?: Maybe<OrderBy>;
};

/** Boolean expression to filter rows from the table "artist_genre". All fields are combined with a logical 'AND'. */
export type ArtistGenreBoolExp = {
  _and?: Maybe<Array<Maybe<ArtistGenreBoolExp>>>;
  _not?: Maybe<ArtistGenreBoolExp>;
  _or?: Maybe<Array<Maybe<ArtistGenreBoolExp>>>;
  artist?: Maybe<ArtistBoolExp>;
  artist_id?: Maybe<UuidComparisonExp>;
  genre?: Maybe<GenreBoolExp>;
  genre_id?: Maybe<UuidComparisonExp>;
  index?: Maybe<IntComparisonExp>;
};

/** unique or primary key constraints on table "artist_genre" */
export enum ArtistGenreConstraint {
  /** unique or primary key constraint */
  ArtistGenreArtistIdGenreIdKey = 'artist_genre_artist_id_genre_id_key',
  /** unique or primary key constraint */
  ArtistGenrePkey = 'artist_genre_pkey'
}

/** input type for incrementing integer column in table "artist_genre" */
export type ArtistGenreIncInput = {
  index?: Maybe<Scalars['Int']>;
};

/** input type for inserting data into table "artist_genre" */
export type ArtistGenreInsertInput = {
  artist?: Maybe<ArtistObjRelInsertInput>;
  artist_id?: Maybe<Scalars['uuid']>;
  genre?: Maybe<GenreObjRelInsertInput>;
  genre_id?: Maybe<Scalars['uuid']>;
  index?: Maybe<Scalars['Int']>;
};

/** aggregate max on columns */
export type ArtistGenreMaxFields = {
  artist_id?: Maybe<Scalars['uuid']>;
  genre_id?: Maybe<Scalars['uuid']>;
  index?: Maybe<Scalars['Int']>;
};

/** order by max() on columns of table "artist_genre" */
export type ArtistGenreMaxOrderBy = {
  artist_id?: Maybe<OrderBy>;
  genre_id?: Maybe<OrderBy>;
  index?: Maybe<OrderBy>;
};

/** aggregate min on columns */
export type ArtistGenreMinFields = {
  artist_id?: Maybe<Scalars['uuid']>;
  genre_id?: Maybe<Scalars['uuid']>;
  index?: Maybe<Scalars['Int']>;
};

/** order by min() on columns of table "artist_genre" */
export type ArtistGenreMinOrderBy = {
  artist_id?: Maybe<OrderBy>;
  genre_id?: Maybe<OrderBy>;
  index?: Maybe<OrderBy>;
};

/** response of any mutation on the table "artist_genre" */
export type ArtistGenreMutationResponse = {
  /** number of affected rows by the mutation */
  affected_rows: Scalars['Int'];
  /** data of the affected rows by the mutation */
  returning: Array<ArtistGenre>;
};

/** input type for inserting object relation for remote table "artist_genre" */
export type ArtistGenreObjRelInsertInput = {
  data: ArtistGenreInsertInput;
  on_conflict?: Maybe<ArtistGenreOnConflict>;
};

/** on conflict condition type for table "artist_genre" */
export type ArtistGenreOnConflict = {
  constraint: ArtistGenreConstraint;
  update_columns: Array<ArtistGenreUpdateColumn>;
  where?: Maybe<ArtistGenreBoolExp>;
};

/** ordering options when selecting data from "artist_genre" */
export type ArtistGenreOrderBy = {
  artist?: Maybe<ArtistOrderBy>;
  artist_id?: Maybe<OrderBy>;
  genre?: Maybe<GenreOrderBy>;
  genre_id?: Maybe<OrderBy>;
  index?: Maybe<OrderBy>;
};

/** primary key columns input for table: "artist_genre" */
export type ArtistGenrePkColumnsInput = {
  artist_id: Scalars['uuid'];
  index: Scalars['Int'];
};

/** select columns of table "artist_genre" */
export enum ArtistGenreSelectColumn {
  /** column name */
  ArtistId = 'artist_id',
  /** column name */
  GenreId = 'genre_id',
  /** column name */
  Index = 'index'
}

/** input type for updating data in table "artist_genre" */
export type ArtistGenreSetInput = {
  artist_id?: Maybe<Scalars['uuid']>;
  genre_id?: Maybe<Scalars['uuid']>;
  index?: Maybe<Scalars['Int']>;
};

/** aggregate stddev on columns */
export type ArtistGenreStddevFields = {
  index?: Maybe<Scalars['Float']>;
};

/** order by stddev() on columns of table "artist_genre" */
export type ArtistGenreStddevOrderBy = {
  index?: Maybe<OrderBy>;
};

/** aggregate stddev_pop on columns */
export type ArtistGenreStddevPopFields = {
  index?: Maybe<Scalars['Float']>;
};

/** order by stddev_pop() on columns of table "artist_genre" */
export type ArtistGenreStddevPopOrderBy = {
  index?: Maybe<OrderBy>;
};

/** aggregate stddev_samp on columns */
export type ArtistGenreStddevSampFields = {
  index?: Maybe<Scalars['Float']>;
};

/** order by stddev_samp() on columns of table "artist_genre" */
export type ArtistGenreStddevSampOrderBy = {
  index?: Maybe<OrderBy>;
};

/** aggregate sum on columns */
export type ArtistGenreSumFields = {
  index?: Maybe<Scalars['Int']>;
};

/** order by sum() on columns of table "artist_genre" */
export type ArtistGenreSumOrderBy = {
  index?: Maybe<OrderBy>;
};

/** update columns of table "artist_genre" */
export enum ArtistGenreUpdateColumn {
  /** column name */
  ArtistId = 'artist_id',
  /** column name */
  GenreId = 'genre_id',
  /** column name */
  Index = 'index'
}

/** aggregate var_pop on columns */
export type ArtistGenreVarPopFields = {
  index?: Maybe<Scalars['Float']>;
};

/** order by var_pop() on columns of table "artist_genre" */
export type ArtistGenreVarPopOrderBy = {
  index?: Maybe<OrderBy>;
};

/** aggregate var_samp on columns */
export type ArtistGenreVarSampFields = {
  index?: Maybe<Scalars['Float']>;
};

/** order by var_samp() on columns of table "artist_genre" */
export type ArtistGenreVarSampOrderBy = {
  index?: Maybe<OrderBy>;
};

/** aggregate variance on columns */
export type ArtistGenreVarianceFields = {
  index?: Maybe<Scalars['Float']>;
};

/** order by variance() on columns of table "artist_genre" */
export type ArtistGenreVarianceOrderBy = {
  index?: Maybe<OrderBy>;
};

/** input type for inserting data into table "artist" */
export type ArtistInsertInput = {
  albums?: Maybe<AlbumArrRelInsertInput>;
  artistSimilarsByArtist2Id?: Maybe<ArtistSimilarArrRelInsertInput>;
  artist_collections?: Maybe<ArtistCollectionArrRelInsertInput>;
  artist_countries?: Maybe<ArtistCountryArrRelInsertInput>;
  artist_genres?: Maybe<ArtistGenreArrRelInsertInput>;
  artist_moods?: Maybe<ArtistMoodArrRelInsertInput>;
  artist_similars?: Maybe<ArtistSimilarArrRelInsertInput>;
  artist_styles?: Maybe<ArtistStyleArrRelInsertInput>;
  id?: Maybe<Scalars['uuid']>;
  image_background?: Maybe<Scalars['String']>;
  image_cover?: Maybe<Scalars['String']>;
  person?: Maybe<PersonObjRelInsertInput>;
  person_id?: Maybe<Scalars['uuid']>;
  summary?: Maybe<Scalars['String']>;
};

/** aggregate max on columns */
export type ArtistMaxFields = {
  id?: Maybe<Scalars['uuid']>;
  image_background?: Maybe<Scalars['String']>;
  image_cover?: Maybe<Scalars['String']>;
  person_id?: Maybe<Scalars['uuid']>;
  summary?: Maybe<Scalars['String']>;
};

/** order by max() on columns of table "artist" */
export type ArtistMaxOrderBy = {
  id?: Maybe<OrderBy>;
  image_background?: Maybe<OrderBy>;
  image_cover?: Maybe<OrderBy>;
  person_id?: Maybe<OrderBy>;
  summary?: Maybe<OrderBy>;
};

/** aggregate min on columns */
export type ArtistMinFields = {
  id?: Maybe<Scalars['uuid']>;
  image_background?: Maybe<Scalars['String']>;
  image_cover?: Maybe<Scalars['String']>;
  person_id?: Maybe<Scalars['uuid']>;
  summary?: Maybe<Scalars['String']>;
};

/** order by min() on columns of table "artist" */
export type ArtistMinOrderBy = {
  id?: Maybe<OrderBy>;
  image_background?: Maybe<OrderBy>;
  image_cover?: Maybe<OrderBy>;
  person_id?: Maybe<OrderBy>;
  summary?: Maybe<OrderBy>;
};

/** columns and relationships of "artist_mood" */
export type ArtistMood = {
  /** An object relationship */
  artist: Artist;
  artist_id: Scalars['uuid'];
  index: Scalars['Int'];
  /** An object relationship */
  mood: Mood;
  mood_id: Scalars['uuid'];
};

/** aggregated selection of "artist_mood" */
export type ArtistMoodAggregate = {
  aggregate?: Maybe<ArtistMoodAggregateFields>;
  nodes: Array<ArtistMood>;
};

/** aggregate fields of "artist_mood" */
export type ArtistMoodAggregateFields = {
  avg?: Maybe<ArtistMoodAvgFields>;
  count?: Maybe<Scalars['Int']>;
  max?: Maybe<ArtistMoodMaxFields>;
  min?: Maybe<ArtistMoodMinFields>;
  stddev?: Maybe<ArtistMoodStddevFields>;
  stddev_pop?: Maybe<ArtistMoodStddevPopFields>;
  stddev_samp?: Maybe<ArtistMoodStddevSampFields>;
  sum?: Maybe<ArtistMoodSumFields>;
  var_pop?: Maybe<ArtistMoodVarPopFields>;
  var_samp?: Maybe<ArtistMoodVarSampFields>;
  variance?: Maybe<ArtistMoodVarianceFields>;
};


/** aggregate fields of "artist_mood" */
export type ArtistMoodAggregateFieldsCountArgs = {
  columns?: Maybe<Array<ArtistMoodSelectColumn>>;
  distinct?: Maybe<Scalars['Boolean']>;
};

/** order by aggregate values of table "artist_mood" */
export type ArtistMoodAggregateOrderBy = {
  avg?: Maybe<ArtistMoodAvgOrderBy>;
  count?: Maybe<OrderBy>;
  max?: Maybe<ArtistMoodMaxOrderBy>;
  min?: Maybe<ArtistMoodMinOrderBy>;
  stddev?: Maybe<ArtistMoodStddevOrderBy>;
  stddev_pop?: Maybe<ArtistMoodStddevPopOrderBy>;
  stddev_samp?: Maybe<ArtistMoodStddevSampOrderBy>;
  sum?: Maybe<ArtistMoodSumOrderBy>;
  var_pop?: Maybe<ArtistMoodVarPopOrderBy>;
  var_samp?: Maybe<ArtistMoodVarSampOrderBy>;
  variance?: Maybe<ArtistMoodVarianceOrderBy>;
};

/** input type for inserting array relation for remote table "artist_mood" */
export type ArtistMoodArrRelInsertInput = {
  data: Array<ArtistMoodInsertInput>;
  on_conflict?: Maybe<ArtistMoodOnConflict>;
};

/** aggregate avg on columns */
export type ArtistMoodAvgFields = {
  index?: Maybe<Scalars['Float']>;
};

/** order by avg() on columns of table "artist_mood" */
export type ArtistMoodAvgOrderBy = {
  index?: Maybe<OrderBy>;
};

/** Boolean expression to filter rows from the table "artist_mood". All fields are combined with a logical 'AND'. */
export type ArtistMoodBoolExp = {
  _and?: Maybe<Array<Maybe<ArtistMoodBoolExp>>>;
  _not?: Maybe<ArtistMoodBoolExp>;
  _or?: Maybe<Array<Maybe<ArtistMoodBoolExp>>>;
  artist?: Maybe<ArtistBoolExp>;
  artist_id?: Maybe<UuidComparisonExp>;
  index?: Maybe<IntComparisonExp>;
  mood?: Maybe<MoodBoolExp>;
  mood_id?: Maybe<UuidComparisonExp>;
};

/** unique or primary key constraints on table "artist_mood" */
export enum ArtistMoodConstraint {
  /** unique or primary key constraint */
  ArtistMoodArtistIdMoodIdKey = 'artist_mood_artist_id_mood_id_key',
  /** unique or primary key constraint */
  ArtistMoodPkey = 'artist_mood_pkey'
}

/** input type for incrementing integer column in table "artist_mood" */
export type ArtistMoodIncInput = {
  index?: Maybe<Scalars['Int']>;
};

/** input type for inserting data into table "artist_mood" */
export type ArtistMoodInsertInput = {
  artist?: Maybe<ArtistObjRelInsertInput>;
  artist_id?: Maybe<Scalars['uuid']>;
  index?: Maybe<Scalars['Int']>;
  mood?: Maybe<MoodObjRelInsertInput>;
  mood_id?: Maybe<Scalars['uuid']>;
};

/** aggregate max on columns */
export type ArtistMoodMaxFields = {
  artist_id?: Maybe<Scalars['uuid']>;
  index?: Maybe<Scalars['Int']>;
  mood_id?: Maybe<Scalars['uuid']>;
};

/** order by max() on columns of table "artist_mood" */
export type ArtistMoodMaxOrderBy = {
  artist_id?: Maybe<OrderBy>;
  index?: Maybe<OrderBy>;
  mood_id?: Maybe<OrderBy>;
};

/** aggregate min on columns */
export type ArtistMoodMinFields = {
  artist_id?: Maybe<Scalars['uuid']>;
  index?: Maybe<Scalars['Int']>;
  mood_id?: Maybe<Scalars['uuid']>;
};

/** order by min() on columns of table "artist_mood" */
export type ArtistMoodMinOrderBy = {
  artist_id?: Maybe<OrderBy>;
  index?: Maybe<OrderBy>;
  mood_id?: Maybe<OrderBy>;
};

/** response of any mutation on the table "artist_mood" */
export type ArtistMoodMutationResponse = {
  /** number of affected rows by the mutation */
  affected_rows: Scalars['Int'];
  /** data of the affected rows by the mutation */
  returning: Array<ArtistMood>;
};

/** input type for inserting object relation for remote table "artist_mood" */
export type ArtistMoodObjRelInsertInput = {
  data: ArtistMoodInsertInput;
  on_conflict?: Maybe<ArtistMoodOnConflict>;
};

/** on conflict condition type for table "artist_mood" */
export type ArtistMoodOnConflict = {
  constraint: ArtistMoodConstraint;
  update_columns: Array<ArtistMoodUpdateColumn>;
  where?: Maybe<ArtistMoodBoolExp>;
};

/** ordering options when selecting data from "artist_mood" */
export type ArtistMoodOrderBy = {
  artist?: Maybe<ArtistOrderBy>;
  artist_id?: Maybe<OrderBy>;
  index?: Maybe<OrderBy>;
  mood?: Maybe<MoodOrderBy>;
  mood_id?: Maybe<OrderBy>;
};

/** primary key columns input for table: "artist_mood" */
export type ArtistMoodPkColumnsInput = {
  artist_id: Scalars['uuid'];
  index: Scalars['Int'];
};

/** select columns of table "artist_mood" */
export enum ArtistMoodSelectColumn {
  /** column name */
  ArtistId = 'artist_id',
  /** column name */
  Index = 'index',
  /** column name */
  MoodId = 'mood_id'
}

/** input type for updating data in table "artist_mood" */
export type ArtistMoodSetInput = {
  artist_id?: Maybe<Scalars['uuid']>;
  index?: Maybe<Scalars['Int']>;
  mood_id?: Maybe<Scalars['uuid']>;
};

/** aggregate stddev on columns */
export type ArtistMoodStddevFields = {
  index?: Maybe<Scalars['Float']>;
};

/** order by stddev() on columns of table "artist_mood" */
export type ArtistMoodStddevOrderBy = {
  index?: Maybe<OrderBy>;
};

/** aggregate stddev_pop on columns */
export type ArtistMoodStddevPopFields = {
  index?: Maybe<Scalars['Float']>;
};

/** order by stddev_pop() on columns of table "artist_mood" */
export type ArtistMoodStddevPopOrderBy = {
  index?: Maybe<OrderBy>;
};

/** aggregate stddev_samp on columns */
export type ArtistMoodStddevSampFields = {
  index?: Maybe<Scalars['Float']>;
};

/** order by stddev_samp() on columns of table "artist_mood" */
export type ArtistMoodStddevSampOrderBy = {
  index?: Maybe<OrderBy>;
};

/** aggregate sum on columns */
export type ArtistMoodSumFields = {
  index?: Maybe<Scalars['Int']>;
};

/** order by sum() on columns of table "artist_mood" */
export type ArtistMoodSumOrderBy = {
  index?: Maybe<OrderBy>;
};

/** update columns of table "artist_mood" */
export enum ArtistMoodUpdateColumn {
  /** column name */
  ArtistId = 'artist_id',
  /** column name */
  Index = 'index',
  /** column name */
  MoodId = 'mood_id'
}

/** aggregate var_pop on columns */
export type ArtistMoodVarPopFields = {
  index?: Maybe<Scalars['Float']>;
};

/** order by var_pop() on columns of table "artist_mood" */
export type ArtistMoodVarPopOrderBy = {
  index?: Maybe<OrderBy>;
};

/** aggregate var_samp on columns */
export type ArtistMoodVarSampFields = {
  index?: Maybe<Scalars['Float']>;
};

/** order by var_samp() on columns of table "artist_mood" */
export type ArtistMoodVarSampOrderBy = {
  index?: Maybe<OrderBy>;
};

/** aggregate variance on columns */
export type ArtistMoodVarianceFields = {
  index?: Maybe<Scalars['Float']>;
};

/** order by variance() on columns of table "artist_mood" */
export type ArtistMoodVarianceOrderBy = {
  index?: Maybe<OrderBy>;
};

/** response of any mutation on the table "artist" */
export type ArtistMutationResponse = {
  /** number of affected rows by the mutation */
  affected_rows: Scalars['Int'];
  /** data of the affected rows by the mutation */
  returning: Array<Artist>;
};

/** input type for inserting object relation for remote table "artist" */
export type ArtistObjRelInsertInput = {
  data: ArtistInsertInput;
  on_conflict?: Maybe<ArtistOnConflict>;
};

/** on conflict condition type for table "artist" */
export type ArtistOnConflict = {
  constraint: ArtistConstraint;
  update_columns: Array<ArtistUpdateColumn>;
  where?: Maybe<ArtistBoolExp>;
};

/** ordering options when selecting data from "artist" */
export type ArtistOrderBy = {
  albums_aggregate?: Maybe<AlbumAggregateOrderBy>;
  artistSimilarsByArtist2Id_aggregate?: Maybe<ArtistSimilarAggregateOrderBy>;
  artist_collections_aggregate?: Maybe<ArtistCollectionAggregateOrderBy>;
  artist_countries_aggregate?: Maybe<ArtistCountryAggregateOrderBy>;
  artist_genres_aggregate?: Maybe<ArtistGenreAggregateOrderBy>;
  artist_moods_aggregate?: Maybe<ArtistMoodAggregateOrderBy>;
  artist_similars_aggregate?: Maybe<ArtistSimilarAggregateOrderBy>;
  artist_styles_aggregate?: Maybe<ArtistStyleAggregateOrderBy>;
  id?: Maybe<OrderBy>;
  image_background?: Maybe<OrderBy>;
  image_cover?: Maybe<OrderBy>;
  person?: Maybe<PersonOrderBy>;
  person_id?: Maybe<OrderBy>;
  summary?: Maybe<OrderBy>;
};

/** primary key columns input for table: "artist" */
export type ArtistPkColumnsInput = {
  id: Scalars['uuid'];
};

/** select columns of table "artist" */
export enum ArtistSelectColumn {
  /** column name */
  Id = 'id',
  /** column name */
  ImageBackground = 'image_background',
  /** column name */
  ImageCover = 'image_cover',
  /** column name */
  PersonId = 'person_id',
  /** column name */
  Summary = 'summary'
}

/** input type for updating data in table "artist" */
export type ArtistSetInput = {
  id?: Maybe<Scalars['uuid']>;
  image_background?: Maybe<Scalars['String']>;
  image_cover?: Maybe<Scalars['String']>;
  person_id?: Maybe<Scalars['uuid']>;
  summary?: Maybe<Scalars['String']>;
};

/** columns and relationships of "artist_similar" */
export type ArtistSimilar = {
  /** An object relationship */
  artist: Artist;
  artist1_id: Scalars['uuid'];
  artist2_id: Scalars['uuid'];
  /** An object relationship */
  artistByArtist1Id: Artist;
};

/** aggregated selection of "artist_similar" */
export type ArtistSimilarAggregate = {
  aggregate?: Maybe<ArtistSimilarAggregateFields>;
  nodes: Array<ArtistSimilar>;
};

/** aggregate fields of "artist_similar" */
export type ArtistSimilarAggregateFields = {
  count?: Maybe<Scalars['Int']>;
  max?: Maybe<ArtistSimilarMaxFields>;
  min?: Maybe<ArtistSimilarMinFields>;
};


/** aggregate fields of "artist_similar" */
export type ArtistSimilarAggregateFieldsCountArgs = {
  columns?: Maybe<Array<ArtistSimilarSelectColumn>>;
  distinct?: Maybe<Scalars['Boolean']>;
};

/** order by aggregate values of table "artist_similar" */
export type ArtistSimilarAggregateOrderBy = {
  count?: Maybe<OrderBy>;
  max?: Maybe<ArtistSimilarMaxOrderBy>;
  min?: Maybe<ArtistSimilarMinOrderBy>;
};

/** input type for inserting array relation for remote table "artist_similar" */
export type ArtistSimilarArrRelInsertInput = {
  data: Array<ArtistSimilarInsertInput>;
  on_conflict?: Maybe<ArtistSimilarOnConflict>;
};

/** Boolean expression to filter rows from the table "artist_similar". All fields are combined with a logical 'AND'. */
export type ArtistSimilarBoolExp = {
  _and?: Maybe<Array<Maybe<ArtistSimilarBoolExp>>>;
  _not?: Maybe<ArtistSimilarBoolExp>;
  _or?: Maybe<Array<Maybe<ArtistSimilarBoolExp>>>;
  artist?: Maybe<ArtistBoolExp>;
  artist1_id?: Maybe<UuidComparisonExp>;
  artist2_id?: Maybe<UuidComparisonExp>;
  artistByArtist1Id?: Maybe<ArtistBoolExp>;
};

/** unique or primary key constraints on table "artist_similar" */
export enum ArtistSimilarConstraint {
  /** unique or primary key constraint */
  ArtistSimilarPkey = 'artist_similar_pkey'
}

/** input type for inserting data into table "artist_similar" */
export type ArtistSimilarInsertInput = {
  artist?: Maybe<ArtistObjRelInsertInput>;
  artist1_id?: Maybe<Scalars['uuid']>;
  artist2_id?: Maybe<Scalars['uuid']>;
  artistByArtist1Id?: Maybe<ArtistObjRelInsertInput>;
};

/** aggregate max on columns */
export type ArtistSimilarMaxFields = {
  artist1_id?: Maybe<Scalars['uuid']>;
  artist2_id?: Maybe<Scalars['uuid']>;
};

/** order by max() on columns of table "artist_similar" */
export type ArtistSimilarMaxOrderBy = {
  artist1_id?: Maybe<OrderBy>;
  artist2_id?: Maybe<OrderBy>;
};

/** aggregate min on columns */
export type ArtistSimilarMinFields = {
  artist1_id?: Maybe<Scalars['uuid']>;
  artist2_id?: Maybe<Scalars['uuid']>;
};

/** order by min() on columns of table "artist_similar" */
export type ArtistSimilarMinOrderBy = {
  artist1_id?: Maybe<OrderBy>;
  artist2_id?: Maybe<OrderBy>;
};

/** response of any mutation on the table "artist_similar" */
export type ArtistSimilarMutationResponse = {
  /** number of affected rows by the mutation */
  affected_rows: Scalars['Int'];
  /** data of the affected rows by the mutation */
  returning: Array<ArtistSimilar>;
};

/** input type for inserting object relation for remote table "artist_similar" */
export type ArtistSimilarObjRelInsertInput = {
  data: ArtistSimilarInsertInput;
  on_conflict?: Maybe<ArtistSimilarOnConflict>;
};

/** on conflict condition type for table "artist_similar" */
export type ArtistSimilarOnConflict = {
  constraint: ArtistSimilarConstraint;
  update_columns: Array<ArtistSimilarUpdateColumn>;
  where?: Maybe<ArtistSimilarBoolExp>;
};

/** ordering options when selecting data from "artist_similar" */
export type ArtistSimilarOrderBy = {
  artist?: Maybe<ArtistOrderBy>;
  artist1_id?: Maybe<OrderBy>;
  artist2_id?: Maybe<OrderBy>;
  artistByArtist1Id?: Maybe<ArtistOrderBy>;
};

/** primary key columns input for table: "artist_similar" */
export type ArtistSimilarPkColumnsInput = {
  artist1_id: Scalars['uuid'];
  artist2_id: Scalars['uuid'];
};

/** select columns of table "artist_similar" */
export enum ArtistSimilarSelectColumn {
  /** column name */
  Artist1Id = 'artist1_id',
  /** column name */
  Artist2Id = 'artist2_id'
}

/** input type for updating data in table "artist_similar" */
export type ArtistSimilarSetInput = {
  artist1_id?: Maybe<Scalars['uuid']>;
  artist2_id?: Maybe<Scalars['uuid']>;
};

/** update columns of table "artist_similar" */
export enum ArtistSimilarUpdateColumn {
  /** column name */
  Artist1Id = 'artist1_id',
  /** column name */
  Artist2Id = 'artist2_id'
}

/** columns and relationships of "artist_style" */
export type ArtistStyle = {
  /** An object relationship */
  artist: Artist;
  artist_id: Scalars['uuid'];
  index: Scalars['Int'];
  /** An object relationship */
  style: Style;
  style_id: Scalars['uuid'];
};

/** aggregated selection of "artist_style" */
export type ArtistStyleAggregate = {
  aggregate?: Maybe<ArtistStyleAggregateFields>;
  nodes: Array<ArtistStyle>;
};

/** aggregate fields of "artist_style" */
export type ArtistStyleAggregateFields = {
  avg?: Maybe<ArtistStyleAvgFields>;
  count?: Maybe<Scalars['Int']>;
  max?: Maybe<ArtistStyleMaxFields>;
  min?: Maybe<ArtistStyleMinFields>;
  stddev?: Maybe<ArtistStyleStddevFields>;
  stddev_pop?: Maybe<ArtistStyleStddevPopFields>;
  stddev_samp?: Maybe<ArtistStyleStddevSampFields>;
  sum?: Maybe<ArtistStyleSumFields>;
  var_pop?: Maybe<ArtistStyleVarPopFields>;
  var_samp?: Maybe<ArtistStyleVarSampFields>;
  variance?: Maybe<ArtistStyleVarianceFields>;
};


/** aggregate fields of "artist_style" */
export type ArtistStyleAggregateFieldsCountArgs = {
  columns?: Maybe<Array<ArtistStyleSelectColumn>>;
  distinct?: Maybe<Scalars['Boolean']>;
};

/** order by aggregate values of table "artist_style" */
export type ArtistStyleAggregateOrderBy = {
  avg?: Maybe<ArtistStyleAvgOrderBy>;
  count?: Maybe<OrderBy>;
  max?: Maybe<ArtistStyleMaxOrderBy>;
  min?: Maybe<ArtistStyleMinOrderBy>;
  stddev?: Maybe<ArtistStyleStddevOrderBy>;
  stddev_pop?: Maybe<ArtistStyleStddevPopOrderBy>;
  stddev_samp?: Maybe<ArtistStyleStddevSampOrderBy>;
  sum?: Maybe<ArtistStyleSumOrderBy>;
  var_pop?: Maybe<ArtistStyleVarPopOrderBy>;
  var_samp?: Maybe<ArtistStyleVarSampOrderBy>;
  variance?: Maybe<ArtistStyleVarianceOrderBy>;
};

/** input type for inserting array relation for remote table "artist_style" */
export type ArtistStyleArrRelInsertInput = {
  data: Array<ArtistStyleInsertInput>;
  on_conflict?: Maybe<ArtistStyleOnConflict>;
};

/** aggregate avg on columns */
export type ArtistStyleAvgFields = {
  index?: Maybe<Scalars['Float']>;
};

/** order by avg() on columns of table "artist_style" */
export type ArtistStyleAvgOrderBy = {
  index?: Maybe<OrderBy>;
};

/** Boolean expression to filter rows from the table "artist_style". All fields are combined with a logical 'AND'. */
export type ArtistStyleBoolExp = {
  _and?: Maybe<Array<Maybe<ArtistStyleBoolExp>>>;
  _not?: Maybe<ArtistStyleBoolExp>;
  _or?: Maybe<Array<Maybe<ArtistStyleBoolExp>>>;
  artist?: Maybe<ArtistBoolExp>;
  artist_id?: Maybe<UuidComparisonExp>;
  index?: Maybe<IntComparisonExp>;
  style?: Maybe<StyleBoolExp>;
  style_id?: Maybe<UuidComparisonExp>;
};

/** unique or primary key constraints on table "artist_style" */
export enum ArtistStyleConstraint {
  /** unique or primary key constraint */
  ArtistStyleArtistIdStyleIdKey = 'artist_style_artist_id_style_id_key',
  /** unique or primary key constraint */
  ArtistStylePkey = 'artist_style_pkey'
}

/** input type for incrementing integer column in table "artist_style" */
export type ArtistStyleIncInput = {
  index?: Maybe<Scalars['Int']>;
};

/** input type for inserting data into table "artist_style" */
export type ArtistStyleInsertInput = {
  artist?: Maybe<ArtistObjRelInsertInput>;
  artist_id?: Maybe<Scalars['uuid']>;
  index?: Maybe<Scalars['Int']>;
  style?: Maybe<StyleObjRelInsertInput>;
  style_id?: Maybe<Scalars['uuid']>;
};

/** aggregate max on columns */
export type ArtistStyleMaxFields = {
  artist_id?: Maybe<Scalars['uuid']>;
  index?: Maybe<Scalars['Int']>;
  style_id?: Maybe<Scalars['uuid']>;
};

/** order by max() on columns of table "artist_style" */
export type ArtistStyleMaxOrderBy = {
  artist_id?: Maybe<OrderBy>;
  index?: Maybe<OrderBy>;
  style_id?: Maybe<OrderBy>;
};

/** aggregate min on columns */
export type ArtistStyleMinFields = {
  artist_id?: Maybe<Scalars['uuid']>;
  index?: Maybe<Scalars['Int']>;
  style_id?: Maybe<Scalars['uuid']>;
};

/** order by min() on columns of table "artist_style" */
export type ArtistStyleMinOrderBy = {
  artist_id?: Maybe<OrderBy>;
  index?: Maybe<OrderBy>;
  style_id?: Maybe<OrderBy>;
};

/** response of any mutation on the table "artist_style" */
export type ArtistStyleMutationResponse = {
  /** number of affected rows by the mutation */
  affected_rows: Scalars['Int'];
  /** data of the affected rows by the mutation */
  returning: Array<ArtistStyle>;
};

/** input type for inserting object relation for remote table "artist_style" */
export type ArtistStyleObjRelInsertInput = {
  data: ArtistStyleInsertInput;
  on_conflict?: Maybe<ArtistStyleOnConflict>;
};

/** on conflict condition type for table "artist_style" */
export type ArtistStyleOnConflict = {
  constraint: ArtistStyleConstraint;
  update_columns: Array<ArtistStyleUpdateColumn>;
  where?: Maybe<ArtistStyleBoolExp>;
};

/** ordering options when selecting data from "artist_style" */
export type ArtistStyleOrderBy = {
  artist?: Maybe<ArtistOrderBy>;
  artist_id?: Maybe<OrderBy>;
  index?: Maybe<OrderBy>;
  style?: Maybe<StyleOrderBy>;
  style_id?: Maybe<OrderBy>;
};

/** primary key columns input for table: "artist_style" */
export type ArtistStylePkColumnsInput = {
  artist_id: Scalars['uuid'];
  index: Scalars['Int'];
};

/** select columns of table "artist_style" */
export enum ArtistStyleSelectColumn {
  /** column name */
  ArtistId = 'artist_id',
  /** column name */
  Index = 'index',
  /** column name */
  StyleId = 'style_id'
}

/** input type for updating data in table "artist_style" */
export type ArtistStyleSetInput = {
  artist_id?: Maybe<Scalars['uuid']>;
  index?: Maybe<Scalars['Int']>;
  style_id?: Maybe<Scalars['uuid']>;
};

/** aggregate stddev on columns */
export type ArtistStyleStddevFields = {
  index?: Maybe<Scalars['Float']>;
};

/** order by stddev() on columns of table "artist_style" */
export type ArtistStyleStddevOrderBy = {
  index?: Maybe<OrderBy>;
};

/** aggregate stddev_pop on columns */
export type ArtistStyleStddevPopFields = {
  index?: Maybe<Scalars['Float']>;
};

/** order by stddev_pop() on columns of table "artist_style" */
export type ArtistStyleStddevPopOrderBy = {
  index?: Maybe<OrderBy>;
};

/** aggregate stddev_samp on columns */
export type ArtistStyleStddevSampFields = {
  index?: Maybe<Scalars['Float']>;
};

/** order by stddev_samp() on columns of table "artist_style" */
export type ArtistStyleStddevSampOrderBy = {
  index?: Maybe<OrderBy>;
};

/** aggregate sum on columns */
export type ArtistStyleSumFields = {
  index?: Maybe<Scalars['Int']>;
};

/** order by sum() on columns of table "artist_style" */
export type ArtistStyleSumOrderBy = {
  index?: Maybe<OrderBy>;
};

/** update columns of table "artist_style" */
export enum ArtistStyleUpdateColumn {
  /** column name */
  ArtistId = 'artist_id',
  /** column name */
  Index = 'index',
  /** column name */
  StyleId = 'style_id'
}

/** aggregate var_pop on columns */
export type ArtistStyleVarPopFields = {
  index?: Maybe<Scalars['Float']>;
};

/** order by var_pop() on columns of table "artist_style" */
export type ArtistStyleVarPopOrderBy = {
  index?: Maybe<OrderBy>;
};

/** aggregate var_samp on columns */
export type ArtistStyleVarSampFields = {
  index?: Maybe<Scalars['Float']>;
};

/** order by var_samp() on columns of table "artist_style" */
export type ArtistStyleVarSampOrderBy = {
  index?: Maybe<OrderBy>;
};

/** aggregate variance on columns */
export type ArtistStyleVarianceFields = {
  index?: Maybe<Scalars['Float']>;
};

/** order by variance() on columns of table "artist_style" */
export type ArtistStyleVarianceOrderBy = {
  index?: Maybe<OrderBy>;
};

/** update columns of table "artist" */
export enum ArtistUpdateColumn {
  /** column name */
  Id = 'id',
  /** column name */
  ImageBackground = 'image_background',
  /** column name */
  ImageCover = 'image_cover',
  /** column name */
  PersonId = 'person_id',
  /** column name */
  Summary = 'summary'
}

/** columns and relationships of "collection" */
export type Collection = {
  /** An array relationship */
  album_collections: Array<AlbumCollection>;
  /** An aggregated array relationship */
  album_collections_aggregate: AlbumCollectionAggregate;
  /** An array relationship */
  artist_collections: Array<ArtistCollection>;
  /** An aggregated array relationship */
  artist_collections_aggregate: ArtistCollectionAggregate;
  id: Scalars['uuid'];
  image_background?: Maybe<Scalars['String']>;
  image_cover?: Maybe<Scalars['String']>;
  /** An array relationship */
  movie_collections: Array<MovieCollection>;
  /** An aggregated array relationship */
  movie_collections_aggregate: MovieCollectionAggregate;
  name: Scalars['String'];
  /** An array relationship */
  show_collections: Array<ShowCollection>;
  /** An aggregated array relationship */
  show_collections_aggregate: ShowCollectionAggregate;
  summary: Scalars['String'];
};


/** columns and relationships of "collection" */
export type CollectionAlbumCollectionsArgs = {
  distinct_on?: Maybe<Array<AlbumCollectionSelectColumn>>;
  limit?: Maybe<Scalars['Int']>;
  offset?: Maybe<Scalars['Int']>;
  order_by?: Maybe<Array<AlbumCollectionOrderBy>>;
  where?: Maybe<AlbumCollectionBoolExp>;
};


/** columns and relationships of "collection" */
export type CollectionAlbumCollectionsAggregateArgs = {
  distinct_on?: Maybe<Array<AlbumCollectionSelectColumn>>;
  limit?: Maybe<Scalars['Int']>;
  offset?: Maybe<Scalars['Int']>;
  order_by?: Maybe<Array<AlbumCollectionOrderBy>>;
  where?: Maybe<AlbumCollectionBoolExp>;
};


/** columns and relationships of "collection" */
export type CollectionArtistCollectionsArgs = {
  distinct_on?: Maybe<Array<ArtistCollectionSelectColumn>>;
  limit?: Maybe<Scalars['Int']>;
  offset?: Maybe<Scalars['Int']>;
  order_by?: Maybe<Array<ArtistCollectionOrderBy>>;
  where?: Maybe<ArtistCollectionBoolExp>;
};


/** columns and relationships of "collection" */
export type CollectionArtistCollectionsAggregateArgs = {
  distinct_on?: Maybe<Array<ArtistCollectionSelectColumn>>;
  limit?: Maybe<Scalars['Int']>;
  offset?: Maybe<Scalars['Int']>;
  order_by?: Maybe<Array<ArtistCollectionOrderBy>>;
  where?: Maybe<ArtistCollectionBoolExp>;
};


/** columns and relationships of "collection" */
export type CollectionMovieCollectionsArgs = {
  distinct_on?: Maybe<Array<MovieCollectionSelectColumn>>;
  limit?: Maybe<Scalars['Int']>;
  offset?: Maybe<Scalars['Int']>;
  order_by?: Maybe<Array<MovieCollectionOrderBy>>;
  where?: Maybe<MovieCollectionBoolExp>;
};


/** columns and relationships of "collection" */
export type CollectionMovieCollectionsAggregateArgs = {
  distinct_on?: Maybe<Array<MovieCollectionSelectColumn>>;
  limit?: Maybe<Scalars['Int']>;
  offset?: Maybe<Scalars['Int']>;
  order_by?: Maybe<Array<MovieCollectionOrderBy>>;
  where?: Maybe<MovieCollectionBoolExp>;
};


/** columns and relationships of "collection" */
export type CollectionShowCollectionsArgs = {
  distinct_on?: Maybe<Array<ShowCollectionSelectColumn>>;
  limit?: Maybe<Scalars['Int']>;
  offset?: Maybe<Scalars['Int']>;
  order_by?: Maybe<Array<ShowCollectionOrderBy>>;
  where?: Maybe<ShowCollectionBoolExp>;
};


/** columns and relationships of "collection" */
export type CollectionShowCollectionsAggregateArgs = {
  distinct_on?: Maybe<Array<ShowCollectionSelectColumn>>;
  limit?: Maybe<Scalars['Int']>;
  offset?: Maybe<Scalars['Int']>;
  order_by?: Maybe<Array<ShowCollectionOrderBy>>;
  where?: Maybe<ShowCollectionBoolExp>;
};

/** aggregated selection of "collection" */
export type CollectionAggregate = {
  aggregate?: Maybe<CollectionAggregateFields>;
  nodes: Array<Collection>;
};

/** aggregate fields of "collection" */
export type CollectionAggregateFields = {
  count?: Maybe<Scalars['Int']>;
  max?: Maybe<CollectionMaxFields>;
  min?: Maybe<CollectionMinFields>;
};


/** aggregate fields of "collection" */
export type CollectionAggregateFieldsCountArgs = {
  columns?: Maybe<Array<CollectionSelectColumn>>;
  distinct?: Maybe<Scalars['Boolean']>;
};

/** order by aggregate values of table "collection" */
export type CollectionAggregateOrderBy = {
  count?: Maybe<OrderBy>;
  max?: Maybe<CollectionMaxOrderBy>;
  min?: Maybe<CollectionMinOrderBy>;
};

/** input type for inserting array relation for remote table "collection" */
export type CollectionArrRelInsertInput = {
  data: Array<CollectionInsertInput>;
  on_conflict?: Maybe<CollectionOnConflict>;
};

/** Boolean expression to filter rows from the table "collection". All fields are combined with a logical 'AND'. */
export type CollectionBoolExp = {
  _and?: Maybe<Array<Maybe<CollectionBoolExp>>>;
  _not?: Maybe<CollectionBoolExp>;
  _or?: Maybe<Array<Maybe<CollectionBoolExp>>>;
  album_collections?: Maybe<AlbumCollectionBoolExp>;
  artist_collections?: Maybe<ArtistCollectionBoolExp>;
  id?: Maybe<UuidComparisonExp>;
  image_background?: Maybe<StringComparisonExp>;
  image_cover?: Maybe<StringComparisonExp>;
  movie_collections?: Maybe<MovieCollectionBoolExp>;
  name?: Maybe<StringComparisonExp>;
  show_collections?: Maybe<ShowCollectionBoolExp>;
  summary?: Maybe<StringComparisonExp>;
};

/** unique or primary key constraints on table "collection" */
export enum CollectionConstraint {
  /** unique or primary key constraint */
  CollectionNameKey = 'collection_name_key',
  /** unique or primary key constraint */
  CollectionPkey = 'collection_pkey'
}

/** input type for inserting data into table "collection" */
export type CollectionInsertInput = {
  album_collections?: Maybe<AlbumCollectionArrRelInsertInput>;
  artist_collections?: Maybe<ArtistCollectionArrRelInsertInput>;
  id?: Maybe<Scalars['uuid']>;
  image_background?: Maybe<Scalars['String']>;
  image_cover?: Maybe<Scalars['String']>;
  movie_collections?: Maybe<MovieCollectionArrRelInsertInput>;
  name?: Maybe<Scalars['String']>;
  show_collections?: Maybe<ShowCollectionArrRelInsertInput>;
  summary?: Maybe<Scalars['String']>;
};

/** aggregate max on columns */
export type CollectionMaxFields = {
  id?: Maybe<Scalars['uuid']>;
  image_background?: Maybe<Scalars['String']>;
  image_cover?: Maybe<Scalars['String']>;
  name?: Maybe<Scalars['String']>;
  summary?: Maybe<Scalars['String']>;
};

/** order by max() on columns of table "collection" */
export type CollectionMaxOrderBy = {
  id?: Maybe<OrderBy>;
  image_background?: Maybe<OrderBy>;
  image_cover?: Maybe<OrderBy>;
  name?: Maybe<OrderBy>;
  summary?: Maybe<OrderBy>;
};

/** aggregate min on columns */
export type CollectionMinFields = {
  id?: Maybe<Scalars['uuid']>;
  image_background?: Maybe<Scalars['String']>;
  image_cover?: Maybe<Scalars['String']>;
  name?: Maybe<Scalars['String']>;
  summary?: Maybe<Scalars['String']>;
};

/** order by min() on columns of table "collection" */
export type CollectionMinOrderBy = {
  id?: Maybe<OrderBy>;
  image_background?: Maybe<OrderBy>;
  image_cover?: Maybe<OrderBy>;
  name?: Maybe<OrderBy>;
  summary?: Maybe<OrderBy>;
};

/** response of any mutation on the table "collection" */
export type CollectionMutationResponse = {
  /** number of affected rows by the mutation */
  affected_rows: Scalars['Int'];
  /** data of the affected rows by the mutation */
  returning: Array<Collection>;
};

/** input type for inserting object relation for remote table "collection" */
export type CollectionObjRelInsertInput = {
  data: CollectionInsertInput;
  on_conflict?: Maybe<CollectionOnConflict>;
};

/** on conflict condition type for table "collection" */
export type CollectionOnConflict = {
  constraint: CollectionConstraint;
  update_columns: Array<CollectionUpdateColumn>;
  where?: Maybe<CollectionBoolExp>;
};

/** ordering options when selecting data from "collection" */
export type CollectionOrderBy = {
  album_collections_aggregate?: Maybe<AlbumCollectionAggregateOrderBy>;
  artist_collections_aggregate?: Maybe<ArtistCollectionAggregateOrderBy>;
  id?: Maybe<OrderBy>;
  image_background?: Maybe<OrderBy>;
  image_cover?: Maybe<OrderBy>;
  movie_collections_aggregate?: Maybe<MovieCollectionAggregateOrderBy>;
  name?: Maybe<OrderBy>;
  show_collections_aggregate?: Maybe<ShowCollectionAggregateOrderBy>;
  summary?: Maybe<OrderBy>;
};

/** primary key columns input for table: "collection" */
export type CollectionPkColumnsInput = {
  id: Scalars['uuid'];
};

/** select columns of table "collection" */
export enum CollectionSelectColumn {
  /** column name */
  Id = 'id',
  /** column name */
  ImageBackground = 'image_background',
  /** column name */
  ImageCover = 'image_cover',
  /** column name */
  Name = 'name',
  /** column name */
  Summary = 'summary'
}

/** input type for updating data in table "collection" */
export type CollectionSetInput = {
  id?: Maybe<Scalars['uuid']>;
  image_background?: Maybe<Scalars['String']>;
  image_cover?: Maybe<Scalars['String']>;
  name?: Maybe<Scalars['String']>;
  summary?: Maybe<Scalars['String']>;
};

/** update columns of table "collection" */
export enum CollectionUpdateColumn {
  /** column name */
  Id = 'id',
  /** column name */
  ImageBackground = 'image_background',
  /** column name */
  ImageCover = 'image_cover',
  /** column name */
  Name = 'name',
  /** column name */
  Summary = 'summary'
}

/** columns and relationships of "country" */
export type Country = {
  /** An array relationship */
  album_countries: Array<AlbumCountry>;
  /** An aggregated array relationship */
  album_countries_aggregate: AlbumCountryAggregate;
  /** An array relationship */
  artist_countries: Array<ArtistCountry>;
  /** An aggregated array relationship */
  artist_countries_aggregate: ArtistCountryAggregate;
  code: Scalars['String'];
  /** An array relationship */
  movie_countries: Array<MovieCountry>;
  /** An aggregated array relationship */
  movie_countries_aggregate: MovieCountryAggregate;
  name: Scalars['String'];
};


/** columns and relationships of "country" */
export type CountryAlbumCountriesArgs = {
  distinct_on?: Maybe<Array<AlbumCountrySelectColumn>>;
  limit?: Maybe<Scalars['Int']>;
  offset?: Maybe<Scalars['Int']>;
  order_by?: Maybe<Array<AlbumCountryOrderBy>>;
  where?: Maybe<AlbumCountryBoolExp>;
};


/** columns and relationships of "country" */
export type CountryAlbumCountriesAggregateArgs = {
  distinct_on?: Maybe<Array<AlbumCountrySelectColumn>>;
  limit?: Maybe<Scalars['Int']>;
  offset?: Maybe<Scalars['Int']>;
  order_by?: Maybe<Array<AlbumCountryOrderBy>>;
  where?: Maybe<AlbumCountryBoolExp>;
};


/** columns and relationships of "country" */
export type CountryArtistCountriesArgs = {
  distinct_on?: Maybe<Array<ArtistCountrySelectColumn>>;
  limit?: Maybe<Scalars['Int']>;
  offset?: Maybe<Scalars['Int']>;
  order_by?: Maybe<Array<ArtistCountryOrderBy>>;
  where?: Maybe<ArtistCountryBoolExp>;
};


/** columns and relationships of "country" */
export type CountryArtistCountriesAggregateArgs = {
  distinct_on?: Maybe<Array<ArtistCountrySelectColumn>>;
  limit?: Maybe<Scalars['Int']>;
  offset?: Maybe<Scalars['Int']>;
  order_by?: Maybe<Array<ArtistCountryOrderBy>>;
  where?: Maybe<ArtistCountryBoolExp>;
};


/** columns and relationships of "country" */
export type CountryMovieCountriesArgs = {
  distinct_on?: Maybe<Array<MovieCountrySelectColumn>>;
  limit?: Maybe<Scalars['Int']>;
  offset?: Maybe<Scalars['Int']>;
  order_by?: Maybe<Array<MovieCountryOrderBy>>;
  where?: Maybe<MovieCountryBoolExp>;
};


/** columns and relationships of "country" */
export type CountryMovieCountriesAggregateArgs = {
  distinct_on?: Maybe<Array<MovieCountrySelectColumn>>;
  limit?: Maybe<Scalars['Int']>;
  offset?: Maybe<Scalars['Int']>;
  order_by?: Maybe<Array<MovieCountryOrderBy>>;
  where?: Maybe<MovieCountryBoolExp>;
};

/** aggregated selection of "country" */
export type CountryAggregate = {
  aggregate?: Maybe<CountryAggregateFields>;
  nodes: Array<Country>;
};

/** aggregate fields of "country" */
export type CountryAggregateFields = {
  count?: Maybe<Scalars['Int']>;
  max?: Maybe<CountryMaxFields>;
  min?: Maybe<CountryMinFields>;
};


/** aggregate fields of "country" */
export type CountryAggregateFieldsCountArgs = {
  columns?: Maybe<Array<CountrySelectColumn>>;
  distinct?: Maybe<Scalars['Boolean']>;
};

/** order by aggregate values of table "country" */
export type CountryAggregateOrderBy = {
  count?: Maybe<OrderBy>;
  max?: Maybe<CountryMaxOrderBy>;
  min?: Maybe<CountryMinOrderBy>;
};

/** input type for inserting array relation for remote table "country" */
export type CountryArrRelInsertInput = {
  data: Array<CountryInsertInput>;
  on_conflict?: Maybe<CountryOnConflict>;
};

/** Boolean expression to filter rows from the table "country". All fields are combined with a logical 'AND'. */
export type CountryBoolExp = {
  _and?: Maybe<Array<Maybe<CountryBoolExp>>>;
  _not?: Maybe<CountryBoolExp>;
  _or?: Maybe<Array<Maybe<CountryBoolExp>>>;
  album_countries?: Maybe<AlbumCountryBoolExp>;
  artist_countries?: Maybe<ArtistCountryBoolExp>;
  code?: Maybe<StringComparisonExp>;
  movie_countries?: Maybe<MovieCountryBoolExp>;
  name?: Maybe<StringComparisonExp>;
};

/** unique or primary key constraints on table "country" */
export enum CountryConstraint {
  /** unique or primary key constraint */
  CountryNameKey = 'country_name_key',
  /** unique or primary key constraint */
  CountryPkey = 'country_pkey'
}

/** input type for inserting data into table "country" */
export type CountryInsertInput = {
  album_countries?: Maybe<AlbumCountryArrRelInsertInput>;
  artist_countries?: Maybe<ArtistCountryArrRelInsertInput>;
  code?: Maybe<Scalars['String']>;
  movie_countries?: Maybe<MovieCountryArrRelInsertInput>;
  name?: Maybe<Scalars['String']>;
};

/** aggregate max on columns */
export type CountryMaxFields = {
  code?: Maybe<Scalars['String']>;
  name?: Maybe<Scalars['String']>;
};

/** order by max() on columns of table "country" */
export type CountryMaxOrderBy = {
  code?: Maybe<OrderBy>;
  name?: Maybe<OrderBy>;
};

/** aggregate min on columns */
export type CountryMinFields = {
  code?: Maybe<Scalars['String']>;
  name?: Maybe<Scalars['String']>;
};

/** order by min() on columns of table "country" */
export type CountryMinOrderBy = {
  code?: Maybe<OrderBy>;
  name?: Maybe<OrderBy>;
};

/** response of any mutation on the table "country" */
export type CountryMutationResponse = {
  /** number of affected rows by the mutation */
  affected_rows: Scalars['Int'];
  /** data of the affected rows by the mutation */
  returning: Array<Country>;
};

/** input type for inserting object relation for remote table "country" */
export type CountryObjRelInsertInput = {
  data: CountryInsertInput;
  on_conflict?: Maybe<CountryOnConflict>;
};

/** on conflict condition type for table "country" */
export type CountryOnConflict = {
  constraint: CountryConstraint;
  update_columns: Array<CountryUpdateColumn>;
  where?: Maybe<CountryBoolExp>;
};

/** ordering options when selecting data from "country" */
export type CountryOrderBy = {
  album_countries_aggregate?: Maybe<AlbumCountryAggregateOrderBy>;
  artist_countries_aggregate?: Maybe<ArtistCountryAggregateOrderBy>;
  code?: Maybe<OrderBy>;
  movie_countries_aggregate?: Maybe<MovieCountryAggregateOrderBy>;
  name?: Maybe<OrderBy>;
};

/** primary key columns input for table: "country" */
export type CountryPkColumnsInput = {
  code: Scalars['String'];
};

/** select columns of table "country" */
export enum CountrySelectColumn {
  /** column name */
  Code = 'code',
  /** column name */
  Name = 'name'
}

/** input type for updating data in table "country" */
export type CountrySetInput = {
  code?: Maybe<Scalars['String']>;
  name?: Maybe<Scalars['String']>;
};

/** update columns of table "country" */
export enum CountryUpdateColumn {
  /** column name */
  Code = 'code',
  /** column name */
  Name = 'name'
}


/** expression to compare columns of type date. All fields are combined with logical 'AND'. */
export type DateComparisonExp = {
  _eq?: Maybe<Scalars['date']>;
  _gt?: Maybe<Scalars['date']>;
  _gte?: Maybe<Scalars['date']>;
  _in?: Maybe<Array<Scalars['date']>>;
  _is_null?: Maybe<Scalars['Boolean']>;
  _lt?: Maybe<Scalars['date']>;
  _lte?: Maybe<Scalars['date']>;
  _neq?: Maybe<Scalars['date']>;
  _nin?: Maybe<Array<Scalars['date']>>;
};

/** columns and relationships of "episode" */
export type Episode = {
  aired?: Maybe<Scalars['date']>;
  content_rating: Scalars['String'];
  /** An array relationship */
  episode_staffs: Array<EpisodeStaff>;
  /** An aggregated array relationship */
  episode_staffs_aggregate: EpisodeStaffAggregate;
  id: Scalars['uuid'];
  image_cover?: Maybe<Scalars['String']>;
  name: Scalars['_text'];
  name_original?: Maybe<Scalars['_text']>;
  name_sort?: Maybe<Scalars['String']>;
  rating?: Maybe<Scalars['numeric']>;
  summary: Scalars['String'];
};


/** columns and relationships of "episode" */
export type EpisodeEpisodeStaffsArgs = {
  distinct_on?: Maybe<Array<EpisodeStaffSelectColumn>>;
  limit?: Maybe<Scalars['Int']>;
  offset?: Maybe<Scalars['Int']>;
  order_by?: Maybe<Array<EpisodeStaffOrderBy>>;
  where?: Maybe<EpisodeStaffBoolExp>;
};


/** columns and relationships of "episode" */
export type EpisodeEpisodeStaffsAggregateArgs = {
  distinct_on?: Maybe<Array<EpisodeStaffSelectColumn>>;
  limit?: Maybe<Scalars['Int']>;
  offset?: Maybe<Scalars['Int']>;
  order_by?: Maybe<Array<EpisodeStaffOrderBy>>;
  where?: Maybe<EpisodeStaffBoolExp>;
};

/** aggregated selection of "episode" */
export type EpisodeAggregate = {
  aggregate?: Maybe<EpisodeAggregateFields>;
  nodes: Array<Episode>;
};

/** aggregate fields of "episode" */
export type EpisodeAggregateFields = {
  avg?: Maybe<EpisodeAvgFields>;
  count?: Maybe<Scalars['Int']>;
  max?: Maybe<EpisodeMaxFields>;
  min?: Maybe<EpisodeMinFields>;
  stddev?: Maybe<EpisodeStddevFields>;
  stddev_pop?: Maybe<EpisodeStddevPopFields>;
  stddev_samp?: Maybe<EpisodeStddevSampFields>;
  sum?: Maybe<EpisodeSumFields>;
  var_pop?: Maybe<EpisodeVarPopFields>;
  var_samp?: Maybe<EpisodeVarSampFields>;
  variance?: Maybe<EpisodeVarianceFields>;
};


/** aggregate fields of "episode" */
export type EpisodeAggregateFieldsCountArgs = {
  columns?: Maybe<Array<EpisodeSelectColumn>>;
  distinct?: Maybe<Scalars['Boolean']>;
};

/** order by aggregate values of table "episode" */
export type EpisodeAggregateOrderBy = {
  avg?: Maybe<EpisodeAvgOrderBy>;
  count?: Maybe<OrderBy>;
  max?: Maybe<EpisodeMaxOrderBy>;
  min?: Maybe<EpisodeMinOrderBy>;
  stddev?: Maybe<EpisodeStddevOrderBy>;
  stddev_pop?: Maybe<EpisodeStddevPopOrderBy>;
  stddev_samp?: Maybe<EpisodeStddevSampOrderBy>;
  sum?: Maybe<EpisodeSumOrderBy>;
  var_pop?: Maybe<EpisodeVarPopOrderBy>;
  var_samp?: Maybe<EpisodeVarSampOrderBy>;
  variance?: Maybe<EpisodeVarianceOrderBy>;
};

/** input type for inserting array relation for remote table "episode" */
export type EpisodeArrRelInsertInput = {
  data: Array<EpisodeInsertInput>;
  on_conflict?: Maybe<EpisodeOnConflict>;
};

/** aggregate avg on columns */
export type EpisodeAvgFields = {
  rating?: Maybe<Scalars['Float']>;
};

/** order by avg() on columns of table "episode" */
export type EpisodeAvgOrderBy = {
  rating?: Maybe<OrderBy>;
};

/** Boolean expression to filter rows from the table "episode". All fields are combined with a logical 'AND'. */
export type EpisodeBoolExp = {
  _and?: Maybe<Array<Maybe<EpisodeBoolExp>>>;
  _not?: Maybe<EpisodeBoolExp>;
  _or?: Maybe<Array<Maybe<EpisodeBoolExp>>>;
  aired?: Maybe<DateComparisonExp>;
  content_rating?: Maybe<StringComparisonExp>;
  episode_staffs?: Maybe<EpisodeStaffBoolExp>;
  id?: Maybe<UuidComparisonExp>;
  image_cover?: Maybe<StringComparisonExp>;
  name?: Maybe<TextComparisonExp>;
  name_original?: Maybe<TextComparisonExp>;
  name_sort?: Maybe<StringComparisonExp>;
  rating?: Maybe<NumericComparisonExp>;
  summary?: Maybe<StringComparisonExp>;
};

/** unique or primary key constraints on table "episode" */
export enum EpisodeConstraint {
  /** unique or primary key constraint */
  EpisodePkey = 'episode_pkey'
}

/** input type for incrementing integer column in table "episode" */
export type EpisodeIncInput = {
  rating?: Maybe<Scalars['numeric']>;
};

/** input type for inserting data into table "episode" */
export type EpisodeInsertInput = {
  aired?: Maybe<Scalars['date']>;
  content_rating?: Maybe<Scalars['String']>;
  episode_staffs?: Maybe<EpisodeStaffArrRelInsertInput>;
  id?: Maybe<Scalars['uuid']>;
  image_cover?: Maybe<Scalars['String']>;
  name?: Maybe<Scalars['_text']>;
  name_original?: Maybe<Scalars['_text']>;
  name_sort?: Maybe<Scalars['String']>;
  rating?: Maybe<Scalars['numeric']>;
  summary?: Maybe<Scalars['String']>;
};

/** aggregate max on columns */
export type EpisodeMaxFields = {
  aired?: Maybe<Scalars['date']>;
  content_rating?: Maybe<Scalars['String']>;
  id?: Maybe<Scalars['uuid']>;
  image_cover?: Maybe<Scalars['String']>;
  name_sort?: Maybe<Scalars['String']>;
  rating?: Maybe<Scalars['numeric']>;
  summary?: Maybe<Scalars['String']>;
};

/** order by max() on columns of table "episode" */
export type EpisodeMaxOrderBy = {
  aired?: Maybe<OrderBy>;
  content_rating?: Maybe<OrderBy>;
  id?: Maybe<OrderBy>;
  image_cover?: Maybe<OrderBy>;
  name_sort?: Maybe<OrderBy>;
  rating?: Maybe<OrderBy>;
  summary?: Maybe<OrderBy>;
};

/** aggregate min on columns */
export type EpisodeMinFields = {
  aired?: Maybe<Scalars['date']>;
  content_rating?: Maybe<Scalars['String']>;
  id?: Maybe<Scalars['uuid']>;
  image_cover?: Maybe<Scalars['String']>;
  name_sort?: Maybe<Scalars['String']>;
  rating?: Maybe<Scalars['numeric']>;
  summary?: Maybe<Scalars['String']>;
};

/** order by min() on columns of table "episode" */
export type EpisodeMinOrderBy = {
  aired?: Maybe<OrderBy>;
  content_rating?: Maybe<OrderBy>;
  id?: Maybe<OrderBy>;
  image_cover?: Maybe<OrderBy>;
  name_sort?: Maybe<OrderBy>;
  rating?: Maybe<OrderBy>;
  summary?: Maybe<OrderBy>;
};

/** response of any mutation on the table "episode" */
export type EpisodeMutationResponse = {
  /** number of affected rows by the mutation */
  affected_rows: Scalars['Int'];
  /** data of the affected rows by the mutation */
  returning: Array<Episode>;
};

/** input type for inserting object relation for remote table "episode" */
export type EpisodeObjRelInsertInput = {
  data: EpisodeInsertInput;
  on_conflict?: Maybe<EpisodeOnConflict>;
};

/** on conflict condition type for table "episode" */
export type EpisodeOnConflict = {
  constraint: EpisodeConstraint;
  update_columns: Array<EpisodeUpdateColumn>;
  where?: Maybe<EpisodeBoolExp>;
};

/** ordering options when selecting data from "episode" */
export type EpisodeOrderBy = {
  aired?: Maybe<OrderBy>;
  content_rating?: Maybe<OrderBy>;
  episode_staffs_aggregate?: Maybe<EpisodeStaffAggregateOrderBy>;
  id?: Maybe<OrderBy>;
  image_cover?: Maybe<OrderBy>;
  name?: Maybe<OrderBy>;
  name_original?: Maybe<OrderBy>;
  name_sort?: Maybe<OrderBy>;
  rating?: Maybe<OrderBy>;
  summary?: Maybe<OrderBy>;
};

/** primary key columns input for table: "episode" */
export type EpisodePkColumnsInput = {
  id: Scalars['uuid'];
};

/** select columns of table "episode" */
export enum EpisodeSelectColumn {
  /** column name */
  Aired = 'aired',
  /** column name */
  ContentRating = 'content_rating',
  /** column name */
  Id = 'id',
  /** column name */
  ImageCover = 'image_cover',
  /** column name */
  Name = 'name',
  /** column name */
  NameOriginal = 'name_original',
  /** column name */
  NameSort = 'name_sort',
  /** column name */
  Rating = 'rating',
  /** column name */
  Summary = 'summary'
}

/** input type for updating data in table "episode" */
export type EpisodeSetInput = {
  aired?: Maybe<Scalars['date']>;
  content_rating?: Maybe<Scalars['String']>;
  id?: Maybe<Scalars['uuid']>;
  image_cover?: Maybe<Scalars['String']>;
  name?: Maybe<Scalars['_text']>;
  name_original?: Maybe<Scalars['_text']>;
  name_sort?: Maybe<Scalars['String']>;
  rating?: Maybe<Scalars['numeric']>;
  summary?: Maybe<Scalars['String']>;
};

/** columns and relationships of "episode_staff" */
export type EpisodeStaff = {
  custom_staff_name?: Maybe<Scalars['String']>;
  /** An object relationship */
  episode: Episode;
  episode_id: Scalars['uuid'];
  id: Scalars['uuid'];
  index: Scalars['Int'];
  /** An object relationship */
  person: Person;
  person_id: Scalars['uuid'];
  /** An object relationship */
  staff: Staff;
  staff_id: Scalars['uuid'];
};

/** aggregated selection of "episode_staff" */
export type EpisodeStaffAggregate = {
  aggregate?: Maybe<EpisodeStaffAggregateFields>;
  nodes: Array<EpisodeStaff>;
};

/** aggregate fields of "episode_staff" */
export type EpisodeStaffAggregateFields = {
  avg?: Maybe<EpisodeStaffAvgFields>;
  count?: Maybe<Scalars['Int']>;
  max?: Maybe<EpisodeStaffMaxFields>;
  min?: Maybe<EpisodeStaffMinFields>;
  stddev?: Maybe<EpisodeStaffStddevFields>;
  stddev_pop?: Maybe<EpisodeStaffStddevPopFields>;
  stddev_samp?: Maybe<EpisodeStaffStddevSampFields>;
  sum?: Maybe<EpisodeStaffSumFields>;
  var_pop?: Maybe<EpisodeStaffVarPopFields>;
  var_samp?: Maybe<EpisodeStaffVarSampFields>;
  variance?: Maybe<EpisodeStaffVarianceFields>;
};


/** aggregate fields of "episode_staff" */
export type EpisodeStaffAggregateFieldsCountArgs = {
  columns?: Maybe<Array<EpisodeStaffSelectColumn>>;
  distinct?: Maybe<Scalars['Boolean']>;
};

/** order by aggregate values of table "episode_staff" */
export type EpisodeStaffAggregateOrderBy = {
  avg?: Maybe<EpisodeStaffAvgOrderBy>;
  count?: Maybe<OrderBy>;
  max?: Maybe<EpisodeStaffMaxOrderBy>;
  min?: Maybe<EpisodeStaffMinOrderBy>;
  stddev?: Maybe<EpisodeStaffStddevOrderBy>;
  stddev_pop?: Maybe<EpisodeStaffStddevPopOrderBy>;
  stddev_samp?: Maybe<EpisodeStaffStddevSampOrderBy>;
  sum?: Maybe<EpisodeStaffSumOrderBy>;
  var_pop?: Maybe<EpisodeStaffVarPopOrderBy>;
  var_samp?: Maybe<EpisodeStaffVarSampOrderBy>;
  variance?: Maybe<EpisodeStaffVarianceOrderBy>;
};

/** input type for inserting array relation for remote table "episode_staff" */
export type EpisodeStaffArrRelInsertInput = {
  data: Array<EpisodeStaffInsertInput>;
  on_conflict?: Maybe<EpisodeStaffOnConflict>;
};

/** aggregate avg on columns */
export type EpisodeStaffAvgFields = {
  index?: Maybe<Scalars['Float']>;
};

/** order by avg() on columns of table "episode_staff" */
export type EpisodeStaffAvgOrderBy = {
  index?: Maybe<OrderBy>;
};

/** Boolean expression to filter rows from the table "episode_staff". All fields are combined with a logical 'AND'. */
export type EpisodeStaffBoolExp = {
  _and?: Maybe<Array<Maybe<EpisodeStaffBoolExp>>>;
  _not?: Maybe<EpisodeStaffBoolExp>;
  _or?: Maybe<Array<Maybe<EpisodeStaffBoolExp>>>;
  custom_staff_name?: Maybe<StringComparisonExp>;
  episode?: Maybe<EpisodeBoolExp>;
  episode_id?: Maybe<UuidComparisonExp>;
  id?: Maybe<UuidComparisonExp>;
  index?: Maybe<IntComparisonExp>;
  person?: Maybe<PersonBoolExp>;
  person_id?: Maybe<UuidComparisonExp>;
  staff?: Maybe<StaffBoolExp>;
  staff_id?: Maybe<UuidComparisonExp>;
};

/** unique or primary key constraints on table "episode_staff" */
export enum EpisodeStaffConstraint {
  /** unique or primary key constraint */
  EpisodeStaffEpisodeIdStaffIdIndexKey = 'episode_staff_episode_id_staff_id_index_key',
  /** unique or primary key constraint */
  EpisodeStaffEpisodeIdStaffIdPersonIdKey = 'episode_staff_episode_id_staff_id_person_id_key',
  /** unique or primary key constraint */
  EpisodeStaffPkey = 'episode_staff_pkey'
}

/** input type for incrementing integer column in table "episode_staff" */
export type EpisodeStaffIncInput = {
  index?: Maybe<Scalars['Int']>;
};

/** input type for inserting data into table "episode_staff" */
export type EpisodeStaffInsertInput = {
  custom_staff_name?: Maybe<Scalars['String']>;
  episode?: Maybe<EpisodeObjRelInsertInput>;
  episode_id?: Maybe<Scalars['uuid']>;
  id?: Maybe<Scalars['uuid']>;
  index?: Maybe<Scalars['Int']>;
  person?: Maybe<PersonObjRelInsertInput>;
  person_id?: Maybe<Scalars['uuid']>;
  staff?: Maybe<StaffObjRelInsertInput>;
  staff_id?: Maybe<Scalars['uuid']>;
};

/** aggregate max on columns */
export type EpisodeStaffMaxFields = {
  custom_staff_name?: Maybe<Scalars['String']>;
  episode_id?: Maybe<Scalars['uuid']>;
  id?: Maybe<Scalars['uuid']>;
  index?: Maybe<Scalars['Int']>;
  person_id?: Maybe<Scalars['uuid']>;
  staff_id?: Maybe<Scalars['uuid']>;
};

/** order by max() on columns of table "episode_staff" */
export type EpisodeStaffMaxOrderBy = {
  custom_staff_name?: Maybe<OrderBy>;
  episode_id?: Maybe<OrderBy>;
  id?: Maybe<OrderBy>;
  index?: Maybe<OrderBy>;
  person_id?: Maybe<OrderBy>;
  staff_id?: Maybe<OrderBy>;
};

/** aggregate min on columns */
export type EpisodeStaffMinFields = {
  custom_staff_name?: Maybe<Scalars['String']>;
  episode_id?: Maybe<Scalars['uuid']>;
  id?: Maybe<Scalars['uuid']>;
  index?: Maybe<Scalars['Int']>;
  person_id?: Maybe<Scalars['uuid']>;
  staff_id?: Maybe<Scalars['uuid']>;
};

/** order by min() on columns of table "episode_staff" */
export type EpisodeStaffMinOrderBy = {
  custom_staff_name?: Maybe<OrderBy>;
  episode_id?: Maybe<OrderBy>;
  id?: Maybe<OrderBy>;
  index?: Maybe<OrderBy>;
  person_id?: Maybe<OrderBy>;
  staff_id?: Maybe<OrderBy>;
};

/** response of any mutation on the table "episode_staff" */
export type EpisodeStaffMutationResponse = {
  /** number of affected rows by the mutation */
  affected_rows: Scalars['Int'];
  /** data of the affected rows by the mutation */
  returning: Array<EpisodeStaff>;
};

/** input type for inserting object relation for remote table "episode_staff" */
export type EpisodeStaffObjRelInsertInput = {
  data: EpisodeStaffInsertInput;
  on_conflict?: Maybe<EpisodeStaffOnConflict>;
};

/** on conflict condition type for table "episode_staff" */
export type EpisodeStaffOnConflict = {
  constraint: EpisodeStaffConstraint;
  update_columns: Array<EpisodeStaffUpdateColumn>;
  where?: Maybe<EpisodeStaffBoolExp>;
};

/** ordering options when selecting data from "episode_staff" */
export type EpisodeStaffOrderBy = {
  custom_staff_name?: Maybe<OrderBy>;
  episode?: Maybe<EpisodeOrderBy>;
  episode_id?: Maybe<OrderBy>;
  id?: Maybe<OrderBy>;
  index?: Maybe<OrderBy>;
  person?: Maybe<PersonOrderBy>;
  person_id?: Maybe<OrderBy>;
  staff?: Maybe<StaffOrderBy>;
  staff_id?: Maybe<OrderBy>;
};

/** primary key columns input for table: "episode_staff" */
export type EpisodeStaffPkColumnsInput = {
  id: Scalars['uuid'];
};

/** select columns of table "episode_staff" */
export enum EpisodeStaffSelectColumn {
  /** column name */
  CustomStaffName = 'custom_staff_name',
  /** column name */
  EpisodeId = 'episode_id',
  /** column name */
  Id = 'id',
  /** column name */
  Index = 'index',
  /** column name */
  PersonId = 'person_id',
  /** column name */
  StaffId = 'staff_id'
}

/** input type for updating data in table "episode_staff" */
export type EpisodeStaffSetInput = {
  custom_staff_name?: Maybe<Scalars['String']>;
  episode_id?: Maybe<Scalars['uuid']>;
  id?: Maybe<Scalars['uuid']>;
  index?: Maybe<Scalars['Int']>;
  person_id?: Maybe<Scalars['uuid']>;
  staff_id?: Maybe<Scalars['uuid']>;
};

/** aggregate stddev on columns */
export type EpisodeStaffStddevFields = {
  index?: Maybe<Scalars['Float']>;
};

/** order by stddev() on columns of table "episode_staff" */
export type EpisodeStaffStddevOrderBy = {
  index?: Maybe<OrderBy>;
};

/** aggregate stddev_pop on columns */
export type EpisodeStaffStddevPopFields = {
  index?: Maybe<Scalars['Float']>;
};

/** order by stddev_pop() on columns of table "episode_staff" */
export type EpisodeStaffStddevPopOrderBy = {
  index?: Maybe<OrderBy>;
};

/** aggregate stddev_samp on columns */
export type EpisodeStaffStddevSampFields = {
  index?: Maybe<Scalars['Float']>;
};

/** order by stddev_samp() on columns of table "episode_staff" */
export type EpisodeStaffStddevSampOrderBy = {
  index?: Maybe<OrderBy>;
};

/** aggregate sum on columns */
export type EpisodeStaffSumFields = {
  index?: Maybe<Scalars['Int']>;
};

/** order by sum() on columns of table "episode_staff" */
export type EpisodeStaffSumOrderBy = {
  index?: Maybe<OrderBy>;
};

/** update columns of table "episode_staff" */
export enum EpisodeStaffUpdateColumn {
  /** column name */
  CustomStaffName = 'custom_staff_name',
  /** column name */
  EpisodeId = 'episode_id',
  /** column name */
  Id = 'id',
  /** column name */
  Index = 'index',
  /** column name */
  PersonId = 'person_id',
  /** column name */
  StaffId = 'staff_id'
}

/** aggregate var_pop on columns */
export type EpisodeStaffVarPopFields = {
  index?: Maybe<Scalars['Float']>;
};

/** order by var_pop() on columns of table "episode_staff" */
export type EpisodeStaffVarPopOrderBy = {
  index?: Maybe<OrderBy>;
};

/** aggregate var_samp on columns */
export type EpisodeStaffVarSampFields = {
  index?: Maybe<Scalars['Float']>;
};

/** order by var_samp() on columns of table "episode_staff" */
export type EpisodeStaffVarSampOrderBy = {
  index?: Maybe<OrderBy>;
};

/** aggregate variance on columns */
export type EpisodeStaffVarianceFields = {
  index?: Maybe<Scalars['Float']>;
};

/** order by variance() on columns of table "episode_staff" */
export type EpisodeStaffVarianceOrderBy = {
  index?: Maybe<OrderBy>;
};

/** aggregate stddev on columns */
export type EpisodeStddevFields = {
  rating?: Maybe<Scalars['Float']>;
};

/** order by stddev() on columns of table "episode" */
export type EpisodeStddevOrderBy = {
  rating?: Maybe<OrderBy>;
};

/** aggregate stddev_pop on columns */
export type EpisodeStddevPopFields = {
  rating?: Maybe<Scalars['Float']>;
};

/** order by stddev_pop() on columns of table "episode" */
export type EpisodeStddevPopOrderBy = {
  rating?: Maybe<OrderBy>;
};

/** aggregate stddev_samp on columns */
export type EpisodeStddevSampFields = {
  rating?: Maybe<Scalars['Float']>;
};

/** order by stddev_samp() on columns of table "episode" */
export type EpisodeStddevSampOrderBy = {
  rating?: Maybe<OrderBy>;
};

/** aggregate sum on columns */
export type EpisodeSumFields = {
  rating?: Maybe<Scalars['numeric']>;
};

/** order by sum() on columns of table "episode" */
export type EpisodeSumOrderBy = {
  rating?: Maybe<OrderBy>;
};

/** update columns of table "episode" */
export enum EpisodeUpdateColumn {
  /** column name */
  Aired = 'aired',
  /** column name */
  ContentRating = 'content_rating',
  /** column name */
  Id = 'id',
  /** column name */
  ImageCover = 'image_cover',
  /** column name */
  Name = 'name',
  /** column name */
  NameOriginal = 'name_original',
  /** column name */
  NameSort = 'name_sort',
  /** column name */
  Rating = 'rating',
  /** column name */
  Summary = 'summary'
}

/** aggregate var_pop on columns */
export type EpisodeVarPopFields = {
  rating?: Maybe<Scalars['Float']>;
};

/** order by var_pop() on columns of table "episode" */
export type EpisodeVarPopOrderBy = {
  rating?: Maybe<OrderBy>;
};

/** aggregate var_samp on columns */
export type EpisodeVarSampFields = {
  rating?: Maybe<Scalars['Float']>;
};

/** order by var_samp() on columns of table "episode" */
export type EpisodeVarSampOrderBy = {
  rating?: Maybe<OrderBy>;
};

/** aggregate variance on columns */
export type EpisodeVarianceFields = {
  rating?: Maybe<Scalars['Float']>;
};

/** order by variance() on columns of table "episode" */
export type EpisodeVarianceOrderBy = {
  rating?: Maybe<OrderBy>;
};

/** columns and relationships of "genre" */
export type Genre = {
  /** An array relationship */
  album_genres: Array<AlbumGenre>;
  /** An aggregated array relationship */
  album_genres_aggregate: AlbumGenreAggregate;
  /** An array relationship */
  artist_genres: Array<ArtistGenre>;
  /** An aggregated array relationship */
  artist_genres_aggregate: ArtistGenreAggregate;
  id: Scalars['uuid'];
  /** An array relationship */
  movie_genres: Array<MovieGenre>;
  /** An aggregated array relationship */
  movie_genres_aggregate: MovieGenreAggregate;
  name: Scalars['String'];
  /** An array relationship */
  show_genres: Array<ShowGenre>;
  /** An aggregated array relationship */
  show_genres_aggregate: ShowGenreAggregate;
  /** An array relationship */
  track_genres: Array<TrackGenre>;
  /** An aggregated array relationship */
  track_genres_aggregate: TrackGenreAggregate;
};


/** columns and relationships of "genre" */
export type GenreAlbumGenresArgs = {
  distinct_on?: Maybe<Array<AlbumGenreSelectColumn>>;
  limit?: Maybe<Scalars['Int']>;
  offset?: Maybe<Scalars['Int']>;
  order_by?: Maybe<Array<AlbumGenreOrderBy>>;
  where?: Maybe<AlbumGenreBoolExp>;
};


/** columns and relationships of "genre" */
export type GenreAlbumGenresAggregateArgs = {
  distinct_on?: Maybe<Array<AlbumGenreSelectColumn>>;
  limit?: Maybe<Scalars['Int']>;
  offset?: Maybe<Scalars['Int']>;
  order_by?: Maybe<Array<AlbumGenreOrderBy>>;
  where?: Maybe<AlbumGenreBoolExp>;
};


/** columns and relationships of "genre" */
export type GenreArtistGenresArgs = {
  distinct_on?: Maybe<Array<ArtistGenreSelectColumn>>;
  limit?: Maybe<Scalars['Int']>;
  offset?: Maybe<Scalars['Int']>;
  order_by?: Maybe<Array<ArtistGenreOrderBy>>;
  where?: Maybe<ArtistGenreBoolExp>;
};


/** columns and relationships of "genre" */
export type GenreArtistGenresAggregateArgs = {
  distinct_on?: Maybe<Array<ArtistGenreSelectColumn>>;
  limit?: Maybe<Scalars['Int']>;
  offset?: Maybe<Scalars['Int']>;
  order_by?: Maybe<Array<ArtistGenreOrderBy>>;
  where?: Maybe<ArtistGenreBoolExp>;
};


/** columns and relationships of "genre" */
export type GenreMovieGenresArgs = {
  distinct_on?: Maybe<Array<MovieGenreSelectColumn>>;
  limit?: Maybe<Scalars['Int']>;
  offset?: Maybe<Scalars['Int']>;
  order_by?: Maybe<Array<MovieGenreOrderBy>>;
  where?: Maybe<MovieGenreBoolExp>;
};


/** columns and relationships of "genre" */
export type GenreMovieGenresAggregateArgs = {
  distinct_on?: Maybe<Array<MovieGenreSelectColumn>>;
  limit?: Maybe<Scalars['Int']>;
  offset?: Maybe<Scalars['Int']>;
  order_by?: Maybe<Array<MovieGenreOrderBy>>;
  where?: Maybe<MovieGenreBoolExp>;
};


/** columns and relationships of "genre" */
export type GenreShowGenresArgs = {
  distinct_on?: Maybe<Array<ShowGenreSelectColumn>>;
  limit?: Maybe<Scalars['Int']>;
  offset?: Maybe<Scalars['Int']>;
  order_by?: Maybe<Array<ShowGenreOrderBy>>;
  where?: Maybe<ShowGenreBoolExp>;
};


/** columns and relationships of "genre" */
export type GenreShowGenresAggregateArgs = {
  distinct_on?: Maybe<Array<ShowGenreSelectColumn>>;
  limit?: Maybe<Scalars['Int']>;
  offset?: Maybe<Scalars['Int']>;
  order_by?: Maybe<Array<ShowGenreOrderBy>>;
  where?: Maybe<ShowGenreBoolExp>;
};


/** columns and relationships of "genre" */
export type GenreTrackGenresArgs = {
  distinct_on?: Maybe<Array<TrackGenreSelectColumn>>;
  limit?: Maybe<Scalars['Int']>;
  offset?: Maybe<Scalars['Int']>;
  order_by?: Maybe<Array<TrackGenreOrderBy>>;
  where?: Maybe<TrackGenreBoolExp>;
};


/** columns and relationships of "genre" */
export type GenreTrackGenresAggregateArgs = {
  distinct_on?: Maybe<Array<TrackGenreSelectColumn>>;
  limit?: Maybe<Scalars['Int']>;
  offset?: Maybe<Scalars['Int']>;
  order_by?: Maybe<Array<TrackGenreOrderBy>>;
  where?: Maybe<TrackGenreBoolExp>;
};

/** aggregated selection of "genre" */
export type GenreAggregate = {
  aggregate?: Maybe<GenreAggregateFields>;
  nodes: Array<Genre>;
};

/** aggregate fields of "genre" */
export type GenreAggregateFields = {
  count?: Maybe<Scalars['Int']>;
  max?: Maybe<GenreMaxFields>;
  min?: Maybe<GenreMinFields>;
};


/** aggregate fields of "genre" */
export type GenreAggregateFieldsCountArgs = {
  columns?: Maybe<Array<GenreSelectColumn>>;
  distinct?: Maybe<Scalars['Boolean']>;
};

/** order by aggregate values of table "genre" */
export type GenreAggregateOrderBy = {
  count?: Maybe<OrderBy>;
  max?: Maybe<GenreMaxOrderBy>;
  min?: Maybe<GenreMinOrderBy>;
};

/** input type for inserting array relation for remote table "genre" */
export type GenreArrRelInsertInput = {
  data: Array<GenreInsertInput>;
  on_conflict?: Maybe<GenreOnConflict>;
};

/** Boolean expression to filter rows from the table "genre". All fields are combined with a logical 'AND'. */
export type GenreBoolExp = {
  _and?: Maybe<Array<Maybe<GenreBoolExp>>>;
  _not?: Maybe<GenreBoolExp>;
  _or?: Maybe<Array<Maybe<GenreBoolExp>>>;
  album_genres?: Maybe<AlbumGenreBoolExp>;
  artist_genres?: Maybe<ArtistGenreBoolExp>;
  id?: Maybe<UuidComparisonExp>;
  movie_genres?: Maybe<MovieGenreBoolExp>;
  name?: Maybe<StringComparisonExp>;
  show_genres?: Maybe<ShowGenreBoolExp>;
  track_genres?: Maybe<TrackGenreBoolExp>;
};

/** unique or primary key constraints on table "genre" */
export enum GenreConstraint {
  /** unique or primary key constraint */
  GenreNameKey = 'genre_name_key',
  /** unique or primary key constraint */
  GenrePkey = 'genre_pkey'
}

/** input type for inserting data into table "genre" */
export type GenreInsertInput = {
  album_genres?: Maybe<AlbumGenreArrRelInsertInput>;
  artist_genres?: Maybe<ArtistGenreArrRelInsertInput>;
  id?: Maybe<Scalars['uuid']>;
  movie_genres?: Maybe<MovieGenreArrRelInsertInput>;
  name?: Maybe<Scalars['String']>;
  show_genres?: Maybe<ShowGenreArrRelInsertInput>;
  track_genres?: Maybe<TrackGenreArrRelInsertInput>;
};

/** aggregate max on columns */
export type GenreMaxFields = {
  id?: Maybe<Scalars['uuid']>;
  name?: Maybe<Scalars['String']>;
};

/** order by max() on columns of table "genre" */
export type GenreMaxOrderBy = {
  id?: Maybe<OrderBy>;
  name?: Maybe<OrderBy>;
};

/** aggregate min on columns */
export type GenreMinFields = {
  id?: Maybe<Scalars['uuid']>;
  name?: Maybe<Scalars['String']>;
};

/** order by min() on columns of table "genre" */
export type GenreMinOrderBy = {
  id?: Maybe<OrderBy>;
  name?: Maybe<OrderBy>;
};

/** response of any mutation on the table "genre" */
export type GenreMutationResponse = {
  /** number of affected rows by the mutation */
  affected_rows: Scalars['Int'];
  /** data of the affected rows by the mutation */
  returning: Array<Genre>;
};

/** input type for inserting object relation for remote table "genre" */
export type GenreObjRelInsertInput = {
  data: GenreInsertInput;
  on_conflict?: Maybe<GenreOnConflict>;
};

/** on conflict condition type for table "genre" */
export type GenreOnConflict = {
  constraint: GenreConstraint;
  update_columns: Array<GenreUpdateColumn>;
  where?: Maybe<GenreBoolExp>;
};

/** ordering options when selecting data from "genre" */
export type GenreOrderBy = {
  album_genres_aggregate?: Maybe<AlbumGenreAggregateOrderBy>;
  artist_genres_aggregate?: Maybe<ArtistGenreAggregateOrderBy>;
  id?: Maybe<OrderBy>;
  movie_genres_aggregate?: Maybe<MovieGenreAggregateOrderBy>;
  name?: Maybe<OrderBy>;
  show_genres_aggregate?: Maybe<ShowGenreAggregateOrderBy>;
  track_genres_aggregate?: Maybe<TrackGenreAggregateOrderBy>;
};

/** primary key columns input for table: "genre" */
export type GenrePkColumnsInput = {
  id: Scalars['uuid'];
};

/** select columns of table "genre" */
export enum GenreSelectColumn {
  /** column name */
  Id = 'id',
  /** column name */
  Name = 'name'
}

/** input type for updating data in table "genre" */
export type GenreSetInput = {
  id?: Maybe<Scalars['uuid']>;
  name?: Maybe<Scalars['String']>;
};

/** update columns of table "genre" */
export enum GenreUpdateColumn {
  /** column name */
  Id = 'id',
  /** column name */
  Name = 'name'
}

/** columns and relationships of "mood" */
export type Mood = {
  /** An array relationship */
  album_moods: Array<AlbumMood>;
  /** An aggregated array relationship */
  album_moods_aggregate: AlbumMoodAggregate;
  /** An array relationship */
  artist_moods: Array<ArtistMood>;
  /** An aggregated array relationship */
  artist_moods_aggregate: ArtistMoodAggregate;
  id: Scalars['uuid'];
  name: Scalars['String'];
  /** An array relationship */
  track_moods: Array<TrackMood>;
  /** An aggregated array relationship */
  track_moods_aggregate: TrackMoodAggregate;
};


/** columns and relationships of "mood" */
export type MoodAlbumMoodsArgs = {
  distinct_on?: Maybe<Array<AlbumMoodSelectColumn>>;
  limit?: Maybe<Scalars['Int']>;
  offset?: Maybe<Scalars['Int']>;
  order_by?: Maybe<Array<AlbumMoodOrderBy>>;
  where?: Maybe<AlbumMoodBoolExp>;
};


/** columns and relationships of "mood" */
export type MoodAlbumMoodsAggregateArgs = {
  distinct_on?: Maybe<Array<AlbumMoodSelectColumn>>;
  limit?: Maybe<Scalars['Int']>;
  offset?: Maybe<Scalars['Int']>;
  order_by?: Maybe<Array<AlbumMoodOrderBy>>;
  where?: Maybe<AlbumMoodBoolExp>;
};


/** columns and relationships of "mood" */
export type MoodArtistMoodsArgs = {
  distinct_on?: Maybe<Array<ArtistMoodSelectColumn>>;
  limit?: Maybe<Scalars['Int']>;
  offset?: Maybe<Scalars['Int']>;
  order_by?: Maybe<Array<ArtistMoodOrderBy>>;
  where?: Maybe<ArtistMoodBoolExp>;
};


/** columns and relationships of "mood" */
export type MoodArtistMoodsAggregateArgs = {
  distinct_on?: Maybe<Array<ArtistMoodSelectColumn>>;
  limit?: Maybe<Scalars['Int']>;
  offset?: Maybe<Scalars['Int']>;
  order_by?: Maybe<Array<ArtistMoodOrderBy>>;
  where?: Maybe<ArtistMoodBoolExp>;
};


/** columns and relationships of "mood" */
export type MoodTrackMoodsArgs = {
  distinct_on?: Maybe<Array<TrackMoodSelectColumn>>;
  limit?: Maybe<Scalars['Int']>;
  offset?: Maybe<Scalars['Int']>;
  order_by?: Maybe<Array<TrackMoodOrderBy>>;
  where?: Maybe<TrackMoodBoolExp>;
};


/** columns and relationships of "mood" */
export type MoodTrackMoodsAggregateArgs = {
  distinct_on?: Maybe<Array<TrackMoodSelectColumn>>;
  limit?: Maybe<Scalars['Int']>;
  offset?: Maybe<Scalars['Int']>;
  order_by?: Maybe<Array<TrackMoodOrderBy>>;
  where?: Maybe<TrackMoodBoolExp>;
};

/** aggregated selection of "mood" */
export type MoodAggregate = {
  aggregate?: Maybe<MoodAggregateFields>;
  nodes: Array<Mood>;
};

/** aggregate fields of "mood" */
export type MoodAggregateFields = {
  count?: Maybe<Scalars['Int']>;
  max?: Maybe<MoodMaxFields>;
  min?: Maybe<MoodMinFields>;
};


/** aggregate fields of "mood" */
export type MoodAggregateFieldsCountArgs = {
  columns?: Maybe<Array<MoodSelectColumn>>;
  distinct?: Maybe<Scalars['Boolean']>;
};

/** order by aggregate values of table "mood" */
export type MoodAggregateOrderBy = {
  count?: Maybe<OrderBy>;
  max?: Maybe<MoodMaxOrderBy>;
  min?: Maybe<MoodMinOrderBy>;
};

/** input type for inserting array relation for remote table "mood" */
export type MoodArrRelInsertInput = {
  data: Array<MoodInsertInput>;
  on_conflict?: Maybe<MoodOnConflict>;
};

/** Boolean expression to filter rows from the table "mood". All fields are combined with a logical 'AND'. */
export type MoodBoolExp = {
  _and?: Maybe<Array<Maybe<MoodBoolExp>>>;
  _not?: Maybe<MoodBoolExp>;
  _or?: Maybe<Array<Maybe<MoodBoolExp>>>;
  album_moods?: Maybe<AlbumMoodBoolExp>;
  artist_moods?: Maybe<ArtistMoodBoolExp>;
  id?: Maybe<UuidComparisonExp>;
  name?: Maybe<StringComparisonExp>;
  track_moods?: Maybe<TrackMoodBoolExp>;
};

/** unique or primary key constraints on table "mood" */
export enum MoodConstraint {
  /** unique or primary key constraint */
  MoodNameKey = 'mood_name_key',
  /** unique or primary key constraint */
  MoodPkey = 'mood_pkey'
}

/** input type for inserting data into table "mood" */
export type MoodInsertInput = {
  album_moods?: Maybe<AlbumMoodArrRelInsertInput>;
  artist_moods?: Maybe<ArtistMoodArrRelInsertInput>;
  id?: Maybe<Scalars['uuid']>;
  name?: Maybe<Scalars['String']>;
  track_moods?: Maybe<TrackMoodArrRelInsertInput>;
};

/** aggregate max on columns */
export type MoodMaxFields = {
  id?: Maybe<Scalars['uuid']>;
  name?: Maybe<Scalars['String']>;
};

/** order by max() on columns of table "mood" */
export type MoodMaxOrderBy = {
  id?: Maybe<OrderBy>;
  name?: Maybe<OrderBy>;
};

/** aggregate min on columns */
export type MoodMinFields = {
  id?: Maybe<Scalars['uuid']>;
  name?: Maybe<Scalars['String']>;
};

/** order by min() on columns of table "mood" */
export type MoodMinOrderBy = {
  id?: Maybe<OrderBy>;
  name?: Maybe<OrderBy>;
};

/** response of any mutation on the table "mood" */
export type MoodMutationResponse = {
  /** number of affected rows by the mutation */
  affected_rows: Scalars['Int'];
  /** data of the affected rows by the mutation */
  returning: Array<Mood>;
};

/** input type for inserting object relation for remote table "mood" */
export type MoodObjRelInsertInput = {
  data: MoodInsertInput;
  on_conflict?: Maybe<MoodOnConflict>;
};

/** on conflict condition type for table "mood" */
export type MoodOnConflict = {
  constraint: MoodConstraint;
  update_columns: Array<MoodUpdateColumn>;
  where?: Maybe<MoodBoolExp>;
};

/** ordering options when selecting data from "mood" */
export type MoodOrderBy = {
  album_moods_aggregate?: Maybe<AlbumMoodAggregateOrderBy>;
  artist_moods_aggregate?: Maybe<ArtistMoodAggregateOrderBy>;
  id?: Maybe<OrderBy>;
  name?: Maybe<OrderBy>;
  track_moods_aggregate?: Maybe<TrackMoodAggregateOrderBy>;
};

/** primary key columns input for table: "mood" */
export type MoodPkColumnsInput = {
  id: Scalars['uuid'];
};

/** select columns of table "mood" */
export enum MoodSelectColumn {
  /** column name */
  Id = 'id',
  /** column name */
  Name = 'name'
}

/** input type for updating data in table "mood" */
export type MoodSetInput = {
  id?: Maybe<Scalars['uuid']>;
  name?: Maybe<Scalars['String']>;
};

/** update columns of table "mood" */
export enum MoodUpdateColumn {
  /** column name */
  Id = 'id',
  /** column name */
  Name = 'name'
}

/** columns and relationships of "movie" */
export type Movie = {
  aired?: Maybe<Scalars['date']>;
  content_rating: Scalars['String'];
  id: Scalars['uuid'];
  image_background?: Maybe<Scalars['String']>;
  image_cover?: Maybe<Scalars['String']>;
  /** An array relationship */
  movie_collections: Array<MovieCollection>;
  /** An aggregated array relationship */
  movie_collections_aggregate: MovieCollectionAggregate;
  /** An array relationship */
  movie_countries: Array<MovieCountry>;
  /** An aggregated array relationship */
  movie_countries_aggregate: MovieCountryAggregate;
  /** An array relationship */
  movie_genres: Array<MovieGenre>;
  /** An aggregated array relationship */
  movie_genres_aggregate: MovieGenreAggregate;
  /** An array relationship */
  movie_staffs: Array<MovieStaff>;
  /** An aggregated array relationship */
  movie_staffs_aggregate: MovieStaffAggregate;
  /** An array relationship */
  movie_studios: Array<MovieStudio>;
  /** An aggregated array relationship */
  movie_studios_aggregate: MovieStudioAggregate;
  name: Scalars['String'];
  name_match: Scalars['String'];
  name_original?: Maybe<Scalars['_text']>;
  name_sort?: Maybe<Scalars['String']>;
  rating?: Maybe<Scalars['numeric']>;
  summary: Scalars['String'];
  tagline: Scalars['String'];
};


/** columns and relationships of "movie" */
export type MovieMovieCollectionsArgs = {
  distinct_on?: Maybe<Array<MovieCollectionSelectColumn>>;
  limit?: Maybe<Scalars['Int']>;
  offset?: Maybe<Scalars['Int']>;
  order_by?: Maybe<Array<MovieCollectionOrderBy>>;
  where?: Maybe<MovieCollectionBoolExp>;
};


/** columns and relationships of "movie" */
export type MovieMovieCollectionsAggregateArgs = {
  distinct_on?: Maybe<Array<MovieCollectionSelectColumn>>;
  limit?: Maybe<Scalars['Int']>;
  offset?: Maybe<Scalars['Int']>;
  order_by?: Maybe<Array<MovieCollectionOrderBy>>;
  where?: Maybe<MovieCollectionBoolExp>;
};


/** columns and relationships of "movie" */
export type MovieMovieCountriesArgs = {
  distinct_on?: Maybe<Array<MovieCountrySelectColumn>>;
  limit?: Maybe<Scalars['Int']>;
  offset?: Maybe<Scalars['Int']>;
  order_by?: Maybe<Array<MovieCountryOrderBy>>;
  where?: Maybe<MovieCountryBoolExp>;
};


/** columns and relationships of "movie" */
export type MovieMovieCountriesAggregateArgs = {
  distinct_on?: Maybe<Array<MovieCountrySelectColumn>>;
  limit?: Maybe<Scalars['Int']>;
  offset?: Maybe<Scalars['Int']>;
  order_by?: Maybe<Array<MovieCountryOrderBy>>;
  where?: Maybe<MovieCountryBoolExp>;
};


/** columns and relationships of "movie" */
export type MovieMovieGenresArgs = {
  distinct_on?: Maybe<Array<MovieGenreSelectColumn>>;
  limit?: Maybe<Scalars['Int']>;
  offset?: Maybe<Scalars['Int']>;
  order_by?: Maybe<Array<MovieGenreOrderBy>>;
  where?: Maybe<MovieGenreBoolExp>;
};


/** columns and relationships of "movie" */
export type MovieMovieGenresAggregateArgs = {
  distinct_on?: Maybe<Array<MovieGenreSelectColumn>>;
  limit?: Maybe<Scalars['Int']>;
  offset?: Maybe<Scalars['Int']>;
  order_by?: Maybe<Array<MovieGenreOrderBy>>;
  where?: Maybe<MovieGenreBoolExp>;
};


/** columns and relationships of "movie" */
export type MovieMovieStaffsArgs = {
  distinct_on?: Maybe<Array<MovieStaffSelectColumn>>;
  limit?: Maybe<Scalars['Int']>;
  offset?: Maybe<Scalars['Int']>;
  order_by?: Maybe<Array<MovieStaffOrderBy>>;
  where?: Maybe<MovieStaffBoolExp>;
};


/** columns and relationships of "movie" */
export type MovieMovieStaffsAggregateArgs = {
  distinct_on?: Maybe<Array<MovieStaffSelectColumn>>;
  limit?: Maybe<Scalars['Int']>;
  offset?: Maybe<Scalars['Int']>;
  order_by?: Maybe<Array<MovieStaffOrderBy>>;
  where?: Maybe<MovieStaffBoolExp>;
};


/** columns and relationships of "movie" */
export type MovieMovieStudiosArgs = {
  distinct_on?: Maybe<Array<MovieStudioSelectColumn>>;
  limit?: Maybe<Scalars['Int']>;
  offset?: Maybe<Scalars['Int']>;
  order_by?: Maybe<Array<MovieStudioOrderBy>>;
  where?: Maybe<MovieStudioBoolExp>;
};


/** columns and relationships of "movie" */
export type MovieMovieStudiosAggregateArgs = {
  distinct_on?: Maybe<Array<MovieStudioSelectColumn>>;
  limit?: Maybe<Scalars['Int']>;
  offset?: Maybe<Scalars['Int']>;
  order_by?: Maybe<Array<MovieStudioOrderBy>>;
  where?: Maybe<MovieStudioBoolExp>;
};

/** aggregated selection of "movie" */
export type MovieAggregate = {
  aggregate?: Maybe<MovieAggregateFields>;
  nodes: Array<Movie>;
};

/** aggregate fields of "movie" */
export type MovieAggregateFields = {
  avg?: Maybe<MovieAvgFields>;
  count?: Maybe<Scalars['Int']>;
  max?: Maybe<MovieMaxFields>;
  min?: Maybe<MovieMinFields>;
  stddev?: Maybe<MovieStddevFields>;
  stddev_pop?: Maybe<MovieStddevPopFields>;
  stddev_samp?: Maybe<MovieStddevSampFields>;
  sum?: Maybe<MovieSumFields>;
  var_pop?: Maybe<MovieVarPopFields>;
  var_samp?: Maybe<MovieVarSampFields>;
  variance?: Maybe<MovieVarianceFields>;
};


/** aggregate fields of "movie" */
export type MovieAggregateFieldsCountArgs = {
  columns?: Maybe<Array<MovieSelectColumn>>;
  distinct?: Maybe<Scalars['Boolean']>;
};

/** order by aggregate values of table "movie" */
export type MovieAggregateOrderBy = {
  avg?: Maybe<MovieAvgOrderBy>;
  count?: Maybe<OrderBy>;
  max?: Maybe<MovieMaxOrderBy>;
  min?: Maybe<MovieMinOrderBy>;
  stddev?: Maybe<MovieStddevOrderBy>;
  stddev_pop?: Maybe<MovieStddevPopOrderBy>;
  stddev_samp?: Maybe<MovieStddevSampOrderBy>;
  sum?: Maybe<MovieSumOrderBy>;
  var_pop?: Maybe<MovieVarPopOrderBy>;
  var_samp?: Maybe<MovieVarSampOrderBy>;
  variance?: Maybe<MovieVarianceOrderBy>;
};

/** input type for inserting array relation for remote table "movie" */
export type MovieArrRelInsertInput = {
  data: Array<MovieInsertInput>;
  on_conflict?: Maybe<MovieOnConflict>;
};

/** aggregate avg on columns */
export type MovieAvgFields = {
  rating?: Maybe<Scalars['Float']>;
};

/** order by avg() on columns of table "movie" */
export type MovieAvgOrderBy = {
  rating?: Maybe<OrderBy>;
};

/** Boolean expression to filter rows from the table "movie". All fields are combined with a logical 'AND'. */
export type MovieBoolExp = {
  _and?: Maybe<Array<Maybe<MovieBoolExp>>>;
  _not?: Maybe<MovieBoolExp>;
  _or?: Maybe<Array<Maybe<MovieBoolExp>>>;
  aired?: Maybe<DateComparisonExp>;
  content_rating?: Maybe<StringComparisonExp>;
  id?: Maybe<UuidComparisonExp>;
  image_background?: Maybe<StringComparisonExp>;
  image_cover?: Maybe<StringComparisonExp>;
  movie_collections?: Maybe<MovieCollectionBoolExp>;
  movie_countries?: Maybe<MovieCountryBoolExp>;
  movie_genres?: Maybe<MovieGenreBoolExp>;
  movie_staffs?: Maybe<MovieStaffBoolExp>;
  movie_studios?: Maybe<MovieStudioBoolExp>;
  name?: Maybe<StringComparisonExp>;
  name_match?: Maybe<StringComparisonExp>;
  name_original?: Maybe<TextComparisonExp>;
  name_sort?: Maybe<StringComparisonExp>;
  rating?: Maybe<NumericComparisonExp>;
  summary?: Maybe<StringComparisonExp>;
  tagline?: Maybe<StringComparisonExp>;
};

/** columns and relationships of "movie_collection" */
export type MovieCollection = {
  /** An object relationship */
  collection: Collection;
  collection_id: Scalars['uuid'];
  index: Scalars['Int'];
  /** An object relationship */
  movie: Movie;
  movie_id: Scalars['uuid'];
};

/** aggregated selection of "movie_collection" */
export type MovieCollectionAggregate = {
  aggregate?: Maybe<MovieCollectionAggregateFields>;
  nodes: Array<MovieCollection>;
};

/** aggregate fields of "movie_collection" */
export type MovieCollectionAggregateFields = {
  avg?: Maybe<MovieCollectionAvgFields>;
  count?: Maybe<Scalars['Int']>;
  max?: Maybe<MovieCollectionMaxFields>;
  min?: Maybe<MovieCollectionMinFields>;
  stddev?: Maybe<MovieCollectionStddevFields>;
  stddev_pop?: Maybe<MovieCollectionStddevPopFields>;
  stddev_samp?: Maybe<MovieCollectionStddevSampFields>;
  sum?: Maybe<MovieCollectionSumFields>;
  var_pop?: Maybe<MovieCollectionVarPopFields>;
  var_samp?: Maybe<MovieCollectionVarSampFields>;
  variance?: Maybe<MovieCollectionVarianceFields>;
};


/** aggregate fields of "movie_collection" */
export type MovieCollectionAggregateFieldsCountArgs = {
  columns?: Maybe<Array<MovieCollectionSelectColumn>>;
  distinct?: Maybe<Scalars['Boolean']>;
};

/** order by aggregate values of table "movie_collection" */
export type MovieCollectionAggregateOrderBy = {
  avg?: Maybe<MovieCollectionAvgOrderBy>;
  count?: Maybe<OrderBy>;
  max?: Maybe<MovieCollectionMaxOrderBy>;
  min?: Maybe<MovieCollectionMinOrderBy>;
  stddev?: Maybe<MovieCollectionStddevOrderBy>;
  stddev_pop?: Maybe<MovieCollectionStddevPopOrderBy>;
  stddev_samp?: Maybe<MovieCollectionStddevSampOrderBy>;
  sum?: Maybe<MovieCollectionSumOrderBy>;
  var_pop?: Maybe<MovieCollectionVarPopOrderBy>;
  var_samp?: Maybe<MovieCollectionVarSampOrderBy>;
  variance?: Maybe<MovieCollectionVarianceOrderBy>;
};

/** input type for inserting array relation for remote table "movie_collection" */
export type MovieCollectionArrRelInsertInput = {
  data: Array<MovieCollectionInsertInput>;
  on_conflict?: Maybe<MovieCollectionOnConflict>;
};

/** aggregate avg on columns */
export type MovieCollectionAvgFields = {
  index?: Maybe<Scalars['Float']>;
};

/** order by avg() on columns of table "movie_collection" */
export type MovieCollectionAvgOrderBy = {
  index?: Maybe<OrderBy>;
};

/** Boolean expression to filter rows from the table "movie_collection". All fields are combined with a logical 'AND'. */
export type MovieCollectionBoolExp = {
  _and?: Maybe<Array<Maybe<MovieCollectionBoolExp>>>;
  _not?: Maybe<MovieCollectionBoolExp>;
  _or?: Maybe<Array<Maybe<MovieCollectionBoolExp>>>;
  collection?: Maybe<CollectionBoolExp>;
  collection_id?: Maybe<UuidComparisonExp>;
  index?: Maybe<IntComparisonExp>;
  movie?: Maybe<MovieBoolExp>;
  movie_id?: Maybe<UuidComparisonExp>;
};

/** unique or primary key constraints on table "movie_collection" */
export enum MovieCollectionConstraint {
  /** unique or primary key constraint */
  MovieCollectionMovieIdCollectionIdKey = 'movie_collection_movie_id_collection_id_key',
  /** unique or primary key constraint */
  MovieCollectionPkey = 'movie_collection_pkey'
}

/** input type for incrementing integer column in table "movie_collection" */
export type MovieCollectionIncInput = {
  index?: Maybe<Scalars['Int']>;
};

/** input type for inserting data into table "movie_collection" */
export type MovieCollectionInsertInput = {
  collection?: Maybe<CollectionObjRelInsertInput>;
  collection_id?: Maybe<Scalars['uuid']>;
  index?: Maybe<Scalars['Int']>;
  movie?: Maybe<MovieObjRelInsertInput>;
  movie_id?: Maybe<Scalars['uuid']>;
};

/** aggregate max on columns */
export type MovieCollectionMaxFields = {
  collection_id?: Maybe<Scalars['uuid']>;
  index?: Maybe<Scalars['Int']>;
  movie_id?: Maybe<Scalars['uuid']>;
};

/** order by max() on columns of table "movie_collection" */
export type MovieCollectionMaxOrderBy = {
  collection_id?: Maybe<OrderBy>;
  index?: Maybe<OrderBy>;
  movie_id?: Maybe<OrderBy>;
};

/** aggregate min on columns */
export type MovieCollectionMinFields = {
  collection_id?: Maybe<Scalars['uuid']>;
  index?: Maybe<Scalars['Int']>;
  movie_id?: Maybe<Scalars['uuid']>;
};

/** order by min() on columns of table "movie_collection" */
export type MovieCollectionMinOrderBy = {
  collection_id?: Maybe<OrderBy>;
  index?: Maybe<OrderBy>;
  movie_id?: Maybe<OrderBy>;
};

/** response of any mutation on the table "movie_collection" */
export type MovieCollectionMutationResponse = {
  /** number of affected rows by the mutation */
  affected_rows: Scalars['Int'];
  /** data of the affected rows by the mutation */
  returning: Array<MovieCollection>;
};

/** input type for inserting object relation for remote table "movie_collection" */
export type MovieCollectionObjRelInsertInput = {
  data: MovieCollectionInsertInput;
  on_conflict?: Maybe<MovieCollectionOnConflict>;
};

/** on conflict condition type for table "movie_collection" */
export type MovieCollectionOnConflict = {
  constraint: MovieCollectionConstraint;
  update_columns: Array<MovieCollectionUpdateColumn>;
  where?: Maybe<MovieCollectionBoolExp>;
};

/** ordering options when selecting data from "movie_collection" */
export type MovieCollectionOrderBy = {
  collection?: Maybe<CollectionOrderBy>;
  collection_id?: Maybe<OrderBy>;
  index?: Maybe<OrderBy>;
  movie?: Maybe<MovieOrderBy>;
  movie_id?: Maybe<OrderBy>;
};

/** primary key columns input for table: "movie_collection" */
export type MovieCollectionPkColumnsInput = {
  index: Scalars['Int'];
  movie_id: Scalars['uuid'];
};

/** select columns of table "movie_collection" */
export enum MovieCollectionSelectColumn {
  /** column name */
  CollectionId = 'collection_id',
  /** column name */
  Index = 'index',
  /** column name */
  MovieId = 'movie_id'
}

/** input type for updating data in table "movie_collection" */
export type MovieCollectionSetInput = {
  collection_id?: Maybe<Scalars['uuid']>;
  index?: Maybe<Scalars['Int']>;
  movie_id?: Maybe<Scalars['uuid']>;
};

/** aggregate stddev on columns */
export type MovieCollectionStddevFields = {
  index?: Maybe<Scalars['Float']>;
};

/** order by stddev() on columns of table "movie_collection" */
export type MovieCollectionStddevOrderBy = {
  index?: Maybe<OrderBy>;
};

/** aggregate stddev_pop on columns */
export type MovieCollectionStddevPopFields = {
  index?: Maybe<Scalars['Float']>;
};

/** order by stddev_pop() on columns of table "movie_collection" */
export type MovieCollectionStddevPopOrderBy = {
  index?: Maybe<OrderBy>;
};

/** aggregate stddev_samp on columns */
export type MovieCollectionStddevSampFields = {
  index?: Maybe<Scalars['Float']>;
};

/** order by stddev_samp() on columns of table "movie_collection" */
export type MovieCollectionStddevSampOrderBy = {
  index?: Maybe<OrderBy>;
};

/** aggregate sum on columns */
export type MovieCollectionSumFields = {
  index?: Maybe<Scalars['Int']>;
};

/** order by sum() on columns of table "movie_collection" */
export type MovieCollectionSumOrderBy = {
  index?: Maybe<OrderBy>;
};

/** update columns of table "movie_collection" */
export enum MovieCollectionUpdateColumn {
  /** column name */
  CollectionId = 'collection_id',
  /** column name */
  Index = 'index',
  /** column name */
  MovieId = 'movie_id'
}

/** aggregate var_pop on columns */
export type MovieCollectionVarPopFields = {
  index?: Maybe<Scalars['Float']>;
};

/** order by var_pop() on columns of table "movie_collection" */
export type MovieCollectionVarPopOrderBy = {
  index?: Maybe<OrderBy>;
};

/** aggregate var_samp on columns */
export type MovieCollectionVarSampFields = {
  index?: Maybe<Scalars['Float']>;
};

/** order by var_samp() on columns of table "movie_collection" */
export type MovieCollectionVarSampOrderBy = {
  index?: Maybe<OrderBy>;
};

/** aggregate variance on columns */
export type MovieCollectionVarianceFields = {
  index?: Maybe<Scalars['Float']>;
};

/** order by variance() on columns of table "movie_collection" */
export type MovieCollectionVarianceOrderBy = {
  index?: Maybe<OrderBy>;
};

/** unique or primary key constraints on table "movie" */
export enum MovieConstraint {
  /** unique or primary key constraint */
  MovieNameMatchKey = 'movie_name_match_key',
  /** unique or primary key constraint */
  MoviePkey = 'movie_pkey'
}

/** columns and relationships of "movie_country" */
export type MovieCountry = {
  /** An object relationship */
  country: Country;
  country_code: Scalars['String'];
  index: Scalars['Int'];
  /** An object relationship */
  movie: Movie;
  movie_id: Scalars['uuid'];
};

/** aggregated selection of "movie_country" */
export type MovieCountryAggregate = {
  aggregate?: Maybe<MovieCountryAggregateFields>;
  nodes: Array<MovieCountry>;
};

/** aggregate fields of "movie_country" */
export type MovieCountryAggregateFields = {
  avg?: Maybe<MovieCountryAvgFields>;
  count?: Maybe<Scalars['Int']>;
  max?: Maybe<MovieCountryMaxFields>;
  min?: Maybe<MovieCountryMinFields>;
  stddev?: Maybe<MovieCountryStddevFields>;
  stddev_pop?: Maybe<MovieCountryStddevPopFields>;
  stddev_samp?: Maybe<MovieCountryStddevSampFields>;
  sum?: Maybe<MovieCountrySumFields>;
  var_pop?: Maybe<MovieCountryVarPopFields>;
  var_samp?: Maybe<MovieCountryVarSampFields>;
  variance?: Maybe<MovieCountryVarianceFields>;
};


/** aggregate fields of "movie_country" */
export type MovieCountryAggregateFieldsCountArgs = {
  columns?: Maybe<Array<MovieCountrySelectColumn>>;
  distinct?: Maybe<Scalars['Boolean']>;
};

/** order by aggregate values of table "movie_country" */
export type MovieCountryAggregateOrderBy = {
  avg?: Maybe<MovieCountryAvgOrderBy>;
  count?: Maybe<OrderBy>;
  max?: Maybe<MovieCountryMaxOrderBy>;
  min?: Maybe<MovieCountryMinOrderBy>;
  stddev?: Maybe<MovieCountryStddevOrderBy>;
  stddev_pop?: Maybe<MovieCountryStddevPopOrderBy>;
  stddev_samp?: Maybe<MovieCountryStddevSampOrderBy>;
  sum?: Maybe<MovieCountrySumOrderBy>;
  var_pop?: Maybe<MovieCountryVarPopOrderBy>;
  var_samp?: Maybe<MovieCountryVarSampOrderBy>;
  variance?: Maybe<MovieCountryVarianceOrderBy>;
};

/** input type for inserting array relation for remote table "movie_country" */
export type MovieCountryArrRelInsertInput = {
  data: Array<MovieCountryInsertInput>;
  on_conflict?: Maybe<MovieCountryOnConflict>;
};

/** aggregate avg on columns */
export type MovieCountryAvgFields = {
  index?: Maybe<Scalars['Float']>;
};

/** order by avg() on columns of table "movie_country" */
export type MovieCountryAvgOrderBy = {
  index?: Maybe<OrderBy>;
};

/** Boolean expression to filter rows from the table "movie_country". All fields are combined with a logical 'AND'. */
export type MovieCountryBoolExp = {
  _and?: Maybe<Array<Maybe<MovieCountryBoolExp>>>;
  _not?: Maybe<MovieCountryBoolExp>;
  _or?: Maybe<Array<Maybe<MovieCountryBoolExp>>>;
  country?: Maybe<CountryBoolExp>;
  country_code?: Maybe<StringComparisonExp>;
  index?: Maybe<IntComparisonExp>;
  movie?: Maybe<MovieBoolExp>;
  movie_id?: Maybe<UuidComparisonExp>;
};

/** unique or primary key constraints on table "movie_country" */
export enum MovieCountryConstraint {
  /** unique or primary key constraint */
  MovieCountryMovieIdCountryCodeKey = 'movie_country_movie_id_country_code_key',
  /** unique or primary key constraint */
  MovieCountryPkey = 'movie_country_pkey'
}

/** input type for incrementing integer column in table "movie_country" */
export type MovieCountryIncInput = {
  index?: Maybe<Scalars['Int']>;
};

/** input type for inserting data into table "movie_country" */
export type MovieCountryInsertInput = {
  country?: Maybe<CountryObjRelInsertInput>;
  country_code?: Maybe<Scalars['String']>;
  index?: Maybe<Scalars['Int']>;
  movie?: Maybe<MovieObjRelInsertInput>;
  movie_id?: Maybe<Scalars['uuid']>;
};

/** aggregate max on columns */
export type MovieCountryMaxFields = {
  country_code?: Maybe<Scalars['String']>;
  index?: Maybe<Scalars['Int']>;
  movie_id?: Maybe<Scalars['uuid']>;
};

/** order by max() on columns of table "movie_country" */
export type MovieCountryMaxOrderBy = {
  country_code?: Maybe<OrderBy>;
  index?: Maybe<OrderBy>;
  movie_id?: Maybe<OrderBy>;
};

/** aggregate min on columns */
export type MovieCountryMinFields = {
  country_code?: Maybe<Scalars['String']>;
  index?: Maybe<Scalars['Int']>;
  movie_id?: Maybe<Scalars['uuid']>;
};

/** order by min() on columns of table "movie_country" */
export type MovieCountryMinOrderBy = {
  country_code?: Maybe<OrderBy>;
  index?: Maybe<OrderBy>;
  movie_id?: Maybe<OrderBy>;
};

/** response of any mutation on the table "movie_country" */
export type MovieCountryMutationResponse = {
  /** number of affected rows by the mutation */
  affected_rows: Scalars['Int'];
  /** data of the affected rows by the mutation */
  returning: Array<MovieCountry>;
};

/** input type for inserting object relation for remote table "movie_country" */
export type MovieCountryObjRelInsertInput = {
  data: MovieCountryInsertInput;
  on_conflict?: Maybe<MovieCountryOnConflict>;
};

/** on conflict condition type for table "movie_country" */
export type MovieCountryOnConflict = {
  constraint: MovieCountryConstraint;
  update_columns: Array<MovieCountryUpdateColumn>;
  where?: Maybe<MovieCountryBoolExp>;
};

/** ordering options when selecting data from "movie_country" */
export type MovieCountryOrderBy = {
  country?: Maybe<CountryOrderBy>;
  country_code?: Maybe<OrderBy>;
  index?: Maybe<OrderBy>;
  movie?: Maybe<MovieOrderBy>;
  movie_id?: Maybe<OrderBy>;
};

/** primary key columns input for table: "movie_country" */
export type MovieCountryPkColumnsInput = {
  index: Scalars['Int'];
  movie_id: Scalars['uuid'];
};

/** select columns of table "movie_country" */
export enum MovieCountrySelectColumn {
  /** column name */
  CountryCode = 'country_code',
  /** column name */
  Index = 'index',
  /** column name */
  MovieId = 'movie_id'
}

/** input type for updating data in table "movie_country" */
export type MovieCountrySetInput = {
  country_code?: Maybe<Scalars['String']>;
  index?: Maybe<Scalars['Int']>;
  movie_id?: Maybe<Scalars['uuid']>;
};

/** aggregate stddev on columns */
export type MovieCountryStddevFields = {
  index?: Maybe<Scalars['Float']>;
};

/** order by stddev() on columns of table "movie_country" */
export type MovieCountryStddevOrderBy = {
  index?: Maybe<OrderBy>;
};

/** aggregate stddev_pop on columns */
export type MovieCountryStddevPopFields = {
  index?: Maybe<Scalars['Float']>;
};

/** order by stddev_pop() on columns of table "movie_country" */
export type MovieCountryStddevPopOrderBy = {
  index?: Maybe<OrderBy>;
};

/** aggregate stddev_samp on columns */
export type MovieCountryStddevSampFields = {
  index?: Maybe<Scalars['Float']>;
};

/** order by stddev_samp() on columns of table "movie_country" */
export type MovieCountryStddevSampOrderBy = {
  index?: Maybe<OrderBy>;
};

/** aggregate sum on columns */
export type MovieCountrySumFields = {
  index?: Maybe<Scalars['Int']>;
};

/** order by sum() on columns of table "movie_country" */
export type MovieCountrySumOrderBy = {
  index?: Maybe<OrderBy>;
};

/** update columns of table "movie_country" */
export enum MovieCountryUpdateColumn {
  /** column name */
  CountryCode = 'country_code',
  /** column name */
  Index = 'index',
  /** column name */
  MovieId = 'movie_id'
}

/** aggregate var_pop on columns */
export type MovieCountryVarPopFields = {
  index?: Maybe<Scalars['Float']>;
};

/** order by var_pop() on columns of table "movie_country" */
export type MovieCountryVarPopOrderBy = {
  index?: Maybe<OrderBy>;
};

/** aggregate var_samp on columns */
export type MovieCountryVarSampFields = {
  index?: Maybe<Scalars['Float']>;
};

/** order by var_samp() on columns of table "movie_country" */
export type MovieCountryVarSampOrderBy = {
  index?: Maybe<OrderBy>;
};

/** aggregate variance on columns */
export type MovieCountryVarianceFields = {
  index?: Maybe<Scalars['Float']>;
};

/** order by variance() on columns of table "movie_country" */
export type MovieCountryVarianceOrderBy = {
  index?: Maybe<OrderBy>;
};

/** columns and relationships of "movie_genre" */
export type MovieGenre = {
  /** An object relationship */
  genre: Genre;
  genre_id: Scalars['uuid'];
  index: Scalars['Int'];
  /** An object relationship */
  movie: Movie;
  movie_id: Scalars['uuid'];
};

/** aggregated selection of "movie_genre" */
export type MovieGenreAggregate = {
  aggregate?: Maybe<MovieGenreAggregateFields>;
  nodes: Array<MovieGenre>;
};

/** aggregate fields of "movie_genre" */
export type MovieGenreAggregateFields = {
  avg?: Maybe<MovieGenreAvgFields>;
  count?: Maybe<Scalars['Int']>;
  max?: Maybe<MovieGenreMaxFields>;
  min?: Maybe<MovieGenreMinFields>;
  stddev?: Maybe<MovieGenreStddevFields>;
  stddev_pop?: Maybe<MovieGenreStddevPopFields>;
  stddev_samp?: Maybe<MovieGenreStddevSampFields>;
  sum?: Maybe<MovieGenreSumFields>;
  var_pop?: Maybe<MovieGenreVarPopFields>;
  var_samp?: Maybe<MovieGenreVarSampFields>;
  variance?: Maybe<MovieGenreVarianceFields>;
};


/** aggregate fields of "movie_genre" */
export type MovieGenreAggregateFieldsCountArgs = {
  columns?: Maybe<Array<MovieGenreSelectColumn>>;
  distinct?: Maybe<Scalars['Boolean']>;
};

/** order by aggregate values of table "movie_genre" */
export type MovieGenreAggregateOrderBy = {
  avg?: Maybe<MovieGenreAvgOrderBy>;
  count?: Maybe<OrderBy>;
  max?: Maybe<MovieGenreMaxOrderBy>;
  min?: Maybe<MovieGenreMinOrderBy>;
  stddev?: Maybe<MovieGenreStddevOrderBy>;
  stddev_pop?: Maybe<MovieGenreStddevPopOrderBy>;
  stddev_samp?: Maybe<MovieGenreStddevSampOrderBy>;
  sum?: Maybe<MovieGenreSumOrderBy>;
  var_pop?: Maybe<MovieGenreVarPopOrderBy>;
  var_samp?: Maybe<MovieGenreVarSampOrderBy>;
  variance?: Maybe<MovieGenreVarianceOrderBy>;
};

/** input type for inserting array relation for remote table "movie_genre" */
export type MovieGenreArrRelInsertInput = {
  data: Array<MovieGenreInsertInput>;
  on_conflict?: Maybe<MovieGenreOnConflict>;
};

/** aggregate avg on columns */
export type MovieGenreAvgFields = {
  index?: Maybe<Scalars['Float']>;
};

/** order by avg() on columns of table "movie_genre" */
export type MovieGenreAvgOrderBy = {
  index?: Maybe<OrderBy>;
};

/** Boolean expression to filter rows from the table "movie_genre". All fields are combined with a logical 'AND'. */
export type MovieGenreBoolExp = {
  _and?: Maybe<Array<Maybe<MovieGenreBoolExp>>>;
  _not?: Maybe<MovieGenreBoolExp>;
  _or?: Maybe<Array<Maybe<MovieGenreBoolExp>>>;
  genre?: Maybe<GenreBoolExp>;
  genre_id?: Maybe<UuidComparisonExp>;
  index?: Maybe<IntComparisonExp>;
  movie?: Maybe<MovieBoolExp>;
  movie_id?: Maybe<UuidComparisonExp>;
};

/** unique or primary key constraints on table "movie_genre" */
export enum MovieGenreConstraint {
  /** unique or primary key constraint */
  MovieGenreMovieIdGenreIdKey = 'movie_genre_movie_id_genre_id_key',
  /** unique or primary key constraint */
  MovieGenrePkey = 'movie_genre_pkey'
}

/** input type for incrementing integer column in table "movie_genre" */
export type MovieGenreIncInput = {
  index?: Maybe<Scalars['Int']>;
};

/** input type for inserting data into table "movie_genre" */
export type MovieGenreInsertInput = {
  genre?: Maybe<GenreObjRelInsertInput>;
  genre_id?: Maybe<Scalars['uuid']>;
  index?: Maybe<Scalars['Int']>;
  movie?: Maybe<MovieObjRelInsertInput>;
  movie_id?: Maybe<Scalars['uuid']>;
};

/** aggregate max on columns */
export type MovieGenreMaxFields = {
  genre_id?: Maybe<Scalars['uuid']>;
  index?: Maybe<Scalars['Int']>;
  movie_id?: Maybe<Scalars['uuid']>;
};

/** order by max() on columns of table "movie_genre" */
export type MovieGenreMaxOrderBy = {
  genre_id?: Maybe<OrderBy>;
  index?: Maybe<OrderBy>;
  movie_id?: Maybe<OrderBy>;
};

/** aggregate min on columns */
export type MovieGenreMinFields = {
  genre_id?: Maybe<Scalars['uuid']>;
  index?: Maybe<Scalars['Int']>;
  movie_id?: Maybe<Scalars['uuid']>;
};

/** order by min() on columns of table "movie_genre" */
export type MovieGenreMinOrderBy = {
  genre_id?: Maybe<OrderBy>;
  index?: Maybe<OrderBy>;
  movie_id?: Maybe<OrderBy>;
};

/** response of any mutation on the table "movie_genre" */
export type MovieGenreMutationResponse = {
  /** number of affected rows by the mutation */
  affected_rows: Scalars['Int'];
  /** data of the affected rows by the mutation */
  returning: Array<MovieGenre>;
};

/** input type for inserting object relation for remote table "movie_genre" */
export type MovieGenreObjRelInsertInput = {
  data: MovieGenreInsertInput;
  on_conflict?: Maybe<MovieGenreOnConflict>;
};

/** on conflict condition type for table "movie_genre" */
export type MovieGenreOnConflict = {
  constraint: MovieGenreConstraint;
  update_columns: Array<MovieGenreUpdateColumn>;
  where?: Maybe<MovieGenreBoolExp>;
};

/** ordering options when selecting data from "movie_genre" */
export type MovieGenreOrderBy = {
  genre?: Maybe<GenreOrderBy>;
  genre_id?: Maybe<OrderBy>;
  index?: Maybe<OrderBy>;
  movie?: Maybe<MovieOrderBy>;
  movie_id?: Maybe<OrderBy>;
};

/** primary key columns input for table: "movie_genre" */
export type MovieGenrePkColumnsInput = {
  index: Scalars['Int'];
  movie_id: Scalars['uuid'];
};

/** select columns of table "movie_genre" */
export enum MovieGenreSelectColumn {
  /** column name */
  GenreId = 'genre_id',
  /** column name */
  Index = 'index',
  /** column name */
  MovieId = 'movie_id'
}

/** input type for updating data in table "movie_genre" */
export type MovieGenreSetInput = {
  genre_id?: Maybe<Scalars['uuid']>;
  index?: Maybe<Scalars['Int']>;
  movie_id?: Maybe<Scalars['uuid']>;
};

/** aggregate stddev on columns */
export type MovieGenreStddevFields = {
  index?: Maybe<Scalars['Float']>;
};

/** order by stddev() on columns of table "movie_genre" */
export type MovieGenreStddevOrderBy = {
  index?: Maybe<OrderBy>;
};

/** aggregate stddev_pop on columns */
export type MovieGenreStddevPopFields = {
  index?: Maybe<Scalars['Float']>;
};

/** order by stddev_pop() on columns of table "movie_genre" */
export type MovieGenreStddevPopOrderBy = {
  index?: Maybe<OrderBy>;
};

/** aggregate stddev_samp on columns */
export type MovieGenreStddevSampFields = {
  index?: Maybe<Scalars['Float']>;
};

/** order by stddev_samp() on columns of table "movie_genre" */
export type MovieGenreStddevSampOrderBy = {
  index?: Maybe<OrderBy>;
};

/** aggregate sum on columns */
export type MovieGenreSumFields = {
  index?: Maybe<Scalars['Int']>;
};

/** order by sum() on columns of table "movie_genre" */
export type MovieGenreSumOrderBy = {
  index?: Maybe<OrderBy>;
};

/** update columns of table "movie_genre" */
export enum MovieGenreUpdateColumn {
  /** column name */
  GenreId = 'genre_id',
  /** column name */
  Index = 'index',
  /** column name */
  MovieId = 'movie_id'
}

/** aggregate var_pop on columns */
export type MovieGenreVarPopFields = {
  index?: Maybe<Scalars['Float']>;
};

/** order by var_pop() on columns of table "movie_genre" */
export type MovieGenreVarPopOrderBy = {
  index?: Maybe<OrderBy>;
};

/** aggregate var_samp on columns */
export type MovieGenreVarSampFields = {
  index?: Maybe<Scalars['Float']>;
};

/** order by var_samp() on columns of table "movie_genre" */
export type MovieGenreVarSampOrderBy = {
  index?: Maybe<OrderBy>;
};

/** aggregate variance on columns */
export type MovieGenreVarianceFields = {
  index?: Maybe<Scalars['Float']>;
};

/** order by variance() on columns of table "movie_genre" */
export type MovieGenreVarianceOrderBy = {
  index?: Maybe<OrderBy>;
};

/** input type for incrementing integer column in table "movie" */
export type MovieIncInput = {
  rating?: Maybe<Scalars['numeric']>;
};

/** input type for inserting data into table "movie" */
export type MovieInsertInput = {
  aired?: Maybe<Scalars['date']>;
  content_rating?: Maybe<Scalars['String']>;
  id?: Maybe<Scalars['uuid']>;
  image_background?: Maybe<Scalars['String']>;
  image_cover?: Maybe<Scalars['String']>;
  movie_collections?: Maybe<MovieCollectionArrRelInsertInput>;
  movie_countries?: Maybe<MovieCountryArrRelInsertInput>;
  movie_genres?: Maybe<MovieGenreArrRelInsertInput>;
  movie_staffs?: Maybe<MovieStaffArrRelInsertInput>;
  movie_studios?: Maybe<MovieStudioArrRelInsertInput>;
  name?: Maybe<Scalars['String']>;
  name_match?: Maybe<Scalars['String']>;
  name_original?: Maybe<Scalars['_text']>;
  name_sort?: Maybe<Scalars['String']>;
  rating?: Maybe<Scalars['numeric']>;
  summary?: Maybe<Scalars['String']>;
  tagline?: Maybe<Scalars['String']>;
};

/** aggregate max on columns */
export type MovieMaxFields = {
  aired?: Maybe<Scalars['date']>;
  content_rating?: Maybe<Scalars['String']>;
  id?: Maybe<Scalars['uuid']>;
  image_background?: Maybe<Scalars['String']>;
  image_cover?: Maybe<Scalars['String']>;
  name?: Maybe<Scalars['String']>;
  name_match?: Maybe<Scalars['String']>;
  name_sort?: Maybe<Scalars['String']>;
  rating?: Maybe<Scalars['numeric']>;
  summary?: Maybe<Scalars['String']>;
  tagline?: Maybe<Scalars['String']>;
};

/** order by max() on columns of table "movie" */
export type MovieMaxOrderBy = {
  aired?: Maybe<OrderBy>;
  content_rating?: Maybe<OrderBy>;
  id?: Maybe<OrderBy>;
  image_background?: Maybe<OrderBy>;
  image_cover?: Maybe<OrderBy>;
  name?: Maybe<OrderBy>;
  name_match?: Maybe<OrderBy>;
  name_sort?: Maybe<OrderBy>;
  rating?: Maybe<OrderBy>;
  summary?: Maybe<OrderBy>;
  tagline?: Maybe<OrderBy>;
};

/** aggregate min on columns */
export type MovieMinFields = {
  aired?: Maybe<Scalars['date']>;
  content_rating?: Maybe<Scalars['String']>;
  id?: Maybe<Scalars['uuid']>;
  image_background?: Maybe<Scalars['String']>;
  image_cover?: Maybe<Scalars['String']>;
  name?: Maybe<Scalars['String']>;
  name_match?: Maybe<Scalars['String']>;
  name_sort?: Maybe<Scalars['String']>;
  rating?: Maybe<Scalars['numeric']>;
  summary?: Maybe<Scalars['String']>;
  tagline?: Maybe<Scalars['String']>;
};

/** order by min() on columns of table "movie" */
export type MovieMinOrderBy = {
  aired?: Maybe<OrderBy>;
  content_rating?: Maybe<OrderBy>;
  id?: Maybe<OrderBy>;
  image_background?: Maybe<OrderBy>;
  image_cover?: Maybe<OrderBy>;
  name?: Maybe<OrderBy>;
  name_match?: Maybe<OrderBy>;
  name_sort?: Maybe<OrderBy>;
  rating?: Maybe<OrderBy>;
  summary?: Maybe<OrderBy>;
  tagline?: Maybe<OrderBy>;
};

/** response of any mutation on the table "movie" */
export type MovieMutationResponse = {
  /** number of affected rows by the mutation */
  affected_rows: Scalars['Int'];
  /** data of the affected rows by the mutation */
  returning: Array<Movie>;
};

/** input type for inserting object relation for remote table "movie" */
export type MovieObjRelInsertInput = {
  data: MovieInsertInput;
  on_conflict?: Maybe<MovieOnConflict>;
};

/** on conflict condition type for table "movie" */
export type MovieOnConflict = {
  constraint: MovieConstraint;
  update_columns: Array<MovieUpdateColumn>;
  where?: Maybe<MovieBoolExp>;
};

/** ordering options when selecting data from "movie" */
export type MovieOrderBy = {
  aired?: Maybe<OrderBy>;
  content_rating?: Maybe<OrderBy>;
  id?: Maybe<OrderBy>;
  image_background?: Maybe<OrderBy>;
  image_cover?: Maybe<OrderBy>;
  movie_collections_aggregate?: Maybe<MovieCollectionAggregateOrderBy>;
  movie_countries_aggregate?: Maybe<MovieCountryAggregateOrderBy>;
  movie_genres_aggregate?: Maybe<MovieGenreAggregateOrderBy>;
  movie_staffs_aggregate?: Maybe<MovieStaffAggregateOrderBy>;
  movie_studios_aggregate?: Maybe<MovieStudioAggregateOrderBy>;
  name?: Maybe<OrderBy>;
  name_match?: Maybe<OrderBy>;
  name_original?: Maybe<OrderBy>;
  name_sort?: Maybe<OrderBy>;
  rating?: Maybe<OrderBy>;
  summary?: Maybe<OrderBy>;
  tagline?: Maybe<OrderBy>;
};

/** primary key columns input for table: "movie" */
export type MoviePkColumnsInput = {
  id: Scalars['uuid'];
};

/** select columns of table "movie" */
export enum MovieSelectColumn {
  /** column name */
  Aired = 'aired',
  /** column name */
  ContentRating = 'content_rating',
  /** column name */
  Id = 'id',
  /** column name */
  ImageBackground = 'image_background',
  /** column name */
  ImageCover = 'image_cover',
  /** column name */
  Name = 'name',
  /** column name */
  NameMatch = 'name_match',
  /** column name */
  NameOriginal = 'name_original',
  /** column name */
  NameSort = 'name_sort',
  /** column name */
  Rating = 'rating',
  /** column name */
  Summary = 'summary',
  /** column name */
  Tagline = 'tagline'
}

/** input type for updating data in table "movie" */
export type MovieSetInput = {
  aired?: Maybe<Scalars['date']>;
  content_rating?: Maybe<Scalars['String']>;
  id?: Maybe<Scalars['uuid']>;
  image_background?: Maybe<Scalars['String']>;
  image_cover?: Maybe<Scalars['String']>;
  name?: Maybe<Scalars['String']>;
  name_match?: Maybe<Scalars['String']>;
  name_original?: Maybe<Scalars['_text']>;
  name_sort?: Maybe<Scalars['String']>;
  rating?: Maybe<Scalars['numeric']>;
  summary?: Maybe<Scalars['String']>;
  tagline?: Maybe<Scalars['String']>;
};

/** columns and relationships of "movie_staff" */
export type MovieStaff = {
  custom_staff_name?: Maybe<Scalars['String']>;
  id: Scalars['uuid'];
  index: Scalars['Int'];
  /** An object relationship */
  movie: Movie;
  movie_id: Scalars['uuid'];
  /** An object relationship */
  person: Person;
  person_id: Scalars['uuid'];
  /** An object relationship */
  staff: Staff;
  staff_id: Scalars['uuid'];
};

/** aggregated selection of "movie_staff" */
export type MovieStaffAggregate = {
  aggregate?: Maybe<MovieStaffAggregateFields>;
  nodes: Array<MovieStaff>;
};

/** aggregate fields of "movie_staff" */
export type MovieStaffAggregateFields = {
  avg?: Maybe<MovieStaffAvgFields>;
  count?: Maybe<Scalars['Int']>;
  max?: Maybe<MovieStaffMaxFields>;
  min?: Maybe<MovieStaffMinFields>;
  stddev?: Maybe<MovieStaffStddevFields>;
  stddev_pop?: Maybe<MovieStaffStddevPopFields>;
  stddev_samp?: Maybe<MovieStaffStddevSampFields>;
  sum?: Maybe<MovieStaffSumFields>;
  var_pop?: Maybe<MovieStaffVarPopFields>;
  var_samp?: Maybe<MovieStaffVarSampFields>;
  variance?: Maybe<MovieStaffVarianceFields>;
};


/** aggregate fields of "movie_staff" */
export type MovieStaffAggregateFieldsCountArgs = {
  columns?: Maybe<Array<MovieStaffSelectColumn>>;
  distinct?: Maybe<Scalars['Boolean']>;
};

/** order by aggregate values of table "movie_staff" */
export type MovieStaffAggregateOrderBy = {
  avg?: Maybe<MovieStaffAvgOrderBy>;
  count?: Maybe<OrderBy>;
  max?: Maybe<MovieStaffMaxOrderBy>;
  min?: Maybe<MovieStaffMinOrderBy>;
  stddev?: Maybe<MovieStaffStddevOrderBy>;
  stddev_pop?: Maybe<MovieStaffStddevPopOrderBy>;
  stddev_samp?: Maybe<MovieStaffStddevSampOrderBy>;
  sum?: Maybe<MovieStaffSumOrderBy>;
  var_pop?: Maybe<MovieStaffVarPopOrderBy>;
  var_samp?: Maybe<MovieStaffVarSampOrderBy>;
  variance?: Maybe<MovieStaffVarianceOrderBy>;
};

/** input type for inserting array relation for remote table "movie_staff" */
export type MovieStaffArrRelInsertInput = {
  data: Array<MovieStaffInsertInput>;
  on_conflict?: Maybe<MovieStaffOnConflict>;
};

/** aggregate avg on columns */
export type MovieStaffAvgFields = {
  index?: Maybe<Scalars['Float']>;
};

/** order by avg() on columns of table "movie_staff" */
export type MovieStaffAvgOrderBy = {
  index?: Maybe<OrderBy>;
};

/** Boolean expression to filter rows from the table "movie_staff". All fields are combined with a logical 'AND'. */
export type MovieStaffBoolExp = {
  _and?: Maybe<Array<Maybe<MovieStaffBoolExp>>>;
  _not?: Maybe<MovieStaffBoolExp>;
  _or?: Maybe<Array<Maybe<MovieStaffBoolExp>>>;
  custom_staff_name?: Maybe<StringComparisonExp>;
  id?: Maybe<UuidComparisonExp>;
  index?: Maybe<IntComparisonExp>;
  movie?: Maybe<MovieBoolExp>;
  movie_id?: Maybe<UuidComparisonExp>;
  person?: Maybe<PersonBoolExp>;
  person_id?: Maybe<UuidComparisonExp>;
  staff?: Maybe<StaffBoolExp>;
  staff_id?: Maybe<UuidComparisonExp>;
};

/** unique or primary key constraints on table "movie_staff" */
export enum MovieStaffConstraint {
  /** unique or primary key constraint */
  MovieStaffMovieIdStaffIdIndexKey = 'movie_staff_movie_id_staff_id_index_key',
  /** unique or primary key constraint */
  MovieStaffMovieIdStaffIdPersonIdKey = 'movie_staff_movie_id_staff_id_person_id_key',
  /** unique or primary key constraint */
  MovieStaffPkey = 'movie_staff_pkey'
}

/** input type for incrementing integer column in table "movie_staff" */
export type MovieStaffIncInput = {
  index?: Maybe<Scalars['Int']>;
};

/** input type for inserting data into table "movie_staff" */
export type MovieStaffInsertInput = {
  custom_staff_name?: Maybe<Scalars['String']>;
  id?: Maybe<Scalars['uuid']>;
  index?: Maybe<Scalars['Int']>;
  movie?: Maybe<MovieObjRelInsertInput>;
  movie_id?: Maybe<Scalars['uuid']>;
  person?: Maybe<PersonObjRelInsertInput>;
  person_id?: Maybe<Scalars['uuid']>;
  staff?: Maybe<StaffObjRelInsertInput>;
  staff_id?: Maybe<Scalars['uuid']>;
};

/** aggregate max on columns */
export type MovieStaffMaxFields = {
  custom_staff_name?: Maybe<Scalars['String']>;
  id?: Maybe<Scalars['uuid']>;
  index?: Maybe<Scalars['Int']>;
  movie_id?: Maybe<Scalars['uuid']>;
  person_id?: Maybe<Scalars['uuid']>;
  staff_id?: Maybe<Scalars['uuid']>;
};

/** order by max() on columns of table "movie_staff" */
export type MovieStaffMaxOrderBy = {
  custom_staff_name?: Maybe<OrderBy>;
  id?: Maybe<OrderBy>;
  index?: Maybe<OrderBy>;
  movie_id?: Maybe<OrderBy>;
  person_id?: Maybe<OrderBy>;
  staff_id?: Maybe<OrderBy>;
};

/** aggregate min on columns */
export type MovieStaffMinFields = {
  custom_staff_name?: Maybe<Scalars['String']>;
  id?: Maybe<Scalars['uuid']>;
  index?: Maybe<Scalars['Int']>;
  movie_id?: Maybe<Scalars['uuid']>;
  person_id?: Maybe<Scalars['uuid']>;
  staff_id?: Maybe<Scalars['uuid']>;
};

/** order by min() on columns of table "movie_staff" */
export type MovieStaffMinOrderBy = {
  custom_staff_name?: Maybe<OrderBy>;
  id?: Maybe<OrderBy>;
  index?: Maybe<OrderBy>;
  movie_id?: Maybe<OrderBy>;
  person_id?: Maybe<OrderBy>;
  staff_id?: Maybe<OrderBy>;
};

/** response of any mutation on the table "movie_staff" */
export type MovieStaffMutationResponse = {
  /** number of affected rows by the mutation */
  affected_rows: Scalars['Int'];
  /** data of the affected rows by the mutation */
  returning: Array<MovieStaff>;
};

/** input type for inserting object relation for remote table "movie_staff" */
export type MovieStaffObjRelInsertInput = {
  data: MovieStaffInsertInput;
  on_conflict?: Maybe<MovieStaffOnConflict>;
};

/** on conflict condition type for table "movie_staff" */
export type MovieStaffOnConflict = {
  constraint: MovieStaffConstraint;
  update_columns: Array<MovieStaffUpdateColumn>;
  where?: Maybe<MovieStaffBoolExp>;
};

/** ordering options when selecting data from "movie_staff" */
export type MovieStaffOrderBy = {
  custom_staff_name?: Maybe<OrderBy>;
  id?: Maybe<OrderBy>;
  index?: Maybe<OrderBy>;
  movie?: Maybe<MovieOrderBy>;
  movie_id?: Maybe<OrderBy>;
  person?: Maybe<PersonOrderBy>;
  person_id?: Maybe<OrderBy>;
  staff?: Maybe<StaffOrderBy>;
  staff_id?: Maybe<OrderBy>;
};

/** primary key columns input for table: "movie_staff" */
export type MovieStaffPkColumnsInput = {
  id: Scalars['uuid'];
};

/** select columns of table "movie_staff" */
export enum MovieStaffSelectColumn {
  /** column name */
  CustomStaffName = 'custom_staff_name',
  /** column name */
  Id = 'id',
  /** column name */
  Index = 'index',
  /** column name */
  MovieId = 'movie_id',
  /** column name */
  PersonId = 'person_id',
  /** column name */
  StaffId = 'staff_id'
}

/** input type for updating data in table "movie_staff" */
export type MovieStaffSetInput = {
  custom_staff_name?: Maybe<Scalars['String']>;
  id?: Maybe<Scalars['uuid']>;
  index?: Maybe<Scalars['Int']>;
  movie_id?: Maybe<Scalars['uuid']>;
  person_id?: Maybe<Scalars['uuid']>;
  staff_id?: Maybe<Scalars['uuid']>;
};

/** aggregate stddev on columns */
export type MovieStaffStddevFields = {
  index?: Maybe<Scalars['Float']>;
};

/** order by stddev() on columns of table "movie_staff" */
export type MovieStaffStddevOrderBy = {
  index?: Maybe<OrderBy>;
};

/** aggregate stddev_pop on columns */
export type MovieStaffStddevPopFields = {
  index?: Maybe<Scalars['Float']>;
};

/** order by stddev_pop() on columns of table "movie_staff" */
export type MovieStaffStddevPopOrderBy = {
  index?: Maybe<OrderBy>;
};

/** aggregate stddev_samp on columns */
export type MovieStaffStddevSampFields = {
  index?: Maybe<Scalars['Float']>;
};

/** order by stddev_samp() on columns of table "movie_staff" */
export type MovieStaffStddevSampOrderBy = {
  index?: Maybe<OrderBy>;
};

/** aggregate sum on columns */
export type MovieStaffSumFields = {
  index?: Maybe<Scalars['Int']>;
};

/** order by sum() on columns of table "movie_staff" */
export type MovieStaffSumOrderBy = {
  index?: Maybe<OrderBy>;
};

/** update columns of table "movie_staff" */
export enum MovieStaffUpdateColumn {
  /** column name */
  CustomStaffName = 'custom_staff_name',
  /** column name */
  Id = 'id',
  /** column name */
  Index = 'index',
  /** column name */
  MovieId = 'movie_id',
  /** column name */
  PersonId = 'person_id',
  /** column name */
  StaffId = 'staff_id'
}

/** aggregate var_pop on columns */
export type MovieStaffVarPopFields = {
  index?: Maybe<Scalars['Float']>;
};

/** order by var_pop() on columns of table "movie_staff" */
export type MovieStaffVarPopOrderBy = {
  index?: Maybe<OrderBy>;
};

/** aggregate var_samp on columns */
export type MovieStaffVarSampFields = {
  index?: Maybe<Scalars['Float']>;
};

/** order by var_samp() on columns of table "movie_staff" */
export type MovieStaffVarSampOrderBy = {
  index?: Maybe<OrderBy>;
};

/** aggregate variance on columns */
export type MovieStaffVarianceFields = {
  index?: Maybe<Scalars['Float']>;
};

/** order by variance() on columns of table "movie_staff" */
export type MovieStaffVarianceOrderBy = {
  index?: Maybe<OrderBy>;
};

/** aggregate stddev on columns */
export type MovieStddevFields = {
  rating?: Maybe<Scalars['Float']>;
};

/** order by stddev() on columns of table "movie" */
export type MovieStddevOrderBy = {
  rating?: Maybe<OrderBy>;
};

/** aggregate stddev_pop on columns */
export type MovieStddevPopFields = {
  rating?: Maybe<Scalars['Float']>;
};

/** order by stddev_pop() on columns of table "movie" */
export type MovieStddevPopOrderBy = {
  rating?: Maybe<OrderBy>;
};

/** aggregate stddev_samp on columns */
export type MovieStddevSampFields = {
  rating?: Maybe<Scalars['Float']>;
};

/** order by stddev_samp() on columns of table "movie" */
export type MovieStddevSampOrderBy = {
  rating?: Maybe<OrderBy>;
};

/** columns and relationships of "movie_studio" */
export type MovieStudio = {
  index: Scalars['Int'];
  /** An object relationship */
  movie: Movie;
  movie_id: Scalars['uuid'];
  /** An object relationship */
  studio: Studio;
  studio_id: Scalars['uuid'];
};

/** aggregated selection of "movie_studio" */
export type MovieStudioAggregate = {
  aggregate?: Maybe<MovieStudioAggregateFields>;
  nodes: Array<MovieStudio>;
};

/** aggregate fields of "movie_studio" */
export type MovieStudioAggregateFields = {
  avg?: Maybe<MovieStudioAvgFields>;
  count?: Maybe<Scalars['Int']>;
  max?: Maybe<MovieStudioMaxFields>;
  min?: Maybe<MovieStudioMinFields>;
  stddev?: Maybe<MovieStudioStddevFields>;
  stddev_pop?: Maybe<MovieStudioStddevPopFields>;
  stddev_samp?: Maybe<MovieStudioStddevSampFields>;
  sum?: Maybe<MovieStudioSumFields>;
  var_pop?: Maybe<MovieStudioVarPopFields>;
  var_samp?: Maybe<MovieStudioVarSampFields>;
  variance?: Maybe<MovieStudioVarianceFields>;
};


/** aggregate fields of "movie_studio" */
export type MovieStudioAggregateFieldsCountArgs = {
  columns?: Maybe<Array<MovieStudioSelectColumn>>;
  distinct?: Maybe<Scalars['Boolean']>;
};

/** order by aggregate values of table "movie_studio" */
export type MovieStudioAggregateOrderBy = {
  avg?: Maybe<MovieStudioAvgOrderBy>;
  count?: Maybe<OrderBy>;
  max?: Maybe<MovieStudioMaxOrderBy>;
  min?: Maybe<MovieStudioMinOrderBy>;
  stddev?: Maybe<MovieStudioStddevOrderBy>;
  stddev_pop?: Maybe<MovieStudioStddevPopOrderBy>;
  stddev_samp?: Maybe<MovieStudioStddevSampOrderBy>;
  sum?: Maybe<MovieStudioSumOrderBy>;
  var_pop?: Maybe<MovieStudioVarPopOrderBy>;
  var_samp?: Maybe<MovieStudioVarSampOrderBy>;
  variance?: Maybe<MovieStudioVarianceOrderBy>;
};

/** input type for inserting array relation for remote table "movie_studio" */
export type MovieStudioArrRelInsertInput = {
  data: Array<MovieStudioInsertInput>;
  on_conflict?: Maybe<MovieStudioOnConflict>;
};

/** aggregate avg on columns */
export type MovieStudioAvgFields = {
  index?: Maybe<Scalars['Float']>;
};

/** order by avg() on columns of table "movie_studio" */
export type MovieStudioAvgOrderBy = {
  index?: Maybe<OrderBy>;
};

/** Boolean expression to filter rows from the table "movie_studio". All fields are combined with a logical 'AND'. */
export type MovieStudioBoolExp = {
  _and?: Maybe<Array<Maybe<MovieStudioBoolExp>>>;
  _not?: Maybe<MovieStudioBoolExp>;
  _or?: Maybe<Array<Maybe<MovieStudioBoolExp>>>;
  index?: Maybe<IntComparisonExp>;
  movie?: Maybe<MovieBoolExp>;
  movie_id?: Maybe<UuidComparisonExp>;
  studio?: Maybe<StudioBoolExp>;
  studio_id?: Maybe<UuidComparisonExp>;
};

/** unique or primary key constraints on table "movie_studio" */
export enum MovieStudioConstraint {
  /** unique or primary key constraint */
  MovieStudioMovieIdStudioIdKey = 'movie_studio_movie_id_studio_id_key',
  /** unique or primary key constraint */
  MovieStudioPkey = 'movie_studio_pkey'
}

/** input type for incrementing integer column in table "movie_studio" */
export type MovieStudioIncInput = {
  index?: Maybe<Scalars['Int']>;
};

/** input type for inserting data into table "movie_studio" */
export type MovieStudioInsertInput = {
  index?: Maybe<Scalars['Int']>;
  movie?: Maybe<MovieObjRelInsertInput>;
  movie_id?: Maybe<Scalars['uuid']>;
  studio?: Maybe<StudioObjRelInsertInput>;
  studio_id?: Maybe<Scalars['uuid']>;
};

/** aggregate max on columns */
export type MovieStudioMaxFields = {
  index?: Maybe<Scalars['Int']>;
  movie_id?: Maybe<Scalars['uuid']>;
  studio_id?: Maybe<Scalars['uuid']>;
};

/** order by max() on columns of table "movie_studio" */
export type MovieStudioMaxOrderBy = {
  index?: Maybe<OrderBy>;
  movie_id?: Maybe<OrderBy>;
  studio_id?: Maybe<OrderBy>;
};

/** aggregate min on columns */
export type MovieStudioMinFields = {
  index?: Maybe<Scalars['Int']>;
  movie_id?: Maybe<Scalars['uuid']>;
  studio_id?: Maybe<Scalars['uuid']>;
};

/** order by min() on columns of table "movie_studio" */
export type MovieStudioMinOrderBy = {
  index?: Maybe<OrderBy>;
  movie_id?: Maybe<OrderBy>;
  studio_id?: Maybe<OrderBy>;
};

/** response of any mutation on the table "movie_studio" */
export type MovieStudioMutationResponse = {
  /** number of affected rows by the mutation */
  affected_rows: Scalars['Int'];
  /** data of the affected rows by the mutation */
  returning: Array<MovieStudio>;
};

/** input type for inserting object relation for remote table "movie_studio" */
export type MovieStudioObjRelInsertInput = {
  data: MovieStudioInsertInput;
  on_conflict?: Maybe<MovieStudioOnConflict>;
};

/** on conflict condition type for table "movie_studio" */
export type MovieStudioOnConflict = {
  constraint: MovieStudioConstraint;
  update_columns: Array<MovieStudioUpdateColumn>;
  where?: Maybe<MovieStudioBoolExp>;
};

/** ordering options when selecting data from "movie_studio" */
export type MovieStudioOrderBy = {
  index?: Maybe<OrderBy>;
  movie?: Maybe<MovieOrderBy>;
  movie_id?: Maybe<OrderBy>;
  studio?: Maybe<StudioOrderBy>;
  studio_id?: Maybe<OrderBy>;
};

/** primary key columns input for table: "movie_studio" */
export type MovieStudioPkColumnsInput = {
  index: Scalars['Int'];
  movie_id: Scalars['uuid'];
};

/** select columns of table "movie_studio" */
export enum MovieStudioSelectColumn {
  /** column name */
  Index = 'index',
  /** column name */
  MovieId = 'movie_id',
  /** column name */
  StudioId = 'studio_id'
}

/** input type for updating data in table "movie_studio" */
export type MovieStudioSetInput = {
  index?: Maybe<Scalars['Int']>;
  movie_id?: Maybe<Scalars['uuid']>;
  studio_id?: Maybe<Scalars['uuid']>;
};

/** aggregate stddev on columns */
export type MovieStudioStddevFields = {
  index?: Maybe<Scalars['Float']>;
};

/** order by stddev() on columns of table "movie_studio" */
export type MovieStudioStddevOrderBy = {
  index?: Maybe<OrderBy>;
};

/** aggregate stddev_pop on columns */
export type MovieStudioStddevPopFields = {
  index?: Maybe<Scalars['Float']>;
};

/** order by stddev_pop() on columns of table "movie_studio" */
export type MovieStudioStddevPopOrderBy = {
  index?: Maybe<OrderBy>;
};

/** aggregate stddev_samp on columns */
export type MovieStudioStddevSampFields = {
  index?: Maybe<Scalars['Float']>;
};

/** order by stddev_samp() on columns of table "movie_studio" */
export type MovieStudioStddevSampOrderBy = {
  index?: Maybe<OrderBy>;
};

/** aggregate sum on columns */
export type MovieStudioSumFields = {
  index?: Maybe<Scalars['Int']>;
};

/** order by sum() on columns of table "movie_studio" */
export type MovieStudioSumOrderBy = {
  index?: Maybe<OrderBy>;
};

/** update columns of table "movie_studio" */
export enum MovieStudioUpdateColumn {
  /** column name */
  Index = 'index',
  /** column name */
  MovieId = 'movie_id',
  /** column name */
  StudioId = 'studio_id'
}

/** aggregate var_pop on columns */
export type MovieStudioVarPopFields = {
  index?: Maybe<Scalars['Float']>;
};

/** order by var_pop() on columns of table "movie_studio" */
export type MovieStudioVarPopOrderBy = {
  index?: Maybe<OrderBy>;
};

/** aggregate var_samp on columns */
export type MovieStudioVarSampFields = {
  index?: Maybe<Scalars['Float']>;
};

/** order by var_samp() on columns of table "movie_studio" */
export type MovieStudioVarSampOrderBy = {
  index?: Maybe<OrderBy>;
};

/** aggregate variance on columns */
export type MovieStudioVarianceFields = {
  index?: Maybe<Scalars['Float']>;
};

/** order by variance() on columns of table "movie_studio" */
export type MovieStudioVarianceOrderBy = {
  index?: Maybe<OrderBy>;
};

/** aggregate sum on columns */
export type MovieSumFields = {
  rating?: Maybe<Scalars['numeric']>;
};

/** order by sum() on columns of table "movie" */
export type MovieSumOrderBy = {
  rating?: Maybe<OrderBy>;
};

/** update columns of table "movie" */
export enum MovieUpdateColumn {
  /** column name */
  Aired = 'aired',
  /** column name */
  ContentRating = 'content_rating',
  /** column name */
  Id = 'id',
  /** column name */
  ImageBackground = 'image_background',
  /** column name */
  ImageCover = 'image_cover',
  /** column name */
  Name = 'name',
  /** column name */
  NameMatch = 'name_match',
  /** column name */
  NameOriginal = 'name_original',
  /** column name */
  NameSort = 'name_sort',
  /** column name */
  Rating = 'rating',
  /** column name */
  Summary = 'summary',
  /** column name */
  Tagline = 'tagline'
}

/** aggregate var_pop on columns */
export type MovieVarPopFields = {
  rating?: Maybe<Scalars['Float']>;
};

/** order by var_pop() on columns of table "movie" */
export type MovieVarPopOrderBy = {
  rating?: Maybe<OrderBy>;
};

/** aggregate var_samp on columns */
export type MovieVarSampFields = {
  rating?: Maybe<Scalars['Float']>;
};

/** order by var_samp() on columns of table "movie" */
export type MovieVarSampOrderBy = {
  rating?: Maybe<OrderBy>;
};

/** aggregate variance on columns */
export type MovieVarianceFields = {
  rating?: Maybe<Scalars['Float']>;
};

/** order by variance() on columns of table "movie" */
export type MovieVarianceOrderBy = {
  rating?: Maybe<OrderBy>;
};

/** mutation root */
export type MutationRoot = {
  /** delete data from the table: "album" */
  delete_album?: Maybe<AlbumMutationResponse>;
  /** delete single row from the table: "album" */
  delete_album_by_pk?: Maybe<Album>;
  /** delete data from the table: "album_collection" */
  delete_album_collection?: Maybe<AlbumCollectionMutationResponse>;
  /** delete single row from the table: "album_collection" */
  delete_album_collection_by_pk?: Maybe<AlbumCollection>;
  /** delete data from the table: "album_country" */
  delete_album_country?: Maybe<AlbumCountryMutationResponse>;
  /** delete single row from the table: "album_country" */
  delete_album_country_by_pk?: Maybe<AlbumCountry>;
  /** delete data from the table: "album_genre" */
  delete_album_genre?: Maybe<AlbumGenreMutationResponse>;
  /** delete single row from the table: "album_genre" */
  delete_album_genre_by_pk?: Maybe<AlbumGenre>;
  /** delete data from the table: "album_mood" */
  delete_album_mood?: Maybe<AlbumMoodMutationResponse>;
  /** delete single row from the table: "album_mood" */
  delete_album_mood_by_pk?: Maybe<AlbumMood>;
  /** delete data from the table: "album_studio" */
  delete_album_studio?: Maybe<AlbumStudioMutationResponse>;
  /** delete single row from the table: "album_studio" */
  delete_album_studio_by_pk?: Maybe<AlbumStudio>;
  /** delete data from the table: "album_style" */
  delete_album_style?: Maybe<AlbumStyleMutationResponse>;
  /** delete single row from the table: "album_style" */
  delete_album_style_by_pk?: Maybe<AlbumStyle>;
  /** delete data from the table: "album_track" */
  delete_album_track?: Maybe<AlbumTrackMutationResponse>;
  /** delete single row from the table: "album_track" */
  delete_album_track_by_pk?: Maybe<AlbumTrack>;
  /** delete data from the table: "artist" */
  delete_artist?: Maybe<ArtistMutationResponse>;
  /** delete single row from the table: "artist" */
  delete_artist_by_pk?: Maybe<Artist>;
  /** delete data from the table: "artist_collection" */
  delete_artist_collection?: Maybe<ArtistCollectionMutationResponse>;
  /** delete single row from the table: "artist_collection" */
  delete_artist_collection_by_pk?: Maybe<ArtistCollection>;
  /** delete data from the table: "artist_country" */
  delete_artist_country?: Maybe<ArtistCountryMutationResponse>;
  /** delete single row from the table: "artist_country" */
  delete_artist_country_by_pk?: Maybe<ArtistCountry>;
  /** delete data from the table: "artist_genre" */
  delete_artist_genre?: Maybe<ArtistGenreMutationResponse>;
  /** delete single row from the table: "artist_genre" */
  delete_artist_genre_by_pk?: Maybe<ArtistGenre>;
  /** delete data from the table: "artist_mood" */
  delete_artist_mood?: Maybe<ArtistMoodMutationResponse>;
  /** delete single row from the table: "artist_mood" */
  delete_artist_mood_by_pk?: Maybe<ArtistMood>;
  /** delete data from the table: "artist_similar" */
  delete_artist_similar?: Maybe<ArtistSimilarMutationResponse>;
  /** delete single row from the table: "artist_similar" */
  delete_artist_similar_by_pk?: Maybe<ArtistSimilar>;
  /** delete data from the table: "artist_style" */
  delete_artist_style?: Maybe<ArtistStyleMutationResponse>;
  /** delete single row from the table: "artist_style" */
  delete_artist_style_by_pk?: Maybe<ArtistStyle>;
  /** delete data from the table: "collection" */
  delete_collection?: Maybe<CollectionMutationResponse>;
  /** delete single row from the table: "collection" */
  delete_collection_by_pk?: Maybe<Collection>;
  /** delete data from the table: "country" */
  delete_country?: Maybe<CountryMutationResponse>;
  /** delete single row from the table: "country" */
  delete_country_by_pk?: Maybe<Country>;
  /** delete data from the table: "episode" */
  delete_episode?: Maybe<EpisodeMutationResponse>;
  /** delete single row from the table: "episode" */
  delete_episode_by_pk?: Maybe<Episode>;
  /** delete data from the table: "episode_staff" */
  delete_episode_staff?: Maybe<EpisodeStaffMutationResponse>;
  /** delete single row from the table: "episode_staff" */
  delete_episode_staff_by_pk?: Maybe<EpisodeStaff>;
  /** delete data from the table: "genre" */
  delete_genre?: Maybe<GenreMutationResponse>;
  /** delete single row from the table: "genre" */
  delete_genre_by_pk?: Maybe<Genre>;
  /** delete data from the table: "mood" */
  delete_mood?: Maybe<MoodMutationResponse>;
  /** delete single row from the table: "mood" */
  delete_mood_by_pk?: Maybe<Mood>;
  /** delete data from the table: "movie" */
  delete_movie?: Maybe<MovieMutationResponse>;
  /** delete single row from the table: "movie" */
  delete_movie_by_pk?: Maybe<Movie>;
  /** delete data from the table: "movie_collection" */
  delete_movie_collection?: Maybe<MovieCollectionMutationResponse>;
  /** delete single row from the table: "movie_collection" */
  delete_movie_collection_by_pk?: Maybe<MovieCollection>;
  /** delete data from the table: "movie_country" */
  delete_movie_country?: Maybe<MovieCountryMutationResponse>;
  /** delete single row from the table: "movie_country" */
  delete_movie_country_by_pk?: Maybe<MovieCountry>;
  /** delete data from the table: "movie_genre" */
  delete_movie_genre?: Maybe<MovieGenreMutationResponse>;
  /** delete single row from the table: "movie_genre" */
  delete_movie_genre_by_pk?: Maybe<MovieGenre>;
  /** delete data from the table: "movie_staff" */
  delete_movie_staff?: Maybe<MovieStaffMutationResponse>;
  /** delete single row from the table: "movie_staff" */
  delete_movie_staff_by_pk?: Maybe<MovieStaff>;
  /** delete data from the table: "movie_studio" */
  delete_movie_studio?: Maybe<MovieStudioMutationResponse>;
  /** delete single row from the table: "movie_studio" */
  delete_movie_studio_by_pk?: Maybe<MovieStudio>;
  /** delete data from the table: "person" */
  delete_person?: Maybe<PersonMutationResponse>;
  /** delete single row from the table: "person" */
  delete_person_by_pk?: Maybe<Person>;
  /** delete data from the table: "show" */
  delete_show?: Maybe<ShowMutationResponse>;
  /** delete single row from the table: "show" */
  delete_show_by_pk?: Maybe<Show>;
  /** delete data from the table: "show_collection" */
  delete_show_collection?: Maybe<ShowCollectionMutationResponse>;
  /** delete single row from the table: "show_collection" */
  delete_show_collection_by_pk?: Maybe<ShowCollection>;
  /** delete data from the table: "show_genre" */
  delete_show_genre?: Maybe<ShowGenreMutationResponse>;
  /** delete single row from the table: "show_genre" */
  delete_show_genre_by_pk?: Maybe<ShowGenre>;
  /** delete data from the table: "show_season" */
  delete_show_season?: Maybe<ShowSeasonMutationResponse>;
  /** delete single row from the table: "show_season" */
  delete_show_season_by_pk?: Maybe<ShowSeason>;
  /** delete data from the table: "show_staff" */
  delete_show_staff?: Maybe<ShowStaffMutationResponse>;
  /** delete single row from the table: "show_staff" */
  delete_show_staff_by_pk?: Maybe<ShowStaff>;
  /** delete data from the table: "staff" */
  delete_staff?: Maybe<StaffMutationResponse>;
  /** delete single row from the table: "staff" */
  delete_staff_by_pk?: Maybe<Staff>;
  /** delete data from the table: "studio" */
  delete_studio?: Maybe<StudioMutationResponse>;
  /** delete single row from the table: "studio" */
  delete_studio_by_pk?: Maybe<Studio>;
  /** delete data from the table: "style" */
  delete_style?: Maybe<StyleMutationResponse>;
  /** delete single row from the table: "style" */
  delete_style_by_pk?: Maybe<Style>;
  /** delete data from the table: "track" */
  delete_track?: Maybe<TrackMutationResponse>;
  /** delete single row from the table: "track" */
  delete_track_by_pk?: Maybe<Track>;
  /** delete data from the table: "track_genre" */
  delete_track_genre?: Maybe<TrackGenreMutationResponse>;
  /** delete single row from the table: "track_genre" */
  delete_track_genre_by_pk?: Maybe<TrackGenre>;
  /** delete data from the table: "track_mood" */
  delete_track_mood?: Maybe<TrackMoodMutationResponse>;
  /** delete single row from the table: "track_mood" */
  delete_track_mood_by_pk?: Maybe<TrackMood>;
  /** insert data into the table: "album" */
  insert_album?: Maybe<AlbumMutationResponse>;
  /** insert data into the table: "album_collection" */
  insert_album_collection?: Maybe<AlbumCollectionMutationResponse>;
  /** insert a single row into the table: "album_collection" */
  insert_album_collection_one?: Maybe<AlbumCollection>;
  /** insert data into the table: "album_country" */
  insert_album_country?: Maybe<AlbumCountryMutationResponse>;
  /** insert a single row into the table: "album_country" */
  insert_album_country_one?: Maybe<AlbumCountry>;
  /** insert data into the table: "album_genre" */
  insert_album_genre?: Maybe<AlbumGenreMutationResponse>;
  /** insert a single row into the table: "album_genre" */
  insert_album_genre_one?: Maybe<AlbumGenre>;
  /** insert data into the table: "album_mood" */
  insert_album_mood?: Maybe<AlbumMoodMutationResponse>;
  /** insert a single row into the table: "album_mood" */
  insert_album_mood_one?: Maybe<AlbumMood>;
  /** insert a single row into the table: "album" */
  insert_album_one?: Maybe<Album>;
  /** insert data into the table: "album_studio" */
  insert_album_studio?: Maybe<AlbumStudioMutationResponse>;
  /** insert a single row into the table: "album_studio" */
  insert_album_studio_one?: Maybe<AlbumStudio>;
  /** insert data into the table: "album_style" */
  insert_album_style?: Maybe<AlbumStyleMutationResponse>;
  /** insert a single row into the table: "album_style" */
  insert_album_style_one?: Maybe<AlbumStyle>;
  /** insert data into the table: "album_track" */
  insert_album_track?: Maybe<AlbumTrackMutationResponse>;
  /** insert a single row into the table: "album_track" */
  insert_album_track_one?: Maybe<AlbumTrack>;
  /** insert data into the table: "artist" */
  insert_artist?: Maybe<ArtistMutationResponse>;
  /** insert data into the table: "artist_collection" */
  insert_artist_collection?: Maybe<ArtistCollectionMutationResponse>;
  /** insert a single row into the table: "artist_collection" */
  insert_artist_collection_one?: Maybe<ArtistCollection>;
  /** insert data into the table: "artist_country" */
  insert_artist_country?: Maybe<ArtistCountryMutationResponse>;
  /** insert a single row into the table: "artist_country" */
  insert_artist_country_one?: Maybe<ArtistCountry>;
  /** insert data into the table: "artist_genre" */
  insert_artist_genre?: Maybe<ArtistGenreMutationResponse>;
  /** insert a single row into the table: "artist_genre" */
  insert_artist_genre_one?: Maybe<ArtistGenre>;
  /** insert data into the table: "artist_mood" */
  insert_artist_mood?: Maybe<ArtistMoodMutationResponse>;
  /** insert a single row into the table: "artist_mood" */
  insert_artist_mood_one?: Maybe<ArtistMood>;
  /** insert a single row into the table: "artist" */
  insert_artist_one?: Maybe<Artist>;
  /** insert data into the table: "artist_similar" */
  insert_artist_similar?: Maybe<ArtistSimilarMutationResponse>;
  /** insert a single row into the table: "artist_similar" */
  insert_artist_similar_one?: Maybe<ArtistSimilar>;
  /** insert data into the table: "artist_style" */
  insert_artist_style?: Maybe<ArtistStyleMutationResponse>;
  /** insert a single row into the table: "artist_style" */
  insert_artist_style_one?: Maybe<ArtistStyle>;
  /** insert data into the table: "collection" */
  insert_collection?: Maybe<CollectionMutationResponse>;
  /** insert a single row into the table: "collection" */
  insert_collection_one?: Maybe<Collection>;
  /** insert data into the table: "country" */
  insert_country?: Maybe<CountryMutationResponse>;
  /** insert a single row into the table: "country" */
  insert_country_one?: Maybe<Country>;
  /** insert data into the table: "episode" */
  insert_episode?: Maybe<EpisodeMutationResponse>;
  /** insert a single row into the table: "episode" */
  insert_episode_one?: Maybe<Episode>;
  /** insert data into the table: "episode_staff" */
  insert_episode_staff?: Maybe<EpisodeStaffMutationResponse>;
  /** insert a single row into the table: "episode_staff" */
  insert_episode_staff_one?: Maybe<EpisodeStaff>;
  /** insert data into the table: "genre" */
  insert_genre?: Maybe<GenreMutationResponse>;
  /** insert a single row into the table: "genre" */
  insert_genre_one?: Maybe<Genre>;
  /** insert data into the table: "mood" */
  insert_mood?: Maybe<MoodMutationResponse>;
  /** insert a single row into the table: "mood" */
  insert_mood_one?: Maybe<Mood>;
  /** insert data into the table: "movie" */
  insert_movie?: Maybe<MovieMutationResponse>;
  /** insert data into the table: "movie_collection" */
  insert_movie_collection?: Maybe<MovieCollectionMutationResponse>;
  /** insert a single row into the table: "movie_collection" */
  insert_movie_collection_one?: Maybe<MovieCollection>;
  /** insert data into the table: "movie_country" */
  insert_movie_country?: Maybe<MovieCountryMutationResponse>;
  /** insert a single row into the table: "movie_country" */
  insert_movie_country_one?: Maybe<MovieCountry>;
  /** insert data into the table: "movie_genre" */
  insert_movie_genre?: Maybe<MovieGenreMutationResponse>;
  /** insert a single row into the table: "movie_genre" */
  insert_movie_genre_one?: Maybe<MovieGenre>;
  /** insert a single row into the table: "movie" */
  insert_movie_one?: Maybe<Movie>;
  /** insert data into the table: "movie_staff" */
  insert_movie_staff?: Maybe<MovieStaffMutationResponse>;
  /** insert a single row into the table: "movie_staff" */
  insert_movie_staff_one?: Maybe<MovieStaff>;
  /** insert data into the table: "movie_studio" */
  insert_movie_studio?: Maybe<MovieStudioMutationResponse>;
  /** insert a single row into the table: "movie_studio" */
  insert_movie_studio_one?: Maybe<MovieStudio>;
  /** insert data into the table: "person" */
  insert_person?: Maybe<PersonMutationResponse>;
  /** insert a single row into the table: "person" */
  insert_person_one?: Maybe<Person>;
  /** insert data into the table: "show" */
  insert_show?: Maybe<ShowMutationResponse>;
  /** insert data into the table: "show_collection" */
  insert_show_collection?: Maybe<ShowCollectionMutationResponse>;
  /** insert a single row into the table: "show_collection" */
  insert_show_collection_one?: Maybe<ShowCollection>;
  /** insert data into the table: "show_genre" */
  insert_show_genre?: Maybe<ShowGenreMutationResponse>;
  /** insert a single row into the table: "show_genre" */
  insert_show_genre_one?: Maybe<ShowGenre>;
  /** insert a single row into the table: "show" */
  insert_show_one?: Maybe<Show>;
  /** insert data into the table: "show_season" */
  insert_show_season?: Maybe<ShowSeasonMutationResponse>;
  /** insert a single row into the table: "show_season" */
  insert_show_season_one?: Maybe<ShowSeason>;
  /** insert data into the table: "show_staff" */
  insert_show_staff?: Maybe<ShowStaffMutationResponse>;
  /** insert a single row into the table: "show_staff" */
  insert_show_staff_one?: Maybe<ShowStaff>;
  /** insert data into the table: "staff" */
  insert_staff?: Maybe<StaffMutationResponse>;
  /** insert a single row into the table: "staff" */
  insert_staff_one?: Maybe<Staff>;
  /** insert data into the table: "studio" */
  insert_studio?: Maybe<StudioMutationResponse>;
  /** insert a single row into the table: "studio" */
  insert_studio_one?: Maybe<Studio>;
  /** insert data into the table: "style" */
  insert_style?: Maybe<StyleMutationResponse>;
  /** insert a single row into the table: "style" */
  insert_style_one?: Maybe<Style>;
  /** insert data into the table: "track" */
  insert_track?: Maybe<TrackMutationResponse>;
  /** insert data into the table: "track_genre" */
  insert_track_genre?: Maybe<TrackGenreMutationResponse>;
  /** insert a single row into the table: "track_genre" */
  insert_track_genre_one?: Maybe<TrackGenre>;
  /** insert data into the table: "track_mood" */
  insert_track_mood?: Maybe<TrackMoodMutationResponse>;
  /** insert a single row into the table: "track_mood" */
  insert_track_mood_one?: Maybe<TrackMood>;
  /** insert a single row into the table: "track" */
  insert_track_one?: Maybe<Track>;
  /** update data of the table: "album" */
  update_album?: Maybe<AlbumMutationResponse>;
  /** update single row of the table: "album" */
  update_album_by_pk?: Maybe<Album>;
  /** update data of the table: "album_collection" */
  update_album_collection?: Maybe<AlbumCollectionMutationResponse>;
  /** update single row of the table: "album_collection" */
  update_album_collection_by_pk?: Maybe<AlbumCollection>;
  /** update data of the table: "album_country" */
  update_album_country?: Maybe<AlbumCountryMutationResponse>;
  /** update single row of the table: "album_country" */
  update_album_country_by_pk?: Maybe<AlbumCountry>;
  /** update data of the table: "album_genre" */
  update_album_genre?: Maybe<AlbumGenreMutationResponse>;
  /** update single row of the table: "album_genre" */
  update_album_genre_by_pk?: Maybe<AlbumGenre>;
  /** update data of the table: "album_mood" */
  update_album_mood?: Maybe<AlbumMoodMutationResponse>;
  /** update single row of the table: "album_mood" */
  update_album_mood_by_pk?: Maybe<AlbumMood>;
  /** update data of the table: "album_studio" */
  update_album_studio?: Maybe<AlbumStudioMutationResponse>;
  /** update single row of the table: "album_studio" */
  update_album_studio_by_pk?: Maybe<AlbumStudio>;
  /** update data of the table: "album_style" */
  update_album_style?: Maybe<AlbumStyleMutationResponse>;
  /** update single row of the table: "album_style" */
  update_album_style_by_pk?: Maybe<AlbumStyle>;
  /** update data of the table: "album_track" */
  update_album_track?: Maybe<AlbumTrackMutationResponse>;
  /** update single row of the table: "album_track" */
  update_album_track_by_pk?: Maybe<AlbumTrack>;
  /** update data of the table: "artist" */
  update_artist?: Maybe<ArtistMutationResponse>;
  /** update single row of the table: "artist" */
  update_artist_by_pk?: Maybe<Artist>;
  /** update data of the table: "artist_collection" */
  update_artist_collection?: Maybe<ArtistCollectionMutationResponse>;
  /** update single row of the table: "artist_collection" */
  update_artist_collection_by_pk?: Maybe<ArtistCollection>;
  /** update data of the table: "artist_country" */
  update_artist_country?: Maybe<ArtistCountryMutationResponse>;
  /** update single row of the table: "artist_country" */
  update_artist_country_by_pk?: Maybe<ArtistCountry>;
  /** update data of the table: "artist_genre" */
  update_artist_genre?: Maybe<ArtistGenreMutationResponse>;
  /** update single row of the table: "artist_genre" */
  update_artist_genre_by_pk?: Maybe<ArtistGenre>;
  /** update data of the table: "artist_mood" */
  update_artist_mood?: Maybe<ArtistMoodMutationResponse>;
  /** update single row of the table: "artist_mood" */
  update_artist_mood_by_pk?: Maybe<ArtistMood>;
  /** update data of the table: "artist_similar" */
  update_artist_similar?: Maybe<ArtistSimilarMutationResponse>;
  /** update single row of the table: "artist_similar" */
  update_artist_similar_by_pk?: Maybe<ArtistSimilar>;
  /** update data of the table: "artist_style" */
  update_artist_style?: Maybe<ArtistStyleMutationResponse>;
  /** update single row of the table: "artist_style" */
  update_artist_style_by_pk?: Maybe<ArtistStyle>;
  /** update data of the table: "collection" */
  update_collection?: Maybe<CollectionMutationResponse>;
  /** update single row of the table: "collection" */
  update_collection_by_pk?: Maybe<Collection>;
  /** update data of the table: "country" */
  update_country?: Maybe<CountryMutationResponse>;
  /** update single row of the table: "country" */
  update_country_by_pk?: Maybe<Country>;
  /** update data of the table: "episode" */
  update_episode?: Maybe<EpisodeMutationResponse>;
  /** update single row of the table: "episode" */
  update_episode_by_pk?: Maybe<Episode>;
  /** update data of the table: "episode_staff" */
  update_episode_staff?: Maybe<EpisodeStaffMutationResponse>;
  /** update single row of the table: "episode_staff" */
  update_episode_staff_by_pk?: Maybe<EpisodeStaff>;
  /** update data of the table: "genre" */
  update_genre?: Maybe<GenreMutationResponse>;
  /** update single row of the table: "genre" */
  update_genre_by_pk?: Maybe<Genre>;
  /** update data of the table: "mood" */
  update_mood?: Maybe<MoodMutationResponse>;
  /** update single row of the table: "mood" */
  update_mood_by_pk?: Maybe<Mood>;
  /** update data of the table: "movie" */
  update_movie?: Maybe<MovieMutationResponse>;
  /** update single row of the table: "movie" */
  update_movie_by_pk?: Maybe<Movie>;
  /** update data of the table: "movie_collection" */
  update_movie_collection?: Maybe<MovieCollectionMutationResponse>;
  /** update single row of the table: "movie_collection" */
  update_movie_collection_by_pk?: Maybe<MovieCollection>;
  /** update data of the table: "movie_country" */
  update_movie_country?: Maybe<MovieCountryMutationResponse>;
  /** update single row of the table: "movie_country" */
  update_movie_country_by_pk?: Maybe<MovieCountry>;
  /** update data of the table: "movie_genre" */
  update_movie_genre?: Maybe<MovieGenreMutationResponse>;
  /** update single row of the table: "movie_genre" */
  update_movie_genre_by_pk?: Maybe<MovieGenre>;
  /** update data of the table: "movie_staff" */
  update_movie_staff?: Maybe<MovieStaffMutationResponse>;
  /** update single row of the table: "movie_staff" */
  update_movie_staff_by_pk?: Maybe<MovieStaff>;
  /** update data of the table: "movie_studio" */
  update_movie_studio?: Maybe<MovieStudioMutationResponse>;
  /** update single row of the table: "movie_studio" */
  update_movie_studio_by_pk?: Maybe<MovieStudio>;
  /** update data of the table: "person" */
  update_person?: Maybe<PersonMutationResponse>;
  /** update single row of the table: "person" */
  update_person_by_pk?: Maybe<Person>;
  /** update data of the table: "show" */
  update_show?: Maybe<ShowMutationResponse>;
  /** update single row of the table: "show" */
  update_show_by_pk?: Maybe<Show>;
  /** update data of the table: "show_collection" */
  update_show_collection?: Maybe<ShowCollectionMutationResponse>;
  /** update single row of the table: "show_collection" */
  update_show_collection_by_pk?: Maybe<ShowCollection>;
  /** update data of the table: "show_genre" */
  update_show_genre?: Maybe<ShowGenreMutationResponse>;
  /** update single row of the table: "show_genre" */
  update_show_genre_by_pk?: Maybe<ShowGenre>;
  /** update data of the table: "show_season" */
  update_show_season?: Maybe<ShowSeasonMutationResponse>;
  /** update single row of the table: "show_season" */
  update_show_season_by_pk?: Maybe<ShowSeason>;
  /** update data of the table: "show_staff" */
  update_show_staff?: Maybe<ShowStaffMutationResponse>;
  /** update single row of the table: "show_staff" */
  update_show_staff_by_pk?: Maybe<ShowStaff>;
  /** update data of the table: "staff" */
  update_staff?: Maybe<StaffMutationResponse>;
  /** update single row of the table: "staff" */
  update_staff_by_pk?: Maybe<Staff>;
  /** update data of the table: "studio" */
  update_studio?: Maybe<StudioMutationResponse>;
  /** update single row of the table: "studio" */
  update_studio_by_pk?: Maybe<Studio>;
  /** update data of the table: "style" */
  update_style?: Maybe<StyleMutationResponse>;
  /** update single row of the table: "style" */
  update_style_by_pk?: Maybe<Style>;
  /** update data of the table: "track" */
  update_track?: Maybe<TrackMutationResponse>;
  /** update single row of the table: "track" */
  update_track_by_pk?: Maybe<Track>;
  /** update data of the table: "track_genre" */
  update_track_genre?: Maybe<TrackGenreMutationResponse>;
  /** update single row of the table: "track_genre" */
  update_track_genre_by_pk?: Maybe<TrackGenre>;
  /** update data of the table: "track_mood" */
  update_track_mood?: Maybe<TrackMoodMutationResponse>;
  /** update single row of the table: "track_mood" */
  update_track_mood_by_pk?: Maybe<TrackMood>;
};


/** mutation root */
export type MutationRootDeleteAlbumArgs = {
  where: AlbumBoolExp;
};


/** mutation root */
export type MutationRootDeleteAlbumByPkArgs = {
  id: Scalars['uuid'];
};


/** mutation root */
export type MutationRootDeleteAlbumCollectionArgs = {
  where: AlbumCollectionBoolExp;
};


/** mutation root */
export type MutationRootDeleteAlbumCollectionByPkArgs = {
  album_id: Scalars['uuid'];
  index: Scalars['Int'];
};


/** mutation root */
export type MutationRootDeleteAlbumCountryArgs = {
  where: AlbumCountryBoolExp;
};


/** mutation root */
export type MutationRootDeleteAlbumCountryByPkArgs = {
  album_id: Scalars['uuid'];
  index: Scalars['Int'];
};


/** mutation root */
export type MutationRootDeleteAlbumGenreArgs = {
  where: AlbumGenreBoolExp;
};


/** mutation root */
export type MutationRootDeleteAlbumGenreByPkArgs = {
  album_id: Scalars['uuid'];
  index: Scalars['Int'];
};


/** mutation root */
export type MutationRootDeleteAlbumMoodArgs = {
  where: AlbumMoodBoolExp;
};


/** mutation root */
export type MutationRootDeleteAlbumMoodByPkArgs = {
  album_id: Scalars['uuid'];
  index: Scalars['Int'];
};


/** mutation root */
export type MutationRootDeleteAlbumStudioArgs = {
  where: AlbumStudioBoolExp;
};


/** mutation root */
export type MutationRootDeleteAlbumStudioByPkArgs = {
  album_id: Scalars['uuid'];
  index: Scalars['Int'];
};


/** mutation root */
export type MutationRootDeleteAlbumStyleArgs = {
  where: AlbumStyleBoolExp;
};


/** mutation root */
export type MutationRootDeleteAlbumStyleByPkArgs = {
  album_id: Scalars['uuid'];
  index: Scalars['Int'];
};


/** mutation root */
export type MutationRootDeleteAlbumTrackArgs = {
  where: AlbumTrackBoolExp;
};


/** mutation root */
export type MutationRootDeleteAlbumTrackByPkArgs = {
  disk: Scalars['Int'];
  index: Scalars['Int'];
  track_id: Scalars['uuid'];
};


/** mutation root */
export type MutationRootDeleteArtistArgs = {
  where: ArtistBoolExp;
};


/** mutation root */
export type MutationRootDeleteArtistByPkArgs = {
  id: Scalars['uuid'];
};


/** mutation root */
export type MutationRootDeleteArtistCollectionArgs = {
  where: ArtistCollectionBoolExp;
};


/** mutation root */
export type MutationRootDeleteArtistCollectionByPkArgs = {
  artist_id: Scalars['uuid'];
  index: Scalars['Int'];
};


/** mutation root */
export type MutationRootDeleteArtistCountryArgs = {
  where: ArtistCountryBoolExp;
};


/** mutation root */
export type MutationRootDeleteArtistCountryByPkArgs = {
  artist_id: Scalars['uuid'];
  index: Scalars['Int'];
};


/** mutation root */
export type MutationRootDeleteArtistGenreArgs = {
  where: ArtistGenreBoolExp;
};


/** mutation root */
export type MutationRootDeleteArtistGenreByPkArgs = {
  artist_id: Scalars['uuid'];
  index: Scalars['Int'];
};


/** mutation root */
export type MutationRootDeleteArtistMoodArgs = {
  where: ArtistMoodBoolExp;
};


/** mutation root */
export type MutationRootDeleteArtistMoodByPkArgs = {
  artist_id: Scalars['uuid'];
  index: Scalars['Int'];
};


/** mutation root */
export type MutationRootDeleteArtistSimilarArgs = {
  where: ArtistSimilarBoolExp;
};


/** mutation root */
export type MutationRootDeleteArtistSimilarByPkArgs = {
  artist1_id: Scalars['uuid'];
  artist2_id: Scalars['uuid'];
};


/** mutation root */
export type MutationRootDeleteArtistStyleArgs = {
  where: ArtistStyleBoolExp;
};


/** mutation root */
export type MutationRootDeleteArtistStyleByPkArgs = {
  artist_id: Scalars['uuid'];
  index: Scalars['Int'];
};


/** mutation root */
export type MutationRootDeleteCollectionArgs = {
  where: CollectionBoolExp;
};


/** mutation root */
export type MutationRootDeleteCollectionByPkArgs = {
  id: Scalars['uuid'];
};


/** mutation root */
export type MutationRootDeleteCountryArgs = {
  where: CountryBoolExp;
};


/** mutation root */
export type MutationRootDeleteCountryByPkArgs = {
  code: Scalars['String'];
};


/** mutation root */
export type MutationRootDeleteEpisodeArgs = {
  where: EpisodeBoolExp;
};


/** mutation root */
export type MutationRootDeleteEpisodeByPkArgs = {
  id: Scalars['uuid'];
};


/** mutation root */
export type MutationRootDeleteEpisodeStaffArgs = {
  where: EpisodeStaffBoolExp;
};


/** mutation root */
export type MutationRootDeleteEpisodeStaffByPkArgs = {
  id: Scalars['uuid'];
};


/** mutation root */
export type MutationRootDeleteGenreArgs = {
  where: GenreBoolExp;
};


/** mutation root */
export type MutationRootDeleteGenreByPkArgs = {
  id: Scalars['uuid'];
};


/** mutation root */
export type MutationRootDeleteMoodArgs = {
  where: MoodBoolExp;
};


/** mutation root */
export type MutationRootDeleteMoodByPkArgs = {
  id: Scalars['uuid'];
};


/** mutation root */
export type MutationRootDeleteMovieArgs = {
  where: MovieBoolExp;
};


/** mutation root */
export type MutationRootDeleteMovieByPkArgs = {
  id: Scalars['uuid'];
};


/** mutation root */
export type MutationRootDeleteMovieCollectionArgs = {
  where: MovieCollectionBoolExp;
};


/** mutation root */
export type MutationRootDeleteMovieCollectionByPkArgs = {
  index: Scalars['Int'];
  movie_id: Scalars['uuid'];
};


/** mutation root */
export type MutationRootDeleteMovieCountryArgs = {
  where: MovieCountryBoolExp;
};


/** mutation root */
export type MutationRootDeleteMovieCountryByPkArgs = {
  index: Scalars['Int'];
  movie_id: Scalars['uuid'];
};


/** mutation root */
export type MutationRootDeleteMovieGenreArgs = {
  where: MovieGenreBoolExp;
};


/** mutation root */
export type MutationRootDeleteMovieGenreByPkArgs = {
  index: Scalars['Int'];
  movie_id: Scalars['uuid'];
};


/** mutation root */
export type MutationRootDeleteMovieStaffArgs = {
  where: MovieStaffBoolExp;
};


/** mutation root */
export type MutationRootDeleteMovieStaffByPkArgs = {
  id: Scalars['uuid'];
};


/** mutation root */
export type MutationRootDeleteMovieStudioArgs = {
  where: MovieStudioBoolExp;
};


/** mutation root */
export type MutationRootDeleteMovieStudioByPkArgs = {
  index: Scalars['Int'];
  movie_id: Scalars['uuid'];
};


/** mutation root */
export type MutationRootDeletePersonArgs = {
  where: PersonBoolExp;
};


/** mutation root */
export type MutationRootDeletePersonByPkArgs = {
  id: Scalars['uuid'];
};


/** mutation root */
export type MutationRootDeleteShowArgs = {
  where: ShowBoolExp;
};


/** mutation root */
export type MutationRootDeleteShowByPkArgs = {
  id: Scalars['uuid'];
};


/** mutation root */
export type MutationRootDeleteShowCollectionArgs = {
  where: ShowCollectionBoolExp;
};


/** mutation root */
export type MutationRootDeleteShowCollectionByPkArgs = {
  index: Scalars['Int'];
  show_id: Scalars['uuid'];
};


/** mutation root */
export type MutationRootDeleteShowGenreArgs = {
  where: ShowGenreBoolExp;
};


/** mutation root */
export type MutationRootDeleteShowGenreByPkArgs = {
  index: Scalars['Int'];
  show_id: Scalars['uuid'];
};


/** mutation root */
export type MutationRootDeleteShowSeasonArgs = {
  where: ShowSeasonBoolExp;
};


/** mutation root */
export type MutationRootDeleteShowSeasonByPkArgs = {
  id: Scalars['uuid'];
};


/** mutation root */
export type MutationRootDeleteShowStaffArgs = {
  where: ShowStaffBoolExp;
};


/** mutation root */
export type MutationRootDeleteShowStaffByPkArgs = {
  id: Scalars['uuid'];
};


/** mutation root */
export type MutationRootDeleteStaffArgs = {
  where: StaffBoolExp;
};


/** mutation root */
export type MutationRootDeleteStaffByPkArgs = {
  id: Scalars['uuid'];
};


/** mutation root */
export type MutationRootDeleteStudioArgs = {
  where: StudioBoolExp;
};


/** mutation root */
export type MutationRootDeleteStudioByPkArgs = {
  id: Scalars['uuid'];
};


/** mutation root */
export type MutationRootDeleteStyleArgs = {
  where: StyleBoolExp;
};


/** mutation root */
export type MutationRootDeleteStyleByPkArgs = {
  id: Scalars['uuid'];
};


/** mutation root */
export type MutationRootDeleteTrackArgs = {
  where: TrackBoolExp;
};


/** mutation root */
export type MutationRootDeleteTrackByPkArgs = {
  id: Scalars['uuid'];
};


/** mutation root */
export type MutationRootDeleteTrackGenreArgs = {
  where: TrackGenreBoolExp;
};


/** mutation root */
export type MutationRootDeleteTrackGenreByPkArgs = {
  index: Scalars['Int'];
  track_id: Scalars['uuid'];
};


/** mutation root */
export type MutationRootDeleteTrackMoodArgs = {
  where: TrackMoodBoolExp;
};


/** mutation root */
export type MutationRootDeleteTrackMoodByPkArgs = {
  index: Scalars['Int'];
  track_id: Scalars['uuid'];
};


/** mutation root */
export type MutationRootInsertAlbumArgs = {
  objects: Array<AlbumInsertInput>;
  on_conflict?: Maybe<AlbumOnConflict>;
};


/** mutation root */
export type MutationRootInsertAlbumCollectionArgs = {
  objects: Array<AlbumCollectionInsertInput>;
  on_conflict?: Maybe<AlbumCollectionOnConflict>;
};


/** mutation root */
export type MutationRootInsertAlbumCollectionOneArgs = {
  object: AlbumCollectionInsertInput;
  on_conflict?: Maybe<AlbumCollectionOnConflict>;
};


/** mutation root */
export type MutationRootInsertAlbumCountryArgs = {
  objects: Array<AlbumCountryInsertInput>;
  on_conflict?: Maybe<AlbumCountryOnConflict>;
};


/** mutation root */
export type MutationRootInsertAlbumCountryOneArgs = {
  object: AlbumCountryInsertInput;
  on_conflict?: Maybe<AlbumCountryOnConflict>;
};


/** mutation root */
export type MutationRootInsertAlbumGenreArgs = {
  objects: Array<AlbumGenreInsertInput>;
  on_conflict?: Maybe<AlbumGenreOnConflict>;
};


/** mutation root */
export type MutationRootInsertAlbumGenreOneArgs = {
  object: AlbumGenreInsertInput;
  on_conflict?: Maybe<AlbumGenreOnConflict>;
};


/** mutation root */
export type MutationRootInsertAlbumMoodArgs = {
  objects: Array<AlbumMoodInsertInput>;
  on_conflict?: Maybe<AlbumMoodOnConflict>;
};


/** mutation root */
export type MutationRootInsertAlbumMoodOneArgs = {
  object: AlbumMoodInsertInput;
  on_conflict?: Maybe<AlbumMoodOnConflict>;
};


/** mutation root */
export type MutationRootInsertAlbumOneArgs = {
  object: AlbumInsertInput;
  on_conflict?: Maybe<AlbumOnConflict>;
};


/** mutation root */
export type MutationRootInsertAlbumStudioArgs = {
  objects: Array<AlbumStudioInsertInput>;
  on_conflict?: Maybe<AlbumStudioOnConflict>;
};


/** mutation root */
export type MutationRootInsertAlbumStudioOneArgs = {
  object: AlbumStudioInsertInput;
  on_conflict?: Maybe<AlbumStudioOnConflict>;
};


/** mutation root */
export type MutationRootInsertAlbumStyleArgs = {
  objects: Array<AlbumStyleInsertInput>;
  on_conflict?: Maybe<AlbumStyleOnConflict>;
};


/** mutation root */
export type MutationRootInsertAlbumStyleOneArgs = {
  object: AlbumStyleInsertInput;
  on_conflict?: Maybe<AlbumStyleOnConflict>;
};


/** mutation root */
export type MutationRootInsertAlbumTrackArgs = {
  objects: Array<AlbumTrackInsertInput>;
  on_conflict?: Maybe<AlbumTrackOnConflict>;
};


/** mutation root */
export type MutationRootInsertAlbumTrackOneArgs = {
  object: AlbumTrackInsertInput;
  on_conflict?: Maybe<AlbumTrackOnConflict>;
};


/** mutation root */
export type MutationRootInsertArtistArgs = {
  objects: Array<ArtistInsertInput>;
  on_conflict?: Maybe<ArtistOnConflict>;
};


/** mutation root */
export type MutationRootInsertArtistCollectionArgs = {
  objects: Array<ArtistCollectionInsertInput>;
  on_conflict?: Maybe<ArtistCollectionOnConflict>;
};


/** mutation root */
export type MutationRootInsertArtistCollectionOneArgs = {
  object: ArtistCollectionInsertInput;
  on_conflict?: Maybe<ArtistCollectionOnConflict>;
};


/** mutation root */
export type MutationRootInsertArtistCountryArgs = {
  objects: Array<ArtistCountryInsertInput>;
  on_conflict?: Maybe<ArtistCountryOnConflict>;
};


/** mutation root */
export type MutationRootInsertArtistCountryOneArgs = {
  object: ArtistCountryInsertInput;
  on_conflict?: Maybe<ArtistCountryOnConflict>;
};


/** mutation root */
export type MutationRootInsertArtistGenreArgs = {
  objects: Array<ArtistGenreInsertInput>;
  on_conflict?: Maybe<ArtistGenreOnConflict>;
};


/** mutation root */
export type MutationRootInsertArtistGenreOneArgs = {
  object: ArtistGenreInsertInput;
  on_conflict?: Maybe<ArtistGenreOnConflict>;
};


/** mutation root */
export type MutationRootInsertArtistMoodArgs = {
  objects: Array<ArtistMoodInsertInput>;
  on_conflict?: Maybe<ArtistMoodOnConflict>;
};


/** mutation root */
export type MutationRootInsertArtistMoodOneArgs = {
  object: ArtistMoodInsertInput;
  on_conflict?: Maybe<ArtistMoodOnConflict>;
};


/** mutation root */
export type MutationRootInsertArtistOneArgs = {
  object: ArtistInsertInput;
  on_conflict?: Maybe<ArtistOnConflict>;
};


/** mutation root */
export type MutationRootInsertArtistSimilarArgs = {
  objects: Array<ArtistSimilarInsertInput>;
  on_conflict?: Maybe<ArtistSimilarOnConflict>;
};


/** mutation root */
export type MutationRootInsertArtistSimilarOneArgs = {
  object: ArtistSimilarInsertInput;
  on_conflict?: Maybe<ArtistSimilarOnConflict>;
};


/** mutation root */
export type MutationRootInsertArtistStyleArgs = {
  objects: Array<ArtistStyleInsertInput>;
  on_conflict?: Maybe<ArtistStyleOnConflict>;
};


/** mutation root */
export type MutationRootInsertArtistStyleOneArgs = {
  object: ArtistStyleInsertInput;
  on_conflict?: Maybe<ArtistStyleOnConflict>;
};


/** mutation root */
export type MutationRootInsertCollectionArgs = {
  objects: Array<CollectionInsertInput>;
  on_conflict?: Maybe<CollectionOnConflict>;
};


/** mutation root */
export type MutationRootInsertCollectionOneArgs = {
  object: CollectionInsertInput;
  on_conflict?: Maybe<CollectionOnConflict>;
};


/** mutation root */
export type MutationRootInsertCountryArgs = {
  objects: Array<CountryInsertInput>;
  on_conflict?: Maybe<CountryOnConflict>;
};


/** mutation root */
export type MutationRootInsertCountryOneArgs = {
  object: CountryInsertInput;
  on_conflict?: Maybe<CountryOnConflict>;
};


/** mutation root */
export type MutationRootInsertEpisodeArgs = {
  objects: Array<EpisodeInsertInput>;
  on_conflict?: Maybe<EpisodeOnConflict>;
};


/** mutation root */
export type MutationRootInsertEpisodeOneArgs = {
  object: EpisodeInsertInput;
  on_conflict?: Maybe<EpisodeOnConflict>;
};


/** mutation root */
export type MutationRootInsertEpisodeStaffArgs = {
  objects: Array<EpisodeStaffInsertInput>;
  on_conflict?: Maybe<EpisodeStaffOnConflict>;
};


/** mutation root */
export type MutationRootInsertEpisodeStaffOneArgs = {
  object: EpisodeStaffInsertInput;
  on_conflict?: Maybe<EpisodeStaffOnConflict>;
};


/** mutation root */
export type MutationRootInsertGenreArgs = {
  objects: Array<GenreInsertInput>;
  on_conflict?: Maybe<GenreOnConflict>;
};


/** mutation root */
export type MutationRootInsertGenreOneArgs = {
  object: GenreInsertInput;
  on_conflict?: Maybe<GenreOnConflict>;
};


/** mutation root */
export type MutationRootInsertMoodArgs = {
  objects: Array<MoodInsertInput>;
  on_conflict?: Maybe<MoodOnConflict>;
};


/** mutation root */
export type MutationRootInsertMoodOneArgs = {
  object: MoodInsertInput;
  on_conflict?: Maybe<MoodOnConflict>;
};


/** mutation root */
export type MutationRootInsertMovieArgs = {
  objects: Array<MovieInsertInput>;
  on_conflict?: Maybe<MovieOnConflict>;
};


/** mutation root */
export type MutationRootInsertMovieCollectionArgs = {
  objects: Array<MovieCollectionInsertInput>;
  on_conflict?: Maybe<MovieCollectionOnConflict>;
};


/** mutation root */
export type MutationRootInsertMovieCollectionOneArgs = {
  object: MovieCollectionInsertInput;
  on_conflict?: Maybe<MovieCollectionOnConflict>;
};


/** mutation root */
export type MutationRootInsertMovieCountryArgs = {
  objects: Array<MovieCountryInsertInput>;
  on_conflict?: Maybe<MovieCountryOnConflict>;
};


/** mutation root */
export type MutationRootInsertMovieCountryOneArgs = {
  object: MovieCountryInsertInput;
  on_conflict?: Maybe<MovieCountryOnConflict>;
};


/** mutation root */
export type MutationRootInsertMovieGenreArgs = {
  objects: Array<MovieGenreInsertInput>;
  on_conflict?: Maybe<MovieGenreOnConflict>;
};


/** mutation root */
export type MutationRootInsertMovieGenreOneArgs = {
  object: MovieGenreInsertInput;
  on_conflict?: Maybe<MovieGenreOnConflict>;
};


/** mutation root */
export type MutationRootInsertMovieOneArgs = {
  object: MovieInsertInput;
  on_conflict?: Maybe<MovieOnConflict>;
};


/** mutation root */
export type MutationRootInsertMovieStaffArgs = {
  objects: Array<MovieStaffInsertInput>;
  on_conflict?: Maybe<MovieStaffOnConflict>;
};


/** mutation root */
export type MutationRootInsertMovieStaffOneArgs = {
  object: MovieStaffInsertInput;
  on_conflict?: Maybe<MovieStaffOnConflict>;
};


/** mutation root */
export type MutationRootInsertMovieStudioArgs = {
  objects: Array<MovieStudioInsertInput>;
  on_conflict?: Maybe<MovieStudioOnConflict>;
};


/** mutation root */
export type MutationRootInsertMovieStudioOneArgs = {
  object: MovieStudioInsertInput;
  on_conflict?: Maybe<MovieStudioOnConflict>;
};


/** mutation root */
export type MutationRootInsertPersonArgs = {
  objects: Array<PersonInsertInput>;
  on_conflict?: Maybe<PersonOnConflict>;
};


/** mutation root */
export type MutationRootInsertPersonOneArgs = {
  object: PersonInsertInput;
  on_conflict?: Maybe<PersonOnConflict>;
};


/** mutation root */
export type MutationRootInsertShowArgs = {
  objects: Array<ShowInsertInput>;
  on_conflict?: Maybe<ShowOnConflict>;
};


/** mutation root */
export type MutationRootInsertShowCollectionArgs = {
  objects: Array<ShowCollectionInsertInput>;
  on_conflict?: Maybe<ShowCollectionOnConflict>;
};


/** mutation root */
export type MutationRootInsertShowCollectionOneArgs = {
  object: ShowCollectionInsertInput;
  on_conflict?: Maybe<ShowCollectionOnConflict>;
};


/** mutation root */
export type MutationRootInsertShowGenreArgs = {
  objects: Array<ShowGenreInsertInput>;
  on_conflict?: Maybe<ShowGenreOnConflict>;
};


/** mutation root */
export type MutationRootInsertShowGenreOneArgs = {
  object: ShowGenreInsertInput;
  on_conflict?: Maybe<ShowGenreOnConflict>;
};


/** mutation root */
export type MutationRootInsertShowOneArgs = {
  object: ShowInsertInput;
  on_conflict?: Maybe<ShowOnConflict>;
};


/** mutation root */
export type MutationRootInsertShowSeasonArgs = {
  objects: Array<ShowSeasonInsertInput>;
  on_conflict?: Maybe<ShowSeasonOnConflict>;
};


/** mutation root */
export type MutationRootInsertShowSeasonOneArgs = {
  object: ShowSeasonInsertInput;
  on_conflict?: Maybe<ShowSeasonOnConflict>;
};


/** mutation root */
export type MutationRootInsertShowStaffArgs = {
  objects: Array<ShowStaffInsertInput>;
  on_conflict?: Maybe<ShowStaffOnConflict>;
};


/** mutation root */
export type MutationRootInsertShowStaffOneArgs = {
  object: ShowStaffInsertInput;
  on_conflict?: Maybe<ShowStaffOnConflict>;
};


/** mutation root */
export type MutationRootInsertStaffArgs = {
  objects: Array<StaffInsertInput>;
  on_conflict?: Maybe<StaffOnConflict>;
};


/** mutation root */
export type MutationRootInsertStaffOneArgs = {
  object: StaffInsertInput;
  on_conflict?: Maybe<StaffOnConflict>;
};


/** mutation root */
export type MutationRootInsertStudioArgs = {
  objects: Array<StudioInsertInput>;
  on_conflict?: Maybe<StudioOnConflict>;
};


/** mutation root */
export type MutationRootInsertStudioOneArgs = {
  object: StudioInsertInput;
  on_conflict?: Maybe<StudioOnConflict>;
};


/** mutation root */
export type MutationRootInsertStyleArgs = {
  objects: Array<StyleInsertInput>;
  on_conflict?: Maybe<StyleOnConflict>;
};


/** mutation root */
export type MutationRootInsertStyleOneArgs = {
  object: StyleInsertInput;
  on_conflict?: Maybe<StyleOnConflict>;
};


/** mutation root */
export type MutationRootInsertTrackArgs = {
  objects: Array<TrackInsertInput>;
  on_conflict?: Maybe<TrackOnConflict>;
};


/** mutation root */
export type MutationRootInsertTrackGenreArgs = {
  objects: Array<TrackGenreInsertInput>;
  on_conflict?: Maybe<TrackGenreOnConflict>;
};


/** mutation root */
export type MutationRootInsertTrackGenreOneArgs = {
  object: TrackGenreInsertInput;
  on_conflict?: Maybe<TrackGenreOnConflict>;
};


/** mutation root */
export type MutationRootInsertTrackMoodArgs = {
  objects: Array<TrackMoodInsertInput>;
  on_conflict?: Maybe<TrackMoodOnConflict>;
};


/** mutation root */
export type MutationRootInsertTrackMoodOneArgs = {
  object: TrackMoodInsertInput;
  on_conflict?: Maybe<TrackMoodOnConflict>;
};


/** mutation root */
export type MutationRootInsertTrackOneArgs = {
  object: TrackInsertInput;
  on_conflict?: Maybe<TrackOnConflict>;
};


/** mutation root */
export type MutationRootUpdateAlbumArgs = {
  _inc?: Maybe<AlbumIncInput>;
  _set?: Maybe<AlbumSetInput>;
  where: AlbumBoolExp;
};


/** mutation root */
export type MutationRootUpdateAlbumByPkArgs = {
  _inc?: Maybe<AlbumIncInput>;
  _set?: Maybe<AlbumSetInput>;
  pk_columns: AlbumPkColumnsInput;
};


/** mutation root */
export type MutationRootUpdateAlbumCollectionArgs = {
  _inc?: Maybe<AlbumCollectionIncInput>;
  _set?: Maybe<AlbumCollectionSetInput>;
  where: AlbumCollectionBoolExp;
};


/** mutation root */
export type MutationRootUpdateAlbumCollectionByPkArgs = {
  _inc?: Maybe<AlbumCollectionIncInput>;
  _set?: Maybe<AlbumCollectionSetInput>;
  pk_columns: AlbumCollectionPkColumnsInput;
};


/** mutation root */
export type MutationRootUpdateAlbumCountryArgs = {
  _inc?: Maybe<AlbumCountryIncInput>;
  _set?: Maybe<AlbumCountrySetInput>;
  where: AlbumCountryBoolExp;
};


/** mutation root */
export type MutationRootUpdateAlbumCountryByPkArgs = {
  _inc?: Maybe<AlbumCountryIncInput>;
  _set?: Maybe<AlbumCountrySetInput>;
  pk_columns: AlbumCountryPkColumnsInput;
};


/** mutation root */
export type MutationRootUpdateAlbumGenreArgs = {
  _inc?: Maybe<AlbumGenreIncInput>;
  _set?: Maybe<AlbumGenreSetInput>;
  where: AlbumGenreBoolExp;
};


/** mutation root */
export type MutationRootUpdateAlbumGenreByPkArgs = {
  _inc?: Maybe<AlbumGenreIncInput>;
  _set?: Maybe<AlbumGenreSetInput>;
  pk_columns: AlbumGenrePkColumnsInput;
};


/** mutation root */
export type MutationRootUpdateAlbumMoodArgs = {
  _inc?: Maybe<AlbumMoodIncInput>;
  _set?: Maybe<AlbumMoodSetInput>;
  where: AlbumMoodBoolExp;
};


/** mutation root */
export type MutationRootUpdateAlbumMoodByPkArgs = {
  _inc?: Maybe<AlbumMoodIncInput>;
  _set?: Maybe<AlbumMoodSetInput>;
  pk_columns: AlbumMoodPkColumnsInput;
};


/** mutation root */
export type MutationRootUpdateAlbumStudioArgs = {
  _inc?: Maybe<AlbumStudioIncInput>;
  _set?: Maybe<AlbumStudioSetInput>;
  where: AlbumStudioBoolExp;
};


/** mutation root */
export type MutationRootUpdateAlbumStudioByPkArgs = {
  _inc?: Maybe<AlbumStudioIncInput>;
  _set?: Maybe<AlbumStudioSetInput>;
  pk_columns: AlbumStudioPkColumnsInput;
};


/** mutation root */
export type MutationRootUpdateAlbumStyleArgs = {
  _inc?: Maybe<AlbumStyleIncInput>;
  _set?: Maybe<AlbumStyleSetInput>;
  where: AlbumStyleBoolExp;
};


/** mutation root */
export type MutationRootUpdateAlbumStyleByPkArgs = {
  _inc?: Maybe<AlbumStyleIncInput>;
  _set?: Maybe<AlbumStyleSetInput>;
  pk_columns: AlbumStylePkColumnsInput;
};


/** mutation root */
export type MutationRootUpdateAlbumTrackArgs = {
  _inc?: Maybe<AlbumTrackIncInput>;
  _set?: Maybe<AlbumTrackSetInput>;
  where: AlbumTrackBoolExp;
};


/** mutation root */
export type MutationRootUpdateAlbumTrackByPkArgs = {
  _inc?: Maybe<AlbumTrackIncInput>;
  _set?: Maybe<AlbumTrackSetInput>;
  pk_columns: AlbumTrackPkColumnsInput;
};


/** mutation root */
export type MutationRootUpdateArtistArgs = {
  _set?: Maybe<ArtistSetInput>;
  where: ArtistBoolExp;
};


/** mutation root */
export type MutationRootUpdateArtistByPkArgs = {
  _set?: Maybe<ArtistSetInput>;
  pk_columns: ArtistPkColumnsInput;
};


/** mutation root */
export type MutationRootUpdateArtistCollectionArgs = {
  _inc?: Maybe<ArtistCollectionIncInput>;
  _set?: Maybe<ArtistCollectionSetInput>;
  where: ArtistCollectionBoolExp;
};


/** mutation root */
export type MutationRootUpdateArtistCollectionByPkArgs = {
  _inc?: Maybe<ArtistCollectionIncInput>;
  _set?: Maybe<ArtistCollectionSetInput>;
  pk_columns: ArtistCollectionPkColumnsInput;
};


/** mutation root */
export type MutationRootUpdateArtistCountryArgs = {
  _inc?: Maybe<ArtistCountryIncInput>;
  _set?: Maybe<ArtistCountrySetInput>;
  where: ArtistCountryBoolExp;
};


/** mutation root */
export type MutationRootUpdateArtistCountryByPkArgs = {
  _inc?: Maybe<ArtistCountryIncInput>;
  _set?: Maybe<ArtistCountrySetInput>;
  pk_columns: ArtistCountryPkColumnsInput;
};


/** mutation root */
export type MutationRootUpdateArtistGenreArgs = {
  _inc?: Maybe<ArtistGenreIncInput>;
  _set?: Maybe<ArtistGenreSetInput>;
  where: ArtistGenreBoolExp;
};


/** mutation root */
export type MutationRootUpdateArtistGenreByPkArgs = {
  _inc?: Maybe<ArtistGenreIncInput>;
  _set?: Maybe<ArtistGenreSetInput>;
  pk_columns: ArtistGenrePkColumnsInput;
};


/** mutation root */
export type MutationRootUpdateArtistMoodArgs = {
  _inc?: Maybe<ArtistMoodIncInput>;
  _set?: Maybe<ArtistMoodSetInput>;
  where: ArtistMoodBoolExp;
};


/** mutation root */
export type MutationRootUpdateArtistMoodByPkArgs = {
  _inc?: Maybe<ArtistMoodIncInput>;
  _set?: Maybe<ArtistMoodSetInput>;
  pk_columns: ArtistMoodPkColumnsInput;
};


/** mutation root */
export type MutationRootUpdateArtistSimilarArgs = {
  _set?: Maybe<ArtistSimilarSetInput>;
  where: ArtistSimilarBoolExp;
};


/** mutation root */
export type MutationRootUpdateArtistSimilarByPkArgs = {
  _set?: Maybe<ArtistSimilarSetInput>;
  pk_columns: ArtistSimilarPkColumnsInput;
};


/** mutation root */
export type MutationRootUpdateArtistStyleArgs = {
  _inc?: Maybe<ArtistStyleIncInput>;
  _set?: Maybe<ArtistStyleSetInput>;
  where: ArtistStyleBoolExp;
};


/** mutation root */
export type MutationRootUpdateArtistStyleByPkArgs = {
  _inc?: Maybe<ArtistStyleIncInput>;
  _set?: Maybe<ArtistStyleSetInput>;
  pk_columns: ArtistStylePkColumnsInput;
};


/** mutation root */
export type MutationRootUpdateCollectionArgs = {
  _set?: Maybe<CollectionSetInput>;
  where: CollectionBoolExp;
};


/** mutation root */
export type MutationRootUpdateCollectionByPkArgs = {
  _set?: Maybe<CollectionSetInput>;
  pk_columns: CollectionPkColumnsInput;
};


/** mutation root */
export type MutationRootUpdateCountryArgs = {
  _set?: Maybe<CountrySetInput>;
  where: CountryBoolExp;
};


/** mutation root */
export type MutationRootUpdateCountryByPkArgs = {
  _set?: Maybe<CountrySetInput>;
  pk_columns: CountryPkColumnsInput;
};


/** mutation root */
export type MutationRootUpdateEpisodeArgs = {
  _inc?: Maybe<EpisodeIncInput>;
  _set?: Maybe<EpisodeSetInput>;
  where: EpisodeBoolExp;
};


/** mutation root */
export type MutationRootUpdateEpisodeByPkArgs = {
  _inc?: Maybe<EpisodeIncInput>;
  _set?: Maybe<EpisodeSetInput>;
  pk_columns: EpisodePkColumnsInput;
};


/** mutation root */
export type MutationRootUpdateEpisodeStaffArgs = {
  _inc?: Maybe<EpisodeStaffIncInput>;
  _set?: Maybe<EpisodeStaffSetInput>;
  where: EpisodeStaffBoolExp;
};


/** mutation root */
export type MutationRootUpdateEpisodeStaffByPkArgs = {
  _inc?: Maybe<EpisodeStaffIncInput>;
  _set?: Maybe<EpisodeStaffSetInput>;
  pk_columns: EpisodeStaffPkColumnsInput;
};


/** mutation root */
export type MutationRootUpdateGenreArgs = {
  _set?: Maybe<GenreSetInput>;
  where: GenreBoolExp;
};


/** mutation root */
export type MutationRootUpdateGenreByPkArgs = {
  _set?: Maybe<GenreSetInput>;
  pk_columns: GenrePkColumnsInput;
};


/** mutation root */
export type MutationRootUpdateMoodArgs = {
  _set?: Maybe<MoodSetInput>;
  where: MoodBoolExp;
};


/** mutation root */
export type MutationRootUpdateMoodByPkArgs = {
  _set?: Maybe<MoodSetInput>;
  pk_columns: MoodPkColumnsInput;
};


/** mutation root */
export type MutationRootUpdateMovieArgs = {
  _inc?: Maybe<MovieIncInput>;
  _set?: Maybe<MovieSetInput>;
  where: MovieBoolExp;
};


/** mutation root */
export type MutationRootUpdateMovieByPkArgs = {
  _inc?: Maybe<MovieIncInput>;
  _set?: Maybe<MovieSetInput>;
  pk_columns: MoviePkColumnsInput;
};


/** mutation root */
export type MutationRootUpdateMovieCollectionArgs = {
  _inc?: Maybe<MovieCollectionIncInput>;
  _set?: Maybe<MovieCollectionSetInput>;
  where: MovieCollectionBoolExp;
};


/** mutation root */
export type MutationRootUpdateMovieCollectionByPkArgs = {
  _inc?: Maybe<MovieCollectionIncInput>;
  _set?: Maybe<MovieCollectionSetInput>;
  pk_columns: MovieCollectionPkColumnsInput;
};


/** mutation root */
export type MutationRootUpdateMovieCountryArgs = {
  _inc?: Maybe<MovieCountryIncInput>;
  _set?: Maybe<MovieCountrySetInput>;
  where: MovieCountryBoolExp;
};


/** mutation root */
export type MutationRootUpdateMovieCountryByPkArgs = {
  _inc?: Maybe<MovieCountryIncInput>;
  _set?: Maybe<MovieCountrySetInput>;
  pk_columns: MovieCountryPkColumnsInput;
};


/** mutation root */
export type MutationRootUpdateMovieGenreArgs = {
  _inc?: Maybe<MovieGenreIncInput>;
  _set?: Maybe<MovieGenreSetInput>;
  where: MovieGenreBoolExp;
};


/** mutation root */
export type MutationRootUpdateMovieGenreByPkArgs = {
  _inc?: Maybe<MovieGenreIncInput>;
  _set?: Maybe<MovieGenreSetInput>;
  pk_columns: MovieGenrePkColumnsInput;
};


/** mutation root */
export type MutationRootUpdateMovieStaffArgs = {
  _inc?: Maybe<MovieStaffIncInput>;
  _set?: Maybe<MovieStaffSetInput>;
  where: MovieStaffBoolExp;
};


/** mutation root */
export type MutationRootUpdateMovieStaffByPkArgs = {
  _inc?: Maybe<MovieStaffIncInput>;
  _set?: Maybe<MovieStaffSetInput>;
  pk_columns: MovieStaffPkColumnsInput;
};


/** mutation root */
export type MutationRootUpdateMovieStudioArgs = {
  _inc?: Maybe<MovieStudioIncInput>;
  _set?: Maybe<MovieStudioSetInput>;
  where: MovieStudioBoolExp;
};


/** mutation root */
export type MutationRootUpdateMovieStudioByPkArgs = {
  _inc?: Maybe<MovieStudioIncInput>;
  _set?: Maybe<MovieStudioSetInput>;
  pk_columns: MovieStudioPkColumnsInput;
};


/** mutation root */
export type MutationRootUpdatePersonArgs = {
  _set?: Maybe<PersonSetInput>;
  where: PersonBoolExp;
};


/** mutation root */
export type MutationRootUpdatePersonByPkArgs = {
  _set?: Maybe<PersonSetInput>;
  pk_columns: PersonPkColumnsInput;
};


/** mutation root */
export type MutationRootUpdateShowArgs = {
  _inc?: Maybe<ShowIncInput>;
  _set?: Maybe<ShowSetInput>;
  where: ShowBoolExp;
};


/** mutation root */
export type MutationRootUpdateShowByPkArgs = {
  _inc?: Maybe<ShowIncInput>;
  _set?: Maybe<ShowSetInput>;
  pk_columns: ShowPkColumnsInput;
};


/** mutation root */
export type MutationRootUpdateShowCollectionArgs = {
  _inc?: Maybe<ShowCollectionIncInput>;
  _set?: Maybe<ShowCollectionSetInput>;
  where: ShowCollectionBoolExp;
};


/** mutation root */
export type MutationRootUpdateShowCollectionByPkArgs = {
  _inc?: Maybe<ShowCollectionIncInput>;
  _set?: Maybe<ShowCollectionSetInput>;
  pk_columns: ShowCollectionPkColumnsInput;
};


/** mutation root */
export type MutationRootUpdateShowGenreArgs = {
  _inc?: Maybe<ShowGenreIncInput>;
  _set?: Maybe<ShowGenreSetInput>;
  where: ShowGenreBoolExp;
};


/** mutation root */
export type MutationRootUpdateShowGenreByPkArgs = {
  _inc?: Maybe<ShowGenreIncInput>;
  _set?: Maybe<ShowGenreSetInput>;
  pk_columns: ShowGenrePkColumnsInput;
};


/** mutation root */
export type MutationRootUpdateShowSeasonArgs = {
  _inc?: Maybe<ShowSeasonIncInput>;
  _set?: Maybe<ShowSeasonSetInput>;
  where: ShowSeasonBoolExp;
};


/** mutation root */
export type MutationRootUpdateShowSeasonByPkArgs = {
  _inc?: Maybe<ShowSeasonIncInput>;
  _set?: Maybe<ShowSeasonSetInput>;
  pk_columns: ShowSeasonPkColumnsInput;
};


/** mutation root */
export type MutationRootUpdateShowStaffArgs = {
  _inc?: Maybe<ShowStaffIncInput>;
  _set?: Maybe<ShowStaffSetInput>;
  where: ShowStaffBoolExp;
};


/** mutation root */
export type MutationRootUpdateShowStaffByPkArgs = {
  _inc?: Maybe<ShowStaffIncInput>;
  _set?: Maybe<ShowStaffSetInput>;
  pk_columns: ShowStaffPkColumnsInput;
};


/** mutation root */
export type MutationRootUpdateStaffArgs = {
  _set?: Maybe<StaffSetInput>;
  where: StaffBoolExp;
};


/** mutation root */
export type MutationRootUpdateStaffByPkArgs = {
  _set?: Maybe<StaffSetInput>;
  pk_columns: StaffPkColumnsInput;
};


/** mutation root */
export type MutationRootUpdateStudioArgs = {
  _set?: Maybe<StudioSetInput>;
  where: StudioBoolExp;
};


/** mutation root */
export type MutationRootUpdateStudioByPkArgs = {
  _set?: Maybe<StudioSetInput>;
  pk_columns: StudioPkColumnsInput;
};


/** mutation root */
export type MutationRootUpdateStyleArgs = {
  _set?: Maybe<StyleSetInput>;
  where: StyleBoolExp;
};


/** mutation root */
export type MutationRootUpdateStyleByPkArgs = {
  _set?: Maybe<StyleSetInput>;
  pk_columns: StylePkColumnsInput;
};


/** mutation root */
export type MutationRootUpdateTrackArgs = {
  _inc?: Maybe<TrackIncInput>;
  _set?: Maybe<TrackSetInput>;
  where: TrackBoolExp;
};


/** mutation root */
export type MutationRootUpdateTrackByPkArgs = {
  _inc?: Maybe<TrackIncInput>;
  _set?: Maybe<TrackSetInput>;
  pk_columns: TrackPkColumnsInput;
};


/** mutation root */
export type MutationRootUpdateTrackGenreArgs = {
  _inc?: Maybe<TrackGenreIncInput>;
  _set?: Maybe<TrackGenreSetInput>;
  where: TrackGenreBoolExp;
};


/** mutation root */
export type MutationRootUpdateTrackGenreByPkArgs = {
  _inc?: Maybe<TrackGenreIncInput>;
  _set?: Maybe<TrackGenreSetInput>;
  pk_columns: TrackGenrePkColumnsInput;
};


/** mutation root */
export type MutationRootUpdateTrackMoodArgs = {
  _inc?: Maybe<TrackMoodIncInput>;
  _set?: Maybe<TrackMoodSetInput>;
  where: TrackMoodBoolExp;
};


/** mutation root */
export type MutationRootUpdateTrackMoodByPkArgs = {
  _inc?: Maybe<TrackMoodIncInput>;
  _set?: Maybe<TrackMoodSetInput>;
  pk_columns: TrackMoodPkColumnsInput;
};


/** expression to compare columns of type numeric. All fields are combined with logical 'AND'. */
export type NumericComparisonExp = {
  _eq?: Maybe<Scalars['numeric']>;
  _gt?: Maybe<Scalars['numeric']>;
  _gte?: Maybe<Scalars['numeric']>;
  _in?: Maybe<Array<Scalars['numeric']>>;
  _is_null?: Maybe<Scalars['Boolean']>;
  _lt?: Maybe<Scalars['numeric']>;
  _lte?: Maybe<Scalars['numeric']>;
  _neq?: Maybe<Scalars['numeric']>;
  _nin?: Maybe<Array<Scalars['numeric']>>;
};

/** column ordering options */
export enum OrderBy {
  /** in the ascending order, nulls last */
  Asc = 'asc',
  /** in the ascending order, nulls first */
  AscNullsFirst = 'asc_nulls_first',
  /** in the ascending order, nulls last */
  AscNullsLast = 'asc_nulls_last',
  /** in the descending order, nulls first */
  Desc = 'desc',
  /** in the descending order, nulls first */
  DescNullsFirst = 'desc_nulls_first',
  /** in the descending order, nulls last */
  DescNullsLast = 'desc_nulls_last'
}

/** columns and relationships of "person" */
export type Person = {
  /** An array relationship */
  artists: Array<Artist>;
  /** An aggregated array relationship */
  artists_aggregate: ArtistAggregate;
  /** An array relationship */
  episode_staffs: Array<EpisodeStaff>;
  /** An aggregated array relationship */
  episode_staffs_aggregate: EpisodeStaffAggregate;
  id: Scalars['uuid'];
  image_cover?: Maybe<Scalars['String']>;
  /** An array relationship */
  movie_staffs: Array<MovieStaff>;
  /** An aggregated array relationship */
  movie_staffs_aggregate: MovieStaffAggregate;
  name: Scalars['String'];
  name_match: Scalars['String'];
  name_sort?: Maybe<Scalars['String']>;
  /** An array relationship */
  show_staffs: Array<ShowStaff>;
  /** An aggregated array relationship */
  show_staffs_aggregate: ShowStaffAggregate;
};


/** columns and relationships of "person" */
export type PersonArtistsArgs = {
  distinct_on?: Maybe<Array<ArtistSelectColumn>>;
  limit?: Maybe<Scalars['Int']>;
  offset?: Maybe<Scalars['Int']>;
  order_by?: Maybe<Array<ArtistOrderBy>>;
  where?: Maybe<ArtistBoolExp>;
};


/** columns and relationships of "person" */
export type PersonArtistsAggregateArgs = {
  distinct_on?: Maybe<Array<ArtistSelectColumn>>;
  limit?: Maybe<Scalars['Int']>;
  offset?: Maybe<Scalars['Int']>;
  order_by?: Maybe<Array<ArtistOrderBy>>;
  where?: Maybe<ArtistBoolExp>;
};


/** columns and relationships of "person" */
export type PersonEpisodeStaffsArgs = {
  distinct_on?: Maybe<Array<EpisodeStaffSelectColumn>>;
  limit?: Maybe<Scalars['Int']>;
  offset?: Maybe<Scalars['Int']>;
  order_by?: Maybe<Array<EpisodeStaffOrderBy>>;
  where?: Maybe<EpisodeStaffBoolExp>;
};


/** columns and relationships of "person" */
export type PersonEpisodeStaffsAggregateArgs = {
  distinct_on?: Maybe<Array<EpisodeStaffSelectColumn>>;
  limit?: Maybe<Scalars['Int']>;
  offset?: Maybe<Scalars['Int']>;
  order_by?: Maybe<Array<EpisodeStaffOrderBy>>;
  where?: Maybe<EpisodeStaffBoolExp>;
};


/** columns and relationships of "person" */
export type PersonMovieStaffsArgs = {
  distinct_on?: Maybe<Array<MovieStaffSelectColumn>>;
  limit?: Maybe<Scalars['Int']>;
  offset?: Maybe<Scalars['Int']>;
  order_by?: Maybe<Array<MovieStaffOrderBy>>;
  where?: Maybe<MovieStaffBoolExp>;
};


/** columns and relationships of "person" */
export type PersonMovieStaffsAggregateArgs = {
  distinct_on?: Maybe<Array<MovieStaffSelectColumn>>;
  limit?: Maybe<Scalars['Int']>;
  offset?: Maybe<Scalars['Int']>;
  order_by?: Maybe<Array<MovieStaffOrderBy>>;
  where?: Maybe<MovieStaffBoolExp>;
};


/** columns and relationships of "person" */
export type PersonShowStaffsArgs = {
  distinct_on?: Maybe<Array<ShowStaffSelectColumn>>;
  limit?: Maybe<Scalars['Int']>;
  offset?: Maybe<Scalars['Int']>;
  order_by?: Maybe<Array<ShowStaffOrderBy>>;
  where?: Maybe<ShowStaffBoolExp>;
};


/** columns and relationships of "person" */
export type PersonShowStaffsAggregateArgs = {
  distinct_on?: Maybe<Array<ShowStaffSelectColumn>>;
  limit?: Maybe<Scalars['Int']>;
  offset?: Maybe<Scalars['Int']>;
  order_by?: Maybe<Array<ShowStaffOrderBy>>;
  where?: Maybe<ShowStaffBoolExp>;
};

/** aggregated selection of "person" */
export type PersonAggregate = {
  aggregate?: Maybe<PersonAggregateFields>;
  nodes: Array<Person>;
};

/** aggregate fields of "person" */
export type PersonAggregateFields = {
  count?: Maybe<Scalars['Int']>;
  max?: Maybe<PersonMaxFields>;
  min?: Maybe<PersonMinFields>;
};


/** aggregate fields of "person" */
export type PersonAggregateFieldsCountArgs = {
  columns?: Maybe<Array<PersonSelectColumn>>;
  distinct?: Maybe<Scalars['Boolean']>;
};

/** order by aggregate values of table "person" */
export type PersonAggregateOrderBy = {
  count?: Maybe<OrderBy>;
  max?: Maybe<PersonMaxOrderBy>;
  min?: Maybe<PersonMinOrderBy>;
};

/** input type for inserting array relation for remote table "person" */
export type PersonArrRelInsertInput = {
  data: Array<PersonInsertInput>;
  on_conflict?: Maybe<PersonOnConflict>;
};

/** Boolean expression to filter rows from the table "person". All fields are combined with a logical 'AND'. */
export type PersonBoolExp = {
  _and?: Maybe<Array<Maybe<PersonBoolExp>>>;
  _not?: Maybe<PersonBoolExp>;
  _or?: Maybe<Array<Maybe<PersonBoolExp>>>;
  artists?: Maybe<ArtistBoolExp>;
  episode_staffs?: Maybe<EpisodeStaffBoolExp>;
  id?: Maybe<UuidComparisonExp>;
  image_cover?: Maybe<StringComparisonExp>;
  movie_staffs?: Maybe<MovieStaffBoolExp>;
  name?: Maybe<StringComparisonExp>;
  name_match?: Maybe<StringComparisonExp>;
  name_sort?: Maybe<StringComparisonExp>;
  show_staffs?: Maybe<ShowStaffBoolExp>;
};

/** unique or primary key constraints on table "person" */
export enum PersonConstraint {
  /** unique or primary key constraint */
  PersonNameMatchKey = 'person_name_match_key',
  /** unique or primary key constraint */
  PersonPkey = 'person_pkey'
}

/** input type for inserting data into table "person" */
export type PersonInsertInput = {
  artists?: Maybe<ArtistArrRelInsertInput>;
  episode_staffs?: Maybe<EpisodeStaffArrRelInsertInput>;
  id?: Maybe<Scalars['uuid']>;
  image_cover?: Maybe<Scalars['String']>;
  movie_staffs?: Maybe<MovieStaffArrRelInsertInput>;
  name?: Maybe<Scalars['String']>;
  name_match?: Maybe<Scalars['String']>;
  name_sort?: Maybe<Scalars['String']>;
  show_staffs?: Maybe<ShowStaffArrRelInsertInput>;
};

/** aggregate max on columns */
export type PersonMaxFields = {
  id?: Maybe<Scalars['uuid']>;
  image_cover?: Maybe<Scalars['String']>;
  name?: Maybe<Scalars['String']>;
  name_match?: Maybe<Scalars['String']>;
  name_sort?: Maybe<Scalars['String']>;
};

/** order by max() on columns of table "person" */
export type PersonMaxOrderBy = {
  id?: Maybe<OrderBy>;
  image_cover?: Maybe<OrderBy>;
  name?: Maybe<OrderBy>;
  name_match?: Maybe<OrderBy>;
  name_sort?: Maybe<OrderBy>;
};

/** aggregate min on columns */
export type PersonMinFields = {
  id?: Maybe<Scalars['uuid']>;
  image_cover?: Maybe<Scalars['String']>;
  name?: Maybe<Scalars['String']>;
  name_match?: Maybe<Scalars['String']>;
  name_sort?: Maybe<Scalars['String']>;
};

/** order by min() on columns of table "person" */
export type PersonMinOrderBy = {
  id?: Maybe<OrderBy>;
  image_cover?: Maybe<OrderBy>;
  name?: Maybe<OrderBy>;
  name_match?: Maybe<OrderBy>;
  name_sort?: Maybe<OrderBy>;
};

/** response of any mutation on the table "person" */
export type PersonMutationResponse = {
  /** number of affected rows by the mutation */
  affected_rows: Scalars['Int'];
  /** data of the affected rows by the mutation */
  returning: Array<Person>;
};

/** input type for inserting object relation for remote table "person" */
export type PersonObjRelInsertInput = {
  data: PersonInsertInput;
  on_conflict?: Maybe<PersonOnConflict>;
};

/** on conflict condition type for table "person" */
export type PersonOnConflict = {
  constraint: PersonConstraint;
  update_columns: Array<PersonUpdateColumn>;
  where?: Maybe<PersonBoolExp>;
};

/** ordering options when selecting data from "person" */
export type PersonOrderBy = {
  artists_aggregate?: Maybe<ArtistAggregateOrderBy>;
  episode_staffs_aggregate?: Maybe<EpisodeStaffAggregateOrderBy>;
  id?: Maybe<OrderBy>;
  image_cover?: Maybe<OrderBy>;
  movie_staffs_aggregate?: Maybe<MovieStaffAggregateOrderBy>;
  name?: Maybe<OrderBy>;
  name_match?: Maybe<OrderBy>;
  name_sort?: Maybe<OrderBy>;
  show_staffs_aggregate?: Maybe<ShowStaffAggregateOrderBy>;
};

/** primary key columns input for table: "person" */
export type PersonPkColumnsInput = {
  id: Scalars['uuid'];
};

/** select columns of table "person" */
export enum PersonSelectColumn {
  /** column name */
  Id = 'id',
  /** column name */
  ImageCover = 'image_cover',
  /** column name */
  Name = 'name',
  /** column name */
  NameMatch = 'name_match',
  /** column name */
  NameSort = 'name_sort'
}

/** input type for updating data in table "person" */
export type PersonSetInput = {
  id?: Maybe<Scalars['uuid']>;
  image_cover?: Maybe<Scalars['String']>;
  name?: Maybe<Scalars['String']>;
  name_match?: Maybe<Scalars['String']>;
  name_sort?: Maybe<Scalars['String']>;
};

/** update columns of table "person" */
export enum PersonUpdateColumn {
  /** column name */
  Id = 'id',
  /** column name */
  ImageCover = 'image_cover',
  /** column name */
  Name = 'name',
  /** column name */
  NameMatch = 'name_match',
  /** column name */
  NameSort = 'name_sort'
}

/** query root */
export type QueryRoot = {
  /** fetch data from the table: "album" */
  album: Array<Album>;
  /** fetch aggregated fields from the table: "album" */
  album_aggregate: AlbumAggregate;
  /** fetch data from the table: "album" using primary key columns */
  album_by_pk?: Maybe<Album>;
  /** fetch data from the table: "album_collection" */
  album_collection: Array<AlbumCollection>;
  /** fetch aggregated fields from the table: "album_collection" */
  album_collection_aggregate: AlbumCollectionAggregate;
  /** fetch data from the table: "album_collection" using primary key columns */
  album_collection_by_pk?: Maybe<AlbumCollection>;
  /** fetch data from the table: "album_country" */
  album_country: Array<AlbumCountry>;
  /** fetch aggregated fields from the table: "album_country" */
  album_country_aggregate: AlbumCountryAggregate;
  /** fetch data from the table: "album_country" using primary key columns */
  album_country_by_pk?: Maybe<AlbumCountry>;
  /** fetch data from the table: "album_genre" */
  album_genre: Array<AlbumGenre>;
  /** fetch aggregated fields from the table: "album_genre" */
  album_genre_aggregate: AlbumGenreAggregate;
  /** fetch data from the table: "album_genre" using primary key columns */
  album_genre_by_pk?: Maybe<AlbumGenre>;
  /** fetch data from the table: "album_mood" */
  album_mood: Array<AlbumMood>;
  /** fetch aggregated fields from the table: "album_mood" */
  album_mood_aggregate: AlbumMoodAggregate;
  /** fetch data from the table: "album_mood" using primary key columns */
  album_mood_by_pk?: Maybe<AlbumMood>;
  /** fetch data from the table: "album_studio" */
  album_studio: Array<AlbumStudio>;
  /** fetch aggregated fields from the table: "album_studio" */
  album_studio_aggregate: AlbumStudioAggregate;
  /** fetch data from the table: "album_studio" using primary key columns */
  album_studio_by_pk?: Maybe<AlbumStudio>;
  /** fetch data from the table: "album_style" */
  album_style: Array<AlbumStyle>;
  /** fetch aggregated fields from the table: "album_style" */
  album_style_aggregate: AlbumStyleAggregate;
  /** fetch data from the table: "album_style" using primary key columns */
  album_style_by_pk?: Maybe<AlbumStyle>;
  /** fetch data from the table: "album_track" */
  album_track: Array<AlbumTrack>;
  /** fetch aggregated fields from the table: "album_track" */
  album_track_aggregate: AlbumTrackAggregate;
  /** fetch data from the table: "album_track" using primary key columns */
  album_track_by_pk?: Maybe<AlbumTrack>;
  /** fetch data from the table: "artist" */
  artist: Array<Artist>;
  /** fetch aggregated fields from the table: "artist" */
  artist_aggregate: ArtistAggregate;
  /** fetch data from the table: "artist" using primary key columns */
  artist_by_pk?: Maybe<Artist>;
  /** fetch data from the table: "artist_collection" */
  artist_collection: Array<ArtistCollection>;
  /** fetch aggregated fields from the table: "artist_collection" */
  artist_collection_aggregate: ArtistCollectionAggregate;
  /** fetch data from the table: "artist_collection" using primary key columns */
  artist_collection_by_pk?: Maybe<ArtistCollection>;
  /** fetch data from the table: "artist_country" */
  artist_country: Array<ArtistCountry>;
  /** fetch aggregated fields from the table: "artist_country" */
  artist_country_aggregate: ArtistCountryAggregate;
  /** fetch data from the table: "artist_country" using primary key columns */
  artist_country_by_pk?: Maybe<ArtistCountry>;
  /** fetch data from the table: "artist_genre" */
  artist_genre: Array<ArtistGenre>;
  /** fetch aggregated fields from the table: "artist_genre" */
  artist_genre_aggregate: ArtistGenreAggregate;
  /** fetch data from the table: "artist_genre" using primary key columns */
  artist_genre_by_pk?: Maybe<ArtistGenre>;
  /** fetch data from the table: "artist_mood" */
  artist_mood: Array<ArtistMood>;
  /** fetch aggregated fields from the table: "artist_mood" */
  artist_mood_aggregate: ArtistMoodAggregate;
  /** fetch data from the table: "artist_mood" using primary key columns */
  artist_mood_by_pk?: Maybe<ArtistMood>;
  /** fetch data from the table: "artist_similar" */
  artist_similar: Array<ArtistSimilar>;
  /** fetch aggregated fields from the table: "artist_similar" */
  artist_similar_aggregate: ArtistSimilarAggregate;
  /** fetch data from the table: "artist_similar" using primary key columns */
  artist_similar_by_pk?: Maybe<ArtistSimilar>;
  /** fetch data from the table: "artist_style" */
  artist_style: Array<ArtistStyle>;
  /** fetch aggregated fields from the table: "artist_style" */
  artist_style_aggregate: ArtistStyleAggregate;
  /** fetch data from the table: "artist_style" using primary key columns */
  artist_style_by_pk?: Maybe<ArtistStyle>;
  /** fetch data from the table: "collection" */
  collection: Array<Collection>;
  /** fetch aggregated fields from the table: "collection" */
  collection_aggregate: CollectionAggregate;
  /** fetch data from the table: "collection" using primary key columns */
  collection_by_pk?: Maybe<Collection>;
  /** fetch data from the table: "country" */
  country: Array<Country>;
  /** fetch aggregated fields from the table: "country" */
  country_aggregate: CountryAggregate;
  /** fetch data from the table: "country" using primary key columns */
  country_by_pk?: Maybe<Country>;
  /** fetch data from the table: "episode" */
  episode: Array<Episode>;
  /** fetch aggregated fields from the table: "episode" */
  episode_aggregate: EpisodeAggregate;
  /** fetch data from the table: "episode" using primary key columns */
  episode_by_pk?: Maybe<Episode>;
  /** fetch data from the table: "episode_staff" */
  episode_staff: Array<EpisodeStaff>;
  /** fetch aggregated fields from the table: "episode_staff" */
  episode_staff_aggregate: EpisodeStaffAggregate;
  /** fetch data from the table: "episode_staff" using primary key columns */
  episode_staff_by_pk?: Maybe<EpisodeStaff>;
  /** fetch data from the table: "genre" */
  genre: Array<Genre>;
  /** fetch aggregated fields from the table: "genre" */
  genre_aggregate: GenreAggregate;
  /** fetch data from the table: "genre" using primary key columns */
  genre_by_pk?: Maybe<Genre>;
  /** fetch data from the table: "mood" */
  mood: Array<Mood>;
  /** fetch aggregated fields from the table: "mood" */
  mood_aggregate: MoodAggregate;
  /** fetch data from the table: "mood" using primary key columns */
  mood_by_pk?: Maybe<Mood>;
  /** fetch data from the table: "movie" */
  movie: Array<Movie>;
  /** fetch aggregated fields from the table: "movie" */
  movie_aggregate: MovieAggregate;
  /** fetch data from the table: "movie" using primary key columns */
  movie_by_pk?: Maybe<Movie>;
  /** fetch data from the table: "movie_collection" */
  movie_collection: Array<MovieCollection>;
  /** fetch aggregated fields from the table: "movie_collection" */
  movie_collection_aggregate: MovieCollectionAggregate;
  /** fetch data from the table: "movie_collection" using primary key columns */
  movie_collection_by_pk?: Maybe<MovieCollection>;
  /** fetch data from the table: "movie_country" */
  movie_country: Array<MovieCountry>;
  /** fetch aggregated fields from the table: "movie_country" */
  movie_country_aggregate: MovieCountryAggregate;
  /** fetch data from the table: "movie_country" using primary key columns */
  movie_country_by_pk?: Maybe<MovieCountry>;
  /** fetch data from the table: "movie_genre" */
  movie_genre: Array<MovieGenre>;
  /** fetch aggregated fields from the table: "movie_genre" */
  movie_genre_aggregate: MovieGenreAggregate;
  /** fetch data from the table: "movie_genre" using primary key columns */
  movie_genre_by_pk?: Maybe<MovieGenre>;
  /** fetch data from the table: "movie_staff" */
  movie_staff: Array<MovieStaff>;
  /** fetch aggregated fields from the table: "movie_staff" */
  movie_staff_aggregate: MovieStaffAggregate;
  /** fetch data from the table: "movie_staff" using primary key columns */
  movie_staff_by_pk?: Maybe<MovieStaff>;
  /** fetch data from the table: "movie_studio" */
  movie_studio: Array<MovieStudio>;
  /** fetch aggregated fields from the table: "movie_studio" */
  movie_studio_aggregate: MovieStudioAggregate;
  /** fetch data from the table: "movie_studio" using primary key columns */
  movie_studio_by_pk?: Maybe<MovieStudio>;
  /** fetch data from the table: "person" */
  person: Array<Person>;
  /** fetch aggregated fields from the table: "person" */
  person_aggregate: PersonAggregate;
  /** fetch data from the table: "person" using primary key columns */
  person_by_pk?: Maybe<Person>;
  /** fetch data from the table: "show" */
  show: Array<Show>;
  /** fetch aggregated fields from the table: "show" */
  show_aggregate: ShowAggregate;
  /** fetch data from the table: "show" using primary key columns */
  show_by_pk?: Maybe<Show>;
  /** fetch data from the table: "show_collection" */
  show_collection: Array<ShowCollection>;
  /** fetch aggregated fields from the table: "show_collection" */
  show_collection_aggregate: ShowCollectionAggregate;
  /** fetch data from the table: "show_collection" using primary key columns */
  show_collection_by_pk?: Maybe<ShowCollection>;
  /** fetch data from the table: "show_genre" */
  show_genre: Array<ShowGenre>;
  /** fetch aggregated fields from the table: "show_genre" */
  show_genre_aggregate: ShowGenreAggregate;
  /** fetch data from the table: "show_genre" using primary key columns */
  show_genre_by_pk?: Maybe<ShowGenre>;
  /** fetch data from the table: "show_season" */
  show_season: Array<ShowSeason>;
  /** fetch aggregated fields from the table: "show_season" */
  show_season_aggregate: ShowSeasonAggregate;
  /** fetch data from the table: "show_season" using primary key columns */
  show_season_by_pk?: Maybe<ShowSeason>;
  /** fetch data from the table: "show_staff" */
  show_staff: Array<ShowStaff>;
  /** fetch aggregated fields from the table: "show_staff" */
  show_staff_aggregate: ShowStaffAggregate;
  /** fetch data from the table: "show_staff" using primary key columns */
  show_staff_by_pk?: Maybe<ShowStaff>;
  /** fetch data from the table: "staff" */
  staff: Array<Staff>;
  /** fetch aggregated fields from the table: "staff" */
  staff_aggregate: StaffAggregate;
  /** fetch data from the table: "staff" using primary key columns */
  staff_by_pk?: Maybe<Staff>;
  /** fetch data from the table: "studio" */
  studio: Array<Studio>;
  /** fetch aggregated fields from the table: "studio" */
  studio_aggregate: StudioAggregate;
  /** fetch data from the table: "studio" using primary key columns */
  studio_by_pk?: Maybe<Studio>;
  /** fetch data from the table: "style" */
  style: Array<Style>;
  /** fetch aggregated fields from the table: "style" */
  style_aggregate: StyleAggregate;
  /** fetch data from the table: "style" using primary key columns */
  style_by_pk?: Maybe<Style>;
  /** fetch data from the table: "track" */
  track: Array<Track>;
  /** fetch aggregated fields from the table: "track" */
  track_aggregate: TrackAggregate;
  /** fetch data from the table: "track" using primary key columns */
  track_by_pk?: Maybe<Track>;
  /** fetch data from the table: "track_genre" */
  track_genre: Array<TrackGenre>;
  /** fetch aggregated fields from the table: "track_genre" */
  track_genre_aggregate: TrackGenreAggregate;
  /** fetch data from the table: "track_genre" using primary key columns */
  track_genre_by_pk?: Maybe<TrackGenre>;
  /** fetch data from the table: "track_mood" */
  track_mood: Array<TrackMood>;
  /** fetch aggregated fields from the table: "track_mood" */
  track_mood_aggregate: TrackMoodAggregate;
  /** fetch data from the table: "track_mood" using primary key columns */
  track_mood_by_pk?: Maybe<TrackMood>;
};


/** query root */
export type QueryRootAlbumArgs = {
  distinct_on?: Maybe<Array<AlbumSelectColumn>>;
  limit?: Maybe<Scalars['Int']>;
  offset?: Maybe<Scalars['Int']>;
  order_by?: Maybe<Array<AlbumOrderBy>>;
  where?: Maybe<AlbumBoolExp>;
};


/** query root */
export type QueryRootAlbumAggregateArgs = {
  distinct_on?: Maybe<Array<AlbumSelectColumn>>;
  limit?: Maybe<Scalars['Int']>;
  offset?: Maybe<Scalars['Int']>;
  order_by?: Maybe<Array<AlbumOrderBy>>;
  where?: Maybe<AlbumBoolExp>;
};


/** query root */
export type QueryRootAlbumByPkArgs = {
  id: Scalars['uuid'];
};


/** query root */
export type QueryRootAlbumCollectionArgs = {
  distinct_on?: Maybe<Array<AlbumCollectionSelectColumn>>;
  limit?: Maybe<Scalars['Int']>;
  offset?: Maybe<Scalars['Int']>;
  order_by?: Maybe<Array<AlbumCollectionOrderBy>>;
  where?: Maybe<AlbumCollectionBoolExp>;
};


/** query root */
export type QueryRootAlbumCollectionAggregateArgs = {
  distinct_on?: Maybe<Array<AlbumCollectionSelectColumn>>;
  limit?: Maybe<Scalars['Int']>;
  offset?: Maybe<Scalars['Int']>;
  order_by?: Maybe<Array<AlbumCollectionOrderBy>>;
  where?: Maybe<AlbumCollectionBoolExp>;
};


/** query root */
export type QueryRootAlbumCollectionByPkArgs = {
  album_id: Scalars['uuid'];
  index: Scalars['Int'];
};


/** query root */
export type QueryRootAlbumCountryArgs = {
  distinct_on?: Maybe<Array<AlbumCountrySelectColumn>>;
  limit?: Maybe<Scalars['Int']>;
  offset?: Maybe<Scalars['Int']>;
  order_by?: Maybe<Array<AlbumCountryOrderBy>>;
  where?: Maybe<AlbumCountryBoolExp>;
};


/** query root */
export type QueryRootAlbumCountryAggregateArgs = {
  distinct_on?: Maybe<Array<AlbumCountrySelectColumn>>;
  limit?: Maybe<Scalars['Int']>;
  offset?: Maybe<Scalars['Int']>;
  order_by?: Maybe<Array<AlbumCountryOrderBy>>;
  where?: Maybe<AlbumCountryBoolExp>;
};


/** query root */
export type QueryRootAlbumCountryByPkArgs = {
  album_id: Scalars['uuid'];
  index: Scalars['Int'];
};


/** query root */
export type QueryRootAlbumGenreArgs = {
  distinct_on?: Maybe<Array<AlbumGenreSelectColumn>>;
  limit?: Maybe<Scalars['Int']>;
  offset?: Maybe<Scalars['Int']>;
  order_by?: Maybe<Array<AlbumGenreOrderBy>>;
  where?: Maybe<AlbumGenreBoolExp>;
};


/** query root */
export type QueryRootAlbumGenreAggregateArgs = {
  distinct_on?: Maybe<Array<AlbumGenreSelectColumn>>;
  limit?: Maybe<Scalars['Int']>;
  offset?: Maybe<Scalars['Int']>;
  order_by?: Maybe<Array<AlbumGenreOrderBy>>;
  where?: Maybe<AlbumGenreBoolExp>;
};


/** query root */
export type QueryRootAlbumGenreByPkArgs = {
  album_id: Scalars['uuid'];
  index: Scalars['Int'];
};


/** query root */
export type QueryRootAlbumMoodArgs = {
  distinct_on?: Maybe<Array<AlbumMoodSelectColumn>>;
  limit?: Maybe<Scalars['Int']>;
  offset?: Maybe<Scalars['Int']>;
  order_by?: Maybe<Array<AlbumMoodOrderBy>>;
  where?: Maybe<AlbumMoodBoolExp>;
};


/** query root */
export type QueryRootAlbumMoodAggregateArgs = {
  distinct_on?: Maybe<Array<AlbumMoodSelectColumn>>;
  limit?: Maybe<Scalars['Int']>;
  offset?: Maybe<Scalars['Int']>;
  order_by?: Maybe<Array<AlbumMoodOrderBy>>;
  where?: Maybe<AlbumMoodBoolExp>;
};


/** query root */
export type QueryRootAlbumMoodByPkArgs = {
  album_id: Scalars['uuid'];
  index: Scalars['Int'];
};


/** query root */
export type QueryRootAlbumStudioArgs = {
  distinct_on?: Maybe<Array<AlbumStudioSelectColumn>>;
  limit?: Maybe<Scalars['Int']>;
  offset?: Maybe<Scalars['Int']>;
  order_by?: Maybe<Array<AlbumStudioOrderBy>>;
  where?: Maybe<AlbumStudioBoolExp>;
};


/** query root */
export type QueryRootAlbumStudioAggregateArgs = {
  distinct_on?: Maybe<Array<AlbumStudioSelectColumn>>;
  limit?: Maybe<Scalars['Int']>;
  offset?: Maybe<Scalars['Int']>;
  order_by?: Maybe<Array<AlbumStudioOrderBy>>;
  where?: Maybe<AlbumStudioBoolExp>;
};


/** query root */
export type QueryRootAlbumStudioByPkArgs = {
  album_id: Scalars['uuid'];
  index: Scalars['Int'];
};


/** query root */
export type QueryRootAlbumStyleArgs = {
  distinct_on?: Maybe<Array<AlbumStyleSelectColumn>>;
  limit?: Maybe<Scalars['Int']>;
  offset?: Maybe<Scalars['Int']>;
  order_by?: Maybe<Array<AlbumStyleOrderBy>>;
  where?: Maybe<AlbumStyleBoolExp>;
};


/** query root */
export type QueryRootAlbumStyleAggregateArgs = {
  distinct_on?: Maybe<Array<AlbumStyleSelectColumn>>;
  limit?: Maybe<Scalars['Int']>;
  offset?: Maybe<Scalars['Int']>;
  order_by?: Maybe<Array<AlbumStyleOrderBy>>;
  where?: Maybe<AlbumStyleBoolExp>;
};


/** query root */
export type QueryRootAlbumStyleByPkArgs = {
  album_id: Scalars['uuid'];
  index: Scalars['Int'];
};


/** query root */
export type QueryRootAlbumTrackArgs = {
  distinct_on?: Maybe<Array<AlbumTrackSelectColumn>>;
  limit?: Maybe<Scalars['Int']>;
  offset?: Maybe<Scalars['Int']>;
  order_by?: Maybe<Array<AlbumTrackOrderBy>>;
  where?: Maybe<AlbumTrackBoolExp>;
};


/** query root */
export type QueryRootAlbumTrackAggregateArgs = {
  distinct_on?: Maybe<Array<AlbumTrackSelectColumn>>;
  limit?: Maybe<Scalars['Int']>;
  offset?: Maybe<Scalars['Int']>;
  order_by?: Maybe<Array<AlbumTrackOrderBy>>;
  where?: Maybe<AlbumTrackBoolExp>;
};


/** query root */
export type QueryRootAlbumTrackByPkArgs = {
  disk: Scalars['Int'];
  index: Scalars['Int'];
  track_id: Scalars['uuid'];
};


/** query root */
export type QueryRootArtistArgs = {
  distinct_on?: Maybe<Array<ArtistSelectColumn>>;
  limit?: Maybe<Scalars['Int']>;
  offset?: Maybe<Scalars['Int']>;
  order_by?: Maybe<Array<ArtistOrderBy>>;
  where?: Maybe<ArtistBoolExp>;
};


/** query root */
export type QueryRootArtistAggregateArgs = {
  distinct_on?: Maybe<Array<ArtistSelectColumn>>;
  limit?: Maybe<Scalars['Int']>;
  offset?: Maybe<Scalars['Int']>;
  order_by?: Maybe<Array<ArtistOrderBy>>;
  where?: Maybe<ArtistBoolExp>;
};


/** query root */
export type QueryRootArtistByPkArgs = {
  id: Scalars['uuid'];
};


/** query root */
export type QueryRootArtistCollectionArgs = {
  distinct_on?: Maybe<Array<ArtistCollectionSelectColumn>>;
  limit?: Maybe<Scalars['Int']>;
  offset?: Maybe<Scalars['Int']>;
  order_by?: Maybe<Array<ArtistCollectionOrderBy>>;
  where?: Maybe<ArtistCollectionBoolExp>;
};


/** query root */
export type QueryRootArtistCollectionAggregateArgs = {
  distinct_on?: Maybe<Array<ArtistCollectionSelectColumn>>;
  limit?: Maybe<Scalars['Int']>;
  offset?: Maybe<Scalars['Int']>;
  order_by?: Maybe<Array<ArtistCollectionOrderBy>>;
  where?: Maybe<ArtistCollectionBoolExp>;
};


/** query root */
export type QueryRootArtistCollectionByPkArgs = {
  artist_id: Scalars['uuid'];
  index: Scalars['Int'];
};


/** query root */
export type QueryRootArtistCountryArgs = {
  distinct_on?: Maybe<Array<ArtistCountrySelectColumn>>;
  limit?: Maybe<Scalars['Int']>;
  offset?: Maybe<Scalars['Int']>;
  order_by?: Maybe<Array<ArtistCountryOrderBy>>;
  where?: Maybe<ArtistCountryBoolExp>;
};


/** query root */
export type QueryRootArtistCountryAggregateArgs = {
  distinct_on?: Maybe<Array<ArtistCountrySelectColumn>>;
  limit?: Maybe<Scalars['Int']>;
  offset?: Maybe<Scalars['Int']>;
  order_by?: Maybe<Array<ArtistCountryOrderBy>>;
  where?: Maybe<ArtistCountryBoolExp>;
};


/** query root */
export type QueryRootArtistCountryByPkArgs = {
  artist_id: Scalars['uuid'];
  index: Scalars['Int'];
};


/** query root */
export type QueryRootArtistGenreArgs = {
  distinct_on?: Maybe<Array<ArtistGenreSelectColumn>>;
  limit?: Maybe<Scalars['Int']>;
  offset?: Maybe<Scalars['Int']>;
  order_by?: Maybe<Array<ArtistGenreOrderBy>>;
  where?: Maybe<ArtistGenreBoolExp>;
};


/** query root */
export type QueryRootArtistGenreAggregateArgs = {
  distinct_on?: Maybe<Array<ArtistGenreSelectColumn>>;
  limit?: Maybe<Scalars['Int']>;
  offset?: Maybe<Scalars['Int']>;
  order_by?: Maybe<Array<ArtistGenreOrderBy>>;
  where?: Maybe<ArtistGenreBoolExp>;
};


/** query root */
export type QueryRootArtistGenreByPkArgs = {
  artist_id: Scalars['uuid'];
  index: Scalars['Int'];
};


/** query root */
export type QueryRootArtistMoodArgs = {
  distinct_on?: Maybe<Array<ArtistMoodSelectColumn>>;
  limit?: Maybe<Scalars['Int']>;
  offset?: Maybe<Scalars['Int']>;
  order_by?: Maybe<Array<ArtistMoodOrderBy>>;
  where?: Maybe<ArtistMoodBoolExp>;
};


/** query root */
export type QueryRootArtistMoodAggregateArgs = {
  distinct_on?: Maybe<Array<ArtistMoodSelectColumn>>;
  limit?: Maybe<Scalars['Int']>;
  offset?: Maybe<Scalars['Int']>;
  order_by?: Maybe<Array<ArtistMoodOrderBy>>;
  where?: Maybe<ArtistMoodBoolExp>;
};


/** query root */
export type QueryRootArtistMoodByPkArgs = {
  artist_id: Scalars['uuid'];
  index: Scalars['Int'];
};


/** query root */
export type QueryRootArtistSimilarArgs = {
  distinct_on?: Maybe<Array<ArtistSimilarSelectColumn>>;
  limit?: Maybe<Scalars['Int']>;
  offset?: Maybe<Scalars['Int']>;
  order_by?: Maybe<Array<ArtistSimilarOrderBy>>;
  where?: Maybe<ArtistSimilarBoolExp>;
};


/** query root */
export type QueryRootArtistSimilarAggregateArgs = {
  distinct_on?: Maybe<Array<ArtistSimilarSelectColumn>>;
  limit?: Maybe<Scalars['Int']>;
  offset?: Maybe<Scalars['Int']>;
  order_by?: Maybe<Array<ArtistSimilarOrderBy>>;
  where?: Maybe<ArtistSimilarBoolExp>;
};


/** query root */
export type QueryRootArtistSimilarByPkArgs = {
  artist1_id: Scalars['uuid'];
  artist2_id: Scalars['uuid'];
};


/** query root */
export type QueryRootArtistStyleArgs = {
  distinct_on?: Maybe<Array<ArtistStyleSelectColumn>>;
  limit?: Maybe<Scalars['Int']>;
  offset?: Maybe<Scalars['Int']>;
  order_by?: Maybe<Array<ArtistStyleOrderBy>>;
  where?: Maybe<ArtistStyleBoolExp>;
};


/** query root */
export type QueryRootArtistStyleAggregateArgs = {
  distinct_on?: Maybe<Array<ArtistStyleSelectColumn>>;
  limit?: Maybe<Scalars['Int']>;
  offset?: Maybe<Scalars['Int']>;
  order_by?: Maybe<Array<ArtistStyleOrderBy>>;
  where?: Maybe<ArtistStyleBoolExp>;
};


/** query root */
export type QueryRootArtistStyleByPkArgs = {
  artist_id: Scalars['uuid'];
  index: Scalars['Int'];
};


/** query root */
export type QueryRootCollectionArgs = {
  distinct_on?: Maybe<Array<CollectionSelectColumn>>;
  limit?: Maybe<Scalars['Int']>;
  offset?: Maybe<Scalars['Int']>;
  order_by?: Maybe<Array<CollectionOrderBy>>;
  where?: Maybe<CollectionBoolExp>;
};


/** query root */
export type QueryRootCollectionAggregateArgs = {
  distinct_on?: Maybe<Array<CollectionSelectColumn>>;
  limit?: Maybe<Scalars['Int']>;
  offset?: Maybe<Scalars['Int']>;
  order_by?: Maybe<Array<CollectionOrderBy>>;
  where?: Maybe<CollectionBoolExp>;
};


/** query root */
export type QueryRootCollectionByPkArgs = {
  id: Scalars['uuid'];
};


/** query root */
export type QueryRootCountryArgs = {
  distinct_on?: Maybe<Array<CountrySelectColumn>>;
  limit?: Maybe<Scalars['Int']>;
  offset?: Maybe<Scalars['Int']>;
  order_by?: Maybe<Array<CountryOrderBy>>;
  where?: Maybe<CountryBoolExp>;
};


/** query root */
export type QueryRootCountryAggregateArgs = {
  distinct_on?: Maybe<Array<CountrySelectColumn>>;
  limit?: Maybe<Scalars['Int']>;
  offset?: Maybe<Scalars['Int']>;
  order_by?: Maybe<Array<CountryOrderBy>>;
  where?: Maybe<CountryBoolExp>;
};


/** query root */
export type QueryRootCountryByPkArgs = {
  code: Scalars['String'];
};


/** query root */
export type QueryRootEpisodeArgs = {
  distinct_on?: Maybe<Array<EpisodeSelectColumn>>;
  limit?: Maybe<Scalars['Int']>;
  offset?: Maybe<Scalars['Int']>;
  order_by?: Maybe<Array<EpisodeOrderBy>>;
  where?: Maybe<EpisodeBoolExp>;
};


/** query root */
export type QueryRootEpisodeAggregateArgs = {
  distinct_on?: Maybe<Array<EpisodeSelectColumn>>;
  limit?: Maybe<Scalars['Int']>;
  offset?: Maybe<Scalars['Int']>;
  order_by?: Maybe<Array<EpisodeOrderBy>>;
  where?: Maybe<EpisodeBoolExp>;
};


/** query root */
export type QueryRootEpisodeByPkArgs = {
  id: Scalars['uuid'];
};


/** query root */
export type QueryRootEpisodeStaffArgs = {
  distinct_on?: Maybe<Array<EpisodeStaffSelectColumn>>;
  limit?: Maybe<Scalars['Int']>;
  offset?: Maybe<Scalars['Int']>;
  order_by?: Maybe<Array<EpisodeStaffOrderBy>>;
  where?: Maybe<EpisodeStaffBoolExp>;
};


/** query root */
export type QueryRootEpisodeStaffAggregateArgs = {
  distinct_on?: Maybe<Array<EpisodeStaffSelectColumn>>;
  limit?: Maybe<Scalars['Int']>;
  offset?: Maybe<Scalars['Int']>;
  order_by?: Maybe<Array<EpisodeStaffOrderBy>>;
  where?: Maybe<EpisodeStaffBoolExp>;
};


/** query root */
export type QueryRootEpisodeStaffByPkArgs = {
  id: Scalars['uuid'];
};


/** query root */
export type QueryRootGenreArgs = {
  distinct_on?: Maybe<Array<GenreSelectColumn>>;
  limit?: Maybe<Scalars['Int']>;
  offset?: Maybe<Scalars['Int']>;
  order_by?: Maybe<Array<GenreOrderBy>>;
  where?: Maybe<GenreBoolExp>;
};


/** query root */
export type QueryRootGenreAggregateArgs = {
  distinct_on?: Maybe<Array<GenreSelectColumn>>;
  limit?: Maybe<Scalars['Int']>;
  offset?: Maybe<Scalars['Int']>;
  order_by?: Maybe<Array<GenreOrderBy>>;
  where?: Maybe<GenreBoolExp>;
};


/** query root */
export type QueryRootGenreByPkArgs = {
  id: Scalars['uuid'];
};


/** query root */
export type QueryRootMoodArgs = {
  distinct_on?: Maybe<Array<MoodSelectColumn>>;
  limit?: Maybe<Scalars['Int']>;
  offset?: Maybe<Scalars['Int']>;
  order_by?: Maybe<Array<MoodOrderBy>>;
  where?: Maybe<MoodBoolExp>;
};


/** query root */
export type QueryRootMoodAggregateArgs = {
  distinct_on?: Maybe<Array<MoodSelectColumn>>;
  limit?: Maybe<Scalars['Int']>;
  offset?: Maybe<Scalars['Int']>;
  order_by?: Maybe<Array<MoodOrderBy>>;
  where?: Maybe<MoodBoolExp>;
};


/** query root */
export type QueryRootMoodByPkArgs = {
  id: Scalars['uuid'];
};


/** query root */
export type QueryRootMovieArgs = {
  distinct_on?: Maybe<Array<MovieSelectColumn>>;
  limit?: Maybe<Scalars['Int']>;
  offset?: Maybe<Scalars['Int']>;
  order_by?: Maybe<Array<MovieOrderBy>>;
  where?: Maybe<MovieBoolExp>;
};


/** query root */
export type QueryRootMovieAggregateArgs = {
  distinct_on?: Maybe<Array<MovieSelectColumn>>;
  limit?: Maybe<Scalars['Int']>;
  offset?: Maybe<Scalars['Int']>;
  order_by?: Maybe<Array<MovieOrderBy>>;
  where?: Maybe<MovieBoolExp>;
};


/** query root */
export type QueryRootMovieByPkArgs = {
  id: Scalars['uuid'];
};


/** query root */
export type QueryRootMovieCollectionArgs = {
  distinct_on?: Maybe<Array<MovieCollectionSelectColumn>>;
  limit?: Maybe<Scalars['Int']>;
  offset?: Maybe<Scalars['Int']>;
  order_by?: Maybe<Array<MovieCollectionOrderBy>>;
  where?: Maybe<MovieCollectionBoolExp>;
};


/** query root */
export type QueryRootMovieCollectionAggregateArgs = {
  distinct_on?: Maybe<Array<MovieCollectionSelectColumn>>;
  limit?: Maybe<Scalars['Int']>;
  offset?: Maybe<Scalars['Int']>;
  order_by?: Maybe<Array<MovieCollectionOrderBy>>;
  where?: Maybe<MovieCollectionBoolExp>;
};


/** query root */
export type QueryRootMovieCollectionByPkArgs = {
  index: Scalars['Int'];
  movie_id: Scalars['uuid'];
};


/** query root */
export type QueryRootMovieCountryArgs = {
  distinct_on?: Maybe<Array<MovieCountrySelectColumn>>;
  limit?: Maybe<Scalars['Int']>;
  offset?: Maybe<Scalars['Int']>;
  order_by?: Maybe<Array<MovieCountryOrderBy>>;
  where?: Maybe<MovieCountryBoolExp>;
};


/** query root */
export type QueryRootMovieCountryAggregateArgs = {
  distinct_on?: Maybe<Array<MovieCountrySelectColumn>>;
  limit?: Maybe<Scalars['Int']>;
  offset?: Maybe<Scalars['Int']>;
  order_by?: Maybe<Array<MovieCountryOrderBy>>;
  where?: Maybe<MovieCountryBoolExp>;
};


/** query root */
export type QueryRootMovieCountryByPkArgs = {
  index: Scalars['Int'];
  movie_id: Scalars['uuid'];
};


/** query root */
export type QueryRootMovieGenreArgs = {
  distinct_on?: Maybe<Array<MovieGenreSelectColumn>>;
  limit?: Maybe<Scalars['Int']>;
  offset?: Maybe<Scalars['Int']>;
  order_by?: Maybe<Array<MovieGenreOrderBy>>;
  where?: Maybe<MovieGenreBoolExp>;
};


/** query root */
export type QueryRootMovieGenreAggregateArgs = {
  distinct_on?: Maybe<Array<MovieGenreSelectColumn>>;
  limit?: Maybe<Scalars['Int']>;
  offset?: Maybe<Scalars['Int']>;
  order_by?: Maybe<Array<MovieGenreOrderBy>>;
  where?: Maybe<MovieGenreBoolExp>;
};


/** query root */
export type QueryRootMovieGenreByPkArgs = {
  index: Scalars['Int'];
  movie_id: Scalars['uuid'];
};


/** query root */
export type QueryRootMovieStaffArgs = {
  distinct_on?: Maybe<Array<MovieStaffSelectColumn>>;
  limit?: Maybe<Scalars['Int']>;
  offset?: Maybe<Scalars['Int']>;
  order_by?: Maybe<Array<MovieStaffOrderBy>>;
  where?: Maybe<MovieStaffBoolExp>;
};


/** query root */
export type QueryRootMovieStaffAggregateArgs = {
  distinct_on?: Maybe<Array<MovieStaffSelectColumn>>;
  limit?: Maybe<Scalars['Int']>;
  offset?: Maybe<Scalars['Int']>;
  order_by?: Maybe<Array<MovieStaffOrderBy>>;
  where?: Maybe<MovieStaffBoolExp>;
};


/** query root */
export type QueryRootMovieStaffByPkArgs = {
  id: Scalars['uuid'];
};


/** query root */
export type QueryRootMovieStudioArgs = {
  distinct_on?: Maybe<Array<MovieStudioSelectColumn>>;
  limit?: Maybe<Scalars['Int']>;
  offset?: Maybe<Scalars['Int']>;
  order_by?: Maybe<Array<MovieStudioOrderBy>>;
  where?: Maybe<MovieStudioBoolExp>;
};


/** query root */
export type QueryRootMovieStudioAggregateArgs = {
  distinct_on?: Maybe<Array<MovieStudioSelectColumn>>;
  limit?: Maybe<Scalars['Int']>;
  offset?: Maybe<Scalars['Int']>;
  order_by?: Maybe<Array<MovieStudioOrderBy>>;
  where?: Maybe<MovieStudioBoolExp>;
};


/** query root */
export type QueryRootMovieStudioByPkArgs = {
  index: Scalars['Int'];
  movie_id: Scalars['uuid'];
};


/** query root */
export type QueryRootPersonArgs = {
  distinct_on?: Maybe<Array<PersonSelectColumn>>;
  limit?: Maybe<Scalars['Int']>;
  offset?: Maybe<Scalars['Int']>;
  order_by?: Maybe<Array<PersonOrderBy>>;
  where?: Maybe<PersonBoolExp>;
};


/** query root */
export type QueryRootPersonAggregateArgs = {
  distinct_on?: Maybe<Array<PersonSelectColumn>>;
  limit?: Maybe<Scalars['Int']>;
  offset?: Maybe<Scalars['Int']>;
  order_by?: Maybe<Array<PersonOrderBy>>;
  where?: Maybe<PersonBoolExp>;
};


/** query root */
export type QueryRootPersonByPkArgs = {
  id: Scalars['uuid'];
};


/** query root */
export type QueryRootShowArgs = {
  distinct_on?: Maybe<Array<ShowSelectColumn>>;
  limit?: Maybe<Scalars['Int']>;
  offset?: Maybe<Scalars['Int']>;
  order_by?: Maybe<Array<ShowOrderBy>>;
  where?: Maybe<ShowBoolExp>;
};


/** query root */
export type QueryRootShowAggregateArgs = {
  distinct_on?: Maybe<Array<ShowSelectColumn>>;
  limit?: Maybe<Scalars['Int']>;
  offset?: Maybe<Scalars['Int']>;
  order_by?: Maybe<Array<ShowOrderBy>>;
  where?: Maybe<ShowBoolExp>;
};


/** query root */
export type QueryRootShowByPkArgs = {
  id: Scalars['uuid'];
};


/** query root */
export type QueryRootShowCollectionArgs = {
  distinct_on?: Maybe<Array<ShowCollectionSelectColumn>>;
  limit?: Maybe<Scalars['Int']>;
  offset?: Maybe<Scalars['Int']>;
  order_by?: Maybe<Array<ShowCollectionOrderBy>>;
  where?: Maybe<ShowCollectionBoolExp>;
};


/** query root */
export type QueryRootShowCollectionAggregateArgs = {
  distinct_on?: Maybe<Array<ShowCollectionSelectColumn>>;
  limit?: Maybe<Scalars['Int']>;
  offset?: Maybe<Scalars['Int']>;
  order_by?: Maybe<Array<ShowCollectionOrderBy>>;
  where?: Maybe<ShowCollectionBoolExp>;
};


/** query root */
export type QueryRootShowCollectionByPkArgs = {
  index: Scalars['Int'];
  show_id: Scalars['uuid'];
};


/** query root */
export type QueryRootShowGenreArgs = {
  distinct_on?: Maybe<Array<ShowGenreSelectColumn>>;
  limit?: Maybe<Scalars['Int']>;
  offset?: Maybe<Scalars['Int']>;
  order_by?: Maybe<Array<ShowGenreOrderBy>>;
  where?: Maybe<ShowGenreBoolExp>;
};


/** query root */
export type QueryRootShowGenreAggregateArgs = {
  distinct_on?: Maybe<Array<ShowGenreSelectColumn>>;
  limit?: Maybe<Scalars['Int']>;
  offset?: Maybe<Scalars['Int']>;
  order_by?: Maybe<Array<ShowGenreOrderBy>>;
  where?: Maybe<ShowGenreBoolExp>;
};


/** query root */
export type QueryRootShowGenreByPkArgs = {
  index: Scalars['Int'];
  show_id: Scalars['uuid'];
};


/** query root */
export type QueryRootShowSeasonArgs = {
  distinct_on?: Maybe<Array<ShowSeasonSelectColumn>>;
  limit?: Maybe<Scalars['Int']>;
  offset?: Maybe<Scalars['Int']>;
  order_by?: Maybe<Array<ShowSeasonOrderBy>>;
  where?: Maybe<ShowSeasonBoolExp>;
};


/** query root */
export type QueryRootShowSeasonAggregateArgs = {
  distinct_on?: Maybe<Array<ShowSeasonSelectColumn>>;
  limit?: Maybe<Scalars['Int']>;
  offset?: Maybe<Scalars['Int']>;
  order_by?: Maybe<Array<ShowSeasonOrderBy>>;
  where?: Maybe<ShowSeasonBoolExp>;
};


/** query root */
export type QueryRootShowSeasonByPkArgs = {
  id: Scalars['uuid'];
};


/** query root */
export type QueryRootShowStaffArgs = {
  distinct_on?: Maybe<Array<ShowStaffSelectColumn>>;
  limit?: Maybe<Scalars['Int']>;
  offset?: Maybe<Scalars['Int']>;
  order_by?: Maybe<Array<ShowStaffOrderBy>>;
  where?: Maybe<ShowStaffBoolExp>;
};


/** query root */
export type QueryRootShowStaffAggregateArgs = {
  distinct_on?: Maybe<Array<ShowStaffSelectColumn>>;
  limit?: Maybe<Scalars['Int']>;
  offset?: Maybe<Scalars['Int']>;
  order_by?: Maybe<Array<ShowStaffOrderBy>>;
  where?: Maybe<ShowStaffBoolExp>;
};


/** query root */
export type QueryRootShowStaffByPkArgs = {
  id: Scalars['uuid'];
};


/** query root */
export type QueryRootStaffArgs = {
  distinct_on?: Maybe<Array<StaffSelectColumn>>;
  limit?: Maybe<Scalars['Int']>;
  offset?: Maybe<Scalars['Int']>;
  order_by?: Maybe<Array<StaffOrderBy>>;
  where?: Maybe<StaffBoolExp>;
};


/** query root */
export type QueryRootStaffAggregateArgs = {
  distinct_on?: Maybe<Array<StaffSelectColumn>>;
  limit?: Maybe<Scalars['Int']>;
  offset?: Maybe<Scalars['Int']>;
  order_by?: Maybe<Array<StaffOrderBy>>;
  where?: Maybe<StaffBoolExp>;
};


/** query root */
export type QueryRootStaffByPkArgs = {
  id: Scalars['uuid'];
};


/** query root */
export type QueryRootStudioArgs = {
  distinct_on?: Maybe<Array<StudioSelectColumn>>;
  limit?: Maybe<Scalars['Int']>;
  offset?: Maybe<Scalars['Int']>;
  order_by?: Maybe<Array<StudioOrderBy>>;
  where?: Maybe<StudioBoolExp>;
};


/** query root */
export type QueryRootStudioAggregateArgs = {
  distinct_on?: Maybe<Array<StudioSelectColumn>>;
  limit?: Maybe<Scalars['Int']>;
  offset?: Maybe<Scalars['Int']>;
  order_by?: Maybe<Array<StudioOrderBy>>;
  where?: Maybe<StudioBoolExp>;
};


/** query root */
export type QueryRootStudioByPkArgs = {
  id: Scalars['uuid'];
};


/** query root */
export type QueryRootStyleArgs = {
  distinct_on?: Maybe<Array<StyleSelectColumn>>;
  limit?: Maybe<Scalars['Int']>;
  offset?: Maybe<Scalars['Int']>;
  order_by?: Maybe<Array<StyleOrderBy>>;
  where?: Maybe<StyleBoolExp>;
};


/** query root */
export type QueryRootStyleAggregateArgs = {
  distinct_on?: Maybe<Array<StyleSelectColumn>>;
  limit?: Maybe<Scalars['Int']>;
  offset?: Maybe<Scalars['Int']>;
  order_by?: Maybe<Array<StyleOrderBy>>;
  where?: Maybe<StyleBoolExp>;
};


/** query root */
export type QueryRootStyleByPkArgs = {
  id: Scalars['uuid'];
};


/** query root */
export type QueryRootTrackArgs = {
  distinct_on?: Maybe<Array<TrackSelectColumn>>;
  limit?: Maybe<Scalars['Int']>;
  offset?: Maybe<Scalars['Int']>;
  order_by?: Maybe<Array<TrackOrderBy>>;
  where?: Maybe<TrackBoolExp>;
};


/** query root */
export type QueryRootTrackAggregateArgs = {
  distinct_on?: Maybe<Array<TrackSelectColumn>>;
  limit?: Maybe<Scalars['Int']>;
  offset?: Maybe<Scalars['Int']>;
  order_by?: Maybe<Array<TrackOrderBy>>;
  where?: Maybe<TrackBoolExp>;
};


/** query root */
export type QueryRootTrackByPkArgs = {
  id: Scalars['uuid'];
};


/** query root */
export type QueryRootTrackGenreArgs = {
  distinct_on?: Maybe<Array<TrackGenreSelectColumn>>;
  limit?: Maybe<Scalars['Int']>;
  offset?: Maybe<Scalars['Int']>;
  order_by?: Maybe<Array<TrackGenreOrderBy>>;
  where?: Maybe<TrackGenreBoolExp>;
};


/** query root */
export type QueryRootTrackGenreAggregateArgs = {
  distinct_on?: Maybe<Array<TrackGenreSelectColumn>>;
  limit?: Maybe<Scalars['Int']>;
  offset?: Maybe<Scalars['Int']>;
  order_by?: Maybe<Array<TrackGenreOrderBy>>;
  where?: Maybe<TrackGenreBoolExp>;
};


/** query root */
export type QueryRootTrackGenreByPkArgs = {
  index: Scalars['Int'];
  track_id: Scalars['uuid'];
};


/** query root */
export type QueryRootTrackMoodArgs = {
  distinct_on?: Maybe<Array<TrackMoodSelectColumn>>;
  limit?: Maybe<Scalars['Int']>;
  offset?: Maybe<Scalars['Int']>;
  order_by?: Maybe<Array<TrackMoodOrderBy>>;
  where?: Maybe<TrackMoodBoolExp>;
};


/** query root */
export type QueryRootTrackMoodAggregateArgs = {
  distinct_on?: Maybe<Array<TrackMoodSelectColumn>>;
  limit?: Maybe<Scalars['Int']>;
  offset?: Maybe<Scalars['Int']>;
  order_by?: Maybe<Array<TrackMoodOrderBy>>;
  where?: Maybe<TrackMoodBoolExp>;
};


/** query root */
export type QueryRootTrackMoodByPkArgs = {
  index: Scalars['Int'];
  track_id: Scalars['uuid'];
};

/** columns and relationships of "show" */
export type Show = {
  aired?: Maybe<Scalars['date']>;
  content_rating: Scalars['String'];
  id: Scalars['uuid'];
  image_background?: Maybe<Scalars['String']>;
  image_cover?: Maybe<Scalars['String']>;
  name: Scalars['String'];
  name_match: Scalars['String'];
  name_original?: Maybe<Scalars['_text']>;
  name_sort?: Maybe<Scalars['String']>;
  rating?: Maybe<Scalars['numeric']>;
  /** An array relationship */
  show_collections: Array<ShowCollection>;
  /** An aggregated array relationship */
  show_collections_aggregate: ShowCollectionAggregate;
  /** An array relationship */
  show_genres: Array<ShowGenre>;
  /** An aggregated array relationship */
  show_genres_aggregate: ShowGenreAggregate;
  /** An array relationship */
  show_seasons: Array<ShowSeason>;
  /** An aggregated array relationship */
  show_seasons_aggregate: ShowSeasonAggregate;
  /** An array relationship */
  show_staffs: Array<ShowStaff>;
  /** An aggregated array relationship */
  show_staffs_aggregate: ShowStaffAggregate;
  summary: Scalars['String'];
  tagline: Scalars['String'];
};


/** columns and relationships of "show" */
export type ShowShowCollectionsArgs = {
  distinct_on?: Maybe<Array<ShowCollectionSelectColumn>>;
  limit?: Maybe<Scalars['Int']>;
  offset?: Maybe<Scalars['Int']>;
  order_by?: Maybe<Array<ShowCollectionOrderBy>>;
  where?: Maybe<ShowCollectionBoolExp>;
};


/** columns and relationships of "show" */
export type ShowShowCollectionsAggregateArgs = {
  distinct_on?: Maybe<Array<ShowCollectionSelectColumn>>;
  limit?: Maybe<Scalars['Int']>;
  offset?: Maybe<Scalars['Int']>;
  order_by?: Maybe<Array<ShowCollectionOrderBy>>;
  where?: Maybe<ShowCollectionBoolExp>;
};


/** columns and relationships of "show" */
export type ShowShowGenresArgs = {
  distinct_on?: Maybe<Array<ShowGenreSelectColumn>>;
  limit?: Maybe<Scalars['Int']>;
  offset?: Maybe<Scalars['Int']>;
  order_by?: Maybe<Array<ShowGenreOrderBy>>;
  where?: Maybe<ShowGenreBoolExp>;
};


/** columns and relationships of "show" */
export type ShowShowGenresAggregateArgs = {
  distinct_on?: Maybe<Array<ShowGenreSelectColumn>>;
  limit?: Maybe<Scalars['Int']>;
  offset?: Maybe<Scalars['Int']>;
  order_by?: Maybe<Array<ShowGenreOrderBy>>;
  where?: Maybe<ShowGenreBoolExp>;
};


/** columns and relationships of "show" */
export type ShowShowSeasonsArgs = {
  distinct_on?: Maybe<Array<ShowSeasonSelectColumn>>;
  limit?: Maybe<Scalars['Int']>;
  offset?: Maybe<Scalars['Int']>;
  order_by?: Maybe<Array<ShowSeasonOrderBy>>;
  where?: Maybe<ShowSeasonBoolExp>;
};


/** columns and relationships of "show" */
export type ShowShowSeasonsAggregateArgs = {
  distinct_on?: Maybe<Array<ShowSeasonSelectColumn>>;
  limit?: Maybe<Scalars['Int']>;
  offset?: Maybe<Scalars['Int']>;
  order_by?: Maybe<Array<ShowSeasonOrderBy>>;
  where?: Maybe<ShowSeasonBoolExp>;
};


/** columns and relationships of "show" */
export type ShowShowStaffsArgs = {
  distinct_on?: Maybe<Array<ShowStaffSelectColumn>>;
  limit?: Maybe<Scalars['Int']>;
  offset?: Maybe<Scalars['Int']>;
  order_by?: Maybe<Array<ShowStaffOrderBy>>;
  where?: Maybe<ShowStaffBoolExp>;
};


/** columns and relationships of "show" */
export type ShowShowStaffsAggregateArgs = {
  distinct_on?: Maybe<Array<ShowStaffSelectColumn>>;
  limit?: Maybe<Scalars['Int']>;
  offset?: Maybe<Scalars['Int']>;
  order_by?: Maybe<Array<ShowStaffOrderBy>>;
  where?: Maybe<ShowStaffBoolExp>;
};

/** aggregated selection of "show" */
export type ShowAggregate = {
  aggregate?: Maybe<ShowAggregateFields>;
  nodes: Array<Show>;
};

/** aggregate fields of "show" */
export type ShowAggregateFields = {
  avg?: Maybe<ShowAvgFields>;
  count?: Maybe<Scalars['Int']>;
  max?: Maybe<ShowMaxFields>;
  min?: Maybe<ShowMinFields>;
  stddev?: Maybe<ShowStddevFields>;
  stddev_pop?: Maybe<ShowStddevPopFields>;
  stddev_samp?: Maybe<ShowStddevSampFields>;
  sum?: Maybe<ShowSumFields>;
  var_pop?: Maybe<ShowVarPopFields>;
  var_samp?: Maybe<ShowVarSampFields>;
  variance?: Maybe<ShowVarianceFields>;
};


/** aggregate fields of "show" */
export type ShowAggregateFieldsCountArgs = {
  columns?: Maybe<Array<ShowSelectColumn>>;
  distinct?: Maybe<Scalars['Boolean']>;
};

/** order by aggregate values of table "show" */
export type ShowAggregateOrderBy = {
  avg?: Maybe<ShowAvgOrderBy>;
  count?: Maybe<OrderBy>;
  max?: Maybe<ShowMaxOrderBy>;
  min?: Maybe<ShowMinOrderBy>;
  stddev?: Maybe<ShowStddevOrderBy>;
  stddev_pop?: Maybe<ShowStddevPopOrderBy>;
  stddev_samp?: Maybe<ShowStddevSampOrderBy>;
  sum?: Maybe<ShowSumOrderBy>;
  var_pop?: Maybe<ShowVarPopOrderBy>;
  var_samp?: Maybe<ShowVarSampOrderBy>;
  variance?: Maybe<ShowVarianceOrderBy>;
};

/** input type for inserting array relation for remote table "show" */
export type ShowArrRelInsertInput = {
  data: Array<ShowInsertInput>;
  on_conflict?: Maybe<ShowOnConflict>;
};

/** aggregate avg on columns */
export type ShowAvgFields = {
  rating?: Maybe<Scalars['Float']>;
};

/** order by avg() on columns of table "show" */
export type ShowAvgOrderBy = {
  rating?: Maybe<OrderBy>;
};

/** Boolean expression to filter rows from the table "show". All fields are combined with a logical 'AND'. */
export type ShowBoolExp = {
  _and?: Maybe<Array<Maybe<ShowBoolExp>>>;
  _not?: Maybe<ShowBoolExp>;
  _or?: Maybe<Array<Maybe<ShowBoolExp>>>;
  aired?: Maybe<DateComparisonExp>;
  content_rating?: Maybe<StringComparisonExp>;
  id?: Maybe<UuidComparisonExp>;
  image_background?: Maybe<StringComparisonExp>;
  image_cover?: Maybe<StringComparisonExp>;
  name?: Maybe<StringComparisonExp>;
  name_match?: Maybe<StringComparisonExp>;
  name_original?: Maybe<TextComparisonExp>;
  name_sort?: Maybe<StringComparisonExp>;
  rating?: Maybe<NumericComparisonExp>;
  show_collections?: Maybe<ShowCollectionBoolExp>;
  show_genres?: Maybe<ShowGenreBoolExp>;
  show_seasons?: Maybe<ShowSeasonBoolExp>;
  show_staffs?: Maybe<ShowStaffBoolExp>;
  summary?: Maybe<StringComparisonExp>;
  tagline?: Maybe<StringComparisonExp>;
};

/** columns and relationships of "show_collection" */
export type ShowCollection = {
  /** An object relationship */
  collection: Collection;
  collection_id: Scalars['uuid'];
  index: Scalars['Int'];
  /** An object relationship */
  show: Show;
  show_id: Scalars['uuid'];
};

/** aggregated selection of "show_collection" */
export type ShowCollectionAggregate = {
  aggregate?: Maybe<ShowCollectionAggregateFields>;
  nodes: Array<ShowCollection>;
};

/** aggregate fields of "show_collection" */
export type ShowCollectionAggregateFields = {
  avg?: Maybe<ShowCollectionAvgFields>;
  count?: Maybe<Scalars['Int']>;
  max?: Maybe<ShowCollectionMaxFields>;
  min?: Maybe<ShowCollectionMinFields>;
  stddev?: Maybe<ShowCollectionStddevFields>;
  stddev_pop?: Maybe<ShowCollectionStddevPopFields>;
  stddev_samp?: Maybe<ShowCollectionStddevSampFields>;
  sum?: Maybe<ShowCollectionSumFields>;
  var_pop?: Maybe<ShowCollectionVarPopFields>;
  var_samp?: Maybe<ShowCollectionVarSampFields>;
  variance?: Maybe<ShowCollectionVarianceFields>;
};


/** aggregate fields of "show_collection" */
export type ShowCollectionAggregateFieldsCountArgs = {
  columns?: Maybe<Array<ShowCollectionSelectColumn>>;
  distinct?: Maybe<Scalars['Boolean']>;
};

/** order by aggregate values of table "show_collection" */
export type ShowCollectionAggregateOrderBy = {
  avg?: Maybe<ShowCollectionAvgOrderBy>;
  count?: Maybe<OrderBy>;
  max?: Maybe<ShowCollectionMaxOrderBy>;
  min?: Maybe<ShowCollectionMinOrderBy>;
  stddev?: Maybe<ShowCollectionStddevOrderBy>;
  stddev_pop?: Maybe<ShowCollectionStddevPopOrderBy>;
  stddev_samp?: Maybe<ShowCollectionStddevSampOrderBy>;
  sum?: Maybe<ShowCollectionSumOrderBy>;
  var_pop?: Maybe<ShowCollectionVarPopOrderBy>;
  var_samp?: Maybe<ShowCollectionVarSampOrderBy>;
  variance?: Maybe<ShowCollectionVarianceOrderBy>;
};

/** input type for inserting array relation for remote table "show_collection" */
export type ShowCollectionArrRelInsertInput = {
  data: Array<ShowCollectionInsertInput>;
  on_conflict?: Maybe<ShowCollectionOnConflict>;
};

/** aggregate avg on columns */
export type ShowCollectionAvgFields = {
  index?: Maybe<Scalars['Float']>;
};

/** order by avg() on columns of table "show_collection" */
export type ShowCollectionAvgOrderBy = {
  index?: Maybe<OrderBy>;
};

/** Boolean expression to filter rows from the table "show_collection". All fields are combined with a logical 'AND'. */
export type ShowCollectionBoolExp = {
  _and?: Maybe<Array<Maybe<ShowCollectionBoolExp>>>;
  _not?: Maybe<ShowCollectionBoolExp>;
  _or?: Maybe<Array<Maybe<ShowCollectionBoolExp>>>;
  collection?: Maybe<CollectionBoolExp>;
  collection_id?: Maybe<UuidComparisonExp>;
  index?: Maybe<IntComparisonExp>;
  show?: Maybe<ShowBoolExp>;
  show_id?: Maybe<UuidComparisonExp>;
};

/** unique or primary key constraints on table "show_collection" */
export enum ShowCollectionConstraint {
  /** unique or primary key constraint */
  ShowCollectionPkey = 'show_collection_pkey',
  /** unique or primary key constraint */
  ShowCollectionShowIdCollectionIdKey = 'show_collection_show_id_collection_id_key'
}

/** input type for incrementing integer column in table "show_collection" */
export type ShowCollectionIncInput = {
  index?: Maybe<Scalars['Int']>;
};

/** input type for inserting data into table "show_collection" */
export type ShowCollectionInsertInput = {
  collection?: Maybe<CollectionObjRelInsertInput>;
  collection_id?: Maybe<Scalars['uuid']>;
  index?: Maybe<Scalars['Int']>;
  show?: Maybe<ShowObjRelInsertInput>;
  show_id?: Maybe<Scalars['uuid']>;
};

/** aggregate max on columns */
export type ShowCollectionMaxFields = {
  collection_id?: Maybe<Scalars['uuid']>;
  index?: Maybe<Scalars['Int']>;
  show_id?: Maybe<Scalars['uuid']>;
};

/** order by max() on columns of table "show_collection" */
export type ShowCollectionMaxOrderBy = {
  collection_id?: Maybe<OrderBy>;
  index?: Maybe<OrderBy>;
  show_id?: Maybe<OrderBy>;
};

/** aggregate min on columns */
export type ShowCollectionMinFields = {
  collection_id?: Maybe<Scalars['uuid']>;
  index?: Maybe<Scalars['Int']>;
  show_id?: Maybe<Scalars['uuid']>;
};

/** order by min() on columns of table "show_collection" */
export type ShowCollectionMinOrderBy = {
  collection_id?: Maybe<OrderBy>;
  index?: Maybe<OrderBy>;
  show_id?: Maybe<OrderBy>;
};

/** response of any mutation on the table "show_collection" */
export type ShowCollectionMutationResponse = {
  /** number of affected rows by the mutation */
  affected_rows: Scalars['Int'];
  /** data of the affected rows by the mutation */
  returning: Array<ShowCollection>;
};

/** input type for inserting object relation for remote table "show_collection" */
export type ShowCollectionObjRelInsertInput = {
  data: ShowCollectionInsertInput;
  on_conflict?: Maybe<ShowCollectionOnConflict>;
};

/** on conflict condition type for table "show_collection" */
export type ShowCollectionOnConflict = {
  constraint: ShowCollectionConstraint;
  update_columns: Array<ShowCollectionUpdateColumn>;
  where?: Maybe<ShowCollectionBoolExp>;
};

/** ordering options when selecting data from "show_collection" */
export type ShowCollectionOrderBy = {
  collection?: Maybe<CollectionOrderBy>;
  collection_id?: Maybe<OrderBy>;
  index?: Maybe<OrderBy>;
  show?: Maybe<ShowOrderBy>;
  show_id?: Maybe<OrderBy>;
};

/** primary key columns input for table: "show_collection" */
export type ShowCollectionPkColumnsInput = {
  index: Scalars['Int'];
  show_id: Scalars['uuid'];
};

/** select columns of table "show_collection" */
export enum ShowCollectionSelectColumn {
  /** column name */
  CollectionId = 'collection_id',
  /** column name */
  Index = 'index',
  /** column name */
  ShowId = 'show_id'
}

/** input type for updating data in table "show_collection" */
export type ShowCollectionSetInput = {
  collection_id?: Maybe<Scalars['uuid']>;
  index?: Maybe<Scalars['Int']>;
  show_id?: Maybe<Scalars['uuid']>;
};

/** aggregate stddev on columns */
export type ShowCollectionStddevFields = {
  index?: Maybe<Scalars['Float']>;
};

/** order by stddev() on columns of table "show_collection" */
export type ShowCollectionStddevOrderBy = {
  index?: Maybe<OrderBy>;
};

/** aggregate stddev_pop on columns */
export type ShowCollectionStddevPopFields = {
  index?: Maybe<Scalars['Float']>;
};

/** order by stddev_pop() on columns of table "show_collection" */
export type ShowCollectionStddevPopOrderBy = {
  index?: Maybe<OrderBy>;
};

/** aggregate stddev_samp on columns */
export type ShowCollectionStddevSampFields = {
  index?: Maybe<Scalars['Float']>;
};

/** order by stddev_samp() on columns of table "show_collection" */
export type ShowCollectionStddevSampOrderBy = {
  index?: Maybe<OrderBy>;
};

/** aggregate sum on columns */
export type ShowCollectionSumFields = {
  index?: Maybe<Scalars['Int']>;
};

/** order by sum() on columns of table "show_collection" */
export type ShowCollectionSumOrderBy = {
  index?: Maybe<OrderBy>;
};

/** update columns of table "show_collection" */
export enum ShowCollectionUpdateColumn {
  /** column name */
  CollectionId = 'collection_id',
  /** column name */
  Index = 'index',
  /** column name */
  ShowId = 'show_id'
}

/** aggregate var_pop on columns */
export type ShowCollectionVarPopFields = {
  index?: Maybe<Scalars['Float']>;
};

/** order by var_pop() on columns of table "show_collection" */
export type ShowCollectionVarPopOrderBy = {
  index?: Maybe<OrderBy>;
};

/** aggregate var_samp on columns */
export type ShowCollectionVarSampFields = {
  index?: Maybe<Scalars['Float']>;
};

/** order by var_samp() on columns of table "show_collection" */
export type ShowCollectionVarSampOrderBy = {
  index?: Maybe<OrderBy>;
};

/** aggregate variance on columns */
export type ShowCollectionVarianceFields = {
  index?: Maybe<Scalars['Float']>;
};

/** order by variance() on columns of table "show_collection" */
export type ShowCollectionVarianceOrderBy = {
  index?: Maybe<OrderBy>;
};

/** unique or primary key constraints on table "show" */
export enum ShowConstraint {
  /** unique or primary key constraint */
  ShowNameMatchKey = 'show_name_match_key',
  /** unique or primary key constraint */
  ShowPkey = 'show_pkey'
}

/** columns and relationships of "show_genre" */
export type ShowGenre = {
  /** An object relationship */
  genre: Genre;
  genre_id: Scalars['uuid'];
  index: Scalars['Int'];
  /** An object relationship */
  show: Show;
  show_id: Scalars['uuid'];
};

/** aggregated selection of "show_genre" */
export type ShowGenreAggregate = {
  aggregate?: Maybe<ShowGenreAggregateFields>;
  nodes: Array<ShowGenre>;
};

/** aggregate fields of "show_genre" */
export type ShowGenreAggregateFields = {
  avg?: Maybe<ShowGenreAvgFields>;
  count?: Maybe<Scalars['Int']>;
  max?: Maybe<ShowGenreMaxFields>;
  min?: Maybe<ShowGenreMinFields>;
  stddev?: Maybe<ShowGenreStddevFields>;
  stddev_pop?: Maybe<ShowGenreStddevPopFields>;
  stddev_samp?: Maybe<ShowGenreStddevSampFields>;
  sum?: Maybe<ShowGenreSumFields>;
  var_pop?: Maybe<ShowGenreVarPopFields>;
  var_samp?: Maybe<ShowGenreVarSampFields>;
  variance?: Maybe<ShowGenreVarianceFields>;
};


/** aggregate fields of "show_genre" */
export type ShowGenreAggregateFieldsCountArgs = {
  columns?: Maybe<Array<ShowGenreSelectColumn>>;
  distinct?: Maybe<Scalars['Boolean']>;
};

/** order by aggregate values of table "show_genre" */
export type ShowGenreAggregateOrderBy = {
  avg?: Maybe<ShowGenreAvgOrderBy>;
  count?: Maybe<OrderBy>;
  max?: Maybe<ShowGenreMaxOrderBy>;
  min?: Maybe<ShowGenreMinOrderBy>;
  stddev?: Maybe<ShowGenreStddevOrderBy>;
  stddev_pop?: Maybe<ShowGenreStddevPopOrderBy>;
  stddev_samp?: Maybe<ShowGenreStddevSampOrderBy>;
  sum?: Maybe<ShowGenreSumOrderBy>;
  var_pop?: Maybe<ShowGenreVarPopOrderBy>;
  var_samp?: Maybe<ShowGenreVarSampOrderBy>;
  variance?: Maybe<ShowGenreVarianceOrderBy>;
};

/** input type for inserting array relation for remote table "show_genre" */
export type ShowGenreArrRelInsertInput = {
  data: Array<ShowGenreInsertInput>;
  on_conflict?: Maybe<ShowGenreOnConflict>;
};

/** aggregate avg on columns */
export type ShowGenreAvgFields = {
  index?: Maybe<Scalars['Float']>;
};

/** order by avg() on columns of table "show_genre" */
export type ShowGenreAvgOrderBy = {
  index?: Maybe<OrderBy>;
};

/** Boolean expression to filter rows from the table "show_genre". All fields are combined with a logical 'AND'. */
export type ShowGenreBoolExp = {
  _and?: Maybe<Array<Maybe<ShowGenreBoolExp>>>;
  _not?: Maybe<ShowGenreBoolExp>;
  _or?: Maybe<Array<Maybe<ShowGenreBoolExp>>>;
  genre?: Maybe<GenreBoolExp>;
  genre_id?: Maybe<UuidComparisonExp>;
  index?: Maybe<IntComparisonExp>;
  show?: Maybe<ShowBoolExp>;
  show_id?: Maybe<UuidComparisonExp>;
};

/** unique or primary key constraints on table "show_genre" */
export enum ShowGenreConstraint {
  /** unique or primary key constraint */
  ShowGenrePkey = 'show_genre_pkey',
  /** unique or primary key constraint */
  ShowGenreShowIdGenreIdKey = 'show_genre_show_id_genre_id_key'
}

/** input type for incrementing integer column in table "show_genre" */
export type ShowGenreIncInput = {
  index?: Maybe<Scalars['Int']>;
};

/** input type for inserting data into table "show_genre" */
export type ShowGenreInsertInput = {
  genre?: Maybe<GenreObjRelInsertInput>;
  genre_id?: Maybe<Scalars['uuid']>;
  index?: Maybe<Scalars['Int']>;
  show?: Maybe<ShowObjRelInsertInput>;
  show_id?: Maybe<Scalars['uuid']>;
};

/** aggregate max on columns */
export type ShowGenreMaxFields = {
  genre_id?: Maybe<Scalars['uuid']>;
  index?: Maybe<Scalars['Int']>;
  show_id?: Maybe<Scalars['uuid']>;
};

/** order by max() on columns of table "show_genre" */
export type ShowGenreMaxOrderBy = {
  genre_id?: Maybe<OrderBy>;
  index?: Maybe<OrderBy>;
  show_id?: Maybe<OrderBy>;
};

/** aggregate min on columns */
export type ShowGenreMinFields = {
  genre_id?: Maybe<Scalars['uuid']>;
  index?: Maybe<Scalars['Int']>;
  show_id?: Maybe<Scalars['uuid']>;
};

/** order by min() on columns of table "show_genre" */
export type ShowGenreMinOrderBy = {
  genre_id?: Maybe<OrderBy>;
  index?: Maybe<OrderBy>;
  show_id?: Maybe<OrderBy>;
};

/** response of any mutation on the table "show_genre" */
export type ShowGenreMutationResponse = {
  /** number of affected rows by the mutation */
  affected_rows: Scalars['Int'];
  /** data of the affected rows by the mutation */
  returning: Array<ShowGenre>;
};

/** input type for inserting object relation for remote table "show_genre" */
export type ShowGenreObjRelInsertInput = {
  data: ShowGenreInsertInput;
  on_conflict?: Maybe<ShowGenreOnConflict>;
};

/** on conflict condition type for table "show_genre" */
export type ShowGenreOnConflict = {
  constraint: ShowGenreConstraint;
  update_columns: Array<ShowGenreUpdateColumn>;
  where?: Maybe<ShowGenreBoolExp>;
};

/** ordering options when selecting data from "show_genre" */
export type ShowGenreOrderBy = {
  genre?: Maybe<GenreOrderBy>;
  genre_id?: Maybe<OrderBy>;
  index?: Maybe<OrderBy>;
  show?: Maybe<ShowOrderBy>;
  show_id?: Maybe<OrderBy>;
};

/** primary key columns input for table: "show_genre" */
export type ShowGenrePkColumnsInput = {
  index: Scalars['Int'];
  show_id: Scalars['uuid'];
};

/** select columns of table "show_genre" */
export enum ShowGenreSelectColumn {
  /** column name */
  GenreId = 'genre_id',
  /** column name */
  Index = 'index',
  /** column name */
  ShowId = 'show_id'
}

/** input type for updating data in table "show_genre" */
export type ShowGenreSetInput = {
  genre_id?: Maybe<Scalars['uuid']>;
  index?: Maybe<Scalars['Int']>;
  show_id?: Maybe<Scalars['uuid']>;
};

/** aggregate stddev on columns */
export type ShowGenreStddevFields = {
  index?: Maybe<Scalars['Float']>;
};

/** order by stddev() on columns of table "show_genre" */
export type ShowGenreStddevOrderBy = {
  index?: Maybe<OrderBy>;
};

/** aggregate stddev_pop on columns */
export type ShowGenreStddevPopFields = {
  index?: Maybe<Scalars['Float']>;
};

/** order by stddev_pop() on columns of table "show_genre" */
export type ShowGenreStddevPopOrderBy = {
  index?: Maybe<OrderBy>;
};

/** aggregate stddev_samp on columns */
export type ShowGenreStddevSampFields = {
  index?: Maybe<Scalars['Float']>;
};

/** order by stddev_samp() on columns of table "show_genre" */
export type ShowGenreStddevSampOrderBy = {
  index?: Maybe<OrderBy>;
};

/** aggregate sum on columns */
export type ShowGenreSumFields = {
  index?: Maybe<Scalars['Int']>;
};

/** order by sum() on columns of table "show_genre" */
export type ShowGenreSumOrderBy = {
  index?: Maybe<OrderBy>;
};

/** update columns of table "show_genre" */
export enum ShowGenreUpdateColumn {
  /** column name */
  GenreId = 'genre_id',
  /** column name */
  Index = 'index',
  /** column name */
  ShowId = 'show_id'
}

/** aggregate var_pop on columns */
export type ShowGenreVarPopFields = {
  index?: Maybe<Scalars['Float']>;
};

/** order by var_pop() on columns of table "show_genre" */
export type ShowGenreVarPopOrderBy = {
  index?: Maybe<OrderBy>;
};

/** aggregate var_samp on columns */
export type ShowGenreVarSampFields = {
  index?: Maybe<Scalars['Float']>;
};

/** order by var_samp() on columns of table "show_genre" */
export type ShowGenreVarSampOrderBy = {
  index?: Maybe<OrderBy>;
};

/** aggregate variance on columns */
export type ShowGenreVarianceFields = {
  index?: Maybe<Scalars['Float']>;
};

/** order by variance() on columns of table "show_genre" */
export type ShowGenreVarianceOrderBy = {
  index?: Maybe<OrderBy>;
};

/** input type for incrementing integer column in table "show" */
export type ShowIncInput = {
  rating?: Maybe<Scalars['numeric']>;
};

/** input type for inserting data into table "show" */
export type ShowInsertInput = {
  aired?: Maybe<Scalars['date']>;
  content_rating?: Maybe<Scalars['String']>;
  id?: Maybe<Scalars['uuid']>;
  image_background?: Maybe<Scalars['String']>;
  image_cover?: Maybe<Scalars['String']>;
  name?: Maybe<Scalars['String']>;
  name_match?: Maybe<Scalars['String']>;
  name_original?: Maybe<Scalars['_text']>;
  name_sort?: Maybe<Scalars['String']>;
  rating?: Maybe<Scalars['numeric']>;
  show_collections?: Maybe<ShowCollectionArrRelInsertInput>;
  show_genres?: Maybe<ShowGenreArrRelInsertInput>;
  show_seasons?: Maybe<ShowSeasonArrRelInsertInput>;
  show_staffs?: Maybe<ShowStaffArrRelInsertInput>;
  summary?: Maybe<Scalars['String']>;
  tagline?: Maybe<Scalars['String']>;
};

/** aggregate max on columns */
export type ShowMaxFields = {
  aired?: Maybe<Scalars['date']>;
  content_rating?: Maybe<Scalars['String']>;
  id?: Maybe<Scalars['uuid']>;
  image_background?: Maybe<Scalars['String']>;
  image_cover?: Maybe<Scalars['String']>;
  name?: Maybe<Scalars['String']>;
  name_match?: Maybe<Scalars['String']>;
  name_sort?: Maybe<Scalars['String']>;
  rating?: Maybe<Scalars['numeric']>;
  summary?: Maybe<Scalars['String']>;
  tagline?: Maybe<Scalars['String']>;
};

/** order by max() on columns of table "show" */
export type ShowMaxOrderBy = {
  aired?: Maybe<OrderBy>;
  content_rating?: Maybe<OrderBy>;
  id?: Maybe<OrderBy>;
  image_background?: Maybe<OrderBy>;
  image_cover?: Maybe<OrderBy>;
  name?: Maybe<OrderBy>;
  name_match?: Maybe<OrderBy>;
  name_sort?: Maybe<OrderBy>;
  rating?: Maybe<OrderBy>;
  summary?: Maybe<OrderBy>;
  tagline?: Maybe<OrderBy>;
};

/** aggregate min on columns */
export type ShowMinFields = {
  aired?: Maybe<Scalars['date']>;
  content_rating?: Maybe<Scalars['String']>;
  id?: Maybe<Scalars['uuid']>;
  image_background?: Maybe<Scalars['String']>;
  image_cover?: Maybe<Scalars['String']>;
  name?: Maybe<Scalars['String']>;
  name_match?: Maybe<Scalars['String']>;
  name_sort?: Maybe<Scalars['String']>;
  rating?: Maybe<Scalars['numeric']>;
  summary?: Maybe<Scalars['String']>;
  tagline?: Maybe<Scalars['String']>;
};

/** order by min() on columns of table "show" */
export type ShowMinOrderBy = {
  aired?: Maybe<OrderBy>;
  content_rating?: Maybe<OrderBy>;
  id?: Maybe<OrderBy>;
  image_background?: Maybe<OrderBy>;
  image_cover?: Maybe<OrderBy>;
  name?: Maybe<OrderBy>;
  name_match?: Maybe<OrderBy>;
  name_sort?: Maybe<OrderBy>;
  rating?: Maybe<OrderBy>;
  summary?: Maybe<OrderBy>;
  tagline?: Maybe<OrderBy>;
};

/** response of any mutation on the table "show" */
export type ShowMutationResponse = {
  /** number of affected rows by the mutation */
  affected_rows: Scalars['Int'];
  /** data of the affected rows by the mutation */
  returning: Array<Show>;
};

/** input type for inserting object relation for remote table "show" */
export type ShowObjRelInsertInput = {
  data: ShowInsertInput;
  on_conflict?: Maybe<ShowOnConflict>;
};

/** on conflict condition type for table "show" */
export type ShowOnConflict = {
  constraint: ShowConstraint;
  update_columns: Array<ShowUpdateColumn>;
  where?: Maybe<ShowBoolExp>;
};

/** ordering options when selecting data from "show" */
export type ShowOrderBy = {
  aired?: Maybe<OrderBy>;
  content_rating?: Maybe<OrderBy>;
  id?: Maybe<OrderBy>;
  image_background?: Maybe<OrderBy>;
  image_cover?: Maybe<OrderBy>;
  name?: Maybe<OrderBy>;
  name_match?: Maybe<OrderBy>;
  name_original?: Maybe<OrderBy>;
  name_sort?: Maybe<OrderBy>;
  rating?: Maybe<OrderBy>;
  show_collections_aggregate?: Maybe<ShowCollectionAggregateOrderBy>;
  show_genres_aggregate?: Maybe<ShowGenreAggregateOrderBy>;
  show_seasons_aggregate?: Maybe<ShowSeasonAggregateOrderBy>;
  show_staffs_aggregate?: Maybe<ShowStaffAggregateOrderBy>;
  summary?: Maybe<OrderBy>;
  tagline?: Maybe<OrderBy>;
};

/** primary key columns input for table: "show" */
export type ShowPkColumnsInput = {
  id: Scalars['uuid'];
};

/** columns and relationships of "show_season" */
export type ShowSeason = {
  id: Scalars['uuid'];
  name: Scalars['String'];
  season: Scalars['Int'];
  /** An object relationship */
  show: Show;
  show_id: Scalars['uuid'];
  summary: Scalars['String'];
};

/** aggregated selection of "show_season" */
export type ShowSeasonAggregate = {
  aggregate?: Maybe<ShowSeasonAggregateFields>;
  nodes: Array<ShowSeason>;
};

/** aggregate fields of "show_season" */
export type ShowSeasonAggregateFields = {
  avg?: Maybe<ShowSeasonAvgFields>;
  count?: Maybe<Scalars['Int']>;
  max?: Maybe<ShowSeasonMaxFields>;
  min?: Maybe<ShowSeasonMinFields>;
  stddev?: Maybe<ShowSeasonStddevFields>;
  stddev_pop?: Maybe<ShowSeasonStddevPopFields>;
  stddev_samp?: Maybe<ShowSeasonStddevSampFields>;
  sum?: Maybe<ShowSeasonSumFields>;
  var_pop?: Maybe<ShowSeasonVarPopFields>;
  var_samp?: Maybe<ShowSeasonVarSampFields>;
  variance?: Maybe<ShowSeasonVarianceFields>;
};


/** aggregate fields of "show_season" */
export type ShowSeasonAggregateFieldsCountArgs = {
  columns?: Maybe<Array<ShowSeasonSelectColumn>>;
  distinct?: Maybe<Scalars['Boolean']>;
};

/** order by aggregate values of table "show_season" */
export type ShowSeasonAggregateOrderBy = {
  avg?: Maybe<ShowSeasonAvgOrderBy>;
  count?: Maybe<OrderBy>;
  max?: Maybe<ShowSeasonMaxOrderBy>;
  min?: Maybe<ShowSeasonMinOrderBy>;
  stddev?: Maybe<ShowSeasonStddevOrderBy>;
  stddev_pop?: Maybe<ShowSeasonStddevPopOrderBy>;
  stddev_samp?: Maybe<ShowSeasonStddevSampOrderBy>;
  sum?: Maybe<ShowSeasonSumOrderBy>;
  var_pop?: Maybe<ShowSeasonVarPopOrderBy>;
  var_samp?: Maybe<ShowSeasonVarSampOrderBy>;
  variance?: Maybe<ShowSeasonVarianceOrderBy>;
};

/** input type for inserting array relation for remote table "show_season" */
export type ShowSeasonArrRelInsertInput = {
  data: Array<ShowSeasonInsertInput>;
  on_conflict?: Maybe<ShowSeasonOnConflict>;
};

/** aggregate avg on columns */
export type ShowSeasonAvgFields = {
  season?: Maybe<Scalars['Float']>;
};

/** order by avg() on columns of table "show_season" */
export type ShowSeasonAvgOrderBy = {
  season?: Maybe<OrderBy>;
};

/** Boolean expression to filter rows from the table "show_season". All fields are combined with a logical 'AND'. */
export type ShowSeasonBoolExp = {
  _and?: Maybe<Array<Maybe<ShowSeasonBoolExp>>>;
  _not?: Maybe<ShowSeasonBoolExp>;
  _or?: Maybe<Array<Maybe<ShowSeasonBoolExp>>>;
  id?: Maybe<UuidComparisonExp>;
  name?: Maybe<StringComparisonExp>;
  season?: Maybe<IntComparisonExp>;
  show?: Maybe<ShowBoolExp>;
  show_id?: Maybe<UuidComparisonExp>;
  summary?: Maybe<StringComparisonExp>;
};

/** unique or primary key constraints on table "show_season" */
export enum ShowSeasonConstraint {
  /** unique or primary key constraint */
  ShowSeasonPkey = 'show_season_pkey',
  /** unique or primary key constraint */
  ShowSeasonShowIdSeasonKey = 'show_season_show_id_season_key'
}

/** input type for incrementing integer column in table "show_season" */
export type ShowSeasonIncInput = {
  season?: Maybe<Scalars['Int']>;
};

/** input type for inserting data into table "show_season" */
export type ShowSeasonInsertInput = {
  id?: Maybe<Scalars['uuid']>;
  name?: Maybe<Scalars['String']>;
  season?: Maybe<Scalars['Int']>;
  show?: Maybe<ShowObjRelInsertInput>;
  show_id?: Maybe<Scalars['uuid']>;
  summary?: Maybe<Scalars['String']>;
};

/** aggregate max on columns */
export type ShowSeasonMaxFields = {
  id?: Maybe<Scalars['uuid']>;
  name?: Maybe<Scalars['String']>;
  season?: Maybe<Scalars['Int']>;
  show_id?: Maybe<Scalars['uuid']>;
  summary?: Maybe<Scalars['String']>;
};

/** order by max() on columns of table "show_season" */
export type ShowSeasonMaxOrderBy = {
  id?: Maybe<OrderBy>;
  name?: Maybe<OrderBy>;
  season?: Maybe<OrderBy>;
  show_id?: Maybe<OrderBy>;
  summary?: Maybe<OrderBy>;
};

/** aggregate min on columns */
export type ShowSeasonMinFields = {
  id?: Maybe<Scalars['uuid']>;
  name?: Maybe<Scalars['String']>;
  season?: Maybe<Scalars['Int']>;
  show_id?: Maybe<Scalars['uuid']>;
  summary?: Maybe<Scalars['String']>;
};

/** order by min() on columns of table "show_season" */
export type ShowSeasonMinOrderBy = {
  id?: Maybe<OrderBy>;
  name?: Maybe<OrderBy>;
  season?: Maybe<OrderBy>;
  show_id?: Maybe<OrderBy>;
  summary?: Maybe<OrderBy>;
};

/** response of any mutation on the table "show_season" */
export type ShowSeasonMutationResponse = {
  /** number of affected rows by the mutation */
  affected_rows: Scalars['Int'];
  /** data of the affected rows by the mutation */
  returning: Array<ShowSeason>;
};

/** input type for inserting object relation for remote table "show_season" */
export type ShowSeasonObjRelInsertInput = {
  data: ShowSeasonInsertInput;
  on_conflict?: Maybe<ShowSeasonOnConflict>;
};

/** on conflict condition type for table "show_season" */
export type ShowSeasonOnConflict = {
  constraint: ShowSeasonConstraint;
  update_columns: Array<ShowSeasonUpdateColumn>;
  where?: Maybe<ShowSeasonBoolExp>;
};

/** ordering options when selecting data from "show_season" */
export type ShowSeasonOrderBy = {
  id?: Maybe<OrderBy>;
  name?: Maybe<OrderBy>;
  season?: Maybe<OrderBy>;
  show?: Maybe<ShowOrderBy>;
  show_id?: Maybe<OrderBy>;
  summary?: Maybe<OrderBy>;
};

/** primary key columns input for table: "show_season" */
export type ShowSeasonPkColumnsInput = {
  id: Scalars['uuid'];
};

/** select columns of table "show_season" */
export enum ShowSeasonSelectColumn {
  /** column name */
  Id = 'id',
  /** column name */
  Name = 'name',
  /** column name */
  Season = 'season',
  /** column name */
  ShowId = 'show_id',
  /** column name */
  Summary = 'summary'
}

/** input type for updating data in table "show_season" */
export type ShowSeasonSetInput = {
  id?: Maybe<Scalars['uuid']>;
  name?: Maybe<Scalars['String']>;
  season?: Maybe<Scalars['Int']>;
  show_id?: Maybe<Scalars['uuid']>;
  summary?: Maybe<Scalars['String']>;
};

/** aggregate stddev on columns */
export type ShowSeasonStddevFields = {
  season?: Maybe<Scalars['Float']>;
};

/** order by stddev() on columns of table "show_season" */
export type ShowSeasonStddevOrderBy = {
  season?: Maybe<OrderBy>;
};

/** aggregate stddev_pop on columns */
export type ShowSeasonStddevPopFields = {
  season?: Maybe<Scalars['Float']>;
};

/** order by stddev_pop() on columns of table "show_season" */
export type ShowSeasonStddevPopOrderBy = {
  season?: Maybe<OrderBy>;
};

/** aggregate stddev_samp on columns */
export type ShowSeasonStddevSampFields = {
  season?: Maybe<Scalars['Float']>;
};

/** order by stddev_samp() on columns of table "show_season" */
export type ShowSeasonStddevSampOrderBy = {
  season?: Maybe<OrderBy>;
};

/** aggregate sum on columns */
export type ShowSeasonSumFields = {
  season?: Maybe<Scalars['Int']>;
};

/** order by sum() on columns of table "show_season" */
export type ShowSeasonSumOrderBy = {
  season?: Maybe<OrderBy>;
};

/** update columns of table "show_season" */
export enum ShowSeasonUpdateColumn {
  /** column name */
  Id = 'id',
  /** column name */
  Name = 'name',
  /** column name */
  Season = 'season',
  /** column name */
  ShowId = 'show_id',
  /** column name */
  Summary = 'summary'
}

/** aggregate var_pop on columns */
export type ShowSeasonVarPopFields = {
  season?: Maybe<Scalars['Float']>;
};

/** order by var_pop() on columns of table "show_season" */
export type ShowSeasonVarPopOrderBy = {
  season?: Maybe<OrderBy>;
};

/** aggregate var_samp on columns */
export type ShowSeasonVarSampFields = {
  season?: Maybe<Scalars['Float']>;
};

/** order by var_samp() on columns of table "show_season" */
export type ShowSeasonVarSampOrderBy = {
  season?: Maybe<OrderBy>;
};

/** aggregate variance on columns */
export type ShowSeasonVarianceFields = {
  season?: Maybe<Scalars['Float']>;
};

/** order by variance() on columns of table "show_season" */
export type ShowSeasonVarianceOrderBy = {
  season?: Maybe<OrderBy>;
};

/** select columns of table "show" */
export enum ShowSelectColumn {
  /** column name */
  Aired = 'aired',
  /** column name */
  ContentRating = 'content_rating',
  /** column name */
  Id = 'id',
  /** column name */
  ImageBackground = 'image_background',
  /** column name */
  ImageCover = 'image_cover',
  /** column name */
  Name = 'name',
  /** column name */
  NameMatch = 'name_match',
  /** column name */
  NameOriginal = 'name_original',
  /** column name */
  NameSort = 'name_sort',
  /** column name */
  Rating = 'rating',
  /** column name */
  Summary = 'summary',
  /** column name */
  Tagline = 'tagline'
}

/** input type for updating data in table "show" */
export type ShowSetInput = {
  aired?: Maybe<Scalars['date']>;
  content_rating?: Maybe<Scalars['String']>;
  id?: Maybe<Scalars['uuid']>;
  image_background?: Maybe<Scalars['String']>;
  image_cover?: Maybe<Scalars['String']>;
  name?: Maybe<Scalars['String']>;
  name_match?: Maybe<Scalars['String']>;
  name_original?: Maybe<Scalars['_text']>;
  name_sort?: Maybe<Scalars['String']>;
  rating?: Maybe<Scalars['numeric']>;
  summary?: Maybe<Scalars['String']>;
  tagline?: Maybe<Scalars['String']>;
};

/** columns and relationships of "show_staff" */
export type ShowStaff = {
  custom_staff_name?: Maybe<Scalars['String']>;
  id: Scalars['uuid'];
  index: Scalars['Int'];
  /** An object relationship */
  person: Person;
  person_id: Scalars['uuid'];
  /** An object relationship */
  show: Show;
  show_id: Scalars['uuid'];
  /** An object relationship */
  staff: Staff;
  staff_id: Scalars['uuid'];
};

/** aggregated selection of "show_staff" */
export type ShowStaffAggregate = {
  aggregate?: Maybe<ShowStaffAggregateFields>;
  nodes: Array<ShowStaff>;
};

/** aggregate fields of "show_staff" */
export type ShowStaffAggregateFields = {
  avg?: Maybe<ShowStaffAvgFields>;
  count?: Maybe<Scalars['Int']>;
  max?: Maybe<ShowStaffMaxFields>;
  min?: Maybe<ShowStaffMinFields>;
  stddev?: Maybe<ShowStaffStddevFields>;
  stddev_pop?: Maybe<ShowStaffStddevPopFields>;
  stddev_samp?: Maybe<ShowStaffStddevSampFields>;
  sum?: Maybe<ShowStaffSumFields>;
  var_pop?: Maybe<ShowStaffVarPopFields>;
  var_samp?: Maybe<ShowStaffVarSampFields>;
  variance?: Maybe<ShowStaffVarianceFields>;
};


/** aggregate fields of "show_staff" */
export type ShowStaffAggregateFieldsCountArgs = {
  columns?: Maybe<Array<ShowStaffSelectColumn>>;
  distinct?: Maybe<Scalars['Boolean']>;
};

/** order by aggregate values of table "show_staff" */
export type ShowStaffAggregateOrderBy = {
  avg?: Maybe<ShowStaffAvgOrderBy>;
  count?: Maybe<OrderBy>;
  max?: Maybe<ShowStaffMaxOrderBy>;
  min?: Maybe<ShowStaffMinOrderBy>;
  stddev?: Maybe<ShowStaffStddevOrderBy>;
  stddev_pop?: Maybe<ShowStaffStddevPopOrderBy>;
  stddev_samp?: Maybe<ShowStaffStddevSampOrderBy>;
  sum?: Maybe<ShowStaffSumOrderBy>;
  var_pop?: Maybe<ShowStaffVarPopOrderBy>;
  var_samp?: Maybe<ShowStaffVarSampOrderBy>;
  variance?: Maybe<ShowStaffVarianceOrderBy>;
};

/** input type for inserting array relation for remote table "show_staff" */
export type ShowStaffArrRelInsertInput = {
  data: Array<ShowStaffInsertInput>;
  on_conflict?: Maybe<ShowStaffOnConflict>;
};

/** aggregate avg on columns */
export type ShowStaffAvgFields = {
  index?: Maybe<Scalars['Float']>;
};

/** order by avg() on columns of table "show_staff" */
export type ShowStaffAvgOrderBy = {
  index?: Maybe<OrderBy>;
};

/** Boolean expression to filter rows from the table "show_staff". All fields are combined with a logical 'AND'. */
export type ShowStaffBoolExp = {
  _and?: Maybe<Array<Maybe<ShowStaffBoolExp>>>;
  _not?: Maybe<ShowStaffBoolExp>;
  _or?: Maybe<Array<Maybe<ShowStaffBoolExp>>>;
  custom_staff_name?: Maybe<StringComparisonExp>;
  id?: Maybe<UuidComparisonExp>;
  index?: Maybe<IntComparisonExp>;
  person?: Maybe<PersonBoolExp>;
  person_id?: Maybe<UuidComparisonExp>;
  show?: Maybe<ShowBoolExp>;
  show_id?: Maybe<UuidComparisonExp>;
  staff?: Maybe<StaffBoolExp>;
  staff_id?: Maybe<UuidComparisonExp>;
};

/** unique or primary key constraints on table "show_staff" */
export enum ShowStaffConstraint {
  /** unique or primary key constraint */
  ShowStaffPkey = 'show_staff_pkey',
  /** unique or primary key constraint */
  ShowStaffShowIdStaffIdIndexKey = 'show_staff_show_id_staff_id_index_key',
  /** unique or primary key constraint */
  ShowStaffShowIdStaffIdPersonIdKey = 'show_staff_show_id_staff_id_person_id_key'
}

/** input type for incrementing integer column in table "show_staff" */
export type ShowStaffIncInput = {
  index?: Maybe<Scalars['Int']>;
};

/** input type for inserting data into table "show_staff" */
export type ShowStaffInsertInput = {
  custom_staff_name?: Maybe<Scalars['String']>;
  id?: Maybe<Scalars['uuid']>;
  index?: Maybe<Scalars['Int']>;
  person?: Maybe<PersonObjRelInsertInput>;
  person_id?: Maybe<Scalars['uuid']>;
  show?: Maybe<ShowObjRelInsertInput>;
  show_id?: Maybe<Scalars['uuid']>;
  staff?: Maybe<StaffObjRelInsertInput>;
  staff_id?: Maybe<Scalars['uuid']>;
};

/** aggregate max on columns */
export type ShowStaffMaxFields = {
  custom_staff_name?: Maybe<Scalars['String']>;
  id?: Maybe<Scalars['uuid']>;
  index?: Maybe<Scalars['Int']>;
  person_id?: Maybe<Scalars['uuid']>;
  show_id?: Maybe<Scalars['uuid']>;
  staff_id?: Maybe<Scalars['uuid']>;
};

/** order by max() on columns of table "show_staff" */
export type ShowStaffMaxOrderBy = {
  custom_staff_name?: Maybe<OrderBy>;
  id?: Maybe<OrderBy>;
  index?: Maybe<OrderBy>;
  person_id?: Maybe<OrderBy>;
  show_id?: Maybe<OrderBy>;
  staff_id?: Maybe<OrderBy>;
};

/** aggregate min on columns */
export type ShowStaffMinFields = {
  custom_staff_name?: Maybe<Scalars['String']>;
  id?: Maybe<Scalars['uuid']>;
  index?: Maybe<Scalars['Int']>;
  person_id?: Maybe<Scalars['uuid']>;
  show_id?: Maybe<Scalars['uuid']>;
  staff_id?: Maybe<Scalars['uuid']>;
};

/** order by min() on columns of table "show_staff" */
export type ShowStaffMinOrderBy = {
  custom_staff_name?: Maybe<OrderBy>;
  id?: Maybe<OrderBy>;
  index?: Maybe<OrderBy>;
  person_id?: Maybe<OrderBy>;
  show_id?: Maybe<OrderBy>;
  staff_id?: Maybe<OrderBy>;
};

/** response of any mutation on the table "show_staff" */
export type ShowStaffMutationResponse = {
  /** number of affected rows by the mutation */
  affected_rows: Scalars['Int'];
  /** data of the affected rows by the mutation */
  returning: Array<ShowStaff>;
};

/** input type for inserting object relation for remote table "show_staff" */
export type ShowStaffObjRelInsertInput = {
  data: ShowStaffInsertInput;
  on_conflict?: Maybe<ShowStaffOnConflict>;
};

/** on conflict condition type for table "show_staff" */
export type ShowStaffOnConflict = {
  constraint: ShowStaffConstraint;
  update_columns: Array<ShowStaffUpdateColumn>;
  where?: Maybe<ShowStaffBoolExp>;
};

/** ordering options when selecting data from "show_staff" */
export type ShowStaffOrderBy = {
  custom_staff_name?: Maybe<OrderBy>;
  id?: Maybe<OrderBy>;
  index?: Maybe<OrderBy>;
  person?: Maybe<PersonOrderBy>;
  person_id?: Maybe<OrderBy>;
  show?: Maybe<ShowOrderBy>;
  show_id?: Maybe<OrderBy>;
  staff?: Maybe<StaffOrderBy>;
  staff_id?: Maybe<OrderBy>;
};

/** primary key columns input for table: "show_staff" */
export type ShowStaffPkColumnsInput = {
  id: Scalars['uuid'];
};

/** select columns of table "show_staff" */
export enum ShowStaffSelectColumn {
  /** column name */
  CustomStaffName = 'custom_staff_name',
  /** column name */
  Id = 'id',
  /** column name */
  Index = 'index',
  /** column name */
  PersonId = 'person_id',
  /** column name */
  ShowId = 'show_id',
  /** column name */
  StaffId = 'staff_id'
}

/** input type for updating data in table "show_staff" */
export type ShowStaffSetInput = {
  custom_staff_name?: Maybe<Scalars['String']>;
  id?: Maybe<Scalars['uuid']>;
  index?: Maybe<Scalars['Int']>;
  person_id?: Maybe<Scalars['uuid']>;
  show_id?: Maybe<Scalars['uuid']>;
  staff_id?: Maybe<Scalars['uuid']>;
};

/** aggregate stddev on columns */
export type ShowStaffStddevFields = {
  index?: Maybe<Scalars['Float']>;
};

/** order by stddev() on columns of table "show_staff" */
export type ShowStaffStddevOrderBy = {
  index?: Maybe<OrderBy>;
};

/** aggregate stddev_pop on columns */
export type ShowStaffStddevPopFields = {
  index?: Maybe<Scalars['Float']>;
};

/** order by stddev_pop() on columns of table "show_staff" */
export type ShowStaffStddevPopOrderBy = {
  index?: Maybe<OrderBy>;
};

/** aggregate stddev_samp on columns */
export type ShowStaffStddevSampFields = {
  index?: Maybe<Scalars['Float']>;
};

/** order by stddev_samp() on columns of table "show_staff" */
export type ShowStaffStddevSampOrderBy = {
  index?: Maybe<OrderBy>;
};

/** aggregate sum on columns */
export type ShowStaffSumFields = {
  index?: Maybe<Scalars['Int']>;
};

/** order by sum() on columns of table "show_staff" */
export type ShowStaffSumOrderBy = {
  index?: Maybe<OrderBy>;
};

/** update columns of table "show_staff" */
export enum ShowStaffUpdateColumn {
  /** column name */
  CustomStaffName = 'custom_staff_name',
  /** column name */
  Id = 'id',
  /** column name */
  Index = 'index',
  /** column name */
  PersonId = 'person_id',
  /** column name */
  ShowId = 'show_id',
  /** column name */
  StaffId = 'staff_id'
}

/** aggregate var_pop on columns */
export type ShowStaffVarPopFields = {
  index?: Maybe<Scalars['Float']>;
};

/** order by var_pop() on columns of table "show_staff" */
export type ShowStaffVarPopOrderBy = {
  index?: Maybe<OrderBy>;
};

/** aggregate var_samp on columns */
export type ShowStaffVarSampFields = {
  index?: Maybe<Scalars['Float']>;
};

/** order by var_samp() on columns of table "show_staff" */
export type ShowStaffVarSampOrderBy = {
  index?: Maybe<OrderBy>;
};

/** aggregate variance on columns */
export type ShowStaffVarianceFields = {
  index?: Maybe<Scalars['Float']>;
};

/** order by variance() on columns of table "show_staff" */
export type ShowStaffVarianceOrderBy = {
  index?: Maybe<OrderBy>;
};

/** aggregate stddev on columns */
export type ShowStddevFields = {
  rating?: Maybe<Scalars['Float']>;
};

/** order by stddev() on columns of table "show" */
export type ShowStddevOrderBy = {
  rating?: Maybe<OrderBy>;
};

/** aggregate stddev_pop on columns */
export type ShowStddevPopFields = {
  rating?: Maybe<Scalars['Float']>;
};

/** order by stddev_pop() on columns of table "show" */
export type ShowStddevPopOrderBy = {
  rating?: Maybe<OrderBy>;
};

/** aggregate stddev_samp on columns */
export type ShowStddevSampFields = {
  rating?: Maybe<Scalars['Float']>;
};

/** order by stddev_samp() on columns of table "show" */
export type ShowStddevSampOrderBy = {
  rating?: Maybe<OrderBy>;
};

/** aggregate sum on columns */
export type ShowSumFields = {
  rating?: Maybe<Scalars['numeric']>;
};

/** order by sum() on columns of table "show" */
export type ShowSumOrderBy = {
  rating?: Maybe<OrderBy>;
};

/** update columns of table "show" */
export enum ShowUpdateColumn {
  /** column name */
  Aired = 'aired',
  /** column name */
  ContentRating = 'content_rating',
  /** column name */
  Id = 'id',
  /** column name */
  ImageBackground = 'image_background',
  /** column name */
  ImageCover = 'image_cover',
  /** column name */
  Name = 'name',
  /** column name */
  NameMatch = 'name_match',
  /** column name */
  NameOriginal = 'name_original',
  /** column name */
  NameSort = 'name_sort',
  /** column name */
  Rating = 'rating',
  /** column name */
  Summary = 'summary',
  /** column name */
  Tagline = 'tagline'
}

/** aggregate var_pop on columns */
export type ShowVarPopFields = {
  rating?: Maybe<Scalars['Float']>;
};

/** order by var_pop() on columns of table "show" */
export type ShowVarPopOrderBy = {
  rating?: Maybe<OrderBy>;
};

/** aggregate var_samp on columns */
export type ShowVarSampFields = {
  rating?: Maybe<Scalars['Float']>;
};

/** order by var_samp() on columns of table "show" */
export type ShowVarSampOrderBy = {
  rating?: Maybe<OrderBy>;
};

/** aggregate variance on columns */
export type ShowVarianceFields = {
  rating?: Maybe<Scalars['Float']>;
};

/** order by variance() on columns of table "show" */
export type ShowVarianceOrderBy = {
  rating?: Maybe<OrderBy>;
};

/** columns and relationships of "staff" */
export type Staff = {
  /** An array relationship */
  episode_staffs: Array<EpisodeStaff>;
  /** An aggregated array relationship */
  episode_staffs_aggregate: EpisodeStaffAggregate;
  id: Scalars['uuid'];
  /** An array relationship */
  movie_staffs: Array<MovieStaff>;
  /** An aggregated array relationship */
  movie_staffs_aggregate: MovieStaffAggregate;
  name: Scalars['String'];
  /** An array relationship */
  show_staffs: Array<ShowStaff>;
  /** An aggregated array relationship */
  show_staffs_aggregate: ShowStaffAggregate;
};


/** columns and relationships of "staff" */
export type StaffEpisodeStaffsArgs = {
  distinct_on?: Maybe<Array<EpisodeStaffSelectColumn>>;
  limit?: Maybe<Scalars['Int']>;
  offset?: Maybe<Scalars['Int']>;
  order_by?: Maybe<Array<EpisodeStaffOrderBy>>;
  where?: Maybe<EpisodeStaffBoolExp>;
};


/** columns and relationships of "staff" */
export type StaffEpisodeStaffsAggregateArgs = {
  distinct_on?: Maybe<Array<EpisodeStaffSelectColumn>>;
  limit?: Maybe<Scalars['Int']>;
  offset?: Maybe<Scalars['Int']>;
  order_by?: Maybe<Array<EpisodeStaffOrderBy>>;
  where?: Maybe<EpisodeStaffBoolExp>;
};


/** columns and relationships of "staff" */
export type StaffMovieStaffsArgs = {
  distinct_on?: Maybe<Array<MovieStaffSelectColumn>>;
  limit?: Maybe<Scalars['Int']>;
  offset?: Maybe<Scalars['Int']>;
  order_by?: Maybe<Array<MovieStaffOrderBy>>;
  where?: Maybe<MovieStaffBoolExp>;
};


/** columns and relationships of "staff" */
export type StaffMovieStaffsAggregateArgs = {
  distinct_on?: Maybe<Array<MovieStaffSelectColumn>>;
  limit?: Maybe<Scalars['Int']>;
  offset?: Maybe<Scalars['Int']>;
  order_by?: Maybe<Array<MovieStaffOrderBy>>;
  where?: Maybe<MovieStaffBoolExp>;
};


/** columns and relationships of "staff" */
export type StaffShowStaffsArgs = {
  distinct_on?: Maybe<Array<ShowStaffSelectColumn>>;
  limit?: Maybe<Scalars['Int']>;
  offset?: Maybe<Scalars['Int']>;
  order_by?: Maybe<Array<ShowStaffOrderBy>>;
  where?: Maybe<ShowStaffBoolExp>;
};


/** columns and relationships of "staff" */
export type StaffShowStaffsAggregateArgs = {
  distinct_on?: Maybe<Array<ShowStaffSelectColumn>>;
  limit?: Maybe<Scalars['Int']>;
  offset?: Maybe<Scalars['Int']>;
  order_by?: Maybe<Array<ShowStaffOrderBy>>;
  where?: Maybe<ShowStaffBoolExp>;
};

/** aggregated selection of "staff" */
export type StaffAggregate = {
  aggregate?: Maybe<StaffAggregateFields>;
  nodes: Array<Staff>;
};

/** aggregate fields of "staff" */
export type StaffAggregateFields = {
  count?: Maybe<Scalars['Int']>;
  max?: Maybe<StaffMaxFields>;
  min?: Maybe<StaffMinFields>;
};


/** aggregate fields of "staff" */
export type StaffAggregateFieldsCountArgs = {
  columns?: Maybe<Array<StaffSelectColumn>>;
  distinct?: Maybe<Scalars['Boolean']>;
};

/** order by aggregate values of table "staff" */
export type StaffAggregateOrderBy = {
  count?: Maybe<OrderBy>;
  max?: Maybe<StaffMaxOrderBy>;
  min?: Maybe<StaffMinOrderBy>;
};

/** input type for inserting array relation for remote table "staff" */
export type StaffArrRelInsertInput = {
  data: Array<StaffInsertInput>;
  on_conflict?: Maybe<StaffOnConflict>;
};

/** Boolean expression to filter rows from the table "staff". All fields are combined with a logical 'AND'. */
export type StaffBoolExp = {
  _and?: Maybe<Array<Maybe<StaffBoolExp>>>;
  _not?: Maybe<StaffBoolExp>;
  _or?: Maybe<Array<Maybe<StaffBoolExp>>>;
  episode_staffs?: Maybe<EpisodeStaffBoolExp>;
  id?: Maybe<UuidComparisonExp>;
  movie_staffs?: Maybe<MovieStaffBoolExp>;
  name?: Maybe<StringComparisonExp>;
  show_staffs?: Maybe<ShowStaffBoolExp>;
};

/** unique or primary key constraints on table "staff" */
export enum StaffConstraint {
  /** unique or primary key constraint */
  StaffPkey = 'staff_pkey'
}

/** input type for inserting data into table "staff" */
export type StaffInsertInput = {
  episode_staffs?: Maybe<EpisodeStaffArrRelInsertInput>;
  id?: Maybe<Scalars['uuid']>;
  movie_staffs?: Maybe<MovieStaffArrRelInsertInput>;
  name?: Maybe<Scalars['String']>;
  show_staffs?: Maybe<ShowStaffArrRelInsertInput>;
};

/** aggregate max on columns */
export type StaffMaxFields = {
  id?: Maybe<Scalars['uuid']>;
  name?: Maybe<Scalars['String']>;
};

/** order by max() on columns of table "staff" */
export type StaffMaxOrderBy = {
  id?: Maybe<OrderBy>;
  name?: Maybe<OrderBy>;
};

/** aggregate min on columns */
export type StaffMinFields = {
  id?: Maybe<Scalars['uuid']>;
  name?: Maybe<Scalars['String']>;
};

/** order by min() on columns of table "staff" */
export type StaffMinOrderBy = {
  id?: Maybe<OrderBy>;
  name?: Maybe<OrderBy>;
};

/** response of any mutation on the table "staff" */
export type StaffMutationResponse = {
  /** number of affected rows by the mutation */
  affected_rows: Scalars['Int'];
  /** data of the affected rows by the mutation */
  returning: Array<Staff>;
};

/** input type for inserting object relation for remote table "staff" */
export type StaffObjRelInsertInput = {
  data: StaffInsertInput;
  on_conflict?: Maybe<StaffOnConflict>;
};

/** on conflict condition type for table "staff" */
export type StaffOnConflict = {
  constraint: StaffConstraint;
  update_columns: Array<StaffUpdateColumn>;
  where?: Maybe<StaffBoolExp>;
};

/** ordering options when selecting data from "staff" */
export type StaffOrderBy = {
  episode_staffs_aggregate?: Maybe<EpisodeStaffAggregateOrderBy>;
  id?: Maybe<OrderBy>;
  movie_staffs_aggregate?: Maybe<MovieStaffAggregateOrderBy>;
  name?: Maybe<OrderBy>;
  show_staffs_aggregate?: Maybe<ShowStaffAggregateOrderBy>;
};

/** primary key columns input for table: "staff" */
export type StaffPkColumnsInput = {
  id: Scalars['uuid'];
};

/** select columns of table "staff" */
export enum StaffSelectColumn {
  /** column name */
  Id = 'id',
  /** column name */
  Name = 'name'
}

/** input type for updating data in table "staff" */
export type StaffSetInput = {
  id?: Maybe<Scalars['uuid']>;
  name?: Maybe<Scalars['String']>;
};

/** update columns of table "staff" */
export enum StaffUpdateColumn {
  /** column name */
  Id = 'id',
  /** column name */
  Name = 'name'
}

/** columns and relationships of "studio" */
export type Studio = {
  /** An array relationship */
  album_studios: Array<AlbumStudio>;
  /** An aggregated array relationship */
  album_studios_aggregate: AlbumStudioAggregate;
  id: Scalars['uuid'];
  /** An array relationship */
  movie_studios: Array<MovieStudio>;
  /** An aggregated array relationship */
  movie_studios_aggregate: MovieStudioAggregate;
  name: Scalars['String'];
};


/** columns and relationships of "studio" */
export type StudioAlbumStudiosArgs = {
  distinct_on?: Maybe<Array<AlbumStudioSelectColumn>>;
  limit?: Maybe<Scalars['Int']>;
  offset?: Maybe<Scalars['Int']>;
  order_by?: Maybe<Array<AlbumStudioOrderBy>>;
  where?: Maybe<AlbumStudioBoolExp>;
};


/** columns and relationships of "studio" */
export type StudioAlbumStudiosAggregateArgs = {
  distinct_on?: Maybe<Array<AlbumStudioSelectColumn>>;
  limit?: Maybe<Scalars['Int']>;
  offset?: Maybe<Scalars['Int']>;
  order_by?: Maybe<Array<AlbumStudioOrderBy>>;
  where?: Maybe<AlbumStudioBoolExp>;
};


/** columns and relationships of "studio" */
export type StudioMovieStudiosArgs = {
  distinct_on?: Maybe<Array<MovieStudioSelectColumn>>;
  limit?: Maybe<Scalars['Int']>;
  offset?: Maybe<Scalars['Int']>;
  order_by?: Maybe<Array<MovieStudioOrderBy>>;
  where?: Maybe<MovieStudioBoolExp>;
};


/** columns and relationships of "studio" */
export type StudioMovieStudiosAggregateArgs = {
  distinct_on?: Maybe<Array<MovieStudioSelectColumn>>;
  limit?: Maybe<Scalars['Int']>;
  offset?: Maybe<Scalars['Int']>;
  order_by?: Maybe<Array<MovieStudioOrderBy>>;
  where?: Maybe<MovieStudioBoolExp>;
};

/** aggregated selection of "studio" */
export type StudioAggregate = {
  aggregate?: Maybe<StudioAggregateFields>;
  nodes: Array<Studio>;
};

/** aggregate fields of "studio" */
export type StudioAggregateFields = {
  count?: Maybe<Scalars['Int']>;
  max?: Maybe<StudioMaxFields>;
  min?: Maybe<StudioMinFields>;
};


/** aggregate fields of "studio" */
export type StudioAggregateFieldsCountArgs = {
  columns?: Maybe<Array<StudioSelectColumn>>;
  distinct?: Maybe<Scalars['Boolean']>;
};

/** order by aggregate values of table "studio" */
export type StudioAggregateOrderBy = {
  count?: Maybe<OrderBy>;
  max?: Maybe<StudioMaxOrderBy>;
  min?: Maybe<StudioMinOrderBy>;
};

/** input type for inserting array relation for remote table "studio" */
export type StudioArrRelInsertInput = {
  data: Array<StudioInsertInput>;
  on_conflict?: Maybe<StudioOnConflict>;
};

/** Boolean expression to filter rows from the table "studio". All fields are combined with a logical 'AND'. */
export type StudioBoolExp = {
  _and?: Maybe<Array<Maybe<StudioBoolExp>>>;
  _not?: Maybe<StudioBoolExp>;
  _or?: Maybe<Array<Maybe<StudioBoolExp>>>;
  album_studios?: Maybe<AlbumStudioBoolExp>;
  id?: Maybe<UuidComparisonExp>;
  movie_studios?: Maybe<MovieStudioBoolExp>;
  name?: Maybe<StringComparisonExp>;
};

/** unique or primary key constraints on table "studio" */
export enum StudioConstraint {
  /** unique or primary key constraint */
  StudioNameKey = 'studio_name_key',
  /** unique or primary key constraint */
  StudioPkey = 'studio_pkey'
}

/** input type for inserting data into table "studio" */
export type StudioInsertInput = {
  album_studios?: Maybe<AlbumStudioArrRelInsertInput>;
  id?: Maybe<Scalars['uuid']>;
  movie_studios?: Maybe<MovieStudioArrRelInsertInput>;
  name?: Maybe<Scalars['String']>;
};

/** aggregate max on columns */
export type StudioMaxFields = {
  id?: Maybe<Scalars['uuid']>;
  name?: Maybe<Scalars['String']>;
};

/** order by max() on columns of table "studio" */
export type StudioMaxOrderBy = {
  id?: Maybe<OrderBy>;
  name?: Maybe<OrderBy>;
};

/** aggregate min on columns */
export type StudioMinFields = {
  id?: Maybe<Scalars['uuid']>;
  name?: Maybe<Scalars['String']>;
};

/** order by min() on columns of table "studio" */
export type StudioMinOrderBy = {
  id?: Maybe<OrderBy>;
  name?: Maybe<OrderBy>;
};

/** response of any mutation on the table "studio" */
export type StudioMutationResponse = {
  /** number of affected rows by the mutation */
  affected_rows: Scalars['Int'];
  /** data of the affected rows by the mutation */
  returning: Array<Studio>;
};

/** input type for inserting object relation for remote table "studio" */
export type StudioObjRelInsertInput = {
  data: StudioInsertInput;
  on_conflict?: Maybe<StudioOnConflict>;
};

/** on conflict condition type for table "studio" */
export type StudioOnConflict = {
  constraint: StudioConstraint;
  update_columns: Array<StudioUpdateColumn>;
  where?: Maybe<StudioBoolExp>;
};

/** ordering options when selecting data from "studio" */
export type StudioOrderBy = {
  album_studios_aggregate?: Maybe<AlbumStudioAggregateOrderBy>;
  id?: Maybe<OrderBy>;
  movie_studios_aggregate?: Maybe<MovieStudioAggregateOrderBy>;
  name?: Maybe<OrderBy>;
};

/** primary key columns input for table: "studio" */
export type StudioPkColumnsInput = {
  id: Scalars['uuid'];
};

/** select columns of table "studio" */
export enum StudioSelectColumn {
  /** column name */
  Id = 'id',
  /** column name */
  Name = 'name'
}

/** input type for updating data in table "studio" */
export type StudioSetInput = {
  id?: Maybe<Scalars['uuid']>;
  name?: Maybe<Scalars['String']>;
};

/** update columns of table "studio" */
export enum StudioUpdateColumn {
  /** column name */
  Id = 'id',
  /** column name */
  Name = 'name'
}

/** columns and relationships of "style" */
export type Style = {
  /** An array relationship */
  album_styles: Array<AlbumStyle>;
  /** An aggregated array relationship */
  album_styles_aggregate: AlbumStyleAggregate;
  /** An array relationship */
  artist_styles: Array<ArtistStyle>;
  /** An aggregated array relationship */
  artist_styles_aggregate: ArtistStyleAggregate;
  id: Scalars['uuid'];
  name: Scalars['String'];
};


/** columns and relationships of "style" */
export type StyleAlbumStylesArgs = {
  distinct_on?: Maybe<Array<AlbumStyleSelectColumn>>;
  limit?: Maybe<Scalars['Int']>;
  offset?: Maybe<Scalars['Int']>;
  order_by?: Maybe<Array<AlbumStyleOrderBy>>;
  where?: Maybe<AlbumStyleBoolExp>;
};


/** columns and relationships of "style" */
export type StyleAlbumStylesAggregateArgs = {
  distinct_on?: Maybe<Array<AlbumStyleSelectColumn>>;
  limit?: Maybe<Scalars['Int']>;
  offset?: Maybe<Scalars['Int']>;
  order_by?: Maybe<Array<AlbumStyleOrderBy>>;
  where?: Maybe<AlbumStyleBoolExp>;
};


/** columns and relationships of "style" */
export type StyleArtistStylesArgs = {
  distinct_on?: Maybe<Array<ArtistStyleSelectColumn>>;
  limit?: Maybe<Scalars['Int']>;
  offset?: Maybe<Scalars['Int']>;
  order_by?: Maybe<Array<ArtistStyleOrderBy>>;
  where?: Maybe<ArtistStyleBoolExp>;
};


/** columns and relationships of "style" */
export type StyleArtistStylesAggregateArgs = {
  distinct_on?: Maybe<Array<ArtistStyleSelectColumn>>;
  limit?: Maybe<Scalars['Int']>;
  offset?: Maybe<Scalars['Int']>;
  order_by?: Maybe<Array<ArtistStyleOrderBy>>;
  where?: Maybe<ArtistStyleBoolExp>;
};

/** aggregated selection of "style" */
export type StyleAggregate = {
  aggregate?: Maybe<StyleAggregateFields>;
  nodes: Array<Style>;
};

/** aggregate fields of "style" */
export type StyleAggregateFields = {
  count?: Maybe<Scalars['Int']>;
  max?: Maybe<StyleMaxFields>;
  min?: Maybe<StyleMinFields>;
};


/** aggregate fields of "style" */
export type StyleAggregateFieldsCountArgs = {
  columns?: Maybe<Array<StyleSelectColumn>>;
  distinct?: Maybe<Scalars['Boolean']>;
};

/** order by aggregate values of table "style" */
export type StyleAggregateOrderBy = {
  count?: Maybe<OrderBy>;
  max?: Maybe<StyleMaxOrderBy>;
  min?: Maybe<StyleMinOrderBy>;
};

/** input type for inserting array relation for remote table "style" */
export type StyleArrRelInsertInput = {
  data: Array<StyleInsertInput>;
  on_conflict?: Maybe<StyleOnConflict>;
};

/** Boolean expression to filter rows from the table "style". All fields are combined with a logical 'AND'. */
export type StyleBoolExp = {
  _and?: Maybe<Array<Maybe<StyleBoolExp>>>;
  _not?: Maybe<StyleBoolExp>;
  _or?: Maybe<Array<Maybe<StyleBoolExp>>>;
  album_styles?: Maybe<AlbumStyleBoolExp>;
  artist_styles?: Maybe<ArtistStyleBoolExp>;
  id?: Maybe<UuidComparisonExp>;
  name?: Maybe<StringComparisonExp>;
};

/** unique or primary key constraints on table "style" */
export enum StyleConstraint {
  /** unique or primary key constraint */
  StyleNameKey = 'style_name_key',
  /** unique or primary key constraint */
  StylePkey = 'style_pkey'
}

/** input type for inserting data into table "style" */
export type StyleInsertInput = {
  album_styles?: Maybe<AlbumStyleArrRelInsertInput>;
  artist_styles?: Maybe<ArtistStyleArrRelInsertInput>;
  id?: Maybe<Scalars['uuid']>;
  name?: Maybe<Scalars['String']>;
};

/** aggregate max on columns */
export type StyleMaxFields = {
  id?: Maybe<Scalars['uuid']>;
  name?: Maybe<Scalars['String']>;
};

/** order by max() on columns of table "style" */
export type StyleMaxOrderBy = {
  id?: Maybe<OrderBy>;
  name?: Maybe<OrderBy>;
};

/** aggregate min on columns */
export type StyleMinFields = {
  id?: Maybe<Scalars['uuid']>;
  name?: Maybe<Scalars['String']>;
};

/** order by min() on columns of table "style" */
export type StyleMinOrderBy = {
  id?: Maybe<OrderBy>;
  name?: Maybe<OrderBy>;
};

/** response of any mutation on the table "style" */
export type StyleMutationResponse = {
  /** number of affected rows by the mutation */
  affected_rows: Scalars['Int'];
  /** data of the affected rows by the mutation */
  returning: Array<Style>;
};

/** input type for inserting object relation for remote table "style" */
export type StyleObjRelInsertInput = {
  data: StyleInsertInput;
  on_conflict?: Maybe<StyleOnConflict>;
};

/** on conflict condition type for table "style" */
export type StyleOnConflict = {
  constraint: StyleConstraint;
  update_columns: Array<StyleUpdateColumn>;
  where?: Maybe<StyleBoolExp>;
};

/** ordering options when selecting data from "style" */
export type StyleOrderBy = {
  album_styles_aggregate?: Maybe<AlbumStyleAggregateOrderBy>;
  artist_styles_aggregate?: Maybe<ArtistStyleAggregateOrderBy>;
  id?: Maybe<OrderBy>;
  name?: Maybe<OrderBy>;
};

/** primary key columns input for table: "style" */
export type StylePkColumnsInput = {
  id: Scalars['uuid'];
};

/** select columns of table "style" */
export enum StyleSelectColumn {
  /** column name */
  Id = 'id',
  /** column name */
  Name = 'name'
}

/** input type for updating data in table "style" */
export type StyleSetInput = {
  id?: Maybe<Scalars['uuid']>;
  name?: Maybe<Scalars['String']>;
};

/** update columns of table "style" */
export enum StyleUpdateColumn {
  /** column name */
  Id = 'id',
  /** column name */
  Name = 'name'
}

/** subscription root */
export type SubscriptionRoot = {
  /** fetch data from the table: "album" */
  album: Array<Album>;
  /** fetch aggregated fields from the table: "album" */
  album_aggregate: AlbumAggregate;
  /** fetch data from the table: "album" using primary key columns */
  album_by_pk?: Maybe<Album>;
  /** fetch data from the table: "album_collection" */
  album_collection: Array<AlbumCollection>;
  /** fetch aggregated fields from the table: "album_collection" */
  album_collection_aggregate: AlbumCollectionAggregate;
  /** fetch data from the table: "album_collection" using primary key columns */
  album_collection_by_pk?: Maybe<AlbumCollection>;
  /** fetch data from the table: "album_country" */
  album_country: Array<AlbumCountry>;
  /** fetch aggregated fields from the table: "album_country" */
  album_country_aggregate: AlbumCountryAggregate;
  /** fetch data from the table: "album_country" using primary key columns */
  album_country_by_pk?: Maybe<AlbumCountry>;
  /** fetch data from the table: "album_genre" */
  album_genre: Array<AlbumGenre>;
  /** fetch aggregated fields from the table: "album_genre" */
  album_genre_aggregate: AlbumGenreAggregate;
  /** fetch data from the table: "album_genre" using primary key columns */
  album_genre_by_pk?: Maybe<AlbumGenre>;
  /** fetch data from the table: "album_mood" */
  album_mood: Array<AlbumMood>;
  /** fetch aggregated fields from the table: "album_mood" */
  album_mood_aggregate: AlbumMoodAggregate;
  /** fetch data from the table: "album_mood" using primary key columns */
  album_mood_by_pk?: Maybe<AlbumMood>;
  /** fetch data from the table: "album_studio" */
  album_studio: Array<AlbumStudio>;
  /** fetch aggregated fields from the table: "album_studio" */
  album_studio_aggregate: AlbumStudioAggregate;
  /** fetch data from the table: "album_studio" using primary key columns */
  album_studio_by_pk?: Maybe<AlbumStudio>;
  /** fetch data from the table: "album_style" */
  album_style: Array<AlbumStyle>;
  /** fetch aggregated fields from the table: "album_style" */
  album_style_aggregate: AlbumStyleAggregate;
  /** fetch data from the table: "album_style" using primary key columns */
  album_style_by_pk?: Maybe<AlbumStyle>;
  /** fetch data from the table: "album_track" */
  album_track: Array<AlbumTrack>;
  /** fetch aggregated fields from the table: "album_track" */
  album_track_aggregate: AlbumTrackAggregate;
  /** fetch data from the table: "album_track" using primary key columns */
  album_track_by_pk?: Maybe<AlbumTrack>;
  /** fetch data from the table: "artist" */
  artist: Array<Artist>;
  /** fetch aggregated fields from the table: "artist" */
  artist_aggregate: ArtistAggregate;
  /** fetch data from the table: "artist" using primary key columns */
  artist_by_pk?: Maybe<Artist>;
  /** fetch data from the table: "artist_collection" */
  artist_collection: Array<ArtistCollection>;
  /** fetch aggregated fields from the table: "artist_collection" */
  artist_collection_aggregate: ArtistCollectionAggregate;
  /** fetch data from the table: "artist_collection" using primary key columns */
  artist_collection_by_pk?: Maybe<ArtistCollection>;
  /** fetch data from the table: "artist_country" */
  artist_country: Array<ArtistCountry>;
  /** fetch aggregated fields from the table: "artist_country" */
  artist_country_aggregate: ArtistCountryAggregate;
  /** fetch data from the table: "artist_country" using primary key columns */
  artist_country_by_pk?: Maybe<ArtistCountry>;
  /** fetch data from the table: "artist_genre" */
  artist_genre: Array<ArtistGenre>;
  /** fetch aggregated fields from the table: "artist_genre" */
  artist_genre_aggregate: ArtistGenreAggregate;
  /** fetch data from the table: "artist_genre" using primary key columns */
  artist_genre_by_pk?: Maybe<ArtistGenre>;
  /** fetch data from the table: "artist_mood" */
  artist_mood: Array<ArtistMood>;
  /** fetch aggregated fields from the table: "artist_mood" */
  artist_mood_aggregate: ArtistMoodAggregate;
  /** fetch data from the table: "artist_mood" using primary key columns */
  artist_mood_by_pk?: Maybe<ArtistMood>;
  /** fetch data from the table: "artist_similar" */
  artist_similar: Array<ArtistSimilar>;
  /** fetch aggregated fields from the table: "artist_similar" */
  artist_similar_aggregate: ArtistSimilarAggregate;
  /** fetch data from the table: "artist_similar" using primary key columns */
  artist_similar_by_pk?: Maybe<ArtistSimilar>;
  /** fetch data from the table: "artist_style" */
  artist_style: Array<ArtistStyle>;
  /** fetch aggregated fields from the table: "artist_style" */
  artist_style_aggregate: ArtistStyleAggregate;
  /** fetch data from the table: "artist_style" using primary key columns */
  artist_style_by_pk?: Maybe<ArtistStyle>;
  /** fetch data from the table: "collection" */
  collection: Array<Collection>;
  /** fetch aggregated fields from the table: "collection" */
  collection_aggregate: CollectionAggregate;
  /** fetch data from the table: "collection" using primary key columns */
  collection_by_pk?: Maybe<Collection>;
  /** fetch data from the table: "country" */
  country: Array<Country>;
  /** fetch aggregated fields from the table: "country" */
  country_aggregate: CountryAggregate;
  /** fetch data from the table: "country" using primary key columns */
  country_by_pk?: Maybe<Country>;
  /** fetch data from the table: "episode" */
  episode: Array<Episode>;
  /** fetch aggregated fields from the table: "episode" */
  episode_aggregate: EpisodeAggregate;
  /** fetch data from the table: "episode" using primary key columns */
  episode_by_pk?: Maybe<Episode>;
  /** fetch data from the table: "episode_staff" */
  episode_staff: Array<EpisodeStaff>;
  /** fetch aggregated fields from the table: "episode_staff" */
  episode_staff_aggregate: EpisodeStaffAggregate;
  /** fetch data from the table: "episode_staff" using primary key columns */
  episode_staff_by_pk?: Maybe<EpisodeStaff>;
  /** fetch data from the table: "genre" */
  genre: Array<Genre>;
  /** fetch aggregated fields from the table: "genre" */
  genre_aggregate: GenreAggregate;
  /** fetch data from the table: "genre" using primary key columns */
  genre_by_pk?: Maybe<Genre>;
  /** fetch data from the table: "mood" */
  mood: Array<Mood>;
  /** fetch aggregated fields from the table: "mood" */
  mood_aggregate: MoodAggregate;
  /** fetch data from the table: "mood" using primary key columns */
  mood_by_pk?: Maybe<Mood>;
  /** fetch data from the table: "movie" */
  movie: Array<Movie>;
  /** fetch aggregated fields from the table: "movie" */
  movie_aggregate: MovieAggregate;
  /** fetch data from the table: "movie" using primary key columns */
  movie_by_pk?: Maybe<Movie>;
  /** fetch data from the table: "movie_collection" */
  movie_collection: Array<MovieCollection>;
  /** fetch aggregated fields from the table: "movie_collection" */
  movie_collection_aggregate: MovieCollectionAggregate;
  /** fetch data from the table: "movie_collection" using primary key columns */
  movie_collection_by_pk?: Maybe<MovieCollection>;
  /** fetch data from the table: "movie_country" */
  movie_country: Array<MovieCountry>;
  /** fetch aggregated fields from the table: "movie_country" */
  movie_country_aggregate: MovieCountryAggregate;
  /** fetch data from the table: "movie_country" using primary key columns */
  movie_country_by_pk?: Maybe<MovieCountry>;
  /** fetch data from the table: "movie_genre" */
  movie_genre: Array<MovieGenre>;
  /** fetch aggregated fields from the table: "movie_genre" */
  movie_genre_aggregate: MovieGenreAggregate;
  /** fetch data from the table: "movie_genre" using primary key columns */
  movie_genre_by_pk?: Maybe<MovieGenre>;
  /** fetch data from the table: "movie_staff" */
  movie_staff: Array<MovieStaff>;
  /** fetch aggregated fields from the table: "movie_staff" */
  movie_staff_aggregate: MovieStaffAggregate;
  /** fetch data from the table: "movie_staff" using primary key columns */
  movie_staff_by_pk?: Maybe<MovieStaff>;
  /** fetch data from the table: "movie_studio" */
  movie_studio: Array<MovieStudio>;
  /** fetch aggregated fields from the table: "movie_studio" */
  movie_studio_aggregate: MovieStudioAggregate;
  /** fetch data from the table: "movie_studio" using primary key columns */
  movie_studio_by_pk?: Maybe<MovieStudio>;
  /** fetch data from the table: "person" */
  person: Array<Person>;
  /** fetch aggregated fields from the table: "person" */
  person_aggregate: PersonAggregate;
  /** fetch data from the table: "person" using primary key columns */
  person_by_pk?: Maybe<Person>;
  /** fetch data from the table: "show" */
  show: Array<Show>;
  /** fetch aggregated fields from the table: "show" */
  show_aggregate: ShowAggregate;
  /** fetch data from the table: "show" using primary key columns */
  show_by_pk?: Maybe<Show>;
  /** fetch data from the table: "show_collection" */
  show_collection: Array<ShowCollection>;
  /** fetch aggregated fields from the table: "show_collection" */
  show_collection_aggregate: ShowCollectionAggregate;
  /** fetch data from the table: "show_collection" using primary key columns */
  show_collection_by_pk?: Maybe<ShowCollection>;
  /** fetch data from the table: "show_genre" */
  show_genre: Array<ShowGenre>;
  /** fetch aggregated fields from the table: "show_genre" */
  show_genre_aggregate: ShowGenreAggregate;
  /** fetch data from the table: "show_genre" using primary key columns */
  show_genre_by_pk?: Maybe<ShowGenre>;
  /** fetch data from the table: "show_season" */
  show_season: Array<ShowSeason>;
  /** fetch aggregated fields from the table: "show_season" */
  show_season_aggregate: ShowSeasonAggregate;
  /** fetch data from the table: "show_season" using primary key columns */
  show_season_by_pk?: Maybe<ShowSeason>;
  /** fetch data from the table: "show_staff" */
  show_staff: Array<ShowStaff>;
  /** fetch aggregated fields from the table: "show_staff" */
  show_staff_aggregate: ShowStaffAggregate;
  /** fetch data from the table: "show_staff" using primary key columns */
  show_staff_by_pk?: Maybe<ShowStaff>;
  /** fetch data from the table: "staff" */
  staff: Array<Staff>;
  /** fetch aggregated fields from the table: "staff" */
  staff_aggregate: StaffAggregate;
  /** fetch data from the table: "staff" using primary key columns */
  staff_by_pk?: Maybe<Staff>;
  /** fetch data from the table: "studio" */
  studio: Array<Studio>;
  /** fetch aggregated fields from the table: "studio" */
  studio_aggregate: StudioAggregate;
  /** fetch data from the table: "studio" using primary key columns */
  studio_by_pk?: Maybe<Studio>;
  /** fetch data from the table: "style" */
  style: Array<Style>;
  /** fetch aggregated fields from the table: "style" */
  style_aggregate: StyleAggregate;
  /** fetch data from the table: "style" using primary key columns */
  style_by_pk?: Maybe<Style>;
  /** fetch data from the table: "track" */
  track: Array<Track>;
  /** fetch aggregated fields from the table: "track" */
  track_aggregate: TrackAggregate;
  /** fetch data from the table: "track" using primary key columns */
  track_by_pk?: Maybe<Track>;
  /** fetch data from the table: "track_genre" */
  track_genre: Array<TrackGenre>;
  /** fetch aggregated fields from the table: "track_genre" */
  track_genre_aggregate: TrackGenreAggregate;
  /** fetch data from the table: "track_genre" using primary key columns */
  track_genre_by_pk?: Maybe<TrackGenre>;
  /** fetch data from the table: "track_mood" */
  track_mood: Array<TrackMood>;
  /** fetch aggregated fields from the table: "track_mood" */
  track_mood_aggregate: TrackMoodAggregate;
  /** fetch data from the table: "track_mood" using primary key columns */
  track_mood_by_pk?: Maybe<TrackMood>;
};


/** subscription root */
export type SubscriptionRootAlbumArgs = {
  distinct_on?: Maybe<Array<AlbumSelectColumn>>;
  limit?: Maybe<Scalars['Int']>;
  offset?: Maybe<Scalars['Int']>;
  order_by?: Maybe<Array<AlbumOrderBy>>;
  where?: Maybe<AlbumBoolExp>;
};


/** subscription root */
export type SubscriptionRootAlbumAggregateArgs = {
  distinct_on?: Maybe<Array<AlbumSelectColumn>>;
  limit?: Maybe<Scalars['Int']>;
  offset?: Maybe<Scalars['Int']>;
  order_by?: Maybe<Array<AlbumOrderBy>>;
  where?: Maybe<AlbumBoolExp>;
};


/** subscription root */
export type SubscriptionRootAlbumByPkArgs = {
  id: Scalars['uuid'];
};


/** subscription root */
export type SubscriptionRootAlbumCollectionArgs = {
  distinct_on?: Maybe<Array<AlbumCollectionSelectColumn>>;
  limit?: Maybe<Scalars['Int']>;
  offset?: Maybe<Scalars['Int']>;
  order_by?: Maybe<Array<AlbumCollectionOrderBy>>;
  where?: Maybe<AlbumCollectionBoolExp>;
};


/** subscription root */
export type SubscriptionRootAlbumCollectionAggregateArgs = {
  distinct_on?: Maybe<Array<AlbumCollectionSelectColumn>>;
  limit?: Maybe<Scalars['Int']>;
  offset?: Maybe<Scalars['Int']>;
  order_by?: Maybe<Array<AlbumCollectionOrderBy>>;
  where?: Maybe<AlbumCollectionBoolExp>;
};


/** subscription root */
export type SubscriptionRootAlbumCollectionByPkArgs = {
  album_id: Scalars['uuid'];
  index: Scalars['Int'];
};


/** subscription root */
export type SubscriptionRootAlbumCountryArgs = {
  distinct_on?: Maybe<Array<AlbumCountrySelectColumn>>;
  limit?: Maybe<Scalars['Int']>;
  offset?: Maybe<Scalars['Int']>;
  order_by?: Maybe<Array<AlbumCountryOrderBy>>;
  where?: Maybe<AlbumCountryBoolExp>;
};


/** subscription root */
export type SubscriptionRootAlbumCountryAggregateArgs = {
  distinct_on?: Maybe<Array<AlbumCountrySelectColumn>>;
  limit?: Maybe<Scalars['Int']>;
  offset?: Maybe<Scalars['Int']>;
  order_by?: Maybe<Array<AlbumCountryOrderBy>>;
  where?: Maybe<AlbumCountryBoolExp>;
};


/** subscription root */
export type SubscriptionRootAlbumCountryByPkArgs = {
  album_id: Scalars['uuid'];
  index: Scalars['Int'];
};


/** subscription root */
export type SubscriptionRootAlbumGenreArgs = {
  distinct_on?: Maybe<Array<AlbumGenreSelectColumn>>;
  limit?: Maybe<Scalars['Int']>;
  offset?: Maybe<Scalars['Int']>;
  order_by?: Maybe<Array<AlbumGenreOrderBy>>;
  where?: Maybe<AlbumGenreBoolExp>;
};


/** subscription root */
export type SubscriptionRootAlbumGenreAggregateArgs = {
  distinct_on?: Maybe<Array<AlbumGenreSelectColumn>>;
  limit?: Maybe<Scalars['Int']>;
  offset?: Maybe<Scalars['Int']>;
  order_by?: Maybe<Array<AlbumGenreOrderBy>>;
  where?: Maybe<AlbumGenreBoolExp>;
};


/** subscription root */
export type SubscriptionRootAlbumGenreByPkArgs = {
  album_id: Scalars['uuid'];
  index: Scalars['Int'];
};


/** subscription root */
export type SubscriptionRootAlbumMoodArgs = {
  distinct_on?: Maybe<Array<AlbumMoodSelectColumn>>;
  limit?: Maybe<Scalars['Int']>;
  offset?: Maybe<Scalars['Int']>;
  order_by?: Maybe<Array<AlbumMoodOrderBy>>;
  where?: Maybe<AlbumMoodBoolExp>;
};


/** subscription root */
export type SubscriptionRootAlbumMoodAggregateArgs = {
  distinct_on?: Maybe<Array<AlbumMoodSelectColumn>>;
  limit?: Maybe<Scalars['Int']>;
  offset?: Maybe<Scalars['Int']>;
  order_by?: Maybe<Array<AlbumMoodOrderBy>>;
  where?: Maybe<AlbumMoodBoolExp>;
};


/** subscription root */
export type SubscriptionRootAlbumMoodByPkArgs = {
  album_id: Scalars['uuid'];
  index: Scalars['Int'];
};


/** subscription root */
export type SubscriptionRootAlbumStudioArgs = {
  distinct_on?: Maybe<Array<AlbumStudioSelectColumn>>;
  limit?: Maybe<Scalars['Int']>;
  offset?: Maybe<Scalars['Int']>;
  order_by?: Maybe<Array<AlbumStudioOrderBy>>;
  where?: Maybe<AlbumStudioBoolExp>;
};


/** subscription root */
export type SubscriptionRootAlbumStudioAggregateArgs = {
  distinct_on?: Maybe<Array<AlbumStudioSelectColumn>>;
  limit?: Maybe<Scalars['Int']>;
  offset?: Maybe<Scalars['Int']>;
  order_by?: Maybe<Array<AlbumStudioOrderBy>>;
  where?: Maybe<AlbumStudioBoolExp>;
};


/** subscription root */
export type SubscriptionRootAlbumStudioByPkArgs = {
  album_id: Scalars['uuid'];
  index: Scalars['Int'];
};


/** subscription root */
export type SubscriptionRootAlbumStyleArgs = {
  distinct_on?: Maybe<Array<AlbumStyleSelectColumn>>;
  limit?: Maybe<Scalars['Int']>;
  offset?: Maybe<Scalars['Int']>;
  order_by?: Maybe<Array<AlbumStyleOrderBy>>;
  where?: Maybe<AlbumStyleBoolExp>;
};


/** subscription root */
export type SubscriptionRootAlbumStyleAggregateArgs = {
  distinct_on?: Maybe<Array<AlbumStyleSelectColumn>>;
  limit?: Maybe<Scalars['Int']>;
  offset?: Maybe<Scalars['Int']>;
  order_by?: Maybe<Array<AlbumStyleOrderBy>>;
  where?: Maybe<AlbumStyleBoolExp>;
};


/** subscription root */
export type SubscriptionRootAlbumStyleByPkArgs = {
  album_id: Scalars['uuid'];
  index: Scalars['Int'];
};


/** subscription root */
export type SubscriptionRootAlbumTrackArgs = {
  distinct_on?: Maybe<Array<AlbumTrackSelectColumn>>;
  limit?: Maybe<Scalars['Int']>;
  offset?: Maybe<Scalars['Int']>;
  order_by?: Maybe<Array<AlbumTrackOrderBy>>;
  where?: Maybe<AlbumTrackBoolExp>;
};


/** subscription root */
export type SubscriptionRootAlbumTrackAggregateArgs = {
  distinct_on?: Maybe<Array<AlbumTrackSelectColumn>>;
  limit?: Maybe<Scalars['Int']>;
  offset?: Maybe<Scalars['Int']>;
  order_by?: Maybe<Array<AlbumTrackOrderBy>>;
  where?: Maybe<AlbumTrackBoolExp>;
};


/** subscription root */
export type SubscriptionRootAlbumTrackByPkArgs = {
  disk: Scalars['Int'];
  index: Scalars['Int'];
  track_id: Scalars['uuid'];
};


/** subscription root */
export type SubscriptionRootArtistArgs = {
  distinct_on?: Maybe<Array<ArtistSelectColumn>>;
  limit?: Maybe<Scalars['Int']>;
  offset?: Maybe<Scalars['Int']>;
  order_by?: Maybe<Array<ArtistOrderBy>>;
  where?: Maybe<ArtistBoolExp>;
};


/** subscription root */
export type SubscriptionRootArtistAggregateArgs = {
  distinct_on?: Maybe<Array<ArtistSelectColumn>>;
  limit?: Maybe<Scalars['Int']>;
  offset?: Maybe<Scalars['Int']>;
  order_by?: Maybe<Array<ArtistOrderBy>>;
  where?: Maybe<ArtistBoolExp>;
};


/** subscription root */
export type SubscriptionRootArtistByPkArgs = {
  id: Scalars['uuid'];
};


/** subscription root */
export type SubscriptionRootArtistCollectionArgs = {
  distinct_on?: Maybe<Array<ArtistCollectionSelectColumn>>;
  limit?: Maybe<Scalars['Int']>;
  offset?: Maybe<Scalars['Int']>;
  order_by?: Maybe<Array<ArtistCollectionOrderBy>>;
  where?: Maybe<ArtistCollectionBoolExp>;
};


/** subscription root */
export type SubscriptionRootArtistCollectionAggregateArgs = {
  distinct_on?: Maybe<Array<ArtistCollectionSelectColumn>>;
  limit?: Maybe<Scalars['Int']>;
  offset?: Maybe<Scalars['Int']>;
  order_by?: Maybe<Array<ArtistCollectionOrderBy>>;
  where?: Maybe<ArtistCollectionBoolExp>;
};


/** subscription root */
export type SubscriptionRootArtistCollectionByPkArgs = {
  artist_id: Scalars['uuid'];
  index: Scalars['Int'];
};


/** subscription root */
export type SubscriptionRootArtistCountryArgs = {
  distinct_on?: Maybe<Array<ArtistCountrySelectColumn>>;
  limit?: Maybe<Scalars['Int']>;
  offset?: Maybe<Scalars['Int']>;
  order_by?: Maybe<Array<ArtistCountryOrderBy>>;
  where?: Maybe<ArtistCountryBoolExp>;
};


/** subscription root */
export type SubscriptionRootArtistCountryAggregateArgs = {
  distinct_on?: Maybe<Array<ArtistCountrySelectColumn>>;
  limit?: Maybe<Scalars['Int']>;
  offset?: Maybe<Scalars['Int']>;
  order_by?: Maybe<Array<ArtistCountryOrderBy>>;
  where?: Maybe<ArtistCountryBoolExp>;
};


/** subscription root */
export type SubscriptionRootArtistCountryByPkArgs = {
  artist_id: Scalars['uuid'];
  index: Scalars['Int'];
};


/** subscription root */
export type SubscriptionRootArtistGenreArgs = {
  distinct_on?: Maybe<Array<ArtistGenreSelectColumn>>;
  limit?: Maybe<Scalars['Int']>;
  offset?: Maybe<Scalars['Int']>;
  order_by?: Maybe<Array<ArtistGenreOrderBy>>;
  where?: Maybe<ArtistGenreBoolExp>;
};


/** subscription root */
export type SubscriptionRootArtistGenreAggregateArgs = {
  distinct_on?: Maybe<Array<ArtistGenreSelectColumn>>;
  limit?: Maybe<Scalars['Int']>;
  offset?: Maybe<Scalars['Int']>;
  order_by?: Maybe<Array<ArtistGenreOrderBy>>;
  where?: Maybe<ArtistGenreBoolExp>;
};


/** subscription root */
export type SubscriptionRootArtistGenreByPkArgs = {
  artist_id: Scalars['uuid'];
  index: Scalars['Int'];
};


/** subscription root */
export type SubscriptionRootArtistMoodArgs = {
  distinct_on?: Maybe<Array<ArtistMoodSelectColumn>>;
  limit?: Maybe<Scalars['Int']>;
  offset?: Maybe<Scalars['Int']>;
  order_by?: Maybe<Array<ArtistMoodOrderBy>>;
  where?: Maybe<ArtistMoodBoolExp>;
};


/** subscription root */
export type SubscriptionRootArtistMoodAggregateArgs = {
  distinct_on?: Maybe<Array<ArtistMoodSelectColumn>>;
  limit?: Maybe<Scalars['Int']>;
  offset?: Maybe<Scalars['Int']>;
  order_by?: Maybe<Array<ArtistMoodOrderBy>>;
  where?: Maybe<ArtistMoodBoolExp>;
};


/** subscription root */
export type SubscriptionRootArtistMoodByPkArgs = {
  artist_id: Scalars['uuid'];
  index: Scalars['Int'];
};


/** subscription root */
export type SubscriptionRootArtistSimilarArgs = {
  distinct_on?: Maybe<Array<ArtistSimilarSelectColumn>>;
  limit?: Maybe<Scalars['Int']>;
  offset?: Maybe<Scalars['Int']>;
  order_by?: Maybe<Array<ArtistSimilarOrderBy>>;
  where?: Maybe<ArtistSimilarBoolExp>;
};


/** subscription root */
export type SubscriptionRootArtistSimilarAggregateArgs = {
  distinct_on?: Maybe<Array<ArtistSimilarSelectColumn>>;
  limit?: Maybe<Scalars['Int']>;
  offset?: Maybe<Scalars['Int']>;
  order_by?: Maybe<Array<ArtistSimilarOrderBy>>;
  where?: Maybe<ArtistSimilarBoolExp>;
};


/** subscription root */
export type SubscriptionRootArtistSimilarByPkArgs = {
  artist1_id: Scalars['uuid'];
  artist2_id: Scalars['uuid'];
};


/** subscription root */
export type SubscriptionRootArtistStyleArgs = {
  distinct_on?: Maybe<Array<ArtistStyleSelectColumn>>;
  limit?: Maybe<Scalars['Int']>;
  offset?: Maybe<Scalars['Int']>;
  order_by?: Maybe<Array<ArtistStyleOrderBy>>;
  where?: Maybe<ArtistStyleBoolExp>;
};


/** subscription root */
export type SubscriptionRootArtistStyleAggregateArgs = {
  distinct_on?: Maybe<Array<ArtistStyleSelectColumn>>;
  limit?: Maybe<Scalars['Int']>;
  offset?: Maybe<Scalars['Int']>;
  order_by?: Maybe<Array<ArtistStyleOrderBy>>;
  where?: Maybe<ArtistStyleBoolExp>;
};


/** subscription root */
export type SubscriptionRootArtistStyleByPkArgs = {
  artist_id: Scalars['uuid'];
  index: Scalars['Int'];
};


/** subscription root */
export type SubscriptionRootCollectionArgs = {
  distinct_on?: Maybe<Array<CollectionSelectColumn>>;
  limit?: Maybe<Scalars['Int']>;
  offset?: Maybe<Scalars['Int']>;
  order_by?: Maybe<Array<CollectionOrderBy>>;
  where?: Maybe<CollectionBoolExp>;
};


/** subscription root */
export type SubscriptionRootCollectionAggregateArgs = {
  distinct_on?: Maybe<Array<CollectionSelectColumn>>;
  limit?: Maybe<Scalars['Int']>;
  offset?: Maybe<Scalars['Int']>;
  order_by?: Maybe<Array<CollectionOrderBy>>;
  where?: Maybe<CollectionBoolExp>;
};


/** subscription root */
export type SubscriptionRootCollectionByPkArgs = {
  id: Scalars['uuid'];
};


/** subscription root */
export type SubscriptionRootCountryArgs = {
  distinct_on?: Maybe<Array<CountrySelectColumn>>;
  limit?: Maybe<Scalars['Int']>;
  offset?: Maybe<Scalars['Int']>;
  order_by?: Maybe<Array<CountryOrderBy>>;
  where?: Maybe<CountryBoolExp>;
};


/** subscription root */
export type SubscriptionRootCountryAggregateArgs = {
  distinct_on?: Maybe<Array<CountrySelectColumn>>;
  limit?: Maybe<Scalars['Int']>;
  offset?: Maybe<Scalars['Int']>;
  order_by?: Maybe<Array<CountryOrderBy>>;
  where?: Maybe<CountryBoolExp>;
};


/** subscription root */
export type SubscriptionRootCountryByPkArgs = {
  code: Scalars['String'];
};


/** subscription root */
export type SubscriptionRootEpisodeArgs = {
  distinct_on?: Maybe<Array<EpisodeSelectColumn>>;
  limit?: Maybe<Scalars['Int']>;
  offset?: Maybe<Scalars['Int']>;
  order_by?: Maybe<Array<EpisodeOrderBy>>;
  where?: Maybe<EpisodeBoolExp>;
};


/** subscription root */
export type SubscriptionRootEpisodeAggregateArgs = {
  distinct_on?: Maybe<Array<EpisodeSelectColumn>>;
  limit?: Maybe<Scalars['Int']>;
  offset?: Maybe<Scalars['Int']>;
  order_by?: Maybe<Array<EpisodeOrderBy>>;
  where?: Maybe<EpisodeBoolExp>;
};


/** subscription root */
export type SubscriptionRootEpisodeByPkArgs = {
  id: Scalars['uuid'];
};


/** subscription root */
export type SubscriptionRootEpisodeStaffArgs = {
  distinct_on?: Maybe<Array<EpisodeStaffSelectColumn>>;
  limit?: Maybe<Scalars['Int']>;
  offset?: Maybe<Scalars['Int']>;
  order_by?: Maybe<Array<EpisodeStaffOrderBy>>;
  where?: Maybe<EpisodeStaffBoolExp>;
};


/** subscription root */
export type SubscriptionRootEpisodeStaffAggregateArgs = {
  distinct_on?: Maybe<Array<EpisodeStaffSelectColumn>>;
  limit?: Maybe<Scalars['Int']>;
  offset?: Maybe<Scalars['Int']>;
  order_by?: Maybe<Array<EpisodeStaffOrderBy>>;
  where?: Maybe<EpisodeStaffBoolExp>;
};


/** subscription root */
export type SubscriptionRootEpisodeStaffByPkArgs = {
  id: Scalars['uuid'];
};


/** subscription root */
export type SubscriptionRootGenreArgs = {
  distinct_on?: Maybe<Array<GenreSelectColumn>>;
  limit?: Maybe<Scalars['Int']>;
  offset?: Maybe<Scalars['Int']>;
  order_by?: Maybe<Array<GenreOrderBy>>;
  where?: Maybe<GenreBoolExp>;
};


/** subscription root */
export type SubscriptionRootGenreAggregateArgs = {
  distinct_on?: Maybe<Array<GenreSelectColumn>>;
  limit?: Maybe<Scalars['Int']>;
  offset?: Maybe<Scalars['Int']>;
  order_by?: Maybe<Array<GenreOrderBy>>;
  where?: Maybe<GenreBoolExp>;
};


/** subscription root */
export type SubscriptionRootGenreByPkArgs = {
  id: Scalars['uuid'];
};


/** subscription root */
export type SubscriptionRootMoodArgs = {
  distinct_on?: Maybe<Array<MoodSelectColumn>>;
  limit?: Maybe<Scalars['Int']>;
  offset?: Maybe<Scalars['Int']>;
  order_by?: Maybe<Array<MoodOrderBy>>;
  where?: Maybe<MoodBoolExp>;
};


/** subscription root */
export type SubscriptionRootMoodAggregateArgs = {
  distinct_on?: Maybe<Array<MoodSelectColumn>>;
  limit?: Maybe<Scalars['Int']>;
  offset?: Maybe<Scalars['Int']>;
  order_by?: Maybe<Array<MoodOrderBy>>;
  where?: Maybe<MoodBoolExp>;
};


/** subscription root */
export type SubscriptionRootMoodByPkArgs = {
  id: Scalars['uuid'];
};


/** subscription root */
export type SubscriptionRootMovieArgs = {
  distinct_on?: Maybe<Array<MovieSelectColumn>>;
  limit?: Maybe<Scalars['Int']>;
  offset?: Maybe<Scalars['Int']>;
  order_by?: Maybe<Array<MovieOrderBy>>;
  where?: Maybe<MovieBoolExp>;
};


/** subscription root */
export type SubscriptionRootMovieAggregateArgs = {
  distinct_on?: Maybe<Array<MovieSelectColumn>>;
  limit?: Maybe<Scalars['Int']>;
  offset?: Maybe<Scalars['Int']>;
  order_by?: Maybe<Array<MovieOrderBy>>;
  where?: Maybe<MovieBoolExp>;
};


/** subscription root */
export type SubscriptionRootMovieByPkArgs = {
  id: Scalars['uuid'];
};


/** subscription root */
export type SubscriptionRootMovieCollectionArgs = {
  distinct_on?: Maybe<Array<MovieCollectionSelectColumn>>;
  limit?: Maybe<Scalars['Int']>;
  offset?: Maybe<Scalars['Int']>;
  order_by?: Maybe<Array<MovieCollectionOrderBy>>;
  where?: Maybe<MovieCollectionBoolExp>;
};


/** subscription root */
export type SubscriptionRootMovieCollectionAggregateArgs = {
  distinct_on?: Maybe<Array<MovieCollectionSelectColumn>>;
  limit?: Maybe<Scalars['Int']>;
  offset?: Maybe<Scalars['Int']>;
  order_by?: Maybe<Array<MovieCollectionOrderBy>>;
  where?: Maybe<MovieCollectionBoolExp>;
};


/** subscription root */
export type SubscriptionRootMovieCollectionByPkArgs = {
  index: Scalars['Int'];
  movie_id: Scalars['uuid'];
};


/** subscription root */
export type SubscriptionRootMovieCountryArgs = {
  distinct_on?: Maybe<Array<MovieCountrySelectColumn>>;
  limit?: Maybe<Scalars['Int']>;
  offset?: Maybe<Scalars['Int']>;
  order_by?: Maybe<Array<MovieCountryOrderBy>>;
  where?: Maybe<MovieCountryBoolExp>;
};


/** subscription root */
export type SubscriptionRootMovieCountryAggregateArgs = {
  distinct_on?: Maybe<Array<MovieCountrySelectColumn>>;
  limit?: Maybe<Scalars['Int']>;
  offset?: Maybe<Scalars['Int']>;
  order_by?: Maybe<Array<MovieCountryOrderBy>>;
  where?: Maybe<MovieCountryBoolExp>;
};


/** subscription root */
export type SubscriptionRootMovieCountryByPkArgs = {
  index: Scalars['Int'];
  movie_id: Scalars['uuid'];
};


/** subscription root */
export type SubscriptionRootMovieGenreArgs = {
  distinct_on?: Maybe<Array<MovieGenreSelectColumn>>;
  limit?: Maybe<Scalars['Int']>;
  offset?: Maybe<Scalars['Int']>;
  order_by?: Maybe<Array<MovieGenreOrderBy>>;
  where?: Maybe<MovieGenreBoolExp>;
};


/** subscription root */
export type SubscriptionRootMovieGenreAggregateArgs = {
  distinct_on?: Maybe<Array<MovieGenreSelectColumn>>;
  limit?: Maybe<Scalars['Int']>;
  offset?: Maybe<Scalars['Int']>;
  order_by?: Maybe<Array<MovieGenreOrderBy>>;
  where?: Maybe<MovieGenreBoolExp>;
};


/** subscription root */
export type SubscriptionRootMovieGenreByPkArgs = {
  index: Scalars['Int'];
  movie_id: Scalars['uuid'];
};


/** subscription root */
export type SubscriptionRootMovieStaffArgs = {
  distinct_on?: Maybe<Array<MovieStaffSelectColumn>>;
  limit?: Maybe<Scalars['Int']>;
  offset?: Maybe<Scalars['Int']>;
  order_by?: Maybe<Array<MovieStaffOrderBy>>;
  where?: Maybe<MovieStaffBoolExp>;
};


/** subscription root */
export type SubscriptionRootMovieStaffAggregateArgs = {
  distinct_on?: Maybe<Array<MovieStaffSelectColumn>>;
  limit?: Maybe<Scalars['Int']>;
  offset?: Maybe<Scalars['Int']>;
  order_by?: Maybe<Array<MovieStaffOrderBy>>;
  where?: Maybe<MovieStaffBoolExp>;
};


/** subscription root */
export type SubscriptionRootMovieStaffByPkArgs = {
  id: Scalars['uuid'];
};


/** subscription root */
export type SubscriptionRootMovieStudioArgs = {
  distinct_on?: Maybe<Array<MovieStudioSelectColumn>>;
  limit?: Maybe<Scalars['Int']>;
  offset?: Maybe<Scalars['Int']>;
  order_by?: Maybe<Array<MovieStudioOrderBy>>;
  where?: Maybe<MovieStudioBoolExp>;
};


/** subscription root */
export type SubscriptionRootMovieStudioAggregateArgs = {
  distinct_on?: Maybe<Array<MovieStudioSelectColumn>>;
  limit?: Maybe<Scalars['Int']>;
  offset?: Maybe<Scalars['Int']>;
  order_by?: Maybe<Array<MovieStudioOrderBy>>;
  where?: Maybe<MovieStudioBoolExp>;
};


/** subscription root */
export type SubscriptionRootMovieStudioByPkArgs = {
  index: Scalars['Int'];
  movie_id: Scalars['uuid'];
};


/** subscription root */
export type SubscriptionRootPersonArgs = {
  distinct_on?: Maybe<Array<PersonSelectColumn>>;
  limit?: Maybe<Scalars['Int']>;
  offset?: Maybe<Scalars['Int']>;
  order_by?: Maybe<Array<PersonOrderBy>>;
  where?: Maybe<PersonBoolExp>;
};


/** subscription root */
export type SubscriptionRootPersonAggregateArgs = {
  distinct_on?: Maybe<Array<PersonSelectColumn>>;
  limit?: Maybe<Scalars['Int']>;
  offset?: Maybe<Scalars['Int']>;
  order_by?: Maybe<Array<PersonOrderBy>>;
  where?: Maybe<PersonBoolExp>;
};


/** subscription root */
export type SubscriptionRootPersonByPkArgs = {
  id: Scalars['uuid'];
};


/** subscription root */
export type SubscriptionRootShowArgs = {
  distinct_on?: Maybe<Array<ShowSelectColumn>>;
  limit?: Maybe<Scalars['Int']>;
  offset?: Maybe<Scalars['Int']>;
  order_by?: Maybe<Array<ShowOrderBy>>;
  where?: Maybe<ShowBoolExp>;
};


/** subscription root */
export type SubscriptionRootShowAggregateArgs = {
  distinct_on?: Maybe<Array<ShowSelectColumn>>;
  limit?: Maybe<Scalars['Int']>;
  offset?: Maybe<Scalars['Int']>;
  order_by?: Maybe<Array<ShowOrderBy>>;
  where?: Maybe<ShowBoolExp>;
};


/** subscription root */
export type SubscriptionRootShowByPkArgs = {
  id: Scalars['uuid'];
};


/** subscription root */
export type SubscriptionRootShowCollectionArgs = {
  distinct_on?: Maybe<Array<ShowCollectionSelectColumn>>;
  limit?: Maybe<Scalars['Int']>;
  offset?: Maybe<Scalars['Int']>;
  order_by?: Maybe<Array<ShowCollectionOrderBy>>;
  where?: Maybe<ShowCollectionBoolExp>;
};


/** subscription root */
export type SubscriptionRootShowCollectionAggregateArgs = {
  distinct_on?: Maybe<Array<ShowCollectionSelectColumn>>;
  limit?: Maybe<Scalars['Int']>;
  offset?: Maybe<Scalars['Int']>;
  order_by?: Maybe<Array<ShowCollectionOrderBy>>;
  where?: Maybe<ShowCollectionBoolExp>;
};


/** subscription root */
export type SubscriptionRootShowCollectionByPkArgs = {
  index: Scalars['Int'];
  show_id: Scalars['uuid'];
};


/** subscription root */
export type SubscriptionRootShowGenreArgs = {
  distinct_on?: Maybe<Array<ShowGenreSelectColumn>>;
  limit?: Maybe<Scalars['Int']>;
  offset?: Maybe<Scalars['Int']>;
  order_by?: Maybe<Array<ShowGenreOrderBy>>;
  where?: Maybe<ShowGenreBoolExp>;
};


/** subscription root */
export type SubscriptionRootShowGenreAggregateArgs = {
  distinct_on?: Maybe<Array<ShowGenreSelectColumn>>;
  limit?: Maybe<Scalars['Int']>;
  offset?: Maybe<Scalars['Int']>;
  order_by?: Maybe<Array<ShowGenreOrderBy>>;
  where?: Maybe<ShowGenreBoolExp>;
};


/** subscription root */
export type SubscriptionRootShowGenreByPkArgs = {
  index: Scalars['Int'];
  show_id: Scalars['uuid'];
};


/** subscription root */
export type SubscriptionRootShowSeasonArgs = {
  distinct_on?: Maybe<Array<ShowSeasonSelectColumn>>;
  limit?: Maybe<Scalars['Int']>;
  offset?: Maybe<Scalars['Int']>;
  order_by?: Maybe<Array<ShowSeasonOrderBy>>;
  where?: Maybe<ShowSeasonBoolExp>;
};


/** subscription root */
export type SubscriptionRootShowSeasonAggregateArgs = {
  distinct_on?: Maybe<Array<ShowSeasonSelectColumn>>;
  limit?: Maybe<Scalars['Int']>;
  offset?: Maybe<Scalars['Int']>;
  order_by?: Maybe<Array<ShowSeasonOrderBy>>;
  where?: Maybe<ShowSeasonBoolExp>;
};


/** subscription root */
export type SubscriptionRootShowSeasonByPkArgs = {
  id: Scalars['uuid'];
};


/** subscription root */
export type SubscriptionRootShowStaffArgs = {
  distinct_on?: Maybe<Array<ShowStaffSelectColumn>>;
  limit?: Maybe<Scalars['Int']>;
  offset?: Maybe<Scalars['Int']>;
  order_by?: Maybe<Array<ShowStaffOrderBy>>;
  where?: Maybe<ShowStaffBoolExp>;
};


/** subscription root */
export type SubscriptionRootShowStaffAggregateArgs = {
  distinct_on?: Maybe<Array<ShowStaffSelectColumn>>;
  limit?: Maybe<Scalars['Int']>;
  offset?: Maybe<Scalars['Int']>;
  order_by?: Maybe<Array<ShowStaffOrderBy>>;
  where?: Maybe<ShowStaffBoolExp>;
};


/** subscription root */
export type SubscriptionRootShowStaffByPkArgs = {
  id: Scalars['uuid'];
};


/** subscription root */
export type SubscriptionRootStaffArgs = {
  distinct_on?: Maybe<Array<StaffSelectColumn>>;
  limit?: Maybe<Scalars['Int']>;
  offset?: Maybe<Scalars['Int']>;
  order_by?: Maybe<Array<StaffOrderBy>>;
  where?: Maybe<StaffBoolExp>;
};


/** subscription root */
export type SubscriptionRootStaffAggregateArgs = {
  distinct_on?: Maybe<Array<StaffSelectColumn>>;
  limit?: Maybe<Scalars['Int']>;
  offset?: Maybe<Scalars['Int']>;
  order_by?: Maybe<Array<StaffOrderBy>>;
  where?: Maybe<StaffBoolExp>;
};


/** subscription root */
export type SubscriptionRootStaffByPkArgs = {
  id: Scalars['uuid'];
};


/** subscription root */
export type SubscriptionRootStudioArgs = {
  distinct_on?: Maybe<Array<StudioSelectColumn>>;
  limit?: Maybe<Scalars['Int']>;
  offset?: Maybe<Scalars['Int']>;
  order_by?: Maybe<Array<StudioOrderBy>>;
  where?: Maybe<StudioBoolExp>;
};


/** subscription root */
export type SubscriptionRootStudioAggregateArgs = {
  distinct_on?: Maybe<Array<StudioSelectColumn>>;
  limit?: Maybe<Scalars['Int']>;
  offset?: Maybe<Scalars['Int']>;
  order_by?: Maybe<Array<StudioOrderBy>>;
  where?: Maybe<StudioBoolExp>;
};


/** subscription root */
export type SubscriptionRootStudioByPkArgs = {
  id: Scalars['uuid'];
};


/** subscription root */
export type SubscriptionRootStyleArgs = {
  distinct_on?: Maybe<Array<StyleSelectColumn>>;
  limit?: Maybe<Scalars['Int']>;
  offset?: Maybe<Scalars['Int']>;
  order_by?: Maybe<Array<StyleOrderBy>>;
  where?: Maybe<StyleBoolExp>;
};


/** subscription root */
export type SubscriptionRootStyleAggregateArgs = {
  distinct_on?: Maybe<Array<StyleSelectColumn>>;
  limit?: Maybe<Scalars['Int']>;
  offset?: Maybe<Scalars['Int']>;
  order_by?: Maybe<Array<StyleOrderBy>>;
  where?: Maybe<StyleBoolExp>;
};


/** subscription root */
export type SubscriptionRootStyleByPkArgs = {
  id: Scalars['uuid'];
};


/** subscription root */
export type SubscriptionRootTrackArgs = {
  distinct_on?: Maybe<Array<TrackSelectColumn>>;
  limit?: Maybe<Scalars['Int']>;
  offset?: Maybe<Scalars['Int']>;
  order_by?: Maybe<Array<TrackOrderBy>>;
  where?: Maybe<TrackBoolExp>;
};


/** subscription root */
export type SubscriptionRootTrackAggregateArgs = {
  distinct_on?: Maybe<Array<TrackSelectColumn>>;
  limit?: Maybe<Scalars['Int']>;
  offset?: Maybe<Scalars['Int']>;
  order_by?: Maybe<Array<TrackOrderBy>>;
  where?: Maybe<TrackBoolExp>;
};


/** subscription root */
export type SubscriptionRootTrackByPkArgs = {
  id: Scalars['uuid'];
};


/** subscription root */
export type SubscriptionRootTrackGenreArgs = {
  distinct_on?: Maybe<Array<TrackGenreSelectColumn>>;
  limit?: Maybe<Scalars['Int']>;
  offset?: Maybe<Scalars['Int']>;
  order_by?: Maybe<Array<TrackGenreOrderBy>>;
  where?: Maybe<TrackGenreBoolExp>;
};


/** subscription root */
export type SubscriptionRootTrackGenreAggregateArgs = {
  distinct_on?: Maybe<Array<TrackGenreSelectColumn>>;
  limit?: Maybe<Scalars['Int']>;
  offset?: Maybe<Scalars['Int']>;
  order_by?: Maybe<Array<TrackGenreOrderBy>>;
  where?: Maybe<TrackGenreBoolExp>;
};


/** subscription root */
export type SubscriptionRootTrackGenreByPkArgs = {
  index: Scalars['Int'];
  track_id: Scalars['uuid'];
};


/** subscription root */
export type SubscriptionRootTrackMoodArgs = {
  distinct_on?: Maybe<Array<TrackMoodSelectColumn>>;
  limit?: Maybe<Scalars['Int']>;
  offset?: Maybe<Scalars['Int']>;
  order_by?: Maybe<Array<TrackMoodOrderBy>>;
  where?: Maybe<TrackMoodBoolExp>;
};


/** subscription root */
export type SubscriptionRootTrackMoodAggregateArgs = {
  distinct_on?: Maybe<Array<TrackMoodSelectColumn>>;
  limit?: Maybe<Scalars['Int']>;
  offset?: Maybe<Scalars['Int']>;
  order_by?: Maybe<Array<TrackMoodOrderBy>>;
  where?: Maybe<TrackMoodBoolExp>;
};


/** subscription root */
export type SubscriptionRootTrackMoodByPkArgs = {
  index: Scalars['Int'];
  track_id: Scalars['uuid'];
};

/** columns and relationships of "track" */
export type Track = {
  /** An array relationship */
  album_tracks: Array<AlbumTrack>;
  /** An aggregated array relationship */
  album_tracks_aggregate: AlbumTrackAggregate;
  id: Scalars['uuid'];
  name: Scalars['String'];
  name_sort?: Maybe<Scalars['String']>;
  rating?: Maybe<Scalars['numeric']>;
  /** An array relationship */
  track_genres: Array<TrackGenre>;
  /** An aggregated array relationship */
  track_genres_aggregate: TrackGenreAggregate;
  /** An array relationship */
  track_moods: Array<TrackMood>;
  /** An aggregated array relationship */
  track_moods_aggregate: TrackMoodAggregate;
};


/** columns and relationships of "track" */
export type TrackAlbumTracksArgs = {
  distinct_on?: Maybe<Array<AlbumTrackSelectColumn>>;
  limit?: Maybe<Scalars['Int']>;
  offset?: Maybe<Scalars['Int']>;
  order_by?: Maybe<Array<AlbumTrackOrderBy>>;
  where?: Maybe<AlbumTrackBoolExp>;
};


/** columns and relationships of "track" */
export type TrackAlbumTracksAggregateArgs = {
  distinct_on?: Maybe<Array<AlbumTrackSelectColumn>>;
  limit?: Maybe<Scalars['Int']>;
  offset?: Maybe<Scalars['Int']>;
  order_by?: Maybe<Array<AlbumTrackOrderBy>>;
  where?: Maybe<AlbumTrackBoolExp>;
};


/** columns and relationships of "track" */
export type TrackTrackGenresArgs = {
  distinct_on?: Maybe<Array<TrackGenreSelectColumn>>;
  limit?: Maybe<Scalars['Int']>;
  offset?: Maybe<Scalars['Int']>;
  order_by?: Maybe<Array<TrackGenreOrderBy>>;
  where?: Maybe<TrackGenreBoolExp>;
};


/** columns and relationships of "track" */
export type TrackTrackGenresAggregateArgs = {
  distinct_on?: Maybe<Array<TrackGenreSelectColumn>>;
  limit?: Maybe<Scalars['Int']>;
  offset?: Maybe<Scalars['Int']>;
  order_by?: Maybe<Array<TrackGenreOrderBy>>;
  where?: Maybe<TrackGenreBoolExp>;
};


/** columns and relationships of "track" */
export type TrackTrackMoodsArgs = {
  distinct_on?: Maybe<Array<TrackMoodSelectColumn>>;
  limit?: Maybe<Scalars['Int']>;
  offset?: Maybe<Scalars['Int']>;
  order_by?: Maybe<Array<TrackMoodOrderBy>>;
  where?: Maybe<TrackMoodBoolExp>;
};


/** columns and relationships of "track" */
export type TrackTrackMoodsAggregateArgs = {
  distinct_on?: Maybe<Array<TrackMoodSelectColumn>>;
  limit?: Maybe<Scalars['Int']>;
  offset?: Maybe<Scalars['Int']>;
  order_by?: Maybe<Array<TrackMoodOrderBy>>;
  where?: Maybe<TrackMoodBoolExp>;
};

/** aggregated selection of "track" */
export type TrackAggregate = {
  aggregate?: Maybe<TrackAggregateFields>;
  nodes: Array<Track>;
};

/** aggregate fields of "track" */
export type TrackAggregateFields = {
  avg?: Maybe<TrackAvgFields>;
  count?: Maybe<Scalars['Int']>;
  max?: Maybe<TrackMaxFields>;
  min?: Maybe<TrackMinFields>;
  stddev?: Maybe<TrackStddevFields>;
  stddev_pop?: Maybe<TrackStddevPopFields>;
  stddev_samp?: Maybe<TrackStddevSampFields>;
  sum?: Maybe<TrackSumFields>;
  var_pop?: Maybe<TrackVarPopFields>;
  var_samp?: Maybe<TrackVarSampFields>;
  variance?: Maybe<TrackVarianceFields>;
};


/** aggregate fields of "track" */
export type TrackAggregateFieldsCountArgs = {
  columns?: Maybe<Array<TrackSelectColumn>>;
  distinct?: Maybe<Scalars['Boolean']>;
};

/** order by aggregate values of table "track" */
export type TrackAggregateOrderBy = {
  avg?: Maybe<TrackAvgOrderBy>;
  count?: Maybe<OrderBy>;
  max?: Maybe<TrackMaxOrderBy>;
  min?: Maybe<TrackMinOrderBy>;
  stddev?: Maybe<TrackStddevOrderBy>;
  stddev_pop?: Maybe<TrackStddevPopOrderBy>;
  stddev_samp?: Maybe<TrackStddevSampOrderBy>;
  sum?: Maybe<TrackSumOrderBy>;
  var_pop?: Maybe<TrackVarPopOrderBy>;
  var_samp?: Maybe<TrackVarSampOrderBy>;
  variance?: Maybe<TrackVarianceOrderBy>;
};

/** input type for inserting array relation for remote table "track" */
export type TrackArrRelInsertInput = {
  data: Array<TrackInsertInput>;
  on_conflict?: Maybe<TrackOnConflict>;
};

/** aggregate avg on columns */
export type TrackAvgFields = {
  rating?: Maybe<Scalars['Float']>;
};

/** order by avg() on columns of table "track" */
export type TrackAvgOrderBy = {
  rating?: Maybe<OrderBy>;
};

/** Boolean expression to filter rows from the table "track". All fields are combined with a logical 'AND'. */
export type TrackBoolExp = {
  _and?: Maybe<Array<Maybe<TrackBoolExp>>>;
  _not?: Maybe<TrackBoolExp>;
  _or?: Maybe<Array<Maybe<TrackBoolExp>>>;
  album_tracks?: Maybe<AlbumTrackBoolExp>;
  id?: Maybe<UuidComparisonExp>;
  name?: Maybe<StringComparisonExp>;
  name_sort?: Maybe<StringComparisonExp>;
  rating?: Maybe<NumericComparisonExp>;
  track_genres?: Maybe<TrackGenreBoolExp>;
  track_moods?: Maybe<TrackMoodBoolExp>;
};

/** unique or primary key constraints on table "track" */
export enum TrackConstraint {
  /** unique or primary key constraint */
  TrackPkey = 'track_pkey'
}

/** columns and relationships of "track_genre" */
export type TrackGenre = {
  /** An object relationship */
  genre: Genre;
  genre_id: Scalars['uuid'];
  index: Scalars['Int'];
  /** An object relationship */
  track: Track;
  track_id: Scalars['uuid'];
};

/** aggregated selection of "track_genre" */
export type TrackGenreAggregate = {
  aggregate?: Maybe<TrackGenreAggregateFields>;
  nodes: Array<TrackGenre>;
};

/** aggregate fields of "track_genre" */
export type TrackGenreAggregateFields = {
  avg?: Maybe<TrackGenreAvgFields>;
  count?: Maybe<Scalars['Int']>;
  max?: Maybe<TrackGenreMaxFields>;
  min?: Maybe<TrackGenreMinFields>;
  stddev?: Maybe<TrackGenreStddevFields>;
  stddev_pop?: Maybe<TrackGenreStddevPopFields>;
  stddev_samp?: Maybe<TrackGenreStddevSampFields>;
  sum?: Maybe<TrackGenreSumFields>;
  var_pop?: Maybe<TrackGenreVarPopFields>;
  var_samp?: Maybe<TrackGenreVarSampFields>;
  variance?: Maybe<TrackGenreVarianceFields>;
};


/** aggregate fields of "track_genre" */
export type TrackGenreAggregateFieldsCountArgs = {
  columns?: Maybe<Array<TrackGenreSelectColumn>>;
  distinct?: Maybe<Scalars['Boolean']>;
};

/** order by aggregate values of table "track_genre" */
export type TrackGenreAggregateOrderBy = {
  avg?: Maybe<TrackGenreAvgOrderBy>;
  count?: Maybe<OrderBy>;
  max?: Maybe<TrackGenreMaxOrderBy>;
  min?: Maybe<TrackGenreMinOrderBy>;
  stddev?: Maybe<TrackGenreStddevOrderBy>;
  stddev_pop?: Maybe<TrackGenreStddevPopOrderBy>;
  stddev_samp?: Maybe<TrackGenreStddevSampOrderBy>;
  sum?: Maybe<TrackGenreSumOrderBy>;
  var_pop?: Maybe<TrackGenreVarPopOrderBy>;
  var_samp?: Maybe<TrackGenreVarSampOrderBy>;
  variance?: Maybe<TrackGenreVarianceOrderBy>;
};

/** input type for inserting array relation for remote table "track_genre" */
export type TrackGenreArrRelInsertInput = {
  data: Array<TrackGenreInsertInput>;
  on_conflict?: Maybe<TrackGenreOnConflict>;
};

/** aggregate avg on columns */
export type TrackGenreAvgFields = {
  index?: Maybe<Scalars['Float']>;
};

/** order by avg() on columns of table "track_genre" */
export type TrackGenreAvgOrderBy = {
  index?: Maybe<OrderBy>;
};

/** Boolean expression to filter rows from the table "track_genre". All fields are combined with a logical 'AND'. */
export type TrackGenreBoolExp = {
  _and?: Maybe<Array<Maybe<TrackGenreBoolExp>>>;
  _not?: Maybe<TrackGenreBoolExp>;
  _or?: Maybe<Array<Maybe<TrackGenreBoolExp>>>;
  genre?: Maybe<GenreBoolExp>;
  genre_id?: Maybe<UuidComparisonExp>;
  index?: Maybe<IntComparisonExp>;
  track?: Maybe<TrackBoolExp>;
  track_id?: Maybe<UuidComparisonExp>;
};

/** unique or primary key constraints on table "track_genre" */
export enum TrackGenreConstraint {
  /** unique or primary key constraint */
  TrackGenrePkey = 'track_genre_pkey',
  /** unique or primary key constraint */
  TrackGenreTrackIdGenreIdKey = 'track_genre_track_id_genre_id_key'
}

/** input type for incrementing integer column in table "track_genre" */
export type TrackGenreIncInput = {
  index?: Maybe<Scalars['Int']>;
};

/** input type for inserting data into table "track_genre" */
export type TrackGenreInsertInput = {
  genre?: Maybe<GenreObjRelInsertInput>;
  genre_id?: Maybe<Scalars['uuid']>;
  index?: Maybe<Scalars['Int']>;
  track?: Maybe<TrackObjRelInsertInput>;
  track_id?: Maybe<Scalars['uuid']>;
};

/** aggregate max on columns */
export type TrackGenreMaxFields = {
  genre_id?: Maybe<Scalars['uuid']>;
  index?: Maybe<Scalars['Int']>;
  track_id?: Maybe<Scalars['uuid']>;
};

/** order by max() on columns of table "track_genre" */
export type TrackGenreMaxOrderBy = {
  genre_id?: Maybe<OrderBy>;
  index?: Maybe<OrderBy>;
  track_id?: Maybe<OrderBy>;
};

/** aggregate min on columns */
export type TrackGenreMinFields = {
  genre_id?: Maybe<Scalars['uuid']>;
  index?: Maybe<Scalars['Int']>;
  track_id?: Maybe<Scalars['uuid']>;
};

/** order by min() on columns of table "track_genre" */
export type TrackGenreMinOrderBy = {
  genre_id?: Maybe<OrderBy>;
  index?: Maybe<OrderBy>;
  track_id?: Maybe<OrderBy>;
};

/** response of any mutation on the table "track_genre" */
export type TrackGenreMutationResponse = {
  /** number of affected rows by the mutation */
  affected_rows: Scalars['Int'];
  /** data of the affected rows by the mutation */
  returning: Array<TrackGenre>;
};

/** input type for inserting object relation for remote table "track_genre" */
export type TrackGenreObjRelInsertInput = {
  data: TrackGenreInsertInput;
  on_conflict?: Maybe<TrackGenreOnConflict>;
};

/** on conflict condition type for table "track_genre" */
export type TrackGenreOnConflict = {
  constraint: TrackGenreConstraint;
  update_columns: Array<TrackGenreUpdateColumn>;
  where?: Maybe<TrackGenreBoolExp>;
};

/** ordering options when selecting data from "track_genre" */
export type TrackGenreOrderBy = {
  genre?: Maybe<GenreOrderBy>;
  genre_id?: Maybe<OrderBy>;
  index?: Maybe<OrderBy>;
  track?: Maybe<TrackOrderBy>;
  track_id?: Maybe<OrderBy>;
};

/** primary key columns input for table: "track_genre" */
export type TrackGenrePkColumnsInput = {
  index: Scalars['Int'];
  track_id: Scalars['uuid'];
};

/** select columns of table "track_genre" */
export enum TrackGenreSelectColumn {
  /** column name */
  GenreId = 'genre_id',
  /** column name */
  Index = 'index',
  /** column name */
  TrackId = 'track_id'
}

/** input type for updating data in table "track_genre" */
export type TrackGenreSetInput = {
  genre_id?: Maybe<Scalars['uuid']>;
  index?: Maybe<Scalars['Int']>;
  track_id?: Maybe<Scalars['uuid']>;
};

/** aggregate stddev on columns */
export type TrackGenreStddevFields = {
  index?: Maybe<Scalars['Float']>;
};

/** order by stddev() on columns of table "track_genre" */
export type TrackGenreStddevOrderBy = {
  index?: Maybe<OrderBy>;
};

/** aggregate stddev_pop on columns */
export type TrackGenreStddevPopFields = {
  index?: Maybe<Scalars['Float']>;
};

/** order by stddev_pop() on columns of table "track_genre" */
export type TrackGenreStddevPopOrderBy = {
  index?: Maybe<OrderBy>;
};

/** aggregate stddev_samp on columns */
export type TrackGenreStddevSampFields = {
  index?: Maybe<Scalars['Float']>;
};

/** order by stddev_samp() on columns of table "track_genre" */
export type TrackGenreStddevSampOrderBy = {
  index?: Maybe<OrderBy>;
};

/** aggregate sum on columns */
export type TrackGenreSumFields = {
  index?: Maybe<Scalars['Int']>;
};

/** order by sum() on columns of table "track_genre" */
export type TrackGenreSumOrderBy = {
  index?: Maybe<OrderBy>;
};

/** update columns of table "track_genre" */
export enum TrackGenreUpdateColumn {
  /** column name */
  GenreId = 'genre_id',
  /** column name */
  Index = 'index',
  /** column name */
  TrackId = 'track_id'
}

/** aggregate var_pop on columns */
export type TrackGenreVarPopFields = {
  index?: Maybe<Scalars['Float']>;
};

/** order by var_pop() on columns of table "track_genre" */
export type TrackGenreVarPopOrderBy = {
  index?: Maybe<OrderBy>;
};

/** aggregate var_samp on columns */
export type TrackGenreVarSampFields = {
  index?: Maybe<Scalars['Float']>;
};

/** order by var_samp() on columns of table "track_genre" */
export type TrackGenreVarSampOrderBy = {
  index?: Maybe<OrderBy>;
};

/** aggregate variance on columns */
export type TrackGenreVarianceFields = {
  index?: Maybe<Scalars['Float']>;
};

/** order by variance() on columns of table "track_genre" */
export type TrackGenreVarianceOrderBy = {
  index?: Maybe<OrderBy>;
};

/** input type for incrementing integer column in table "track" */
export type TrackIncInput = {
  rating?: Maybe<Scalars['numeric']>;
};

/** input type for inserting data into table "track" */
export type TrackInsertInput = {
  album_tracks?: Maybe<AlbumTrackArrRelInsertInput>;
  id?: Maybe<Scalars['uuid']>;
  name?: Maybe<Scalars['String']>;
  name_sort?: Maybe<Scalars['String']>;
  rating?: Maybe<Scalars['numeric']>;
  track_genres?: Maybe<TrackGenreArrRelInsertInput>;
  track_moods?: Maybe<TrackMoodArrRelInsertInput>;
};

/** aggregate max on columns */
export type TrackMaxFields = {
  id?: Maybe<Scalars['uuid']>;
  name?: Maybe<Scalars['String']>;
  name_sort?: Maybe<Scalars['String']>;
  rating?: Maybe<Scalars['numeric']>;
};

/** order by max() on columns of table "track" */
export type TrackMaxOrderBy = {
  id?: Maybe<OrderBy>;
  name?: Maybe<OrderBy>;
  name_sort?: Maybe<OrderBy>;
  rating?: Maybe<OrderBy>;
};

/** aggregate min on columns */
export type TrackMinFields = {
  id?: Maybe<Scalars['uuid']>;
  name?: Maybe<Scalars['String']>;
  name_sort?: Maybe<Scalars['String']>;
  rating?: Maybe<Scalars['numeric']>;
};

/** order by min() on columns of table "track" */
export type TrackMinOrderBy = {
  id?: Maybe<OrderBy>;
  name?: Maybe<OrderBy>;
  name_sort?: Maybe<OrderBy>;
  rating?: Maybe<OrderBy>;
};

/** columns and relationships of "track_mood" */
export type TrackMood = {
  index: Scalars['Int'];
  /** An object relationship */
  mood: Mood;
  mood_id: Scalars['uuid'];
  /** An object relationship */
  track: Track;
  track_id: Scalars['uuid'];
};

/** aggregated selection of "track_mood" */
export type TrackMoodAggregate = {
  aggregate?: Maybe<TrackMoodAggregateFields>;
  nodes: Array<TrackMood>;
};

/** aggregate fields of "track_mood" */
export type TrackMoodAggregateFields = {
  avg?: Maybe<TrackMoodAvgFields>;
  count?: Maybe<Scalars['Int']>;
  max?: Maybe<TrackMoodMaxFields>;
  min?: Maybe<TrackMoodMinFields>;
  stddev?: Maybe<TrackMoodStddevFields>;
  stddev_pop?: Maybe<TrackMoodStddevPopFields>;
  stddev_samp?: Maybe<TrackMoodStddevSampFields>;
  sum?: Maybe<TrackMoodSumFields>;
  var_pop?: Maybe<TrackMoodVarPopFields>;
  var_samp?: Maybe<TrackMoodVarSampFields>;
  variance?: Maybe<TrackMoodVarianceFields>;
};


/** aggregate fields of "track_mood" */
export type TrackMoodAggregateFieldsCountArgs = {
  columns?: Maybe<Array<TrackMoodSelectColumn>>;
  distinct?: Maybe<Scalars['Boolean']>;
};

/** order by aggregate values of table "track_mood" */
export type TrackMoodAggregateOrderBy = {
  avg?: Maybe<TrackMoodAvgOrderBy>;
  count?: Maybe<OrderBy>;
  max?: Maybe<TrackMoodMaxOrderBy>;
  min?: Maybe<TrackMoodMinOrderBy>;
  stddev?: Maybe<TrackMoodStddevOrderBy>;
  stddev_pop?: Maybe<TrackMoodStddevPopOrderBy>;
  stddev_samp?: Maybe<TrackMoodStddevSampOrderBy>;
  sum?: Maybe<TrackMoodSumOrderBy>;
  var_pop?: Maybe<TrackMoodVarPopOrderBy>;
  var_samp?: Maybe<TrackMoodVarSampOrderBy>;
  variance?: Maybe<TrackMoodVarianceOrderBy>;
};

/** input type for inserting array relation for remote table "track_mood" */
export type TrackMoodArrRelInsertInput = {
  data: Array<TrackMoodInsertInput>;
  on_conflict?: Maybe<TrackMoodOnConflict>;
};

/** aggregate avg on columns */
export type TrackMoodAvgFields = {
  index?: Maybe<Scalars['Float']>;
};

/** order by avg() on columns of table "track_mood" */
export type TrackMoodAvgOrderBy = {
  index?: Maybe<OrderBy>;
};

/** Boolean expression to filter rows from the table "track_mood". All fields are combined with a logical 'AND'. */
export type TrackMoodBoolExp = {
  _and?: Maybe<Array<Maybe<TrackMoodBoolExp>>>;
  _not?: Maybe<TrackMoodBoolExp>;
  _or?: Maybe<Array<Maybe<TrackMoodBoolExp>>>;
  index?: Maybe<IntComparisonExp>;
  mood?: Maybe<MoodBoolExp>;
  mood_id?: Maybe<UuidComparisonExp>;
  track?: Maybe<TrackBoolExp>;
  track_id?: Maybe<UuidComparisonExp>;
};

/** unique or primary key constraints on table "track_mood" */
export enum TrackMoodConstraint {
  /** unique or primary key constraint */
  TrackMoodPkey = 'track_mood_pkey',
  /** unique or primary key constraint */
  TrackMoodTrackIdMoodIdKey = 'track_mood_track_id_mood_id_key'
}

/** input type for incrementing integer column in table "track_mood" */
export type TrackMoodIncInput = {
  index?: Maybe<Scalars['Int']>;
};

/** input type for inserting data into table "track_mood" */
export type TrackMoodInsertInput = {
  index?: Maybe<Scalars['Int']>;
  mood?: Maybe<MoodObjRelInsertInput>;
  mood_id?: Maybe<Scalars['uuid']>;
  track?: Maybe<TrackObjRelInsertInput>;
  track_id?: Maybe<Scalars['uuid']>;
};

/** aggregate max on columns */
export type TrackMoodMaxFields = {
  index?: Maybe<Scalars['Int']>;
  mood_id?: Maybe<Scalars['uuid']>;
  track_id?: Maybe<Scalars['uuid']>;
};

/** order by max() on columns of table "track_mood" */
export type TrackMoodMaxOrderBy = {
  index?: Maybe<OrderBy>;
  mood_id?: Maybe<OrderBy>;
  track_id?: Maybe<OrderBy>;
};

/** aggregate min on columns */
export type TrackMoodMinFields = {
  index?: Maybe<Scalars['Int']>;
  mood_id?: Maybe<Scalars['uuid']>;
  track_id?: Maybe<Scalars['uuid']>;
};

/** order by min() on columns of table "track_mood" */
export type TrackMoodMinOrderBy = {
  index?: Maybe<OrderBy>;
  mood_id?: Maybe<OrderBy>;
  track_id?: Maybe<OrderBy>;
};

/** response of any mutation on the table "track_mood" */
export type TrackMoodMutationResponse = {
  /** number of affected rows by the mutation */
  affected_rows: Scalars['Int'];
  /** data of the affected rows by the mutation */
  returning: Array<TrackMood>;
};

/** input type for inserting object relation for remote table "track_mood" */
export type TrackMoodObjRelInsertInput = {
  data: TrackMoodInsertInput;
  on_conflict?: Maybe<TrackMoodOnConflict>;
};

/** on conflict condition type for table "track_mood" */
export type TrackMoodOnConflict = {
  constraint: TrackMoodConstraint;
  update_columns: Array<TrackMoodUpdateColumn>;
  where?: Maybe<TrackMoodBoolExp>;
};

/** ordering options when selecting data from "track_mood" */
export type TrackMoodOrderBy = {
  index?: Maybe<OrderBy>;
  mood?: Maybe<MoodOrderBy>;
  mood_id?: Maybe<OrderBy>;
  track?: Maybe<TrackOrderBy>;
  track_id?: Maybe<OrderBy>;
};

/** primary key columns input for table: "track_mood" */
export type TrackMoodPkColumnsInput = {
  index: Scalars['Int'];
  track_id: Scalars['uuid'];
};

/** select columns of table "track_mood" */
export enum TrackMoodSelectColumn {
  /** column name */
  Index = 'index',
  /** column name */
  MoodId = 'mood_id',
  /** column name */
  TrackId = 'track_id'
}

/** input type for updating data in table "track_mood" */
export type TrackMoodSetInput = {
  index?: Maybe<Scalars['Int']>;
  mood_id?: Maybe<Scalars['uuid']>;
  track_id?: Maybe<Scalars['uuid']>;
};

/** aggregate stddev on columns */
export type TrackMoodStddevFields = {
  index?: Maybe<Scalars['Float']>;
};

/** order by stddev() on columns of table "track_mood" */
export type TrackMoodStddevOrderBy = {
  index?: Maybe<OrderBy>;
};

/** aggregate stddev_pop on columns */
export type TrackMoodStddevPopFields = {
  index?: Maybe<Scalars['Float']>;
};

/** order by stddev_pop() on columns of table "track_mood" */
export type TrackMoodStddevPopOrderBy = {
  index?: Maybe<OrderBy>;
};

/** aggregate stddev_samp on columns */
export type TrackMoodStddevSampFields = {
  index?: Maybe<Scalars['Float']>;
};

/** order by stddev_samp() on columns of table "track_mood" */
export type TrackMoodStddevSampOrderBy = {
  index?: Maybe<OrderBy>;
};

/** aggregate sum on columns */
export type TrackMoodSumFields = {
  index?: Maybe<Scalars['Int']>;
};

/** order by sum() on columns of table "track_mood" */
export type TrackMoodSumOrderBy = {
  index?: Maybe<OrderBy>;
};

/** update columns of table "track_mood" */
export enum TrackMoodUpdateColumn {
  /** column name */
  Index = 'index',
  /** column name */
  MoodId = 'mood_id',
  /** column name */
  TrackId = 'track_id'
}

/** aggregate var_pop on columns */
export type TrackMoodVarPopFields = {
  index?: Maybe<Scalars['Float']>;
};

/** order by var_pop() on columns of table "track_mood" */
export type TrackMoodVarPopOrderBy = {
  index?: Maybe<OrderBy>;
};

/** aggregate var_samp on columns */
export type TrackMoodVarSampFields = {
  index?: Maybe<Scalars['Float']>;
};

/** order by var_samp() on columns of table "track_mood" */
export type TrackMoodVarSampOrderBy = {
  index?: Maybe<OrderBy>;
};

/** aggregate variance on columns */
export type TrackMoodVarianceFields = {
  index?: Maybe<Scalars['Float']>;
};

/** order by variance() on columns of table "track_mood" */
export type TrackMoodVarianceOrderBy = {
  index?: Maybe<OrderBy>;
};

/** response of any mutation on the table "track" */
export type TrackMutationResponse = {
  /** number of affected rows by the mutation */
  affected_rows: Scalars['Int'];
  /** data of the affected rows by the mutation */
  returning: Array<Track>;
};

/** input type for inserting object relation for remote table "track" */
export type TrackObjRelInsertInput = {
  data: TrackInsertInput;
  on_conflict?: Maybe<TrackOnConflict>;
};

/** on conflict condition type for table "track" */
export type TrackOnConflict = {
  constraint: TrackConstraint;
  update_columns: Array<TrackUpdateColumn>;
  where?: Maybe<TrackBoolExp>;
};

/** ordering options when selecting data from "track" */
export type TrackOrderBy = {
  album_tracks_aggregate?: Maybe<AlbumTrackAggregateOrderBy>;
  id?: Maybe<OrderBy>;
  name?: Maybe<OrderBy>;
  name_sort?: Maybe<OrderBy>;
  rating?: Maybe<OrderBy>;
  track_genres_aggregate?: Maybe<TrackGenreAggregateOrderBy>;
  track_moods_aggregate?: Maybe<TrackMoodAggregateOrderBy>;
};

/** primary key columns input for table: "track" */
export type TrackPkColumnsInput = {
  id: Scalars['uuid'];
};

/** select columns of table "track" */
export enum TrackSelectColumn {
  /** column name */
  Id = 'id',
  /** column name */
  Name = 'name',
  /** column name */
  NameSort = 'name_sort',
  /** column name */
  Rating = 'rating'
}

/** input type for updating data in table "track" */
export type TrackSetInput = {
  id?: Maybe<Scalars['uuid']>;
  name?: Maybe<Scalars['String']>;
  name_sort?: Maybe<Scalars['String']>;
  rating?: Maybe<Scalars['numeric']>;
};

/** aggregate stddev on columns */
export type TrackStddevFields = {
  rating?: Maybe<Scalars['Float']>;
};

/** order by stddev() on columns of table "track" */
export type TrackStddevOrderBy = {
  rating?: Maybe<OrderBy>;
};

/** aggregate stddev_pop on columns */
export type TrackStddevPopFields = {
  rating?: Maybe<Scalars['Float']>;
};

/** order by stddev_pop() on columns of table "track" */
export type TrackStddevPopOrderBy = {
  rating?: Maybe<OrderBy>;
};

/** aggregate stddev_samp on columns */
export type TrackStddevSampFields = {
  rating?: Maybe<Scalars['Float']>;
};

/** order by stddev_samp() on columns of table "track" */
export type TrackStddevSampOrderBy = {
  rating?: Maybe<OrderBy>;
};

/** aggregate sum on columns */
export type TrackSumFields = {
  rating?: Maybe<Scalars['numeric']>;
};

/** order by sum() on columns of table "track" */
export type TrackSumOrderBy = {
  rating?: Maybe<OrderBy>;
};

/** update columns of table "track" */
export enum TrackUpdateColumn {
  /** column name */
  Id = 'id',
  /** column name */
  Name = 'name',
  /** column name */
  NameSort = 'name_sort',
  /** column name */
  Rating = 'rating'
}

/** aggregate var_pop on columns */
export type TrackVarPopFields = {
  rating?: Maybe<Scalars['Float']>;
};

/** order by var_pop() on columns of table "track" */
export type TrackVarPopOrderBy = {
  rating?: Maybe<OrderBy>;
};

/** aggregate var_samp on columns */
export type TrackVarSampFields = {
  rating?: Maybe<Scalars['Float']>;
};

/** order by var_samp() on columns of table "track" */
export type TrackVarSampOrderBy = {
  rating?: Maybe<OrderBy>;
};

/** aggregate variance on columns */
export type TrackVarianceFields = {
  rating?: Maybe<Scalars['Float']>;
};

/** order by variance() on columns of table "track" */
export type TrackVarianceOrderBy = {
  rating?: Maybe<OrderBy>;
};


/** expression to compare columns of type uuid. All fields are combined with logical 'AND'. */
export type UuidComparisonExp = {
  _eq?: Maybe<Scalars['uuid']>;
  _gt?: Maybe<Scalars['uuid']>;
  _gte?: Maybe<Scalars['uuid']>;
  _in?: Maybe<Array<Scalars['uuid']>>;
  _is_null?: Maybe<Scalars['Boolean']>;
  _lt?: Maybe<Scalars['uuid']>;
  _lte?: Maybe<Scalars['uuid']>;
  _neq?: Maybe<Scalars['uuid']>;
  _nin?: Maybe<Array<Scalars['uuid']>>;
};

export type InsertAlbumMutationVariables = Exact<{
  artistId: Scalars['uuid'];
  summary: Scalars['String'];
  rating?: Maybe<Scalars['numeric']>;
  nameSort?: Maybe<Scalars['String']>;
  nameMatch: Scalars['String'];
  name: Scalars['String'];
  imageCover?: Maybe<Scalars['String']>;
  imageBackground?: Maybe<Scalars['String']>;
  aired?: Maybe<Scalars['date']>;
}>;


export type InsertAlbumMutation = { insertAlbum?: Maybe<Pick<Album, 'id'>> };

export type UpdateAlbumImageCoverMutationVariables = Exact<{
  id: Scalars['uuid'];
  imageCover: Scalars['String'];
}>;


export type UpdateAlbumImageCoverMutation = { updateAlbumImage?: Maybe<Pick<Album, 'id'>> };

export type UpdateAlbumImageBackgroundMutationVariables = Exact<{
  id: Scalars['uuid'];
  imageBackground: Scalars['String'];
}>;


export type UpdateAlbumImageBackgroundMutation = { updateAlbumImage?: Maybe<Pick<Album, 'id'>> };

export type InsertArtistCollectionMutationVariables = Exact<{
  artistId: Scalars['uuid'];
  collectionId: Scalars['uuid'];
  index: Scalars['Int'];
}>;


export type InsertArtistCollectionMutation = { insertArtistCollection?: Maybe<(
    Pick<ArtistCollection, 'index'>
    & { artistId: ArtistCollection['artist_id'], collectionId: ArtistCollection['collection_id'] }
  )> };

export type InsertArtistMutationVariables = Exact<{
  personId: Scalars['uuid'];
  imageBackground?: Maybe<Scalars['String']>;
  imageCover?: Maybe<Scalars['String']>;
  summary?: Maybe<Scalars['String']>;
}>;


export type InsertArtistMutation = { insertArtist?: Maybe<Pick<Artist, 'id'>> };

export type UpdateArtistImageCoverMutationVariables = Exact<{
  id: Scalars['uuid'];
  imageCover: Scalars['String'];
}>;


export type UpdateArtistImageCoverMutation = { updateArtistImage?: Maybe<Pick<Artist, 'id'>> };

export type UpdateArtistImageBackgroundMutationVariables = Exact<{
  id: Scalars['uuid'];
  imageBackground: Scalars['String'];
}>;


export type UpdateArtistImageBackgroundMutation = { updateArtistImage?: Maybe<Pick<Artist, 'id'>> };

export type InsertCollectionMutationVariables = Exact<{
  name: Scalars['String'];
  summary: Scalars['String'];
  imageBackground?: Maybe<Scalars['String']>;
  imageCover?: Maybe<Scalars['String']>;
}>;


export type InsertCollectionMutation = { insertCollection?: Maybe<Pick<Collection, 'id'>> };

export type UpdateCollectionImageCoverMutationVariables = Exact<{
  id: Scalars['uuid'];
  imageCover: Scalars['String'];
}>;


export type UpdateCollectionImageCoverMutation = { updateCollectionImage?: Maybe<Pick<Collection, 'id'>> };

export type UpdateCollectionImageBackgroundMutationVariables = Exact<{
  id: Scalars['uuid'];
  imageBackground: Scalars['String'];
}>;


export type UpdateCollectionImageBackgroundMutation = { updateCollectionImage?: Maybe<Pick<Collection, 'id'>> };

export type InsertPersonMutationVariables = Exact<{
  name: Scalars['String'];
  nameMatch: Scalars['String'];
  nameSort?: Maybe<Scalars['String']>;
  imageCover?: Maybe<Scalars['String']>;
}>;


export type InsertPersonMutation = { insertPerson?: Maybe<Pick<Person, 'id'>> };

export type UpdatePersonImageMutationVariables = Exact<{
  id: Scalars['uuid'];
  imageCover: Scalars['String'];
}>;


export type UpdatePersonImageMutation = { updatePersonImage?: Maybe<Pick<Person, 'id'>> };

export type GetArtistCollectionQueryVariables = Exact<{
  artistId: Scalars['uuid'];
}>;


export type GetArtistCollectionQuery = { getArtistCollection?: Maybe<{ artist_collections: Array<{ collection: Pick<Collection, 'id' | 'name'> }> }> };

export type FindCollectionByNameQueryVariables = Exact<{
  name: Scalars['String'];
}>;


export type FindCollectionByNameQuery = { collection: Array<Pick<Collection, 'id' | 'name'>> };

export type FindPersonByNameMatchQueryVariables = Exact<{
  nameMatch: Scalars['String'];
}>;


export type FindPersonByNameMatchQuery = { person: Array<Pick<Person, 'id'>> };


export const InsertAlbumDocument = gql`
    mutation insertAlbum($artistId: uuid!, $summary: String!, $rating: numeric, $nameSort: String, $nameMatch: String!, $name: String!, $imageCover: String, $imageBackground: String, $aired: date) {
  insertAlbum: insert_album_one(
    object: {artist_id: $artistId, image_background: $imageBackground, image_cover: $imageCover, name: $name, name_sort: $nameSort, rating: $rating, summary: $summary, aired: $aired, name_match: $nameMatch}
  ) {
    id
  }
}
    `;
export const UpdateAlbumImageCoverDocument = gql`
    mutation updateAlbumImageCover($id: uuid!, $imageCover: String!) {
  updateAlbumImage: update_album_by_pk(
    pk_columns: {id: $id}
    _set: {image_cover: $imageCover}
  ) {
    id
  }
}
    `;
export const UpdateAlbumImageBackgroundDocument = gql`
    mutation updateAlbumImageBackground($id: uuid!, $imageBackground: String!) {
  updateAlbumImage: update_album_by_pk(
    pk_columns: {id: $id}
    _set: {image_background: $imageBackground}
  ) {
    id
  }
}
    `;
export const InsertArtistCollectionDocument = gql`
    mutation insertArtistCollection($artistId: uuid!, $collectionId: uuid!, $index: Int!) {
  insertArtistCollection: insert_artist_collection_one(
    object: {collection_id: $collectionId, index: $index, artist_id: $artistId}
  ) {
    index
    artistId: artist_id
    collectionId: collection_id
  }
}
    `;
export const InsertArtistDocument = gql`
    mutation insertArtist($personId: uuid!, $imageBackground: String, $imageCover: String, $summary: String = "") {
  insertArtist: insert_artist_one(
    object: {person_id: $personId, summary: $summary, image_cover: $imageCover, image_background: $imageBackground}
  ) {
    id
  }
}
    `;
export const UpdateArtistImageCoverDocument = gql`
    mutation updateArtistImageCover($id: uuid!, $imageCover: String!) {
  updateArtistImage: update_artist_by_pk(
    pk_columns: {id: $id}
    _set: {image_cover: $imageCover}
  ) {
    id
  }
}
    `;
export const UpdateArtistImageBackgroundDocument = gql`
    mutation updateArtistImageBackground($id: uuid!, $imageBackground: String!) {
  updateArtistImage: update_artist_by_pk(
    pk_columns: {id: $id}
    _set: {image_background: $imageBackground}
  ) {
    id
  }
}
    `;
export const InsertCollectionDocument = gql`
    mutation insertCollection($name: String!, $summary: String!, $imageBackground: String, $imageCover: String) {
  insertCollection: insert_collection_one(
    object: {name: $name, summary: $summary, image_background: $imageBackground, image_cover: $imageCover}
  ) {
    id
  }
}
    `;
export const UpdateCollectionImageCoverDocument = gql`
    mutation updateCollectionImageCover($id: uuid!, $imageCover: String!) {
  updateCollectionImage: update_collection_by_pk(
    pk_columns: {id: $id}
    _set: {image_cover: $imageCover}
  ) {
    id
  }
}
    `;
export const UpdateCollectionImageBackgroundDocument = gql`
    mutation updateCollectionImageBackground($id: uuid!, $imageBackground: String!) {
  updateCollectionImage: update_collection_by_pk(
    pk_columns: {id: $id}
    _set: {image_background: $imageBackground}
  ) {
    id
  }
}
    `;
export const InsertPersonDocument = gql`
    mutation insertPerson($name: String!, $nameMatch: String!, $nameSort: String, $imageCover: String) {
  insertPerson: insert_person_one(
    object: {name: $name, name_match: $nameMatch, name_sort: $nameSort, image_cover: $imageCover}
  ) {
    id
  }
}
    `;
export const UpdatePersonImageDocument = gql`
    mutation updatePersonImage($id: uuid!, $imageCover: String!) {
  updatePersonImage: update_person_by_pk(
    pk_columns: {id: $id}
    _set: {image_cover: $imageCover}
  ) {
    id
  }
}
    `;
export const GetArtistCollectionDocument = gql`
    query getArtistCollection($artistId: uuid!) {
  getArtistCollection: artist_by_pk(id: $artistId) {
    artist_collections {
      collection {
        id
        name
      }
    }
  }
}
    `;
export const FindCollectionByNameDocument = gql`
    query findCollectionByName($name: String!) {
  collection(where: {name: {_eq: $name}}) {
    id
    name
  }
}
    `;
export const FindPersonByNameMatchDocument = gql`
    query findPersonByNameMatch($nameMatch: String!) {
  person(where: {name_match: {_eq: $nameMatch}}) {
    id
  }
}
    `;

export type SdkFunctionWrapper = <T>(action: () => Promise<T>) => Promise<T>;


const defaultWrapper: SdkFunctionWrapper = sdkFunction => sdkFunction();
export function getSdk(client: GraphQLClient, withWrapper: SdkFunctionWrapper = defaultWrapper) {
  return {
    insertAlbum(variables: InsertAlbumMutationVariables, requestHeaders?: Dom.RequestInit["headers"]): Promise<InsertAlbumMutation> {
      return withWrapper(() => client.request<InsertAlbumMutation>(print(InsertAlbumDocument), variables, requestHeaders));
    },
    updateAlbumImageCover(variables: UpdateAlbumImageCoverMutationVariables, requestHeaders?: Dom.RequestInit["headers"]): Promise<UpdateAlbumImageCoverMutation> {
      return withWrapper(() => client.request<UpdateAlbumImageCoverMutation>(print(UpdateAlbumImageCoverDocument), variables, requestHeaders));
    },
    updateAlbumImageBackground(variables: UpdateAlbumImageBackgroundMutationVariables, requestHeaders?: Dom.RequestInit["headers"]): Promise<UpdateAlbumImageBackgroundMutation> {
      return withWrapper(() => client.request<UpdateAlbumImageBackgroundMutation>(print(UpdateAlbumImageBackgroundDocument), variables, requestHeaders));
    },
    insertArtistCollection(variables: InsertArtistCollectionMutationVariables, requestHeaders?: Dom.RequestInit["headers"]): Promise<InsertArtistCollectionMutation> {
      return withWrapper(() => client.request<InsertArtistCollectionMutation>(print(InsertArtistCollectionDocument), variables, requestHeaders));
    },
    insertArtist(variables: InsertArtistMutationVariables, requestHeaders?: Dom.RequestInit["headers"]): Promise<InsertArtistMutation> {
      return withWrapper(() => client.request<InsertArtistMutation>(print(InsertArtistDocument), variables, requestHeaders));
    },
    updateArtistImageCover(variables: UpdateArtistImageCoverMutationVariables, requestHeaders?: Dom.RequestInit["headers"]): Promise<UpdateArtistImageCoverMutation> {
      return withWrapper(() => client.request<UpdateArtistImageCoverMutation>(print(UpdateArtistImageCoverDocument), variables, requestHeaders));
    },
    updateArtistImageBackground(variables: UpdateArtistImageBackgroundMutationVariables, requestHeaders?: Dom.RequestInit["headers"]): Promise<UpdateArtistImageBackgroundMutation> {
      return withWrapper(() => client.request<UpdateArtistImageBackgroundMutation>(print(UpdateArtistImageBackgroundDocument), variables, requestHeaders));
    },
    insertCollection(variables: InsertCollectionMutationVariables, requestHeaders?: Dom.RequestInit["headers"]): Promise<InsertCollectionMutation> {
      return withWrapper(() => client.request<InsertCollectionMutation>(print(InsertCollectionDocument), variables, requestHeaders));
    },
    updateCollectionImageCover(variables: UpdateCollectionImageCoverMutationVariables, requestHeaders?: Dom.RequestInit["headers"]): Promise<UpdateCollectionImageCoverMutation> {
      return withWrapper(() => client.request<UpdateCollectionImageCoverMutation>(print(UpdateCollectionImageCoverDocument), variables, requestHeaders));
    },
    updateCollectionImageBackground(variables: UpdateCollectionImageBackgroundMutationVariables, requestHeaders?: Dom.RequestInit["headers"]): Promise<UpdateCollectionImageBackgroundMutation> {
      return withWrapper(() => client.request<UpdateCollectionImageBackgroundMutation>(print(UpdateCollectionImageBackgroundDocument), variables, requestHeaders));
    },
    insertPerson(variables: InsertPersonMutationVariables, requestHeaders?: Dom.RequestInit["headers"]): Promise<InsertPersonMutation> {
      return withWrapper(() => client.request<InsertPersonMutation>(print(InsertPersonDocument), variables, requestHeaders));
    },
    updatePersonImage(variables: UpdatePersonImageMutationVariables, requestHeaders?: Dom.RequestInit["headers"]): Promise<UpdatePersonImageMutation> {
      return withWrapper(() => client.request<UpdatePersonImageMutation>(print(UpdatePersonImageDocument), variables, requestHeaders));
    },
    getArtistCollection(variables: GetArtistCollectionQueryVariables, requestHeaders?: Dom.RequestInit["headers"]): Promise<GetArtistCollectionQuery> {
      return withWrapper(() => client.request<GetArtistCollectionQuery>(print(GetArtistCollectionDocument), variables, requestHeaders));
    },
    findCollectionByName(variables: FindCollectionByNameQueryVariables, requestHeaders?: Dom.RequestInit["headers"]): Promise<FindCollectionByNameQuery> {
      return withWrapper(() => client.request<FindCollectionByNameQuery>(print(FindCollectionByNameDocument), variables, requestHeaders));
    },
    findPersonByNameMatch(variables: FindPersonByNameMatchQueryVariables, requestHeaders?: Dom.RequestInit["headers"]): Promise<FindPersonByNameMatchQuery> {
      return withWrapper(() => client.request<FindPersonByNameMatchQuery>(print(FindPersonByNameMatchDocument), variables, requestHeaders));
    }
  };
}
export type Sdk = ReturnType<typeof getSdk>;