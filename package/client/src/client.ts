import { getSdk, Sdk } from "./sdk.generated";
import { GraphQLClient } from "graphql-request";

export interface ClientOption {
  apiUrl: string;
  adminSecret?: string;
  bearerToken?: string;
}

export const createClient = (opts: ClientOption): Sdk => {
  const { apiUrl, adminSecret, bearerToken } = opts;
  const headers: Record<string, string> = {};
  if (adminSecret) {
    headers["x-hasura-admin-secret"] = adminSecret;
  }
  if (bearerToken) {
    headers["Authorization"] = `Bearer ${bearerToken}`;
  }
  const client = new GraphQLClient(apiUrl, { headers });
  return getSdk(client);
};
