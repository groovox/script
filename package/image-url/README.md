# @groovox/image-url

Generate url against [@groovox/transform-image] and [@groovox/upload-image].

## Getting Started

First, you must have a working [@groovox/transform-image] and [@groovox/upload-image] running.

[@groovox/transform-image]: https://gitlab.com/groovox/serverless/-/tree/master/function/transform-image
[@groovox/upload-image]: https://gitlab.com/groovox/serverless/-/tree/master/function/upload-image
