import { sign } from "jsonwebtoken";
import { URL } from "url";

import type { ImageApi, StaticPayload, TransformPayload } from "./type";

export const generateTransform = (
  payload: TransformPayload,
  api: ImageApi
): string => {
  const { apiUrl, jwtSecret } = api;
  const jwt = sign(payload, jwtSecret);
  const url = new URL(apiUrl);
  url.searchParams.set("q", jwt);
  return url.toString();
};

export const generateStatic = (
  payload: StaticPayload,
  api: ImageApi
): string => {
  const { apiUrl } = api;
  const { key, format, method } = payload;
  const url = new URL(`${method}/${key}.${format}`, apiUrl);
  return url.toString();
};
