import type { Sharp } from "sharp";
import type { Duplex } from "stream";

// Use toFormat instead
type FormatMethod =
  | "png"
  | "jpeg"
  | "webp"
  | "avif"
  | "heif"
  | "tiff"
  | "toFile"
  | "toBuffer";
export type SharpMethod = Exclude<keyof Sharp, keyof Duplex | FormatMethod>;
export type SharpParameters<T extends SharpMethod> = Parameters<Sharp[T]>;
type ToFormatParameters<T extends any[]> = T extends [infer Q, ...infer R]
  ? [Q | "origin", ...R]
  : never;

export type TransformParameters<T extends SharpMethod> = T extends "toFormat"
  ? ToFormatParameters<SharpParameters<T>>
  : SharpParameters<T>;

export type ImageQuery<T extends SharpMethod> = [T, ...TransformParameters<T>];

export interface TransformPayload {
  key: string;
  image: ImageQuery<SharpMethod>[];
}

export interface StaticPayload {
  key: string;
  format: string;
  method: string;
}

export interface UploadPayload {
  key: string;
  maxSize?: number;
  expire?: number;
}

export interface UploadResult {
  postURL: string;
  formData: {
    [key: string]: any;
  };
}

export interface ImageApi {
  apiUrl: string;
  jwtSecret: string;
}
