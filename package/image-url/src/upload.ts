import { sign } from "jsonwebtoken";
import axios, { AxiosResponse } from "axios";
import FormData from "form-data";
import { createReadStream } from "fs";
import fileType from "file-type";

import type { ImageApi, UploadPayload, UploadResult } from "./type";

export const generateUpload = async (
  payload: UploadPayload,
  api: ImageApi
): Promise<UploadResult> => {
  const { apiUrl, jwtSecret } = api;
  const jwt = sign(payload, jwtSecret);
  const res = await axios.post<UploadResult, AxiosResponse<UploadResult>>(
    apiUrl,
    jwt,
    { headers: { "Content-Type": "text/plain" } }
  );
  return res.data;
};

const getContentLength = async (data: FormData): Promise<number> =>
  new Promise((resolve, reject) => {
    data.getLength((err, length) => {
      if (err) {
        reject(err);
      } else {
        resolve(length);
      }
    });
  });

export const upload = async (
  result: UploadResult,
  filePath: string
): Promise<void> => {
  const { postURL, formData } = result;
  const data = new FormData();
  Object.entries(formData).forEach(([key, value]) => {
    data.append(key, value);
  });
  const { mime } = (await fileType.fromFile(filePath)) || {};
  data.append("Content-Type", mime);
  data.append("file", createReadStream(filePath));
  const contentLength = await getContentLength(data);
  await axios.post(postURL, data, {
    headers: {
      "Content-Length": contentLength,
      "Content-Type": `multipart/form-data; boundary=${data.getBoundary()}`
    }
  });
};
