import { cosmiconfig } from "cosmiconfig";
import { CheerioRoot } from "@groovox/request";

interface Person {
  name: string;
  nameMatch: string;
  nameSort: string;
  imageUrl: string;
}

export interface Configuration {
  url: string;
  output: string;
  parse: (html: CheerioRoot) => Promise<Person[]>;
}

export const readConfig = async (): Promise<Configuration> => {
  const result = await cosmiconfig("fetch-people").search();
  return result?.config;
};
