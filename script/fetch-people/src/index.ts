import { getHtml } from "@groovox/request";
import { dump } from "js-yaml";
import { writeFile } from "fs/promises";

import { readConfig } from "./config";

const main = async (): Promise<void> => {
  const { url, parse, output } = await readConfig();
  const html = await getHtml(url);
  const people = await parse(html);
  const yaml = dump({ people }, { indent: 2 });
  await writeFile(output, yaml);
};

main();
