import { cosmiconfig } from "cosmiconfig";

export interface TransformImage {
  api: string;
  secret: string;
}

export interface Groovox {
  api: string;
  secret: string;
}

export interface Configuration {
  groovox: Groovox;
  image: TransformImage;
  file: string;
}

export const readConfig = async (): Promise<Configuration> => {
  const result = await cosmiconfig("insert-people").search();
  return result?.config;
};
