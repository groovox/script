import yaml from "js-yaml";
import { readFile } from "fs/promises";
import { Context, insertPerson } from "@groovox/function";
import { createClient } from "@groovox/client";

import { readConfig } from "./config";

interface Person {
  name: string;
  nameMatch: string;
  nameSort: string;
  imageUrl: string;
}

const main = async (): Promise<void> => {
  const { groovox, image, file } = await readConfig();
  const yml = await readFile(file, "utf8");
  const result = yaml.safeLoad(yml);
  if (typeof result === "string" || !result) {
    throw new Error("Unknown info file.");
  }
  const { people } = result as { people: Person[] };
  const client = createClient({
    apiUrl: groovox.api,
    adminSecret: groovox.secret
  });
  const ctx: Context = {
    client,
    image: { jwtSecret: image.secret, apiUrl: image.api }
  };
  await Promise.all(
    people.map(person => {
      const { imageUrl, ...vars } = person;
      return insertPerson(ctx, vars, { imageCoverPath: imageUrl });
    })
  );
};

main();
