# @groovox/script

[![License][license_badge]][license] [![Pipelines][pipelines_badge]][pipelines]

Example scripts on how to interact with Groovox. All the script is written in TypeScript. You must compile them before running or use [ts-node].

All the user scripts will inside `src` and all the support libraries will be inside `package`.

[license]: https://gitlab.com/groovox/script/blob/master/LICENSE
[license_badge]: https://img.shields.io/badge/license-Apache--2.0-green
[pipelines]: https://gitlab.com/groovox/script/pipelines
[pipelines_badge]: https://gitlab.com/groovox/script/badges/master/pipeline.svg
[ts-node]: https://www.npmjs.com/ts-node
